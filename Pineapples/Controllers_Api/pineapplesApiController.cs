﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;

namespace Pineapples.Controllers
{
    public class PineapplesApiController : ApiController
    {
        private readonly DataLayer.IDSFactory _factory;
        protected PineapplesApiController(DataLayer.IDSFactory factory)
        {
            _factory = factory;
        }
        protected DataLayer.IDSFactory Factory
        {
            get { return _factory; }
        }

        protected string Context
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["context"] ?? String.Empty;
            }
        }
        protected string AppDataPath
        {
            get
            {
                return System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data");
            }
        }
    }
}