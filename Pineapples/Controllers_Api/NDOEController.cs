﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Softwords.Web;
using OfficeOpenXml;
using Microsoft.AspNet.SignalR;
using Pineapples.Hubs;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/ndoe")]
    public class NdoeController : PineapplesApiController
    {
        public NdoeController(DataLayer.IDSFactory factory) : base(factory) { }


        /// <summary>
        /// uploads and stores the file
        /// </summary>
        /// <returns>a summary of the data in the file, together with the file ID</returns>
        [HttpPost]
        [Route(@"upload")]
        public async Task<object> Upload()
        {

            ListObject lo = null;
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartFormDataStreamProvider(AppDataPath);

            var files = await Request.Content.ReadAsMultipartAsync(provider);
            Dictionary<string, object> results = new Dictionary<string, object>();

            foreach (MultipartFileData fd in files.FileData)
            {
                string filename = fd.Headers.ContentDisposition.FileName;
                filename = filename.Replace("\"", "");          // tends to com back with quotes around
                string ext = System.IO.Path.GetExtension(filename);

                Guid g;
                OfficeOpenXml.ExcelPackage p;
                using (System.IO.FileStream fstrm = new System.IO.FileStream(fd.LocalFileName, System.IO.FileMode.Open))
                {
                    g = Providers.FileDB.Store(fstrm, ext);
                    // add a document record for this file

                    // create an Excel workbook object from the stream
                    p = new ExcelPackage(fstrm);
                };
                // we can get rid of the local file now
                System.IO.File.Delete(fd.LocalFileName);

                // get a little bit of data out of here to send back to confirm before uploading

                results.Add("id", g);
                results.Add("schoolYear", p.Workbook.Names["nm_SchoolYear"].Value);
                results.Add("state", p.Workbook.Names["nm_State"].Value);

                // we are going to step through each sheet and collect all school nos and validate them in one go
                XDocument schools = XDocument.Parse("<Schools/>");
                // for example, go to the 'Students' sheet...
                ExcelWorksheet wk = p.Workbook.Worksheets["Students"];
                // get the first table off that sheet...
                OfficeOpenXml.Table.ExcelTable tbl = wk.Tables[0];
                // create a ListObject from that table
                lo = new ListObject(tbl);
                results.Add("students", lo.Count());

                schools.Root.Add(collectSchools(lo, wk.Name));

                wk = p.Workbook.Worksheets["Schools"];
                tbl = wk.Tables[0];
                lo = new ListObject(tbl);
                results.Add("schools", lo.Count());

                schools.Root.Add(collectSchools(lo, wk.Name));

                wk = p.Workbook.Worksheets["SchoolStaff"];
                tbl = wk.Tables[0];
                lo = new ListObject(tbl);
                results.Add("staff", lo.Count());

                schools.Root.Add(collectSchools(lo, wk.Name));

                wk = p.Workbook.Worksheets["WASH"];
                tbl = wk.Tables[0];
                lo = new ListObject(tbl);
                results.Add("washInfo", lo.Count());

                schools.Root.Add(collectSchools(lo, wk.Name));

                // Now we have all the schools in a hierarchy - validate they are ok
                IDataResult dataresult = await Ds.NdoeValidateSchoolsAsync(schools);
                results.Add("validations", dataresult.ResultSet);

            }
            return results;      // only the last one? but we expect only one at a time.....?
        }

        /// <summary>
        /// Processes the nominated file
        /// </summary>
        /// <param name="fileId">guid to the Excel document in the FileDB</param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"process/{fileId}")]
        public async Task<object> Process(string fileId)
        {
            Guid g = new Guid(fileId);
            ExcelPackage p;
            System.Xml.Linq.XDocument xd = null;
            IDataResult results;

            // first get the file based on its Id
            string path = Providers.FileDB.GetFilePath(fileId);
            using (System.IO.FileStream fstrm = new System.IO.FileStream(path, System.IO.FileMode.Open))
            {
                // create an Excel workbook object from the stream
                p = new ExcelPackage(fstrm);
            };
            string schoolYear = (string)p.Workbook.Names["nm_SchoolYear"].Value;
            string stateName = (string)p.Workbook.Names["nm_State"].Value;

            // prepare to signalr
            NdoeProcessState state = new NdoeProcessState();
            // process schools.....
            ExcelWorksheet wk = p.Workbook.Worksheets["Schools"];
            // get the first table off that sheet...
            OfficeOpenXml.Table.ExcelTable tbl = wk.Tables[0];
            // create a ListObject from that table
            ListObject lo = new ListObject(tbl);
            // create the xml from the list object
            xd = lo.ToXml();
            // send to the database
            state.schools = "loading...";
            Hub.Clients.Group(fileId).progress(fileId, state);
            await Ds.NdoeSchoolsAsync(prepXml(xd, stateName, schoolYear), g, User.Identity.Name);
            // tell the client
            state.schools = "Completed";
            Hub.Clients.Group(fileId).progress(fileId, state);

            // students...
            wk = p.Workbook.Worksheets["Students"];
            tbl = wk.Tables[0];
            lo = new ListObject(tbl);
            xd = lo.ToXml();
            state.students = "loading...";
            Hub.Clients.Group(fileId).progress(fileId, state);
            results = await Ds.NdoeStudentsAsync(prepXml(xd, stateName, schoolYear), g, User.Identity.Name);
            state.students = "Completed";
            Hub.Clients.Group(fileId).progress(fileId, state);

            // staff...
            wk = p.Workbook.Worksheets["SchoolStaff"];
            tbl = wk.Tables[0];
            lo = new ListObject(tbl);
            xd = lo.ToXml();
            state.staff = "loading...";
            Hub.Clients.Group(fileId).progress(fileId, state);
            await Ds.NdoeStaffAsync(prepXml(xd, stateName, schoolYear), g, User.Identity.Name);
            state.staff = "Completed";
            Hub.Clients.Group(fileId).progress(fileId, state);

            // wash...
            wk = p.Workbook.Worksheets["WASH"];
            tbl = wk.Tables[0];
            lo = new ListObject(tbl);
            xd = lo.ToXml();
            state.wash = "loading...";
            Hub.Clients.Group(fileId).progress(fileId, state);
            await Ds.NdoeWashAsync(prepXml(xd, stateName, schoolYear), g, User.Identity.Name);
            state.wash = "Completed";
            Hub.Clients.Group(fileId).progress(fileId, state);

            return results;         // we are interested in the student results
        }


        readonly Lazy<IHubContext<INdoeProcessCallbacks>> hub = new Lazy<IHubContext<INdoeProcessCallbacks>>(
            () => GlobalHost.ConnectionManager.GetHubContext<INdoeProcessCallbacks>("ndoeprocess")
        );

        protected IHubContext<INdoeProcessCallbacks> Hub
        {
            get { return hub.Value; }
        }

        private IDSSurveyEntry Ds
        {
            get { return Factory.SurveyEntry(); }
        }

        #region helpers

        private XElement collectSchools(ListObject lo, string sheetName)
        {
            XElement xrow;
            XElement xsheet;
            xsheet = new XElement("Sheet");
            xsheet.SetAttributeValue("name", sheetName);
            foreach (ListRow r in lo)
            {
                xrow = new XElement("School_No", r["School_No"]);
                xrow.SetAttributeValue("Index", r.idx);
                // it turns out that to get the sheet name by backtracking up the xml hierarchy
                // is monumentally slow
                // so we add it on each and every node :(
                xrow.SetAttributeValue("Sheet", sheetName);
                xsheet.Add(xrow);
            }
            return xsheet;
        }
        /// <summary>
        /// add the district and survey year as attributes to the root node
        /// a convenient way to get these into the stored proc
        /// </summary>
        /// <param name="xd"></param>
        private XDocument prepXml(XDocument xd, string districtName, string schoolYear)
        {
            xd.Root.SetAttributeValue("state", districtName);
            xd.Root.SetAttributeValue("schoolYear", schoolYear);
            return xd;
        }

        #endregion
    }
}
