﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;


using Newtonsoft.Json.Linq;

namespace Pineapples.Controllers
{
    public class SchoolScatterController : PineapplesApiController
    {
    public SchoolScatterController(DataLayer.IDSFactory factory) : base(factory) { }

    //public class ScatterOptions
    //{
    //    public int baseYear = 2014;
    //    public string XSet = "Enrolment";
    //    public string YSet = "Enrolment";
    //    public int XYearOffset = 0;
    //    public int YYearOffset = 1;

    //    public string Xsp
    //    {
    //        get { return getSP(XSet);}

    //    }
    //    public string Ysp
    //    {
    //        get { return getSP(YSet); }

    //    }


    //    private string getSP(string dset)
    //    {
    //        switch (dset)
    //        {
    //        case "Enrolment":
    //            return "pSchoolRead.schoolDataSetEnrolment";
    //        case "Repeaters":
    //            return "pSchoolRead.schoolDataSetRepeaterCount";

    //        case "Teachers":
    //            return "pSchoolRead.schoolDataSetTeacherCount";
    //        case "TeachersQualCert":
    //            return "pSchoolRead.schoolDataSetTeacherQualCertCount";
    //        case "TeachersQualCertPerc":
    //            return "pSchoolRead.schoolDataSetTeacherQualCertPerc";

    //        }
    //        return "pSchoolRead.schoolDataSetEnrolment";
    //    }
    //}



        [HttpPost]
        [ActionName("getScatter")]
        public Object getSchoolScatter(Data.SchoolScatterOptions opts)
        {
            return Factory.SchoolScatter().getScatter(opts);
        }
    }
}
