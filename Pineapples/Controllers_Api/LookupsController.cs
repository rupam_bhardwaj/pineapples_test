﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;

namespace Pineapples.Controllers
{
    public class LookupsController : PineapplesApiController
    {
        public LookupsController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpGet]
        [Route("api/lookups/collection/core")]
        public object CoreLookups()
        {
            return Ds.CoreLookups();
        }

        [HttpGet]
        [Route("api/lookups/collection/pa")]
        public object PaLookups()
        {
          return Ds.PaLookups();
        }

        [HttpGet]
        [Route("api/lookups/getlist/{name}")]
        public object GetList(string name)
        {
            string[] lookups = new string[1] { name };
            return Ds.getLookupSets(lookups);
        }

        [HttpPost]
        [Route("api/lookups/{listName}/{code}")]
        public object Save(string listName, string code, Pineapples.Data.Models.LookupEntry entry)
        {

            return null;
        }

        /// <summary>
        /// Return info on the loookup lists that can be edited
        /// To do - push this into the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/lookups/editabletables")]
        public object editableTables()
        {
            //TO DO - implement this in the database similar to desktop pineapples
            List<string> tables = new List<string>();

            tables.Add("Banks");
            tables.Add("BookTGs");
            tables.Add("BuildingMaterials");
            tables.Add("BuildingSizeClasses");
            tables.Add("BuildingSubTypes");
            tables.Add("BuildingTypes");
            tables.Add("BuildingValuationClasses");
            tables.Add("ChildProtections");
            tables.Add("CivilStatusValues");
            tables.Add("ConditionCodes");
            tables.Add("Currencies");
            tables.Add("DistanceCodes");
            tables.Add("LocalElectorates");
            tables.Add("NationalElectorates");
            tables.Add("ExamTypes");
            tables.Add("InspectionTypes");
            tables.Add("LandOwnerships");
            tables.Add("LandUses");
            tables.Add("Languages");
            tables.Add("LanguageGroups");
            tables.Add("MaterialTypes");
            tables.Add("Nationalities");
            tables.Add("Navigations");
            tables.Add("PayAllowances");
            tables.Add("PNAReasons");
            tables.Add("PrimaryTransitions");
            tables.Add("PupilTableTypes");
            tables.Add("QualityIssues");
            tables.Add("RandMs");
            tables.Add("Regions");
            tables.Add("RoomFunctions");
            tables.Add("RoomTypes");
            tables.Add("RubbishDisposals");
            tables.Add("SalaryLevels");
            tables.Add("SalaryPoints");
            tables.Add("SchoolBuildingTypes");
            tables.Add("SchoolClasses");
            tables.Add("SchoolLinkTypes");
            tables.Add("SchoolRegistrations");
            tables.Add("SecondaryTransitions");
            tables.Add("SecondaryTransitionItems");
            tables.Add("ServiceTypes");
            tables.Add("Shifts");
            tables.Add("SiteHosts");
            tables.Add("Subjects");
            tables.Add("TeacherHousingTypes");
            tables.Add("TeacherLinkTypes");
            tables.Add("TeacherQuals");
            tables.Add("TeacherRoles");
            tables.Add("TeacherStatusValues");
            tables.Add("TeacherTrainingTypes");
            tables.Add("ToiletTypes");
            tables.Add("WaterSupplyTypes");
            tables.Add("WorkItemProgressValues");
            tables.Add("WorkItemTypes");
            tables.Add("WorkOrderStatusValues");

            return tables;
        }


        private IDSLookups Ds
        {
            get { return Factory.Lookups(); }
        }
    }
}
