﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;

// TODO: Obsolete this file. Routes in here are declared and used from
// within SchoolController.cs

namespace Pineapples.Controllers
{
    [RoutePrefix("api/survey")]
    public class SurveyController : PineapplesApiController
    {
        public SurveyController(DataLayer.IDSFactory factory) : base(factory) { }


        #region eSurvey support
        [HttpGet]
        [Route(@"audit/{schoolNo}/{year:int}")]
        public object PupilTable(string schoolNo, int year)
        {
            return Factory.Survey().AuditLog(schoolNo, year); //.ResultSet;
        }

        #endregion
    }
}
