﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/books")]
    public class BooksController : PineapplesApiController
    {
        public BooksController(DataLayer.IDSFactory factory) : base(factory) { }

        #region Book methods


        [HttpPost]
        [Route(@"")]
        [Route(@"{bookCode}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public HttpResponseMessage Create(BookBinder book)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(book, (ClaimsIdentity)User.Identity));
                return response;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpPut]
        [Route(@"{bookCode}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Update(BookBinder book)
        {
            try
            {
                return Ds.Update(book, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpGet]
        [Route(@"{bookCode}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public object Read(string bookCode)
        {
            return Ds.Read(bookCode);
        }

        [HttpDelete]
        [Route(@"{bookCode}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Delete([FromBody] BookBinder book)
        {
            // TO DO
            return book.definedProps;
        }
        #endregion

        #region Book Collection methods
        [HttpPost]
        [ActionName("filter")]
        public object Filter(BookFilter fltr)
        {
            return Ds.Filter(fltr);
            //  return fltr.Search();
        }

        #endregion

        private IDSBooks Ds
        {
            get { return Factory.Books(); }
        }
    }
}
