﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Pineapples.Providers
{
    /// <summary>
    /// a simple static class to read and write filestreams to the file system, storing htem
    /// by a random guid name
    /// the guid is split into a prefix that determines a folder, the rest is the file name
    /// this limits the number of files in each folder
    /// </summary>
    public class FileDB
    {
      
        public static string dbBase()
        {
            string path = System.Web.Configuration.WebConfigurationManager.AppSettings["FileDb"];
            return path;
        }
        /// <summary>
        /// Store a file in the FileDB
        /// </summary>
        /// <param name="strm">stream opened on the file</param>
        /// <param name="ext">extension of the file</param>
        /// <returns>the identifier of the file</returns>
        public static Guid Store(Stream strm, string ext)
        {
            // we can add the file in here
            Guid g = Guid.NewGuid();
            string s = g.ToString();

            string f = s.Substring(0, 4);
            s = s.Substring(4);

            string fldr = Path.Combine(dbBase(), f);
            if (!System.IO.Directory.Exists(fldr))
            {
                Directory.CreateDirectory(fldr);
            }
            string fname = Path.Combine(fldr, s);
            fname = Path.ChangeExtension(fname, ext);
            FileStream fstrm = new FileStream(fname, FileMode.Create);
            strm.CopyTo(fstrm);
            fstrm.Close();
            return g;
        }

        /// <summary>
        /// Get the file as a stream
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Stream Get(string id)
        {
            string path = GetFilePath(id);
            FileStream strm = new FileStream(path, FileMode.Open);
            return strm;
        }

        /// <summary>
        /// Remove a file from the fileDB
        /// </summary>
        /// <param name="id"></param>
        public static void Remove(string id)
        {
            string path = GetFilePath(id);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        /// <summary>
        /// Return the file path to the file in the FileDB
        /// </summary>
        /// <param name="path">the identifier of the file possibly with the extension ( as returned by</param>
        /// <returns>the full path, including extension</returns>
        public static string GetFilePath(string path)
        {
            string fldr = path.Substring(0, 4);
            string fn = path.Substring(4);
            path = Path.Combine(dbBase(), fldr);
            // allow searching with or without extension on the filename
            if (Path.GetExtension(fn) == String.Empty)
            {
                string[] fnames = Directory.GetFiles(path, fn + ".*");
                if (fnames.Length > 0)
                    fn = fnames[0];
            }
            return Path.Combine(path, fn);
        }

        /// <summary>
        /// Return the filename part of the FileDB entry with a given id
        /// </summary>
        /// <param name="id">the identifier</param>
        /// <returns>the filename, with extension, without path</returns>
        public static string GetTokenWithExtension(string id)
        {
            string path = id;
            string fldr = path.Substring(0, 4);
            string fn = path.Substring(4);
            path = Path.Combine(dbBase(), fldr);
            // allow searching with or without extension on the filename
            if (Path.GetExtension(fn) == String.Empty)
            {
                string[] fnames = Directory.GetFiles(path, fn + ".*");
                fn = fnames[0];
                return Path.GetFileName(fn);
            }
            return id;
        }

    }
}