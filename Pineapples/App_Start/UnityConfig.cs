using Microsoft.Practices.Unity;
using System.Web.Http;
using Pineapples.Data;
using Pineapples.Data.DataLayer;


using Unity.WebApi;

namespace Pineapples
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			    var container = new UnityContainer();

          // register all your components with the container here
          // it is NOT necessary to register your controllers

          // e.g. container.RegisterType<ITestService, TestService>();

          // create DSFactory with the no parameter constructor
          container.RegisterType<IDSFactory, DSFactory>(new InjectionConstructor());

          // this is the registration for web api
          GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
          // this is the registration for mvc
          System.Web.Mvc.DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
        }
    }
}