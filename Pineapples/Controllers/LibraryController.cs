﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Http;
using Pineapples.Data;

namespace Pineapples.Controllers
{
    public class LibraryController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        private readonly Pineapples.Data.DataLayer.IDSFactory _factory;
        protected Pineapples.Data.DataLayer.IDSFactory Factory
        {
            get { return _factory; }
        }
        public LibraryController(Pineapples.Data.DataLayer.IDSFactory factory)
        {
            _factory = factory;
        }
        [HttpGet]
        [Route(@"library/{id}")]
        [Route(@"library/{id}/{rotate:int}")]
        [Route(@"library/{id}/{rotate:int}/{size:int}")]
        public FileStreamResult GetLibraryDocument(string id, int rotate=0, int size=0)
        {
            
            string path = Providers.FileDB.GetFilePath(id);
            string name = Providers.FileDB.GetTokenWithExtension(id);
            Guid g = new Guid(id);
            var doc = Factory.Context.Documents.Find(g);
            if (doc != null)
            {
                name = doc.docTitle;
            }
            // Check to see if the file exists.
            if (!System.IO.File.Exists(path))
            {
                throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No image exists at {path}");
            }
            System.IO.FileStream fstrm = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true);
            switch (System.IO.Path.GetExtension(name.ToLower()))
            {
                // these are extension we want the browser to pick up and display, rather than download
                // note that all the image types are already handled by imageprocessor
                // so special cases here are limited to pdf....?
                case ".pdf":
                    // not supplying the name argument in this format means you don't get an "attachment" content-disposition header
                    return File(fstrm, MimeMapping.GetMimeMapping(path));
            }
            // everything else allow to download with its geniune name
            return File(fstrm, MimeMapping.GetMimeMapping(path), name);
        }

        /// <summary>
        /// this method will return a thumbnail based solely on the extension
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"thumb/{extension}/{size?}")]
        public FileStreamResult GetgetThumbnail(string extension, int? size)
        {
            string path = String.Empty;
            switch (extension.ToLower().Replace(".",""))
            {
                case "pdf":
                    path = "file-pdf-icon-{0}.png";
                    break;
                case "xls":
                case "xlsx":
                case "xlsm":
                case "csv":
                    path = "file-excel-icon-{0}.png";
                    break;
                case "doc":
                case "docx":
                case "docm":
                    path = "file-word-icon-{0}.png";
                    break;
                case "ppt":
                case "pptx":
                case "pptm":
                    path = "file-powerpoint-icon-{0}.png";
                    break;
                case "zip":
                    path = "file-zip-icon-{0}.png";
                    break;
                case "jpeg":
                case "jpg":
                case "png":
                case "gif":
                case "bmp":
                    path = "file-img-icon-{0}.png";
                    break;
                case "mp3":
                case "wma":
                case "flac":
                    path = "file-sound-icon-{0}.png";
                    break;
                case "avi":
                case "mp4":
                case "mov":
                case "flv":
                    path = "file-video-icon-{0}.png";
                    break;
                case "txt":
                    path = "file-text-icon-{0}.png";
                    break;
                default:
                    path = "file-icon-{0}.png";     // no sizing options
                    break;

            }
            if (size == null || size == 0)
            {
                size = 72;
            }
            else
            {
                
                if (size < 32)
                    size = 24;
                if (size >= 32 && size < 48)
                    size = 32;
                if (size >= 48 && size < 64)
                    size = 48;
                if (size >= 64 && size < 72)
                    size = 64;
                if (size >= 72 && size < 96)
                    size = 72;
                if (size >= 96 && size < 128)
                    size = 96;
                if (size >= 128)
                    size = 128;

            }
            path = string.Format(path, size);
            path = System.Web.Hosting.HostingEnvironment.MapPath(@"~\assets\img\" + path);
            // Check to see if the file exists.
            if (!System.IO.File.Exists(path))
            {
                throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No image exists at {path}");
            }
            System.IO.FileStream fstrm = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true);

            return File(fstrm, MimeMapping.GetMimeMapping(path));
        }
    }
}
