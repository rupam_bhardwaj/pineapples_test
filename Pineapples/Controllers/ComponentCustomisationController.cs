﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pineapples.mvcControllers
{
    /// <summary>
    /// servers up template fragments that customise external components,
    /// in particular, ui-grid. 
    /// </summary>
  
    public class ComponentCustomisationController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        #region ui-grid
        [Route("uigrid/{template}")]
        public ActionResult uigridTemplate(string template)
        {
            return View("uigrid/" + template);
        }
        #endregion

    }
}