﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace Pineapples.Models
{
    /// <summary>
    /// this class represents an Excel ListObject
    /// for which there is not explcit support in EPPlus
    /// The ListObject class supports the enumeration of ListRows
    /// and both ListObject andListRow support index and name indexers for columns
    /// </summary>
    /// 
    [JsonConverter(typeof(ListObjectJsonConverter))]
    public class ListObject : IEnumerable<ListRow>
    {
        private object[,] values;
        internal Dictionary<string, int> colnames;

        /// <summary>
        /// Constructor allows an OfficeXml ExcelTable to be handled as a ListObject
        /// </summary>
        /// <param name="tbl">the excel table representing the list object</param>
        public ListObject(OfficeOpenXml.Table.ExcelTable tbl)
        {
            _listObject(tbl);
        }

        private void _listObject(OfficeOpenXml.Table.ExcelTable tbl)
        {
            ExcelRange rng = tbl.WorkSheet.Cells[tbl.Address.Address];
            OfficeOpenXml.Table.ExcelTableColumnCollection cols = tbl.Columns;

            values = (object[,])rng.Value;
            Rows = rng.Rows - 1;    // becuase the first row is the headers

            // map column names back to their index
            colnames = new Dictionary<string, int>();
            for (int i = 0; i < cols.Count; i++)
            {
                string nm = cols[i].Name.Replace(":", "_")
                    .Replace(" ", "_").Replace("?", "").Replace("'", "").Replace("#", "Num").Replace("\v", "");
                string f = nm.Substring(0, 1);
                switch (f)
                    {
                        case "0":
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                        case "6":
                        case "7":
                        case "8":
                        case "9":
                            nm = "NM_" + nm;
                            break;
                    }
                colnames.Add(nm, i);
            }
            Cols = cols.Count;
            Name = tbl.Name;
        }
        /// <summary>
        /// This contructor assumes we have a set of named columns starting in cel A1
        /// however, these may not be formatted as a Table
        /// </summary>
        /// <param name="wk"></param>
        public ListObject(OfficeOpenXml.ExcelWorksheet wk)
        {
            OfficeOpenXml.ExcelRangeBase lastcol = wk.Cells.Last(c => c.Start.Row == 1);
            OfficeOpenXml.ExcelRangeBase lastrow = wk.Cells.Last(c => c.Start.Column == 1);

            var rng = wk.Cells[1, 1, lastrow.End.Row, lastcol.End.Column];
            OfficeOpenXml.Table.ExcelTable tbl = wk.Tables.Add(rng, "epplusTable");
            // create a ListObject from that table
            _listObject(tbl);
        }

        /// <summary>
        /// Name of the ListObject
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Number of rows in the ListObject
        /// </summary>
        public int Rows { get; private set; }

        /// <summary>
        /// Number of columns in the ListObject
        /// </summary>
        public int Cols { get; private set; }

        /// <summary>
        /// Return the row at the specifed position in the list
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public ListRow Row(int idx)
        {
            return new ListRow(this, idx);
        }

        /// <summary>
        ///  return the index of the column 
        /// </summary>
        /// <param name="colname"></param>
        /// <returns></returns>
        public int ColIndex(string colname)
        {
            if (colnames.ContainsKey(colname))
                return colnames[colname];
            return -1;
        }
        public IEnumerator<ListRow> GetEnumerator()
        {
            return new ListObjectEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ListObjectEnumerator(this);
        }

        public object this[int rowindex, int colindex]
        {
            get
            {
                return values[rowindex + 1, colindex];
            }
        }
        public object this[int rowindex, string colname]
        {
            get
            {
                int colindex = colnames[colname];
                return values[rowindex + 1, colindex];
            }
        }

        /// <summary>
        /// Render as Xml with columns as attributes of row
        /// </summary>
        /// <returns></returns>
        public XDocument ToXml()
        {
            XDocument xd = XDocument.Parse("<ListObject/>");
            XElement xrow;
            String[] c = colnames.Keys.ToArray();
            foreach (ListRow row in this)
            {
                xrow = new XElement("row");
                xrow.SetAttributeValue("Index", row.idx);
                for (int i = 0; i < Cols; i++)
                {
                    if (row[i] != null)
                    {
                        object v = row[i];
                        if (v is String)
                        {
                            v = v.ToString().Replace("\v", "");
                        }

                        xrow.SetAttributeValue(c[i], v);
                    }
                }
                xd.Root.Add(xrow);
            }
            return xd;
        }
    }

    [JsonConverter(typeof(ListRowJsonConverter))]
    public class ListRow
    {
        internal ListObject lo;
        internal int idx;
        internal ListRow(ListObject lo, int idx)
        {
            this.lo = lo;
            this.idx = idx;
        }

        public object this[int col]
        {
            get
            {
                return lo[idx, col];
            }
        }
        public object this[string colname]
        {
            get
            {
                return lo[idx, colname];
            }
        }
    }

    class ListObjectEnumerator : IEnumerator<ListRow>
    {
        private int index;
        private ListObject lo;
        internal ListObjectEnumerator(ListObject lo)
        {
            this.lo = lo;
            index = -1; // ie BOF - MoveNextis called before Current
        }

        public ListRow Current
        {
            get
            {
                return lo.Row(index);
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return lo.Row(index);
            }
        }

        public void Dispose()
        {
            lo = null;
        }

        public bool MoveNext()
        {
            index++;
            if (index < lo.Rows)
                return true;
            return false;
        }

        public void Reset()
        {
            index = 0;
        }
    }

    /// 
    class ListObjectJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(ListObject));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            ListObject lo = value as ListObject;
            List<ListRow> ll = new List<ListRow>();

            foreach (ListRow row in lo)
            {
                ll.Add(row);
            }
            serializer.Serialize(writer, ll);
        }
    }
    class ListRowJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(ListRow));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            ListRow row = value as ListRow;
            Dictionary<string, object> rd = new Dictionary<string, object>();

            foreach (string colname in row.lo.colnames.Keys)
            {
                if (row[colname] != null)
                    rd.Add(colname, row[colname]);
            }
            serializer.Serialize(writer, rd);
        }
    }
}