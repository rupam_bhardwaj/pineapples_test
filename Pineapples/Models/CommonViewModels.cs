﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pineapples.Models
{
    public class SearcherModel
    {

    }

    // this class helps to configure common pages where 
    // the content varies according to the top level feature - application applicant etc
    public class FeatureModel
    {
        public string Feature { get; set; }
    }

  

    public class SchoolSearcherModel
    {

        public SchoolSearcherModel()
        {
            //Pineapples.Data.Lookups lkp = new Pineapples.Data.Lookups();
            //string[] lookups = new string[] {
            //    "schoolTypes"
            //};
            //Lookups = lkp.LookupSet(lookups);
        }

        public System.Data.DataSet Lookups { get; set; }
    }


    public class TeacherSearcherModel
    {

        public TeacherSearcherModel()
        {
            //Pineapples.Data.Lookups lkp = new Pineapples.Data.Lookups();
            //string[] lookups = new string[] {
            //    "schoolTypes"
            //};
            //Lookups = lkp.LookupSet(lookups);
        }

        public System.Data.DataSet Lookups { get; set; }
    }

    public class StudentSearcherModel
    {

        public StudentSearcherModel()
        {
            //Pineapples.Data.Lookups lkp = new Pineapples.Data.Lookups();
            //string[] lookups = new string[] {
            //    "studentTypes"
            //};
            //Lookups = lkp.LookupSet(lookups);
        }

        public System.Data.DataSet Lookups { get; set; }
    }

    public class ExamSearcherModel
    {

        public ExamSearcherModel()
        {
            //Pineapples.Data.Lookups lkp = new Pineapples.Data.Lookups();
            //string[] lookups = new string[] {
            //    "examTypes"
            //};
            //Lookups = lkp.LookupSet(lookups);
        }

        public System.Data.DataSet Lookups { get; set; }
    }

    public class IndicatorsModel
    {
        public string System { get; set; }
        public string Format { get; set;}
    }


}