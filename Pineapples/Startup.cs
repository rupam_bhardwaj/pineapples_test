﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;


// 2014 11 11 see this for what makes this class "startup"
//http://blogs.msdn.com/b/praburaj/archive/2013/12/02/owin-startup-class-not-detected.aspx
[assembly: OwinStartup(typeof(Pineapples.Startup))]

namespace Pineapples
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
