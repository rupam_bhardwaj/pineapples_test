SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 June 2014
-- Description:	Generate kml directly from schools
-- =============================================

CREATE PROCEDURE [dbo].[SchoolsKml]
	-- Add the parameters for the stored procedure here

	@balloonTextTemplate nvarchar(max) = null -- supplies the text node of BallonStyle in schoolStyle
	-- this can be an iframe , using a reference to $[description] to get a specific school argument
	-- e.g.
	-- <iframe height="600" width="600" src="http://localhost/schoolProfile.aspx?schNo=$[description]"/>'
	-- if balloon text begins with @ ( e.g.@ScatterBasic ) it is assumed to be an entry in the table sysHtmlTemplates
	, @descriptions xml = null
-- @descriptions looks like this <data><d key="--schNo--">[school data...][<ExtendedData>...</ExtendedData>].</d></data>
-- The text under each sch item ('d' node) ends up in the description of that placemark.
-- Any ExtendedData nodes are copied verbatim to the kml, as ExternalData under the school's place mark.
-- This makes the extended data fields available for use in the custom balloon see the kml docuenation for details:
-- https://developers.google.com/kml/documentation/kmlreference

-- If custom descriptions are not supplied, and there is a custom balloon template, then schNo is used as description
-- if no custom descriptions and no balloon template, a default of school type, district and authority is used.
-- this architecture allows other procedures to create custom
-- kml by calling this function supplying these two arguments - see SchoolScatterChart

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- get the balloon template from the table if the argument is a template name - prefixed with @<template name>

if (@BalloonTextTemplate like '@%')
	Select @balloonTextTemplate = tmplContent
	FROM dbo.sysHtmlTemplates
	WHERE tmplKey = substring(@BalloonTextTemplate,2,len(@BalloonTextTemplate) - 1)


Declare @t TABLE
(
	tag int
	, parent int
	, seq nvarchar(50)
	, placemarkname nvarchar(200)
	, placemarkdescription nvarchar(max)
	, ExtendedData xml		-- quoted verbatim into extended data
	, dsc nvarchar(200)
	, coords nvarchar(200)
)

declare @dt TABLE
(
	schNo nvarchar(50)
	, schdesc nvarchar(max)
	, extendedData xml
)

if (@descriptions is not null) begin
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @descriptions
	INSERT INTO @dt
	SElect [key]
	, value
	, extendedData
	FROM OPENXML(@idoc,'//d',2)
	WITH
	(
		[key]	nvarchar(50)			'@key'
		, [value] nvarchar(max)			'text()'
		, extendedData xml				'ExtendedData'
	)
	EXEC sp_xml_removedocument @idoc
end

-- tag 1 for kml
INSERT INTO @t
(
tag
, parent
, seq
)
SELECT 1, null, ''

-- tag 2 for document
INSERT INTO @t
(
tag
, parent
, seq
)
SELECT 2, 1, ''


-- folder elements - document is parent

INSERT INTO @t
(
tag
, parent
, seq

)
Select DISTINCT 5
, 2			-- document is parent
, schType
FROM Schools
WHERE schLat is not null

-- schol elements (Placemark)
INSERT INTO @t
(
tag
, parent
, seq
, placemarkname
, ExtendedData
)
Select 10
, 5			-- parent is folder
, schType	-- sorted by type to put in correct folder
, Schools.schNo + ': ' + schName
, ExtendedData
FROM Schools
LEFT JOIN @dt DT
	ON Schools.schNo = DT.schNo
WHERE schLat is not null


-- description elements (child of Placemark)
INSERT INTO @t
(
tag
, parent
, seq
, placemarkname
, placemarkdescription
)
Select 15
, 10			-- parent is placemark
, schType	-- sorted by type to put in correct folder
, Schools.schNo + ': ' + schName
, isnull(dt.schDesc,		-- use the custom description if provided
			-- if no custom description - use the schNo if there is a balloontemplate
			-- this allows a balloontemplate to get a simple argument for a web page using $[description]
			case
				when @BalloonTextTemplate is null then
					stDescription + '<br/>' + dName + '<br/>' + DA.Authority
				else
					Schools.schNo
			end
		)
FROM Schools
	LEFT JOIN SchoolTypes ST
		ON Schools.schType = ST.stCode
	LEFT JOIN DimensionAuthority DA
		ON schAuth = DA.AuthorityCode
	LEFT JOIN lkpIslands I
		ON Schools.iCode = I.iCode

LEFT JOIN @dt DT
	ON Schools.schNo = DT.schNo
WHERE schLat is not null


-- snippet elements (child of Placemark)
INSERT INTO @t
(
tag
, parent
, seq
, placemarkname
, placemarkdescription
)
Select 17
, 10			-- parent is placemark
, schType	-- sorted by type to put in correct folder
, Schools.schNo + ': ' + schName
, dName + '<br/>' + DA.Authority		-- snippet is only 2 lines max
FROM Schools
	LEFT JOIN SchoolTypes ST
		ON Schools.schType = ST.stCode
	LEFT JOIN DimensionAuthority DA
		ON schAuth = DA.AuthorityCode
	LEFT JOIN lkpIslands I
		ON Schools.iCode = I.iCode

LEFT JOIN @dt DT
	ON Schools.schNo = DT.schNo
WHERE schLat is not null


-- point element - child of Placemark
INSERT INTO @t
(
tag
, parent
, seq
, placemarkname
, coords
)
Select 20
, 10 -- parent is placemark
, schType
, schNo + ': ' + schName
, Convert(nvarchar(13),schLong) + ',' + Convert(nvarchar(13), schLat) + ',0'
FROM Schools
WHERE schLat is not null


-- context data
declare @country nvarchar(100)
declare @appname nvarchar(100)

Select @country = paramText
FROM sysParams
WHERe paramName = 'USER_NATION'

Select @appname = paramText
FROM sysParams
WHERe paramName = 'APP_NAME'

declare @schStyle nvarchar(max)
-- apart from the text element, style is hard-coded for now
select @schStyle = '
<Style id="schoolStyle" xmlns="http://www.opengis.net/kml/2.2">
	<BalloonStyle>
		<text><![CDATA['
			+ isnull(@BalloonTextTemplate, '$[description]') +
		']]></text>
	</BalloonStyle>
	<LabelStyle>
        <scale>.8</scale>
      </LabelStyle>
      <IconStyle>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/paddle/ylw-blank.png</href>
        </Icon>
      </IconStyle>
</Style>
'

-- render the kml
Select
Tag, Parent
, 'http://www.opengis.net/kml/2.2' [kml!1!xmlns]
, @Country + ' Schools' [Document!2!name!element]
, 'Kml file created by ' + @appname [Document!2!description!element]
, @schStyle [Document!2!!xml]
, seq	[Folder!5!name!element]
, placemarkname [Placemark!10!name!Element]
, '#schoolStyle' [Placemark!10!styleUrl!element]
, convert(nvarchar(max),ExtendedData)		[Placemark!10!!xml]
, placemarkdescription [description!15!!CDATA]
, placemarkdescription [snippet!17!!CDATA]
, coords [Point!20!coordinates!Element]
FRom @t
ORDER BY seq, placemarkname, Tag
FOR XML EXPLICIT


END
GO

