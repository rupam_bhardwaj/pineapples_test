SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ebseChange]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- remove the ebse records for the affected schools
delete from ebse
from ebse inner join #ebseChange on ebse.schno = #ebseChange.schno

-- recalculate the ebse records for those affected schools
insert into ebse
select *
from dbo.tfnESTIMATE_BestSurveyEnrolments() E
WHERE e.schno = any(select schno from #ebseChange)

drop table #ebseChange
END
GO

