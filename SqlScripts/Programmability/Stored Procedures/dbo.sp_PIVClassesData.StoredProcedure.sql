SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-01-20
-- Description:	data from the 2010 classes format
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVClassesData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT
	EE.schNo,
	EE.LifeYear AS [Survey Year],
	EE.bestssID,
	EE.surveyDimensionssID,
	EE.Estimate,
	EE.Offset AS [Age of Data],
	EE.bestYear AS [Year of Data],
	EE.bestssqLevel AS DataQualityLevel,
	EE.ActualssqLevel AS SurveyYearQualityLevel

	, C.pcHrsWeek as HoursPerWeek
	, DimensionLanguage.*
	, dimensionSubject.*
	,DR.*
	, pclM as EnrolM
	, pclF as EnrolF
	, pclNum as EnrolU
	, pclLevel as levelCode

, isnull(pctHrsWeek, C.pcHrsWeek) TeacherHoursPerWeek
, tID

from
#ebse EE inner join
Classes C
on EE.bestssID = ssID
inner join DimensionRank DR
on EE.LifeYear = DR.svyYear and EE.schNo = DR.schNo
left join ClassLevel CL
on C.pcID = CL.pcID
left join ClassTEacher CT
on C.pcID = Ct.pcID
left join DimensionLanguage
on C.pcLang = DimensionLanguage.LanguageCode
left join DimensionSubject
on DimensionSubject.[Subject Code] = C.subjCode


drop table #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVClassesData] TO [pEnrolmentRead] AS [dbo]
GO

