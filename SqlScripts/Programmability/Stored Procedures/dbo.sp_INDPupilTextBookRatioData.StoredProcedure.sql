SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 11 2007
-- Description:	Data for pupil text book ratios
-- =============================================
CREATE PROCEDURE [dbo].[sp_INDPupilTextBookRatioData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- begin by creating a union query to denormalise the following:
-- enrolments
-- number of text books
-- number of readers

-- we estimate enrolments and resources separately

CREATE Table #data
(
	LifeYear int,
	[SchNo] nvarchar(50),
	[levelCode] nvarchar(10),
	[Subject] nvarchar(50),
	Enrol int,
	NumTextBooks int,
	NumTextBooksGood int,
	NumTextBooksFair int,
	NumTextBooksPoor int,
	NumReaders int,
	NumReadersGood int,
	NumReadersFair int,
	NumReadersPoor int,

)

Select *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyenrolments()

Select *
INTO #ebt
FROM dbo.tfnESTIMATE_BestSurveyResourceCategory('Text Books')

Select *
INTO #ebr
FROM dbo.tfnESTIMATE_BestSurveyResourceCategory('Readers')

INSERT INTO #data
(LifeYear, schNo, levelCode, Enrol)
SELECT
	E.LifeYear,
	E.SchNo,
	S.levelCode,
	S.Enrol
FROM
	#ebse E
	INNER JOIN pEnrolmentRead.ssIDEnrolmentLevel S
		ON E.bestssID = S.ssID

-- now textbooks

INSERT INTO #data
(LifeYear, schNo, levelCode,[Subject],
NumTextBooks,
NumTextBooksGood,NumTextBooksFair,NumTextBooksPoor)
SELECT
	E.LifeYear,
	E.SchNo,
	T.reslevel,
	T.subject,
	T.NumTextbooks,
	T.NumTextbooksGood,
	T.NumTextbooksFair,
	T.NumTextbooksPoor

FROM
	#ebt E
	INNER JOIN ssIDTextbooksLevelSubject T
		ON E.bestssID = T.ssID
	WHERE numTextbooks > 0

-- and readers
INSERT INTO #data
(LifeYear, schNo, levelCode,[Subject],
NumReaders,
NumReadersGood,NumReadersFair,NumReadersPoor)
SELECT
	E.LifeYear,
	E.SchNo,
	T.reslevel,
	T.subject,
	T.NumReaders,
	T.NumReadersGood,
	T.NumReadersFair,
	T.NumReadersPoor

FROM
	#ebr E
	INNER JOIN ssIDReadersLevelSubject T
		ON E.bestssID = T.ssID
	WHERE numReaders > 0


Select
	Q.LifeYear as [Survey Year],

	E.surveyDimensionssID,
	E.Estimate [Estimate Enrol],
	E.bestyear [Year of Enrol Data],
	E.offset [Age of Enrol Data],
	T.Estimate [Estimate Textbooks],
	T.bestyear [Year of Textbook Data],
	T.offset [Age of Textbook Data],

	Q.levelCode,
	Q.subject,
	Enrol,
	NumTextbooks,
	NumTextbooksGood,
	NumTextbooksFair,
	NumTextbooksPoor,

	NumReaders,
	NumReadersGood,
	NumReadersFair,
	NumReadersPoor

from
(
	Select
		LifeYear,
		schno,
		D.levelCode,
		D.subject,
		sum(Enrol) as Enrol,
		sum(NumTextbooks) NumTextbooks,
		sum(NumTextbooksGood) NumTextbooksGood,
		sum(NumTextbooksFair) NumTextbooksFair,
		sum(NumTextbooksPoor) NumTextbooksPoor,

		sum(NumReaders) NumReaders,
		sum(NumReadersGood) NumReadersGood,
		sum(NumReadersFair) NumReadersFair,
		sum(NumReadersPoor) NumReadersPoor
	FROM #data D
	GROUP BY LifeYear, schNo, levelCode, D.[Subject]
) Q
LEFT JOIN #ebse E
	ON Q.LifeYear = E.LifeYear and Q.schNo = E.schNo
LEFT JOIN #ebt T
	ON Q.LifeYear = T.LifeYear and Q.schNo = T.schNo


drop table #ebse
drop table #ebt
drop table #data
END
GO
GRANT EXECUTE ON [dbo].[sp_INDPupilTextBookRatioData] TO [pSchoolRead] AS [dbo]
GO

