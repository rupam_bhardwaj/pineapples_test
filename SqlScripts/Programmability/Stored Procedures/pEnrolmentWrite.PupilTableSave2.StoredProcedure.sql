SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 1 2016
-- Description:	save from web site
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[PupilTableSave2]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50),
	@SurveyYear int,
	@TableName nvarchar(50),
	@grid xml,
	@user nvarchar(50) = null
	WITH Execute as 'pineapples'
AS
BEGIN

/*

	 as of 7 10 2009, the table codes map to data Codes in metaPupilTableDefs, so
	that more than one type of table can write to the same ptCode in Pupil Tables
	For legacy applications, this will work the same (since there will be in metaPupilTableDEfs a generic record for each ptCode)


*/

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select @user = isnull(@user, original_login())

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;


	declare @surveyID int
	declare @tdefRows nvarchar(50)		-- the row source from metadata
	declare @tdefCols nvarchar(50)		-- the col source
	declare @codeType nvarchar(50)

	-- get the survey ID
	Select @SurveyId = ssId
	from SchoolSurvey SS
	WHERE schNo = @SchoolNo
	AND svyYear = @SurveyYear

	-- to do - create a survey if necessary


	declare @grdtbl table(	seq int IDENTITY,
							row nvarchar(20) collate database_default,
							col nvarchar(20) collate database_default,
							M int NULL,
							F int NULL)
	-- this is the set of column tags for the grid
	declare @colSet table( code nvarchar(20) collate database_default
							)
	-- this is the set of row tags for the grid
	declare @rowSet table( code nvarchar(20)  collate database_default
							)

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid

-- populate the table of grid values
	INSERT @grdtbl (row, col, M, F)
	Select * from OPENXML (@idoc, '/PupilTable/Data/RowColData',2)
		WITH (

				row nvarchar(20) 'Row',
				col nvarchar(20) 'Col',
				M int 'M/text()',
				F int 'F/text()')
-- get the complete set of columns
	INSERT @colSet
	Select colcode from OPENXML(@idoc, '/PupilTable/Cols/LookupTable',2)
		WITH (
				colcode nvarchar(20) 'codeCode'
			)

-- get the complete set of rows
	INSERT @rowSet
	Select rowcode from OPENXML(@idoc, '/PupilTable/Rows/LookupTable',2)
		WITH (
				rowcode nvarchar(20) 'codeCode'
			)
-- 13 12 2007 this supports pineapples @school, which will send grids without colSet
-- or rowSet, assuming that all data will be supplied in one grid
declare @rowRange int;
select @rowRange = count(*) from @rowSet

declare @colRange int;
select @colRange = count(*) from @colSet

-- delete
-- if column and rowset specified, restrict the delete to that range
-- otherwise delete all

-- transaction control

/*
	-- update the records that match
	UPDATE PupilTables
		set ptM = G.M,
			ptF = G.F
	from PupilTables P inner join @grdtbl G
		on P.ptRow = G.Row and P.ptCol = G.Col

	WHERE P.ssID = @surveyID
			and P.ptCode = @codeType

	INSERT INTO PupilTables(ssID, ptCode, ptRow, ptCol, ptM, ptF)
	SELECT @surveyID, @codeType, G.Row, G.Col, G.M,G.F
	FROM @grdtbl G left join (Select * from PupilTables
	WHERE ssID = @surveyID and ptCode = @codeType) P
	on G.Row = p.ptRow and G.col = p.ptCol
	WHERE ptID is null
    -- Insert statements for procedure here
	-- SELECT @grid
*/
declare @rowSplit int; --0 = ptAge, 1 = ptLevel, 2= ptRow
declare @colSplit int; --0 = ptAge, 1 = ptLevel, 2= ptCol
declare @pageSplit int; --0 = nothing goes in page 1=row number goes in page

select @rowSplit =
	case
		when tdefRows is null then -1			-- to support pupiltable formats
		when tdefRows like 'AGE%' then 0
		when tdefRows like 'LEVEL%' then 1
		else 2
	end,
	@colSplit =
	case
		when tdefCols is null then 1		-- level is the default for columns
		when tdefCols like 'AGE%' then 0
		when tdefCols like 'LEVEL%' then 1
		else 2
	end,
	@pagesplit =
	case
		when tdefRows = '<?>' then 1
		else 0
	end,
	@codetype = tdefDataCode
	from MetaPupilTableDefs
	WHERE tdefCode = @tableName		-- this is the table definition

If (@@RowCount = 0) begin
	Select @ErrorMessage = 'The table code ' + @tableName + ' is not defined in Pineapples. check the Table Defs in Survey Designer'
	RAISERROR(@ErrorMessage,16,2)
end
-- start the transaction


begin transaction

begin try

	-- delete only specified rows and columns
	DELETE from PupilTables
	FROM PupilTables, @rowSet R, @colSet C
	WHERE ssID = @SurveyID
		AND ptCode = @codeType
		--AND (ptPage = @page or (@page is null))
		AND (	(@rowsplit = -1) OR
				( @rowsplit = 0 and ptAge = R.code) OR
			 (@rowsplit = 1 and ptLevel = R.code)  OR
			 (@rowsplit = 2 and ptRow = R.code)
			)
		AND (( @colsplit = 0 and ptAge = C.code) OR
			 (@colsplit = 1 and ptLevel = C.code)  OR
			 (@colsplit = 2 and ptCol = C.code)
			)
	------Select count(*) from @RowSet
	------select @rowsplit
	------select * from @grdtbl

	INSERT INTO PupilTables(ssID, ptTableDef, ptCode, ptPage, ptLevel, ptAge, ptRow, ptCol, ptM, ptF)
	SELECT @surveyID,
	@TableName,
	@codeType,

	case	when @pagesplit = 1 then convert(int, D.seq)	-- use the actual row number
														--, created as an identity
														-- this is not fully supported becuase need handling of <?>
														-- row code in export
			else null
	end,
	-- for level, test the split values
	case
		when @rowsplit = 1 then R.code			-- the row split is Level
		when @colsplit = 1 then C.code			-- the column split is Level ( most common)
		--else YearLevel
	end,

	-- for age
	case
		when @rowsplit = 0 then convert(int,R.code)			--
		when @colsplit = 0 then convert(int,C.code)
		else null				-- ??? to support tertiary with yearLevel tag on the values node
	end,
	--row generic field ptRow
	case
		when @rowsplit = 2 then R.code
		else null
	end,
	--col
	case
		when @colsplit = 2 then C.code
		else null
	end,
	-- values
	D.M, D.F
	FROM @grdtbl D
	 	INNER JOIN @rowSet R
			ON D.row = R.code
		INNER JOIN @colSet C
			ON D.col = C.code

		declare @auditID int
		select @Tablename = @Tablename + ' (web)'
		exec @auditID = audit.logAudit
				'SchoolSurvey'
				, @Tablename
				, 'U'
				, @@rowcount
				, @user

		INSERT INTO Audit.auditRow
		(
				auditID
				, arowKey
		)
		VALUES ( @auditID, @SurveyID)
end try

begin catch

	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,@tableName
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit
if @@trancount > 0
	commit transaction

return

END
GO

