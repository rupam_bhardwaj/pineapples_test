SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Number of teachers at the school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolDataSetTeacherCount]
	-- Add the parameters for the stored procedure here
	@Year int
	, @SchoolNo nvarchar(50) = null
	, @GovtOther nvarchar(10) = null
	WITH EXECUTE AS 'pineapples'
AS
BEGIN

	Select schNo, count(DISTINCT TI.tID), Estimate, ActualssqLevel
	FROM dbo.tfnESTIMATE_BestSurveyTeachers(@Year, DEFAULT) EE
		INNER JOIN TeacherSurvey TS
			ON EE.bestssID = TS.ssID
		INNER JOIN TeacherIdentity TI
			ON TS.tID = TI.tID

	WHERE lifeYear = @Year
		AND (SchNo = @SchoolNo or @SchoolNo is null)
		AND ((TI.tPayroll is not null and @GovtOther = 'Y')
				or (TI.tPayroll is null and @GovtOther = 'N')
				or @GovtOther is null
			)
	GROUP BY SchNo, Estimate, ActualssqLevel

END
GO

