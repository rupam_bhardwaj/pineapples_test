SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 5 2010
-- Description:	renumber all positions
-- =============================================
CREATE PROCEDURE [pEstablishmentOps].[RenumberAllPositions]
	-- Add the parameters for the stored procedure here
	@Confirm int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin transaction

	----DECLARE c CURSOR FOR
	----Select estpNo, schNo from Establishment
	----ORDER BY estpNo
 ----   -- Insert statements for procedure here

 ----   open c
 ----   close c
    ----deallocate c
    declare @estpNo nvarchar(20)
    declare @SchoolNo nvarchar(50)

    declare @NumPositions int

    Select @NumPositions = count(estpNo)
    from Establishment

    Select estpNo
    , row_number() OVER (ORDER BY estpNo) NUM
    INTO #e
    FROM Establishment

    DECLARE @NewNums TABLE
    (
		posNo nvarchar(20)
		, num int
	)

print @NumPositions
    INSERT INTO @NewNums
    EXEC pEstablishmentWriteX.MakeEstablishmentNumbers null, @NumPositions


	INSERT INTO Establishment
	(
		[estpNo]
	   ,[schNo]
	   ,[estpScope]
	   ,[estpAuth]
	   ,[estpRoleGrade]
	   ,[estpActiveDate]
	   ,[estpClosedDate]
	   ,[estpClosedReason]
	   ,[estpTitle]
	   ,[estpDescription]
	   ,[estpSubject]
	   ,[estpFlag]
	   ,[estpCreateDate]
	   ,[estpCreateUser]
	   ,[estpEditDate]
	   ,[estpEditUser]
	   ,[estpConfirmed]
	   ,[estpSendDate]
	   ,[estpSendUser]
	   ,[estpSendBatch]
	   ,[estpNote]
	 )
	 Select
		PosNo
		, schNo
		, estpScope
		,[estpAuth]
	   ,[estpRoleGrade]
	   ,[estpActiveDate]
	   ,[estpClosedDate]
	   ,[estpClosedReason]
	   ,[estpTitle]
	   ,[estpDescription]
	   ,[estpSubject]
	   ,[estpFlag]
	   ,[estpCreateDate]
	   ,[estpCreateUser]
	   ,[estpEditDate]
	   ,[estpEditUser]
	   ,[estpConfirmed]
	   ,[estpSendDate]
	   ,[estpSendUser]
	   ,[estpSendBatch]
	   ,[estpNote]
	 FROM Establishment
	  INNER JOIN #e
		ON Establishment.estpNo = #e.estpNo
	  INNER JOIN @NewNums N
		ON #e.num = N.num


	 -- next move any appointments

	 UPDATE TeacherAppointment
		SET estpNo = PosNo
	 FROM TEacherAppointment
		INNER JOIN #e
			ON TEacherAppointment.estpNo = #e.estpNo
		INNER JOIN @NewNums N
			ON #e.num = N.num

	 DELETE
	 FROM Establishment
	 WHERE EstpNo in (Select estpNo from #E)


 --   FETCH NEXT from C
 --   INTO @estpNo, @SchoolNo

 --   WHILE @@FETCH_STATUS = 0 begin

	--	exec pEstablishmentOps.ChangePositionNumber @estpNo, null
	--	 FETCH NEXT from C
	--	INTO @estpNo, @SchoolNo

	--end
	Select estpNo, PosNo
    FROM #e INNER JOIN @NewNums N
		ON #e.num = N.num

		drop table #e

if @Confirm = 1
	commit transaction
else
	rollback


END
GO

