SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2 8 2009
-- Description:	Lite version of EFA12 query used for REconstructed Cohort calculations
-- =============================================
CREATE PROCEDURE [dbo].[EFA12LiteSaved]
	-- Add the parameters for the stored procedure here
	(@Consolidation int = 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

create table #efa
(
	schNo nvarchar(50),
	svyYear int,
	surveyDimensionssID int,

	LevelCode nvarchar(20),
	YearOfEd int,
	EnrolM int,
	EnrolF int,
	EnrolNYM int,
	EnrolNYF int,
	EnrolNYNextLevelM int,
	EnrolNYNextLevelF int,
	RepM int,
	RepF int,
	RepNYM int,
	RepNYF int,
	RepNYNextLevelM int,
	RepNYNextLevelF int,
	Estimate smallint,
	[Year Of Data] int,
	[Age of Data] int,
	[Estimate NY] smallint,
	[Year Of Data NY] int,
	[Age of Data NY] int
)


insert into #efa
exec dbo.sp_efacore 1


if @Consolidation = 0 or @Consolidation = 4	or @Consolidation = 5		-- these use Rank
	BEGIN

		select *
		into #ebse
		from dbo.tfnESTIMATE_BestSurveyEnrolments()

		exec BuildRank

		SELECT     EE.schNo
					, EE.LifeYear AS [Survey Year]
					, dbo.Enrollments.enAge
					, dbo.Enrollments.enLevel
					, enM
					, enF
					, EE.bestssID
					, EE.surveyDimensionssID
					, EE.Estimate
					, EE.Offset AS [Age of Data]
					, EE.bestYear AS [Year of Data]
					, EE.bestssqLevel AS DataQualityLevel
					, EE.ActualssqLevel AS SurveyYearQualityLevel
					, dbo.Enrollments.enAge - (dbo.Survey.svyPSAge + dbo.lkpLevels.lvlYear - 1) AS AgeOffset
					, dbo.Survey.svyYear - dbo.Enrollments.enAge AS YearOfBirth

						,	DR.[District Rank]
						,	DR.[District Dectile]
						,	DR.[District Quartile]
						,	DR.[Rank]
						,	DR.Dectile
						,	DR.Quartile


		into #eRank
		FROM         #ebse EE INNER JOIN
							  dbo.Enrollments ON EE.bestssID = dbo.Enrollments.ssID INNER JOIN
							  dbo.lkpLevels ON dbo.Enrollments.enLevel = dbo.lkpLevels.codeCode INNER JOIN
							  dbo.Survey ON EE.LifeYear = dbo.Survey.svyYear
								LEFT JOIN DimensionRank DR
								on EE.LifeYear = DR.svyYEar
								and EE.schNo = DR.schNo

		DROP TABLE #ebse

	END


-- school survey data
-- at school level, or higher
if @consolidation = 0 or @Consolidation = 5
		BEGIN
			print @Consolidation
			Select * INTO #ds
				from DimensionSchoolSurveyNoYear
				ORDER BY [Survey ID]

		END
else

--
--we make a leaner subset of DimensionSchoolSurvey
	Select ssId [Survey ID]
		, ssSchType SchoolTypeCode
		, ssAuth AuthorityCode
		, iGroup [District Code]
	INTO #dsSmall

	from DimensionSchoolSurveySub S
		INNER JOIN Islands I
		on s.iCode = I.iCode;


-- take a subset of these
SELECT
	LevelCode
	, [Level]
	, [Year of Education]
	, SectorCode
	, Sector
	, edLevelCode
	, [Education Level]
	, edLevelAltCode
	, [Education Level Alt]
	, edLevelAlt2Code
	, [Education Level Alt2]
	, [ISCED Level Name]
	, [ISCED Level]
	, [ISCED SubClass]

into #dl
FROM        DimensionLevel ORDER BY [Year of Education],levelCode


select * into #G
from DimensionGender

--if @Consolidation = 0
	-- this is equivalent to the full detail as assembled by the Pineapples client
	-- from vtblEnrolmentEst, DimensionSchoolSurvey, DimensionLevel, DimensionGender

-- all agregations present District, School Type and Gender
-- and possibly Age, Level, Authority


if @Consolidation = 0
	BEGIN
-- this option will return the full detail that we currently get from
-- EFA12, but without client-side assembly


		Select E.[Survey Year]

			, e.Estimate
			, E.[Age of Data]
			, e.[Year of Data]
			, e.DataQualityLevel
			, e.SurveyYearQualityLevel

-- age
			, e.enAge as Age
			, e.AgeOffset
			, e.YearOfBirth
-- gender
			, G.*
-- level
			, DL.*
-- school/survey
			, DS.*
-- rank

			,	E.[District Rank]
			,	E.[District Dectile]
			,	E.[District Quartile]
			,	E.[Rank]
			,	E.Dectile
			,	E.Quartile

			, e.bestssID
			, e.surveyDimensionssID


		, (case when G.GenderCode = 'M' then enM else enF end) Enrol
		, (enM) EnrolM
		, (enF) EnrolF


	from #eRank E
		INNER JOIN #DL DL
		on E.enLevel = DL.[LevelCode]
		INNER JOIN #DS DS
		on DS.[Survey ID] = e.surveyDimensionssID

		CROSS JOIN #G G

	END			-- consolidation = 0


IF @Consolidation = 1
	BEGIN
-- aggregates up to  level, authority

	Select * into #da
	from DimensionAuthority;

	WITH X AS
	(

		Select E.svyYear [Survey Year]
			, E.Estimate
			, levelCode
			, DS.SchoolTypeCode
			, DS.[District Code]
			, DS.AuthorityCode

			, sum(E.EnrolM) EnrolM
			, sum(E.EnrolF) EnrolF
			, sum(E.EnrolNYM) EnrolNYM
			, sum(E.EnrolNYF) EnrolNYF
			, sum(E.EnrolNYNextLevelM) EnrolNYNextLevelM
			, sum(E.EnrolNYNextLevelF) EnrolNYNextLevelF
			, sum(E.RepM) RepM
			, sum(E.RepF) RepF
			, sum(E.RepNYM) RepNYM
			, sum(E.RepNYF) RepNYF
			, sum(E.RepNYNextLevelM) RepNYNextLevelM
			, sum(E.RepNYNextLevelF) RepNYNextLevelF
			, sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYM]+[EnrolM])/cast(([Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM
			, sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYF]+[EnrolF])/cast(([Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF
		from #efa E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID

		group by E.svyYear
			, E.levelCode
			, E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]
			, DS.AuthorityCode

	)
		Select X.[Survey Year]
			, X.Estimate
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*
			, DA.*

			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.SlopedEnrolM else X.SlopedEnrolF end) SlopedEnrol
			, (case when G.GenderCode = 'M' then X.enrolNYM else X.enrolNYF end) EnrolNY
			, (case when G.GenderCode = 'M' then X.enrolNYNextLevelM else X.enrolNYNextLevelF end) EnrolNYNextLevel
			, (case when G.GenderCode = 'M' then X.RepM else X.RepF end) Rep
			, (case when G.GenderCode = 'M' then X.RepNYM else X.RepNYF end) RepNY
			, (case when G.GenderCode = 'M' then X.RepNYNextLevelM else X.RepNYNextLevelF end) RepNYNextLevel


		from X
			INNER JOIN #DL DL
			on X.levelCode = DL.[LevelCode]
			INNER JOIN #DA DA
			on X.AuthorityCode = DA.[AuthorityCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G


	END

IF @Consolidation = 2
-- aggregates up to age, level

	-- don;t have age
	BEGIN

	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]


			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #afe E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = #e.surveyDimensionssID

		group by [Survey Year], enAge, enLevel
			,#E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

	)
		Select X.[Survey Year]
			, X.Estimate
			, X.Age
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*

			, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
			, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol

		from X
			INNER JOIN #DL DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G


	END
IF @Consolidation = 3
-- aggregates up to level
	BEGIN

	WITH X AS
	(
		Select E.svyYear [Survey Year]
			, E.Estimate
			, levelCode
			, DS.SchoolTypeCode
			, DS.[District Code]


			, sum(E.EnrolM) EnrolM
			, sum(E.EnrolF) EnrolF
			, sum(E.EnrolNYM) EnrolNYM
			, sum(E.EnrolNYF) EnrolNYF
			, sum(E.EnrolNYNextLevelM) EnrolNYNextLevelM
			, sum(E.EnrolNYNextLevelF) EnrolNYNextLevelF
			, sum(E.RepM) RepM
			, sum(E.RepF) RepF
			, sum(E.RepNYM) RepNYM
			, sum(E.RepNYF) RepNYF
			, sum(E.RepNYNextLevelM) RepNYNextLevelM
			, sum(E.RepNYNextLevelF) RepNYNextLevelF
			, sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYM]+[EnrolM])/cast(([Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM
			, sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYF]+[EnrolF])/cast(([Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF
		from #efa E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID

		group by E.svyYear
			, E.levelCode
			, E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

	)
		Select X.[Survey Year]
			, X.Estimate
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*

			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.SlopedEnrolM else X.SlopedEnrolF end) SlopedEnrol
			, (case when G.GenderCode = 'M' then X.enrolNYM else X.enrolNYF end) EnrolNY
			, (case when G.GenderCode = 'M' then X.enrolNYNextLevelM else X.enrolNYNextLevelF end) EnrolNYNextLevel
			, (case when G.GenderCode = 'M' then X.RepM else X.RepF end) Rep
			, (case when G.GenderCode = 'M' then X.RepNYM else X.RepNYF end) RepNY
			, (case when G.GenderCode = 'M' then X.RepNYNextLevelM else X.RepNYNextLevelF end) RepNYNextLevel


		from X
			INNER JOIN #DL DL
			on X.levelCode = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END

IF @Consolidation = 4
-- aggregates up to level and rank
	BEGIN


	WITH X AS
	(


		Select E.svyYear [Survey Year]
			, E.Estimate
			, levelCode
			, DS.SchoolTypeCode
			, DS.[District Code]

			, R.[District Dectile]
			, R.[District Quartile]

			, R.Dectile
			, R.Quartile


			, sum(E.EnrolM) EnrolM
			, sum(E.EnrolF) EnrolF
			, sum(E.EnrolNYM) EnrolNYM
			, sum(E.EnrolNYF) EnrolNYF
			, sum(E.EnrolNYNextLevelM) EnrolNYNextLevelM
			, sum(E.EnrolNYNextLevelF) EnrolNYNextLevelF
			, sum(E.RepM) RepM
			, sum(E.RepF) RepF
			, sum(E.RepNYM) RepNYM
			, sum(E.RepNYF) RepNYF
			, sum(E.RepNYNextLevelM) RepNYNextLevelM
			, sum(E.RepNYNextLevelF) RepNYNextLevelF
			, sum(
				case when E.[Age of Data] > 0
					then (E.[Age of Data]*[EnrolNYM]+[EnrolM])/cast((E.[Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM
			, sum(
				case when E.[Age of Data] > 0
					then (E.[Age of Data]*[EnrolNYF]+[EnrolF])/cast((E.[Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF
		from #efa E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID
			left join Dimensionrank R
			on E.svyYear = R.[svyYear] and E.schNo = R.schNo

		group by E.svyYear
			, E.levelCode
			, E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]
			, R.[District Dectile]
			, R.[District Quartile]

			, R.Dectile
			, R.Quartile

	)
		Select X.[Survey Year]
			, X.Estimate
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*
			, [District Dectile]
			, [District Quartile]

			, Dectile
			, Quartile
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.SlopedEnrolM else X.SlopedEnrolF end) SlopedEnrol
			, (case when G.GenderCode = 'M' then X.enrolNYM else X.enrolNYF end) EnrolNY
			, (case when G.GenderCode = 'M' then X.enrolNYNextLevelM else X.enrolNYNextLevelF end) EnrolNYNextLevel
			, (case when G.GenderCode = 'M' then X.RepM else X.RepF end) Rep
			, (case when G.GenderCode = 'M' then X.RepNYM else X.RepNYF end) RepNY
			, (case when G.GenderCode = 'M' then X.RepNYNextLevelM else X.RepNYNextLevelF end) RepNYNextLevel


		from X
			INNER JOIN #DL DL
			on X.levelCode = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END

if @Consolidation = 5
	BEGIN
-- this option returns school level data, but wiothout ages,
-- and a restricted set of columns


	WITH X AS
	(
		Select E.[Survey Year]
			, E.Estimate
			, enLevel
			, DS.SchoolTypeCode
			, DS.[District Code]

			, DS.[School No]
			, DS.[School Name]
			, DS.SchoolID_Name
			, DS.SchoolID_Name_Type

			, E.[District Rank]
			, E.[District Dectile]
			, E.[District Quartile]

			, E.[Rank]
			, E.Dectile
			, E.Quartile

			, sum(enM) EnrolM
			, sum(enF) EnrolF


		from #eRank E
			INNER JOIN #ds DS
			on DS.[Survey ID] = e.surveyDimensionssID
		group by [Survey Year]
			, enLevel
			,E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]

			, DS.[School No]
			, DS.[School Name]
			, DS.SchoolID_Name
			, DS.SchoolID_Name_Type

			, E.[District Rank]
			, E.[District Dectile]
			, E.[District Quartile]

			, E.[Rank]
			, E.Dectile
			, E.Quartile


	)
		Select X.[Survey Year]
			, X.Estimate
			, X.[School No]
			, X.[School Name]
		--	, X.SchoolID_Name
		--	, X.SchoolID_Name_Type

			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, G.*
		--	, DL.*
			, DL.LevelCode
			, DL.Level
		--	, X.[District Rank]
		--	, X.[District Dectile]
		--	, X.[District Quartile]
		--	, X.[Rank]
		--	, X.Dectile
		--	, X.Quartile

		--	, (case when G.GenderCode = 'M' then X.enrolM else null end) EnrolM
		--	, (case when G.GenderCode = 'F' then X.enrolF else null end) EnrolF
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol

		from X
			INNER JOIN #DL DL
			on X.enLevel = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END


END
GO

