SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pEnrolmentWrite].[xfdfEnrolment]
	-- Add the parameters for the stored procedure here
	@surveyID int,
	@grid xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @grdtbl table(	Age int,
							EnLevel nvarchar(20) collate database_default,
							M int,
							F int)
	-- this is the set of column tags for the grid
	declare @colSet table(

	c nvarchar(2)
	, ck nvarchar(20)
	, cv nvarchar(100)
							)

	-- this is the set of row tags for the grid
	declare @rowSet table(

	r nvarchar(2)
	, rk nvarchar(20)
	, rv nvarchar(100)
	, rAge int
	)

	-- data n the grid
	declare @data TABLE
	(

		c			nvarchar(2)
		, r			nvarchar(2)
		, M		int
		, F		int

	)

	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'

	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid, @xmlns


-- populate the table of grid values
	INSERT INTO @data
	Select c,r,M,F

	FROM OPENXML(@idoc,'/x:field/x:field[@name="D"]/x:field/x:field',2)
	WITH
	(

		c			nvarchar(10)		'@name'
		, r			nvarchar(10)		'../@name'
		, M		int					'x:field[@name="M"]/x:value'
		, F		int					'x:field[@name="F"]/x:value'
		)
		WHERE r <> 'T' and c <> 'T'
-- get the complete set of columns
	INSERT INTO @colSet
	Select *
	FROM OPENXML(@idoc ,'/x:field/x:field[@name="C"]/x:field',2)
	WITH
	(
		c			nvarchar(2)			'@name'
		, ck			nvarchar(10)			'x:field[@name="K"]/x:value'
		, cv			nvarchar(20)			'x:field[@name="V"]/x:value'
	)
	WHERE c <> 'T'			-- ignore any totals
-- get the complete set of rows
	INSERT INTO @rowSet
		select r, rk, rv, convert(int, rv)
		FROM OPENXML(@idoc ,'/x:field/x:field[@name="R"]/x:field',2)
		WITH
		(
			r			nvarchar(2)			'@name'
			, rk			nvarchar(2)			'x:field[@name="K"]/x:value'
			, rv			nvarchar(20)			'x:field[@name="V"]/x:value'
		)
		WHERE r <> 'T'			-- ignore any totals


-- before doing anything we'll regret , ensure the table is well formed
-- ie that every r number is in @rowSet

declare @count int
Select @count = count(DISTINCT r)
FROM @data D
WHERE r not in (Select r from @rowSet)

if (@count > 0) begin
	RAISERROR('Not all rows are mapped to a value',16,16);
end

Select @count = count(DISTINCT c)
FROM @data D
WHERE c not in (Select c from @colSet)

if (@count > 0) begin
	RAISERROR('Not all columns are mapped to a value',16,16);
end

-- transaction control
begin transaction

begin try

	-- delete exisitng records within the scope of this grid
	DELETE from Enrollments
	from Enrollments
	WHERE ssID = @SurveyID
		AND (
			enAge = any (Select rAge from @rowSet)
			)
		AND (
			enLevel = any (Select ck from @colSet)
			)

	-- insert from the grid
	exec audit.xfdfInsert @SurveyID, 'Enrolment records deleted','Enrolment',@@rowcount

	INSERT INTO Enrollments(ssID, enAge, enLevel, enOrigin, enM, enF)
	SELECT @surveyID, rAge, ck,
		null,
	-- values
	M, F

	FROM @data D
		INNER JOIN @rowSet R
			ON D.R = R.R
		INNER JOIN @colSet C
			ON D.C = C.C
	WHERE M is not null or F is not null
	exec audit.xfdfInsert @SurveyID, 'Enrolment records inserted','Enrolment',@@rowcount
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end
	exec audit.xfdfError @SurveyID, @ErrorMessage,'Enrolment'
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

exec LogActivity 'updateEnrolment', @SurveyID
return


END
GO

