SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 8 2011
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pFinanceReadX].[GrantsFilter]
	-- Add the parameters for the stored procedure here
	@GrantYear int = null
	, @PaymentNo int = null
	, @Status nvarchar(20) = null
	, @Estimated int = null
	, @Adjusted int = null
	, @SchoolNo nvarchar(50) = null
	, @SchoolType nvarchar(10) = null
	, @Authority nvarchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @Keys TABLE
(
	sgID int
	, TotalPayment decimal(16,2)
	, RecNo int
)

INSERT INTO @Keys
(
	sgId
	, TotalPayment
	, RecNo
)

SELECT sgID
, TotalPayment
, row_number() OVER (ORDER BY sortcol)
FROM
	(SELECT SG.sgID
		, TP.TotalPayment
		, SG.sgID sortCol

   FROM SchoolGrant SG
	LEFT JOIN (Select sgID,
			sum(sgiValue) TotalPayment
			FROM SchoolGrantItems GROUP BY sgID) TP
		ON SG.sgID = TP.sgID
	INNER JOIN Schools S
		ON SG.schNo = S.schNo
	LEFT JOIN SchoolYearHistory SYH
		ON SG.schNo = SYH.schNo
		AND SG.sgYear = SYH.syYear
	WHERE
		(SG.sgYear = @GrantYear or @GrantYear is null)
		AND (SG.sgPaymentNo = @PaymentNo or @PaymentNo is null)
		AND (SG.sgEstimate = @Estimated or @Estimated is null)
		AND (SG.sgRecalculated = @Adjusted or @Adjusted is null)
		AND (SG.sgStatus = @Status or @Status is null)

		AND (SG.schNo = @SchoolNo or @SchoolNo is null)
		AND (isnull(SYH.systcode, S.schType) = @SchoolType or @SchoolType is null)
		AND (isnull(SYH.syAuth, S.schauth) = @Authority or @Authority is null)
	) sub

Select SG.*
   , S.schName
   , TotalPayment

   FROM SchoolGrant SG
	LEFT JOIN (Select sgID,
			sum(sgiValue) TotalPayment
			FROM SchoolGrantItems GROUP BY sgID) TP
		ON SG.sgID = TP.sgID
	INNER JOIN Schools S
		ON SG.schNo = S.schNo
	LEFT JOIN SchoolYearHistory SYH
		ON SG.schNo = SYH.schNo
		AND SG.sgYear = SYH.syYear
	WHERE sg.sgID in (Select sgId from @Keys)

-- return the summary
	SELECT count(*) NumMatches
	, sum(totalPayment) SelectedTotal
	FROM @Keys K
END
GO

