SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolDataSetEnrolment]
	-- Add the parameters for the stored procedure here
	@Year int
	, @SchoolNo nvarchar(50) = null
	, @ClassLevel nvarchar(10) = null
AS
BEGIN

	Select * from pSchoolRead.tfnSchoolDataSetEnrolment(@Year, @SchoolNo, @ClassLevel, NULL)

END
GO

