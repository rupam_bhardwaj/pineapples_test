SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[MakeListxtb]
	-- Add the parameters for the stored procedure here
WITH EXECUTE AS 'pineapples'
AS
BEGIN

	DECLARE @lists varchar(2000)
	DECLARE @listfields varchar(2000)

	SET @lists = ''
	set @listfields = ''
	--// Get a list of the pivot columns that are important to you.
	SELECT @lists = @lists + '[' + convert(varchar(20),lstName,101) + '],'
	FROM (SELECT Distinct lstName FROM lists) l
	SELECT @listfields = @listfields + '[' + convert(varchar(20),lstName,101) + '] nvarchar(50),'
	FROM (SELECT Distinct lstName FROM lists) l

	--// Remove the trailing comma
	if (len(@lists) > 0 )
		SET @lists = LEFT(@lists, LEN(@lists) - 1)
	else
		SET @lists = ''

	if (LEN(@listFields) > 0)
		set @listfields = LEFT(@listfields, LEN(@listfields) - 1)
	else
		set @listFields = ''

	--// Now execute the Select with the PIVOT and dynamically add the list
	--// of dates for the columns
	declare @cmd nvarchar(2000)
	----set @cmd=  'ALTER FUNCTION listxtb()
	----	RETURNS
	----	@Table_Var TABLE
	----	( schNo nvarchar(50),
	----	' + @listfields + '
	----	)
	----	AS
	----	BEGIN
	----	INSERT INTO @Table_Var SELECT schNo, ' + @lists +
	----	' FROM listschools PIVOT (MAX(lstValue) FOR lstName IN (' + @lists + ')) p
	----	RETURN
	----	END'
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[listxtab]'))
	DROP VIEW [dbo].[listxtab]

	-- recreate the view

	if (LEN(@lists) > 0)
		set @cmd=  'CREATE VIEW dbo.listxtab

			AS
			SELECT schNo, ' + @lists +
			' FROM listschools PIVOT (MAX(lstValue) FOR lstName IN (' + @lists + ')) p'
	else
		set @cmd=  'CREATE VIEW dbo.listxtab

			AS
			SELECT schNo FROM listschools '

	exec (@cmd)
	GRANT VIEW DEFINITION on ListXTab to public
	GRANT SELECT on ListXTab to pSchoolRead
	exec sp_refreshview 'dbo.DimensionSchoolSurveyNoYear'		-- sometimes this doesn;t pick up the new columns
	exec sp_refreshview 'dbo.DimensionSchoolSurvey'		-- sometimes this doesn;t pick up the new columns
END
GO
GRANT EXECUTE ON [dbo].[MakeListxtb] TO [pSchoolRead] AS [dbo]
GO

