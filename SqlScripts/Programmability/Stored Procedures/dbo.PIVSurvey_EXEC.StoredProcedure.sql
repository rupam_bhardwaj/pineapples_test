SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	Execute toilets pivot - this assumes we have already created
-- #tmpPivColsToilets in the calling procedure
-- the model is:
----- PIV 9 e.g.ToiletsSanitation
		--> calls PIV <data columns> <group>
		--> this family all create the temp table, then call PIV EXec
---- this oversomes the problem that
	-- you cannot have two SELECT INTO #tmp in the same proc
	-- tmp table are in scope downstram but not upstream. This is becuase
	-- they are deleted when the SP that created them exits

-- =============================================
CREATE PROCEDURE [dbo].[PIVSurvey_EXEC]
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select * into
#ebst
from dbo.tfnESTIMATE_BestSurveyEnrolments() E
WHERE (E.schNo = @SchNo or @SchNo is null)
	and (E.LifeYear = @SurveyYear or @SurveyYear is null)

print 'DimensionColumns'
print @DimensionColumns

print 'DataColumns'
print @DataColumns

print 'Group'
print @Group


SELECT
EBT.LifeYear AS [Survey Year],
EBT.Estimate,
EBT.bestYear AS [Year of Data],
EBT.Offset AS [Age of Data],
EBT.bestssqLevel AS [Data Quality Level],
EBT.ActualssqLevel AS [Survey Year Quality Level],
EBT.surveyDimensionssID,
COLS.*
INTO #tmppiv
FROM #ebst AS EBT
	INNER JOIN #tmppivcols COLS
		ON EBT.bestssID = COLS.ssID


-----------------------------------------------------
-- output
-----------------------------------------------------

if (@DimensionColumns is null) begin
	SELECT T.*
	FROM #tmppiv T
end


if (@DimensionColumns= 'ALL') begin
	SELECT T.*
	, DSS.*
	FROM #tmppiv T
	INNER JOIN DimensionSchoolSurvey DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end

if (@DimensionColumns= 'CORE') begin
	if (@group is null) begin
		if (@DataColumns is null)
			SELECT T.*
			, DSS.*
			FROM #tmppiv T
			INNER JOIN DimensionSchoolSurveyCore DSS
				ON T.surveyDimensionssID = Dss.[survey Id]
		if (@DataColumns = 'ENROL')
			SELECT T.[Survey Year]
			, T.Estimate
			, T.Enrol
			, DSS.*
			FROM #tmppiv T
			INNER JOIN DimensionSchoolSurveyCore DSS
				ON T.surveyDimensionssID = Dss.[survey Id]
	end
	else
	if (@group = 'AGELEVELGENDER') begin
		if (@DataColumns = 'ENROL')
			SELECT  sum(T.Enrol)
				, T.[Survey Year], T.Estimate
				, [District Code], [District]
				  ,[AuthorityCode], [Authority], [AuthorityType]
				  ,[AuthorityGroup], [LanguageCode], [Language], [LanguageGroup]
				  ,[SchoolTypeCode], [SchoolType]
				  , edLevelCode, [Education Level]
				  , levelCode, age, GenderCode, Gender
				FROM #tmppiv T
				INNER JOIN DimensionSchoolSurveyCore DSS
					ON T.surveyDimensionssID = Dss.[survey Id]
				GROUP BY
				T.[Survey Year], T.Estimate
				, [District Code], [District]
				  ,[AuthorityCode], [Authority], [AuthorityType]
				  ,[AuthorityGroup], [LanguageCode], [Language], [LanguageGroup]
				  ,[SchoolTypeCode], [SchoolType]
				  , levelCode, age, GenderCode, Gender
	end
	if (@group = 'SUM') begin
		if (@DataColumns is null)
				SELECT
				sum([Number]) Number,

				[Survey Year], Estimate, [District Code], [District]
				  ,[Island], [AuthorityCode], [Authority], [AuthorityType]
				  ,[AuthorityGroup], [LanguageCode], [Language], [LanguageGroup]
				  ,[SchoolTypeCode], [SchoolType]

				  -- groupings specific to this dataset
				  , [Type], UsedBy
				FROM #tmppiv T
				INNER JOIN DimensionSchoolSurveyCORE DSS
					ON T.surveyDimensionssID = Dss.[survey Id]
				GROUP BY
				[Survey Year], Estimate, [District Code], [District]
				  ,[Island], [AuthorityCode], [Authority], [AuthorityType]
				  ,[AuthorityGroup], [LanguageCode], [Language], [LanguageGroup]
				  ,[SchoolTypeCode], [SchoolType]
				  , [Type], UsedBy

			if (@DataColumns = 'ENROL')
			print 'core-enrol-sum'
				SELECT  sum(T.Enrol)
				, T.[Survey Year], T.Estimate
				, [District Code], [District]
				  ,[AuthorityCode], [Authority], [AuthorityType]
				  ,[AuthorityGroup], [LanguageCode], [Language], [LanguageGroup]
				  ,[SchoolTypeCode], [SchoolType]
				  , edLevelCode, [Education Level]
				FROM #tmppiv T
				INNER JOIN DimensionSchoolSurveyCore DSS
					ON T.surveyDimensionssID = Dss.[survey Id]
				GROUP BY
				T.[Survey Year], T.Estimate
				, [District Code], [District]
				  ,[AuthorityCode], [Authority], [AuthorityType]
				  ,[AuthorityGroup], [LanguageCode], [Language], [LanguageGroup]
				  ,[SchoolTypeCode], [SchoolType]
				  , edLevelCode, [Education Level]

	end

end

if (@DimensionColumns= 'WEB') begin
	if (@DataColumns is null)
		SELECT T.*
		, DSS.*
		FROM #tmppiv T
		INNER JOIN DimensionSchoolSurveyWeb DSS
			ON T.surveyDimensionssID = Dss.[surveyId]
	if (@DataColumns = 'ENROL')
		SELECT T.[Survey Year]
		, T.Estimate
		, T.Enrol
		, DSS.*
		FROM #tmppiv T
		INNER JOIN DimensionSchoolSurveyWeb DSS
			ON T.surveyDimensionssID = Dss.[surveyId]

end
if (@DimensionColumns= 'CORESUMM') begin
	SELECT

	sum([Number]) Number,

	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

      -- groupings specific to this dataset
      , [Type]
      , UsedBy

	FROM #tmppiv T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	GROUP BY
	[Survey Year]
	, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

      , [Type]
      , UsedBy

end

-- variant on core including electorates, region and popgis ids, lat/long/elev
--
if (@DimensionColumns= 'GEO') begin

	SELECT T.*
	, DSS.*
	FROM #tmppiv T
	INNER JOIN DimensionSchoolSurveyGeo DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'RANK') begin
-- rebuild the rank table
	exec buildRank


	SELECT T.*
	, DSS.*

      -- from DimensionRank
      ,	DRANK.[School Enrol],
		DRANK.[District Rank],
		DRANK.[District Decile],
		DRANK.[District Quartile],
		DRANK.[Rank],
		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmppiv T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear


end


if (@DimensionColumns= 'RANKSUMM') begin
-- rebuild the rank table
	exec buildRank

	if (@DataColumns is null)
		SELECT
		sum(Number) Number,

		[Survey Year]
		, Estimate
		 ,[District Code]
		  ,[District]
		  ,[Island]
		  ,[AuthorityCode]
		  ,[Authority]
		  ,[AuthorityType]
		  ,[AuthorityGroup]
		  ,[LanguageCode]
		  ,[Language]
		  ,[LanguageGroup]
		  ,[SchoolTypeCode]
		  ,[SchoolType]
	-- specific to this dataset
	--      , [Type]
	--      , UsedBy
		  -- from DimensionRank
			, sum(DRANK.[School Enrol]) [School Enrol]
			, DRANK.[District Decile],
			DRANK.[District Quartile],

			DRANK.[Decile],
			DRANK.[Quartile]

		FROM #tmppiv T
		INNER JOIN DimensionSchoolSurveyCORE DSS
			ON T.surveyDimensionssID = Dss.[survey Id]
		LEFT JOIN DimensionRank DRANK
			on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear

		GROUP BY
		[Survey Year]
		, Estimate
		 , [District Code]
		  ,[District]
		  ,[Island]
		  ,[AuthorityCode]
		  ,[Authority]
		  ,[AuthorityType]
		  ,[AuthorityGroup]
		  ,[LanguageCode]
		  ,[Language]
		  ,[LanguageGroup]
		  ,[SchoolTypeCode]
		  ,[SchoolType]

	-- specific to this dataset
	--      , [Type]
	--      , UsedBy

	-- from DimensionRank

		, DRANK.[District Decile],
		DRANK.[District Quartile],
		--DRANK.[Rank],			don;t use rank when grouping
		DRANK.[Decile],
		DRANK.[Quartile]

	if (@DataColumns = 'ENROL')
		SELECT
		sum(Enrol) Enrol,

		[Survey Year]
		, Estimate
		 ,[District Code]
		  ,[District]
		  ,[Island]
		  ,[AuthorityCode]
		  ,[Authority]
		  ,[AuthorityType]
		  ,[AuthorityGroup]
		  ,[LanguageCode]
		  ,[Language]
		  ,[LanguageGroup]
		  ,[SchoolTypeCode]
		  ,[SchoolType]
	-- specific to this dataset
	--      , [Type]
	--      , UsedBy
		  -- from DimensionRank
			, sum(DRANK.[School Enrol]) [School Enrol]
			, DRANK.[District Decile],
			DRANK.[District Quartile],

			DRANK.[Decile],
			DRANK.[Quartile]

		FROM #tmppiv T
		INNER JOIN DimensionSchoolSurveyCORE DSS
			ON T.surveyDimensionssID = Dss.[survey Id]
		LEFT JOIN DimensionRank DRANK
			on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear

		GROUP BY
		[Survey Year]
		, Estimate
		 , [District Code]
		  ,[District]
		  ,[Island]
		  ,[AuthorityCode]
		  ,[Authority]
		  ,[AuthorityType]
		  ,[AuthorityGroup]
		  ,[LanguageCode]
		  ,[Language]
		  ,[LanguageGroup]
		  ,[SchoolTypeCode]
		  ,[SchoolType]

	-- specific to this dataset
	--      , [Type]
	--      , UsedBy

	-- from DimensionRank

		, DRANK.[District Decile],
		DRANK.[District Quartile],
		--DRANK.[Rank],			don;t use rank when grouping
		DRANK.[Decile],
		DRANK.[Quartile]


END

drop table #ebst
drop table #tmppiv


END
GO

