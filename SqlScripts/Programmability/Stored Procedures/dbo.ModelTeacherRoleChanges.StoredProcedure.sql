SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian LEwis
-- Create date: 25 4 2010
-- Description:	model changes to the structure of Teacher Roles
-- =============================================
CREATE PROCEDURE [dbo].[ModelTeacherRoleChanges]
	-- Add the parameters for the stored procedure here
	@Confirm int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- cleanups for SI Roles
--


--HD and HOD is a duplicate - remove HOD
begin transaction

UPDATE TEacherSurvey
SET tchRole = 'HD'
WHERE tchRole = 'HOD'

UPDATE TEacherSurvey
SET tchRole = 'HS'
WHERE tchRole = 'SUBJH'

UPDATE TEacherSurvey
SET tchRole = 'CTE'
WHERE tchRole = 'ECE'

UPDATE TEacherSurvey
SET tchRole = 'DHT'
WHERE tchRole = 'DHTA'

-- TIT becomes TP, TE or TS

UPDATE TeacherSurvey
SET tchRole = 'TTP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'TIT'
AND ssSchtype = 'PS'

UPDATE TeacherSurvey
SET tchRole = 'TTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'TIT'
AND ssSchtype = 'ECE'

UPDATE TeacherSurvey
SET tchRole = 'TTS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'TIT'
AND ssSchtype in ('PSS','NSS')


-- SEN becomes STS, STE or STP

UPDATE TeacherSurvey
SET tchRole = 'STP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'SEN'
AND ssSchtype = 'PS'

UPDATE TeacherSurvey
SET tchRole = 'STE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'SEN'
AND ssSchtype = 'ECE'

UPDATE TeacherSurvey
SET tchRole = 'STS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'SEN'
AND ssSchtype in ('PSS','NSS')

-- PROB becomes becomes PROBP, PROBE, PROBS
UPDATE TeacherSurvey
SET tchRole = 'PROBP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROB'
AND ssSchtype = 'PS'

UPDATE TeacherSurvey
SET tchRole = 'PROBE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROB'
AND ssSchtype = 'ECE'

UPDATE TeacherSurvey
SET tchRole = 'PROBS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROB'
AND ssSchtype in ('PSS','NSS')

--
UPDATE TeacherSurvey
SET tchRole = 'PROBV'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROB'
AND ssSchtype in ('RTC')

UPDATE TeacherSurvey
SET tchRole = 'CT'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROBP'

UPDATE TeacherSurvey
SET tchRole = 'CTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROBE'

UPDATE TeacherSurvey
SET tchRole = 'CTS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROBS'

UPDATE TeacherSurvey
SET tchRole = 'CTV'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROBV'
AND ssSchtype in ('RTC')


-- separate class teachers in ECE and secondary
UPDATE TeacherSurvey
SET tchRole = 'CTS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'CT'
AND ssSchtype in ('PSS','NSS')

UPDATE TeacherSurvey
SET tchRole = 'CTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'CT'
AND ssSchtype = 'ECE'

UPDATE TeacherSurvey
SET tchRole = 'CTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'CTECE'

-- ASST becomes becomes ATP, ATE, ATS
UPDATE TeacherSurvey
SET tchRole = 'ATP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'ASST'
AND ssSchtype = 'PS'

UPDATE TeacherSurvey
SET tchRole = 'ATE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'ASST'
AND ssSchtype = 'ECE'

UPDATE TeacherSurvey
SET tchRole = 'ATS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'ASST'
AND ssSchtype in ('PSS','NSS')


-- HT may be primary or ECE
--
UPDATE TeacherSurvey
SET tchRole = 'HTECE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'HT'
AND ssSchtype = 'ECE'

UPDATE TeacherSurvey
SET tchRole = 'DHTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'DHT'
AND ssSchtype = 'ECE'

-- a DHT in a secondary school schould be a DP
UPDATE TeacherSurvey
SET tchRole = 'DP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'DHT'
AND ssSchtype in ('PSS','NSS')


-- and a DP in a TVT should be a DPV
UPDATE TeacherSurvey
SET tchRole = 'DPV'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'DP'
AND ssSchtype in ('RTC')

-- and a DP in a PRI should be a DHT
UPDATE TeacherSurvey
SET tchRole = 'DHT'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'DP'
AND ssSchtype in ('PRI')

-- and a DPP in PRI sector should be a DHT
UPDATE TeacherSurvey
SET tchRole = 'DHT'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'DPP'
AND tchSector = 'PRI'

UPDATE TeacherSurvey
SET tchRole = 'DP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'DPP'
AND tchSector = 'SEC'
-- rationalise Senior teachers
UPDATE TeacherSurvey
SET tchRole = 'STE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('SEN','SSEC')
AND ssSchtype = 'ECE'

UPDATE TeacherSurvey
SET tchRole = 'STP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('SEN','SSEC')
AND ssSchtype = 'PS'


UPDATE TeacherSurvey
SET tchRole = 'STS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('SEN','SSEC')
AND ssSchtype in ('PSS','NSS')

UPDATE TEacherSurvey
SET tchRole = 'STS'
WHERE tchRole = 'SSEC'			-- we already picked out the ones in primary

UPDATE TeacherSurvey			-- this is just miscoded
SET tchRole = 'CT'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('SEC')
AND ssSchtype in ( 'PS')

UPDATE TeacherSurvey			-- this is just miscoded
SET tchRole = 'CTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('SEC')
AND ssSchtype in ( 'ECE')

UPDATE TeacherSurvey			-- this is just miscoded
SET tchRole = 'CTV'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('SEC')
AND ssSchtype in ( 'TVET')


UPDATE TeacherSurvey			-- change of code
SET tchRole = 'CTS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('SEC')

UPDATE TeacherSurvey
SET tchSector = CalcSector
FROM TEacherSurvey TS
CROSS APPLY pTeacherRead.TEacherSurveySectorAudit(tchsID) AU
WHERE CalcSector is not null

-- now we have the best idea of sector, we may clear up some CHS roles
UPDATE TeacherSurvey			-- change of code
SET tchRole = 'PROBS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROB'
and tchSector = 'SEC'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'PROBP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROB'
and tchSector = 'PRI'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'PROBE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'PROB'
and tchSector = 'ECE'

-- now we have the best idea of sector, we may clear up some CHS roles
UPDATE TeacherSurvey			-- change of code
SET tchRole = 'ATS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('ASST','1A')
and tchSector = 'SEC'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'ATP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('ASST','1A')
and tchSector = 'PRI'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'ATE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ('ASST','1A')
and tchSector = 'ECE'

-- now we have the best idea of sector, we may clear up some CHS roles
UPDATE TeacherSurvey			-- change of code
SET tchRole = 'STS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'SEN'
and tchSector = 'SEC'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'STP'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'SEN'
and tchSector = 'PRI'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'STE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'SEN'
and tchSector = 'ECE'

-- (A) acting
UPDATE TeacherSurvey			-- change of code
SET tchRole = 'CTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = '(A)'
and tchSector = 'ECE'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'CTS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = '(A)'
and tchSector = 'SEC'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'CT'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = '(A)'
and tchSector = 'PRI'

-- Principal Means SEcondary, use PV for TVET
UPDATE TeacherSurvey			-- change of code
SET tchRole = 'PV'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole = 'P'
and tchSector = 'TVT'


-- relief teachers are Class Teachers
UPDATE TeacherSurvey			-- change of code
SET tchRole = 'CT'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ( 'REL', 'ATTC')
and tchSector = 'PRI'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'CTS'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ( 'REL', 'ATTC')
and tchSector = 'SEC'

UPDATE TeacherSurvey			-- change of code
SET tchRole = 'CTE'
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tchRole in ( 'REL', 'ATTC')
and tchSector = 'ECE'

-- the model
Select svyYear, ssSchType, tchSector, tchRole, count(*) as NumRole
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
GROUP BY tchRole, svyYEar, ssSChType, tchSEctor
ORDER BY svyYEar, ssSchType, tchRole


If @Confirm =1
	commit transaction
else
	rollback

END
GO

