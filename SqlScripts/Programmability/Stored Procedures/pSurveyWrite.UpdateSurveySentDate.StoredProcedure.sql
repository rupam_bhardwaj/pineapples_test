SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 05 2010
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[UpdateSurveySentDate]

	-- Add the parameters for the stored procedure here
	@SentDate datetime,
	@TargetDate datetime = null,
	@Overwrite int = 0,
	@SurveyControlFilter xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE SchoolYearHistory
		SET saSent = case when @OverWrite = 1 then @SentDate else isnull(saSent, @SentDate) end
			, saReceivedTarget = case when @OverWrite = 1 then @TargetDate else isnull(saReceivedTarget, @TargetDate) end
    WHERE syID in
		(Select syID from pSurveyRead.fnSurveyControlFilterXML(@SurveyControlFilter))
		and syDormant = 0

END
GO

