SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Number of teachers at the school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolDataSetPTR]
	-- Add the parameters for the stored procedure here
	@BaseYear int
	, @SchoolNo nvarchar(50) = null
	, @GovtOther nvarchar(10) = null

	WITH EXECUTE AS 'pineapples'
AS
BEGIN

DECLARE @XData TABLE
(
schNo nvarchar(50)
, XValue float
, Estimate int
, XQuality int
)


DECLARE @YData TABLE
(
schNo nvarchar(50)
, YValue float
, Estimate int
, YQuality int
)


------ set up the XData
-- first appracoh was to nest this, but yu cannot nest INSERT EXEC
-- this is the only place we can hard code the source, so the INSERT EXEC can go from here
-- That's why the function exists
INSERT INTO @XData
SELECT *
FROM pSchoolRead.tfnSchoolDataSetTeacherCount( @BaseYear, @SchoolNo, @GovtOther)

--exec pSchoolRead.SchoolDataSetTeacherCount @BaseYear, @SchoolNo, @GovtOther

INSERT INTO @YData
SELECT *
FROM pSchoolRead.tfnSchoolDataSetEnrolment( @BaseYear, @SchoolNo, null,null)

--exec pSchoolRead.SchoolDataSetEnrolment @BaseYear, @SchoolNo


	Select
	S.schNo
	, case when isnull(XValue,0) = 0 then null else YValue/XValue end
	, case when X.Estimate = 1 or Y.Estimate = 1 then 1 else 0 end
	, case when isnull(X.XQuality,0) > isnull(Y.YQuality,0) then X.XQuality
			else Y.YQuality end

	from Schools S
	LEFT JOIN @XData X
		ON S.schNo = X.schNo
	LEFT JOIN @YData Y
		ON S.schNo = Y.schNo
END
GO

