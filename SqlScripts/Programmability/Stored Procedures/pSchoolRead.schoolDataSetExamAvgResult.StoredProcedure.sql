SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Number of teachers at the school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolDataSetExamAvgResult]
	-- Add the parameters for the stored procedure here
	@BaseYear int
	, @SchoolNo nvarchar(50) = null
	, @examCode nvarchar(20)
	WITH EXECUTE AS 'pineapples'
AS
BEGIN

	Select schNo
	, sum(exsScore) /sum(exsCandidates)
	, 0
	, null
	from ExamScores
		INNER JOIN Exams
			ON ExamScores.exID = Exams.exID
	WHERE (exCode = @ExamCode or @ExamCode is null)
		AND exYear = @BaseYear
		AND (schNo = @SchoolNo or @SchoolNo is null)
	group by schNo


END
GO

