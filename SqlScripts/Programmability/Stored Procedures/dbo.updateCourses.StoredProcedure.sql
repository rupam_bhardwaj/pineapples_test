SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 6 2009
-- Description:	Update courses information from XML
-- =============================================
CREATE PROCEDURE [dbo].[updateCourses]
	-- Add the parameters for the stored procedure here
	@ssID int = 0,
	@courseXML xml

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @courseTable table
(
	[CourseName] [nvarchar](50) NULL,
	[Mode] [nvarchar](10) NULL,
	[Faculty] [nvarchar](50) NULL,
	[Dept] [nvarchar](50) NULL,
	[DurationPerAnnum] [int] NULL,
	[scrsCost] [money] NULL,
	[Accred] [nvarchar](50) NULL,
	[Years] [int] NULL,
	[Places] [int] NULL,
	[Teachers] [int] NULL,
	[ApplicantsM] [int] NULL,
	[ApplicantsF] [int] NULL,
	[IntakeM] [int] NULL,
	[IntakeF] [int] NULL,
	[GraduatesM] [int] NULL,
	[GraduatesF] [int] NULL

	)

	declare @courseYears table
	(
	[CourseName] [nvarchar](50) NULL,
	[Mode] [nvarchar](10) NULL,
	[Faculty] [nvarchar](50) NULL,
	[Dept] [nvarchar](50) NULL,
	yearLevel	int,
	T	int,
	P	int
	)

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @courseXML

-- populate the table of grid values
	INSERT @courseTable
	Select * from OPENXML (@idoc, '/Courses/Course',2)
		WITH (
			[CourseName] [nvarchar](50) '@courseName',
			[Mode] [nvarchar](10) '@mode',
			[Faculty] [nvarchar](50) '@faculty',
			[Dept] [nvarchar](50) '@dept',
			[DurationPerAnnum] [int] '@durationPerAnnum',
			[scrsCost] [money] '@costPerPupil',
			[Accred] [nvarchar](50) '@accreditation',
			[Years] [int] '@years',
			[Places] [int] '@places',
			[Teachers] [int] '@teachers',
			[ApplicantsM] [int] 'Applicants/@M',
			[ApplicantsF] [int] 'Applicants/@F',
			[IntakeM] [int]'Intake/@M',
			[IntakeF] [int] 'Intake/@F',
			[GraduatesM] [int] 'Graduates/@M',
			[GraduatesF] [int] 'Graduates/@F'

		)
	INSERT @courseYears
	Select * from OPENXML (@idoc, '/Courses/Course/Enrolments/Enrolment',2)
		WITH (
			[CourseName] [nvarchar](50) '../../@courseName'
			, [Mode] [nvarchar](10) '../../@mode'
			, [Faculty] [nvarchar](50) '../../@faculty'
			, [Dept] [nvarchar](50) '../../@dept'
			, yearLevel int '@yearLevel'
			, T int '@T'
			, P int '@P'
			)
begin transaction

begin try
	-- delete the exisitng ones
	-- this cascades the delete of surveyyearcourseYears
	Delete
	from SurveyYearCourses
	where ssID = @ssID

	-- add back the main table

	insert into SurveyYearCourses
	(
	[ssID],
	[scrsName] ,
	[scrsMode] ,
	[scrsFaculty] ,
	[scrsDept] ,
	[scrsDurationPerAnnum] ,
	[scrsCost],
	[scrsAccred],
	[scrsYears] ,
	[scrsPlaces],
	[scrsTeachers] ,
	[scrsApplicantsM],
	[scrsApplicantsF] ,
	[scrsIntakeM] ,
	[scrsIntakeF] ,
	[scrsGraduatesM] ,
	[scrsGraduatesF]
	)
	Select
	@ssID,
	[CourseName],
	mode ,
	[Faculty] ,
	[Dept],
	[DurationPerAnnum] ,
	[scrsCost],
	[Accred] ,
	[Years],
	[Places],
	[Teachers] ,
	[ApplicantsM],
	[ApplicantsF] ,
	[IntakeM] ,
	[IntakeF] ,
	[GraduatesM] ,
	[GraduatesF]

	from @courseTable

	Insert INTO SurveyYearCourseYear
	(scrsId, sycyYear, sycyContactT, sycyContactP)
	Select scrsID, yearLevel, T, P
from SurveyYearCourses SYC
	INNER JOIN @courseYears CY on SYC.scrsName = CY.CourseName
	WHERE SYC.ssID = @ssID
	and isnull(SYC.scrsMode,'') = ISNULL(CY.Mode , '')
	and isnull(SYC.scrsFaculty,'') = ISNULL(CY.Faculty , '')
	and isnull(SYC.scrsDept,'') = ISNULL(CY.Dept , '')


end try

begin catch
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction


return


    -- Insert statements for procedure here

END
GO
GRANT EXECUTE ON [dbo].[updateCourses] TO [pSurveyWrite] AS [dbo]
GO

