SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 05 2016
-- Description:	Extended read of teacher for web site
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherReadEx]
	@teacherID int
AS
BEGIN

	SET NOCOUNT ON;

    Select *
 	from TeacherIdentity TI
	WHERE tID = @teacherID

	Select tchsID
	, TS.ssID
	, SS.schNo
	, SS.svyYear
	, S.schName
	, tchRole
	, R.codeDescription roleName
	, tchTAM
	, TS.tchClass
	, TS.tchClassMax
	, TA.estpNo
	, isnull(TA.schNo, E.schNo) apptSchNo
	, apptSchool.schName apptSchName
	, TA.taDate
	, TA.taEndDate
	, case when isnull(TA.schNo, E.schNo) is null then null
		when isnull(TA.schNo, E.schNo) = SS.schNo then 1
		else 0 end apptConfirmed

	from TeacherSurvey TS
		INNER JOIN SchoolSurvey SS
			ON TS.ssID = SS.ssID
		INNER JOIN Survey SVY
			ON SS.svyYear= SVY.svyYear
		INNER JOIN Schools S
			ON SS.schNo = S.schNo
		LEFT JOIN TRTeacherRole R
			ON TS.tchRole = R.codeCode
		LEFT JOIN TeacherAppointment TA
			ON TA.tID = @TeacherID
			AND TA.taDate <= SVY.svyCensusDate
			AND (TA.taEndDate is null or TA.taEndDate > SVY.svyCensusDate)
		LEFT JOIN Establishment E
			ON TA.estpNo = E.estpNo
		LEFT JOIN Schools ApptSchool
			ON isnull(TA.schNo, E.schNo) = ApptSchool.schNo
	WHERE TS.tID = @teacherID
	ORDER BY SS.svyYear DESC

	Select *
	from TeacherAppointment
	WHERE tID = @teacherID

	Select *
	from TeacherTraining
	WHERE tID = @teacherID


	SELECT *
	from pTeacherRead.TeacherLinks
	WHERE tID = @teacherID

	select TOP 105 P.tpsID
	, tpsPeriodStart
	, tpsPeriodEnd
	, tpsTitle
	, tpsShortName
	, tpsGross
	, tpsPayslip
	, tpsPosition
	from TeacherPayslips P
	INNER JOIN TeacherIdentity TI
		ON P.tpsPayroll = TI.tPayroll
	WHERE tID = @TeacherID
	ORDER BY tpsPEriodStart DESC


END
GO

