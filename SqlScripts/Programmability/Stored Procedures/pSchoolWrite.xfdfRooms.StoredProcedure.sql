SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 11 2014
-- Description:	Update classrooms from xfdf format

-- =============================================
CREATE PROCEDURE [pSchoolWrite].[xfdfRooms]
	-- Add the parameters for the stored procedure here
	@SurveyID int,
	@roomXML xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @tag nvarchar(50) = 'Rooms'
-- Validate the arguments
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;


	if @SurveyID is null
		begin
			set @err = 999
			set @ErrorMessage = 'Null SurveyID passed to xfdfRooms'
			set @ErrorSeverity = 16
			set @ErrorState = 0

			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			return @err
		end


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @roomXml, @xmlns

	declare @roomType nvarchar(20) = 'CLASS'				-- for now

	-- this is the set of column tags for the grid
	declare @colSet table(

	c nvarchar(2)
	, ck nvarchar(20)
	, cv nvarchar(100)
							)

	-- this is the set of row tags for the furniture in the grid
	declare @fRowSet table(

		furnituregroup nvarchar(2)
		, r nvarchar(2)
		, rk nvarchar(20)
		, rv nvarchar(100)

	)

	declare @roomtbl table(

		RoomType			nvarchar(10),
		QualityCode			nvarchar(50),
		RoomNumber			smallint,
		RoomTitle			nvarchar(50),
		BuildingReviewID	int,
		ClassLevel			nvarchar(10),
		RoomLength			float,
		RoomWidth			float,
		RoomSize			float,
		YearConstruction	int,
		Materials			nvarchar(2),
		MaterialsRoof		nvarchar(2),
		MaterialsFloor		nvarchar(2),
		Condition			nvarchar(1),
		RoomUse				nvarchar(200),
		SecureDoor			smallint,
		SecureWindows		smallint,
		WheelChair			smallint,
		CapF				int,
		CapM				int,
		ShowersF			int,
		ShowersM			int,
		ToiletsF			int,
		ToiletsM			int,
		Laundry				smallint,
		Kitchen				smallint,

		Beds				int,
		Matresses			int,
		Cupboards			int,

		WoodStove			int,
		GasStove			int,
		ElecStove			int,
		LockedStorage		int,
		ColdStorage			int,
		LunchDay			int,
		LunchBoarder		int,
		Location			nvarchar(50),
		Sector				nvarchar(10),
		RoomGUID			uniqueidentifier
	 )

	declare @furntbl table(
		RoomNumber int,
		RoomType nvarchar(10),
		furnituregroup nvarchar(2),
		FurnType nvarchar(50),
		Num int
	)

	declare @furncondition table(
		RoomNumber int,
		furnituregroup nvarchar(2),
		Condition nvarchar(1)
	)


-- get the complete set of columns; ie classroom numbers in this document
-- note this may be 00-09 or 00-29 or 10-29 ( extra roomd form?)  depending on which form is filled out
	INSERT INTO @colSet
	Select *
	FROM OPENXML(@idoc ,'/x:field/x:field[@name="C"]/x:field',2)
	WITH
	(
		c			nvarchar(2)			'@name'			-- the room number
		, ck			int			'x:field[@name="K"]/x:value'			-- also the room number
		, cv			nvarchar(20)			'x:field[@name="V"]/x:value'
	)

	-- there is no rowset for rooms, but there are for furniture
	--


	INSERT INTO @fRowSet
	Select *
	FROM OPENXML(@idoc ,'/x:field/x:field/x:field[@name="R"]/x:field',2)
	-- this position on the nodes Class.xx.R.nn where xx is the furniture group
	WITH
	(
		furnituregroup		nvarchar(2)			'../../@name'	-- F FT FS
		, r					nvarchar(2)			'@name'		-- 00 01 02
		, rk					nvarchar(20)		'x:field[@name="K"]/x:value'
		, rv					nvarchar(50)		'x:field[@name="V"]/x:value'
		)


	INSERT INTO @furntbl
	(
		RoomNumber
		, RoomType
		, furnituregroup
		, FurnType

		, Num
	)
	Select CC.ck
		, @RoomType
		, X.furnituregroup
		, RR.rk
		, convert(int,X.num)
		-- this is the path to the furniture data
		FROM OPENXML(@idoc ,'/x:field/x:field/x:field[@name="D"]/x:field/x:field',2)
		WITH
		(
			furnituregroup		nvarchar(2)			  '../../../@name'		-- F, FT, FS
			, r					nvarchar(2)				'../@name'
			, c					nvarchar(2)				'@name'
			, num				nvarchar(10)			'x:value'
		) X
		LEFT JOIN @colSet CC
			ON X.c = CC.c
		LEFT JOIN @fRowSet RR
			ON X.furnituregroup = RR.furnituregroup
			AND X.r = RR.r
		WHERE X.r <> 'C'


	-- the condition is accessible only through a differnet path
		INSERT INTO @furncondition
		(
			RoomNumber
			, furnituregroup
			, Condition
		)
			Select CC.ck
			, X.furnituregroup
			,case condition when 'G' then 'A' when 'F' then 'B' when 'P' then 'C' else condition end
			FROM OPENXML(@idoc ,'/x:field/x:field/x:field[@name="D"]/x:field[@name="C"]/x:field',2)
			WITH
			(
				furnituregroup		nvarchar(2) 		    '../../../@name'		-- F, FT, FS
				, c					nvarchar(2)				'@name'
				, condition			nvarchar(1)				'x:value'

			) X
			LEFT JOIN @colSet CC
				ON X.c = CC.c
			WHERE X.condition is not null


	-- next is the room data
	INSERT INTO @roomtbl
	(
		RoomType
		, QualityCode
		, RoomNumber
		--RoomTitle
		--BuildingReviewID
		, ClassLevel
		--RoomLength
		--RoomWidth
		,RoomSize
		,YearConstruction
		, Materials
		, MaterialsRoof
		, MaterialsFloor
		, Condition
		, RoomUse

		, SecureDoor
		, SecureWindows
		, WheelChair
		-- dormitory
		, CapF
		, CapM
		, ShowersF
		, ShowersM
		, ToiletsF
		, ToiletsM
		, Laundry
		--Kitchen

		--Beds
		--Matresses
		--Cupboards

		, WoodStove
		, GasStove
		, ElecStove
		, LockedStorage
		, ColdStorage
		, LunchDay
		, LunchBoarder
		--Location
		--Sector
		--RoomGUID
	)
		Select
		@roomType
		, @RoomType		-- for the quality code, probably not always what we want
		, CC.ck			-- not in the data room number is 1 - based, all cols on form are 0 based
						-- but ck is assumed to be 1-based e.g. C.00.K = 1
		, classlevel
		, size
		, yearBuilt
		, nullif(material,'00') -- handle (select) in the dropdown returning 00
		, nullif(materialRoof,'00')
		, nullif(materialFloor,'00')
		, case condition when 'G' then 'A' when 'F' then 'B' when 'P' then 'C' else condition end
		, roomuse

		, case SecureDoor when 'Y' then 1 when 'N' then 0 else null end
		, case SecureWindow when 'Y' then 1 when 'N' then 0 else null end
		, case wheelChair when 'Y' then 1 when 'N' then 0 else null end

		-- dormitory
		, capM
		, capF
		, toiletsM
		, toiletsF
		, showersM
		, showersF
		, case laundry when 'Y' then 1 when 'N' then 0 else null end
		-- for future use ....
		, woodStove
		, gasStove
		, elecStove
		, lockedStorage
		, coldStorage
		, lunchDay
		, lunchBoarder

		FROM OPENXML(@idoc ,'/x:field/x:field[@name="D"]/x:field',2)
		-- this positions on the column
		WITH (
				c				nvarchar(2)				'@name'
				, classlevel	nvarchar(10)			'x:field[@name="Level"]/x:value'
				, size			Decimal(8,2)			'x:field[@name="Size"]/x:value'
				, yearBuilt		int						'x:field[@name="Year"]/x:value'
				, material		nvarchar(2)				'x:field[@name="Mat"]/x:value'
				, materialRoof	nvarchar(2)				'x:field[@name="MatRoof"]/x:value'
				, materialFloor nvarchar(2)				'x:field[@name="MatFloor"]/x:value'
				, condition		nvarchar(1)				'x:field[@name="C"]/x:value'
				, roomuse		nvarchar(20)			'x:field[@name="Use"]/x:value'

				, secureDoor	nvarchar(1)				'x:field[@name="Door"]/x:value'
				, secureWindow	nvarchar(1)				'x:field[@name="Window"]/x:value'
				, wheelChair	nvarchar(1)				'x:field[@name="WChair"]/x:value'

				-- dormitories
				, capM			int						'x:field[@name="Capacity"]/x:field[@name="M"]/x:value'
				, capF			int						'x:field[@name="Capacity"]/x:field[@name="F"]/x:value'
				, toiletsM		int						'x:field[@name="Toilets"]/x:field[@name="M"]/x:value'
				, toiletsF		int						'x:field[@name="Toilets"]/x:field[@name="F"]/x:value'
				, showersM		int						'x:field[@name="Showers"]/x:field[@name="M"]/x:value'
				, showersF		int						'x:field[@name="Showers"]/x:field[@name="F"]/x:value'

				, laundry		nvarchar(1)				'x:field[@name="Laundry"]/x:value'


				-- for future use ....
				, woodStove		int						'@woodStove'
				, gasStove		int						'@gasStove'
				, elecStove		int						'@elecStove'
				, lockedStorage int						'@lockedStorage'
				, coldStorage	nvarchar(1)				'@coldStorage'
				, lunchDay		int						'@lunchDay'
				, lunchBoarder	int						'@lunchBoarder'
				--Location nvarchar(50) '../@location',		-- location is on the room tag
				--Sector nvarchar(10) '../@sector',		-- forced school type IS NOW CALLED SECTOR
				--RoomGUID uniqueidentifier '@roomGUID'
			)	X
			LEFT JOIN @colSet CC
				ON X.c = CC.c
			-- remove unwanted records at this point
			WHERE size is not null
			or yearBuilt is not null
			or nullif(material,'00') is not null
			or nullif(materialRoof,'00') is not null
			or nullif(materialFloor,'00') is not null
			or condition is not null
			or roomuse is not null
			or securedoor is not null
			or secureWindow is not null
			or Wheelchair is not null

			or WoodStove is not null
			or gasStove is not null
			or ElecStove is not null
			or LockedStorage is not null
			or coldStorage is not null
			or lunchDay is not null
			or lunchBoarder is not null

-- this is the path to the furniture data
-- select * from @fRowSet


		declare @typestbl table(
								RoomType nvarchar(10)
								)
		declare @deletedRooms table(
								rmID int
								)

		declare @insertedRooms table(
								RoomType nvarchar(10),
								RoomNumber int
								)

-- the room types encompassed in this load
-- for now, this is just @roomType
	------INSERT @typestbl
	------SELECT * from OPENXML (@idoc, '/Rooms/RoomTypes/RoomType',2)
	------	WITH (
	------			RoomType nvarchar(10) '.'
	------		)
INSERT @typestbl
SELECT @roomtype

    -- Insert statements for procedure here
--	SELECT * from @typestbl
--	SELECT * from @roomtbl
--	sELECT * FROM @fURNTBL

declare @Location nvarchar(50)
declare @Sector nvarchar(10)

--------Select @Location = Location,
--------		@Sector = Sector
--------	from OPENXML (@idoc, '/Rooms',2)
--------		WITH (
--------				Location nvarchar(50) '@location'
--------				, Sector nvarchar(10) '@sector'
--------			  )

-- deal separately with inserts , updates, and deletes

-- deletes first - these room type/room no combination are no longer there
-- select @Location, @SchoolType
-- the rooms that aren;t there any more
-- have to match the Location and SchoolType


INSERT @deletedRooms
Select rmID from Rooms
left join @roomtbl TMP
on Rooms.rmType = TMP.roomType
and Rooms.RmNo = TMP.roomNumber
WHERE Rooms.ssID = @SurveyID
and Rooms.rmType = any (Select roomType from @typestbl)
and Rooms.rmNo = any(select ck from @colSet)			-- restrict to the room numbers on thsi page
and (Rooms.rmLocation = @Location or @Location is null)
and (Rooms.rmSchLevel = @Sector or @Sector is null)
and TMP.roomNumber is null


--	Select * from @deletedrooms


INSERT @insertedRooms
Select roomType, roomNumber from @roomtbl TMP
left join (Select rmType, rmNo from Rooms WHERE
			ssID = @SurveyID
			and (Rooms.rmLocation = @Location or @Location is null)
			and (Rooms.rmSchLevel = @Sector or @Sector is null)
			) R
on TMP.roomType = R.rmType
	and TMP.roomNumber = R.rmNo
where R.rmNo is null


/*
Select * from @deletedRooms
Select * from @insertedRooms
*/


--- transaction can start here

begin transaction

begin try

-- now delete ALL the furniture
	DELETE from Furniture
	from Furniture F
	INNER JOIN Rooms  R on F.rmID = R.rmID
	WHERE R.ssID = @SurveyID
	and R.rmType = any (Select roomType from @typestbl)
	and (R.rmLocation = @Location or @Location is null)
	and (R.rmSchLevel = @Sector or @Sector is null)

-- unreference the deleted rooms on Classes
	UPDATE Classes
		SET rmID = null
	from Classes C
		WHERE C.ssID = @surveyID			-- this is kind of a hint really
		and rmID = any (Select rmID from @deletedRooms)


-- delete the DELETED rooms
	DELETE from Rooms
	from Rooms R
	WHERE R.rmID = any (Select rmID from @deletedRooms)


-- update the UPDATED Rooms - we preserve the rmID this way

	UPDATE Rooms
		SET rmQualityCode = TMP.QualityCode
		, rmTitle = TMP.RoomTitle
		, brevID = TMP.BuildingReviewID
		, rmLength = TMP.RoomLength
		, rmWidth = TMP.RoomWidth
		, rmSize = TMP.roomSize
		, rmYear = TMP.YearConstruction
		, rmMaterials = TMP.Materials
		, rmMAterialRoof = TMP.MaterialsRoof
		, rmMaterialFloor = TMP.MaterialsFloor
		, rmCondition =	TMP.Condition

		, rmUse = TMP.RoomUse
		, rmSecureDoor = TMP.SecureDoor
		, rmSecureWindows = TMP.SecureWindows
		, rmCapF =			TMP.CapF
		, rmCapM = TMP.CapM
		, rmShowersF = TMP.ShowersF
		, rmShowersM = TMP.ShowersM
		, rmToiletsF = TMP.ToiletsF
		, rmToiletsM = TMP.ToiletsM
		, rmLaundry = TMP.Laundry
		, rmKitchen = TMP.Kitchen

		, rmBeds = TMP.Beds
		, rmMatresses = TMP.Matresses
		, rmCupboards = TMP.Cupboards

		, rmStoveElec = TMP.ElecStove
		, rmStoveGas = TMP.GasStove
		, rmStoveWood = TMP.WoodStove
		, rmLockedStore = TMP.LockedStorage
		, rmRefrig = TMP.ColdStorage
		, rmLunchDay = TMP.LunchDay
		, rmLunchBoarder = TMP.LunchBoarder
		, rmLocation = TMP.Location
		, rmSchLevel = TMP.Sector
		, rmLevel = TMP.ClassLevel
		, rmGUID = TMP.roomGUID


	From Rooms R INNER JOIN @roomtbl TMP
			on R.rmType = TMP.roomType
			and R.RmNo = TMP.roomNumber
			WHERE R.ssID = @SurveyID
			and R.rmType = any (Select roomType from @typestbl)
			and (R.rmLocation = @Location or @Location is null)
			and (R.rmSchLevel = @Sector or @Sector is null)


	exec audit.xfdfInsert @SurveyID, 'Rooms updated',@tag,@@rowcount

-- add the rooms back

	INSERT INTO Rooms(ssID, rmType, rmQualityCode,
			rmNo, rmTitle, brevID, rmSchLevel,
			rmLength, rmWidth,
			rmSize, rmYear,
			rmMaterials, rmMaterialRoof, rmMaterialFloor,
			rmCondition, rmUse,
			rmSecureDoor, rmSecureWindows,
			rmCapF, rmCapM,
			rmShowersF, rmShowersM,
			rmToiletsF, rmToiletsM,
			rmLaundry,
			rmKitchen,
			rmBeds, rmMatresses, rmCupboards,

			rmStoveElec, rmStoveGas, rmStoveWood,
			rmLockedStore, rmRefrig,
			rmLunchDay, rmLunchBoarder
			, rmLocation
			, rmLevel

)
	Select @surveyID, TMP.RoomType, TMP.QualityCode,
			TMP.RoomNumber, TMP.RoomTitle, TMP.BuildingReviewID, TMP.Sector,
			TMP.RoomLength, TMP.RoomWidth,
			TMP.RoomSize, TMP.YearConstruction,
			TMP.Materials,TMP.MaterialsRoof,TMP.MaterialsFloor,
			TMP.Condition, TMP.RoomUse,
			TMP.SecureDoor,TMP.SecureWindows,
			TMP.CapF, TMP.CapM,
			TMP.ShowersF, TMP.ShowersM,
			TMP.ToiletsF, TMP.ToiletsM,
			TMP.Laundry,
			TMP.Kitchen,
			TMP.Beds, TMP.Matresses, TMP.Cupboards,
			TMP.ElecStove, TMP.GasStove, TMP.WoodStove,
			TMP.LockedStorage, TMP.ColdStorage,
			TMP.LunchDay, TMP.LunchBoarder
			, TMP.Location
			, TMP.ClassLevel
	From @roomtbl TMP
	inner join @insertedRooms I
		on TMP.roomType = I.roomType
		and tmp.roomNumber = I.roomNumber

	exec audit.xfdfInsert @SurveyID, 'Rooms inserted',@tag,@@rowcount
-- add ALL the furniture back

	INSERT INTO Furniture(ssID,fCode,fLevel,fNum,rmID, fCond)
	Select @SurveyID, F.FurnType, null, F.Num,
			R.rmID, FC.Condition
	From @furntbl F INNER JOIN Rooms R
		on F.RoomNumber = R.rmNo
		AND F.RoomType = R.rmType
	LEFT JOIN @furncondition FC
		ON F.RoomNumber = FC.RoomNumber
		AND F.furnituregroup = FC.furnituregroup
	WHERE R.ssID = @SurveyID
			and (R.rmLocation = @Location or @Location is null)
			and (R.rmSchLevel = @Sector or @Sector is null)

			AND ( F.num is not null OR FC.condition is not null)

	exec audit.xfdfInsert @SurveyID, 'Furniture inserted',@tag,@@rowcount


/*
	 select * from Rooms WHERE ssID = @SurveyID
	 select * from furniture WHERE ssID = @SurveyID
*/

end try

begin catch

	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage, @tag
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

return

END
GO

