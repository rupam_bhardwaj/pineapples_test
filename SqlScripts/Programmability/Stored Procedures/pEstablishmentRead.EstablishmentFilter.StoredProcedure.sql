SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-18
-- Description:	Search establishment position records
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[EstablishmentFilter]
	-- Add the parameters for the stored procedure here
	@SummaryOnly int	-- 0 = return full data 1= summary only 2 = estpNo, tID and summary
	, @PositionNo nvarchar(20) = null
	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null
	, @Active int = 1
	, @Role nvarchar(10) = null
	, @RoleGrade nvarchar(10) = null
	, @ActiveDateMin datetime = null
	, @ActiveDateMax datetime = null
	, @ClosedDateMin datetime = null
	, @ClosedDateMax datetime = null
	, @AsAtDate datetime = null
	, @TeacherID int = null


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@AsAtDate is null)
		Select @AsAtDate = common.dropTime(getdate())

	declare @PayPeriodStart datetime
	Select @PayPeriodStart = max(tpsPeriodStart)
	from TeacherPayslips
	WHERE tpsPeriodStart <= @AsAtDate
    -- Insert statements for procedure here

Select
	E.*
	, RG.roleCode
	, RG.rgSalaryLevelMin
	, RG.rgSalaryLevelMax
	, Schools.schAuth
	, Schools.SchName
	, Schools.schType
	, TI.tNAmePrefix
	, TI.tGiven
	, TI.tMiddleNames
	, TI.tSurname
	, TI.tNameSuffix
	, TI.tShortName
	, TI.tFullName
	, TI.tLongName
	, TI.tPayroll
	, TI.tDOB
	, TI.tSex
	, slips.tpsGross
	, slips.tpsPaypoint
	, slips.tpsSalaryPoint
INTO #tmp
from
	(
		Select E.estpNo
		, E.estpScope
		, case estpScope
				when 'A' then TA.schNo else E.schNo end schNo
		, case estpScope
				when 'A' then TA.taRoleGrade else E.estpRoleGrade end estpRoleGrade
		, estpAuth
		, estpActiveDate
		, estpClosedDate
		, estpFlag
		, estpTitle
		, estpSubject
		, taDate
		, taEndDate
		, taID
		, tID
		, ta.spCode

		from Establishment E
			LEFT JOIN
				(Select *
					FROM TeacherAppointment
				) TA
				ON E.estpNo = TA.estpNo
				AND TA.taDate <= @AsAtDate
				AND (TA.taendDate >= @AsATDate or TA.taEndDate is null)
		WHERE
			(E.estpNo like @PositionNo or @PositionNo is null)
			AND (tID = @TeacherID  OR @TeacherId is null)

			AND
			( estpActiveDate <=@AsAtDate)
			AND (estpClosedDate >= @AsAtDate or estpClosedDate is null)
	) E
	LEFT JOIN	-- becuase Authority scope has no school
		-- does it make a slight optimisation pushing these criteria to the top,
		-- instead of at the end?
		(Select Schools.schNo, schName, schType, schAuth
				, max(svyYear) lastTeacherInfo
			from Schools
				LEFT JOIN
					(Select schNo, svyYear
							from SchoolSurvey
								INNER JOIN TeacherSurvey
									ON SchoolSurvey.ssID = TeacherSurvey.ssID
					) u1
					ON Schools.schNo = u1.schNo
			GROUP BY Schools.schNo, schName, schType, schAuth

		) Schools
		ON E.schNo = Schools.schNo	-- so we are picking up the correct school for the supernumerary
	LEFT JOIN RoleGrades RG
		ON RG.rgCode = E.estpRoleGrade
	LEFT JOIN TEacherIdentity TI
		ON TI.tID = E.tID
	LEFT JOIN
			( Select tpsPayroll, tpsGRoss, tpsPaypoint, tpsSalaryPoint, tpsPeriodStart
				from TeacherPaySlips
			) Slips
			ON slips.tpsPeriodStart = @PayPeriodStart
		   AND Slips.tpsPayroll = TI.tPayroll

WHERE
		(E.schNo = @schoolNo or @schoolNo is null)
		AND (E.estpRoleGrade = @RoleGrade OR @roleGrade is null)

		AND (
			(@Authority is null)
			or (SchAuth = @Authority and estpScope is null)
			or (estpAuth = @Authority and estpScope = 'A')
			)
		AND (schType = @SchoolType or @SchoolType is null)

		AND (RG.roleCode = @Role or @Role is null)

if (@SummaryOnly = 0)
	Select *
	from #tmp

if (@SummaryOnly = 2)
	Select estpNo
		, tID
	from #tmp


Select count(*) TotalPositions
	, sum(case when tID is null then 0 else 1 end) TotalAppointments
from #tmp

drop table #tmp

END
GO

