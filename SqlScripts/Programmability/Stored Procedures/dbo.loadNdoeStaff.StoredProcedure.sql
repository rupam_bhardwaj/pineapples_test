SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of teacher data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeStaff]
@xml xml
, @filereference uniqueidentifier
, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- totals by education level

declare @ndoeStaff TABLE (
	rowIndex int
    , SchoolYear                 nvarchar(100) NULL
    , State                      nvarchar(100) NULL
    , School_Name                nvarchar(100) NULL
    , School_No                  nvarchar(100) NULL
    , School_Type                nvarchar(100) NULL
    , Office                     nvarchar(100) NULL
    , First_Name                 nvarchar(100) NULL
    , Middle_Name                nvarchar(100) NULL
    , Last_Name                  nvarchar(100) NULL
    , Full_Name                  nvarchar(100) NULL
    , Gender                     nvarchar(100) NULL
    , inDate_of_Birth            nvarchar(100) NULL
	, Age                        nvarchar(100) NULL
    , Citizenship                nvarchar(100) NULL
    , Ethnicity                  nvarchar(100) NULL
    , FSM_SSN                    nvarchar(100) NULL
    , OTHER_SSN                  nvarchar(100) NULL
    , Highest_Qualification      nvarchar(100) NULL
    , Field_of_Study                           nvarchar(100) NULL
    , Year_of_Completion                       nvarchar(100) NULL
    , Highest_Ed_Qualification                 nvarchar(100) NULL
    , Year_Of_Completion2                      nvarchar(100) NULL
    , Employment_Status                        nvarchar(100) NULL
    , Reason                     nvarchar(100) NULL
    , Job_Title                  nvarchar(100) NULL
    , Organization               nvarchar(100) NULL
    , Staff_Type                 nvarchar(100) NULL
    , Teacher_Type               nvarchar(100) NULL
    , inDate_of_Hire             nvarchar(100) NULL
    , inDate_Of_Exit             nvarchar(100) NULL
    , Annual_Salary              nvarchar(100) NULL
    , Funding_Source             nvarchar(100) NULL
    , ECE                        nvarchar(1) NULL
    , Grade_1                    nvarchar(1) NULL
    , Grade_2                    nvarchar(1) NULL
    , Grade_3                    nvarchar(1) NULL
    , Grade_4                    nvarchar(1) NULL
    , Grade_5                    nvarchar(1) NULL
    , Grade_6                    nvarchar(1) NULL
    , Grade_7                    nvarchar(1) NULL
    , Grade_8                    nvarchar(1) NULL
    , Grade_9                    nvarchar(1) NULL
    , Grade_10                   nvarchar(1) NULL
    , Grade_11                   nvarchar(1) NULL
    , Grade_12                   nvarchar(1) NULL
    , Admin                      nvarchar(1) NULL
    , Other                      nvarchar(1) NULL
    , Total_Days_Absence         int NULL
    , Maths                      int NULL
    , Science                    int NULL
    , Language                   int NULL
    , Competency                 int NULL
    , fileReference				 uniqueidentifier
	--
	, ssID						 int
	, tID						 int
	--
    , Date_of_Birth              date NULL
    , Date_of_Hire               date NULL
    , Date_Of_Exit               date NULL
	, minYearTaught			     int
	, maxYearTaught				 int
)


-- use try / catch
begin try

declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),3,4) as int) + 1
From @xml.nodes('ListObject') as V(v)

Select @districtID = dID
from Districts WHERE dName = @districtName

print @SurveyYear
print @DistrictID


INSERT INTO @ndoeStaff
(
rowIndex
, SchoolYear
, State
, School_Name
, School_No
, School_Type
, Office
, First_Name
, Middle_Name
, Last_Name
, Full_Name
, Gender
, inDate_of_Birth
, Age
, Citizenship
, Ethnicity
, FSM_SSN
, OTHER_SSN
, Highest_Qualification
, Field_of_Study
, Year_of_Completion
, Highest_Ed_Qualification
, Year_Of_Completion2
, Employment_Status
, Reason
, Job_Title
, Organization
, Staff_Type
, Teacher_Type
, inDate_of_Hire
, inDate_Of_Exit
, Annual_Salary
, Funding_Source
, ECE
, Grade_1
, Grade_2
, Grade_3
, Grade_4
, Grade_5
, Grade_6
, Grade_7
, Grade_8
, Grade_9
, Grade_10
, Grade_11
, Grade_12
, Admin
, Other
, Total_Days_Absence
, Maths
, Science
, Language
, Competency
, filereference
)
SELECT
nullif(ltrim(v.value('@Index', 'int')),'')                           [Index]

, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                           [SchoolYear]
, nullif(ltrim(v.value('@State', 'nvarchar(100)')),'')                              [State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                        [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                          [School_No]
, nullif(ltrim(v.value('@School_Type', 'nvarchar(100)')),'')                        [School_Type]
, nullif(ltrim(v.value('@Office', 'nvarchar(100)')),'')                             [Office]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                         [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                        [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                          [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                          [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')                             [Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')                      [Date_of_Birth]
, nullif(ltrim(v.value('@Age', 'nvarchar(100)')),'')                                [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                        [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                          [Ethnicity]
, nullif(ltrim(v.value('@FSM_SSN', 'nvarchar(100)')),'')                            [FSM_SSN]
, nullif(ltrim(v.value('@OTHER_SSN', 'nvarchar(100)')),'')                          [OTHER_SSN]
, nullif(ltrim(v.value('@Highest_Qualification', 'nvarchar(100)')),'')              [Highest_Qualification]
, nullif(ltrim(v.value('@Field_of_Study', 'nvarchar(100)')),'')                     [Field_of_Study]
, nullif(ltrim(v.value('@Year_of_Completion', 'nvarchar(100)')),'')                 [Year_of_Completion]
, nullif(ltrim(v.value('@Highest_Ed_Qualification', 'nvarchar(100)')),'')           [Highest_Ed_Qualification]
, nullif(ltrim(v.value('@Year_Of_Completion2', 'nvarchar(100)')),'')                [Year_Of_Completion2]
, nullif(ltrim(v.value('@Employment_Status', 'nvarchar(100)')),'')                  [Employment_Status]
, nullif(ltrim(v.value('@Reason', 'nvarchar(100)')),'')                             [Reason]
, nullif(ltrim(v.value('@Job_Title', 'nvarchar(100)')),'')                          [Job_Title]
, nullif(ltrim(v.value('@Organization', 'nvarchar(100)')),'')                       [Organization]
, nullif(ltrim(v.value('@Staff_Type', 'nvarchar(100)')),'')                         [Staff_Type]
, nullif(ltrim(v.value('@Teacher_Type', 'nvarchar(100)')),'')                       [Teacher_Type]
, nullif(ltrim(v.value('@Date_of_Hire', 'nvarchar(100)')),'')                       [Date_of_Hire]
, nullif(ltrim(v.value('@Date_of_Exit', 'nvarchar(100)')),'')                       [Date_Of_Exit]
, nullif(ltrim(v.value('@Annual_Salary', 'nvarchar(100)')),'')                      [Annual_Salary]
, nullif(ltrim(v.value('@Funding_Source', 'nvarchar(100)')),'')                     [Funding_Source]
, nullif(ltrim(v.value('@ECE', 'nvarchar(100)')),'')                                [ECE]
, nullif(ltrim(v.value('@Grade_1', 'nvarchar(1)')),'')                                          [Grade_1]
, nullif(ltrim(v.value('@Grade_2', 'nvarchar(1)')),'')                                          [Grade_2]
, nullif(ltrim(v.value('@Grade_3', 'nvarchar(1)')),'')                                          [Grade_3]
, nullif(ltrim(v.value('@Grade_4', 'nvarchar(1)')),'')                                          [Grade_4]
, nullif(ltrim(v.value('@Grade_5', 'nvarchar(1)')),'')                                          [Grade_5]
, nullif(ltrim(v.value('@Grade_6', 'nvarchar(1)')),'')                                          [Grade_6]
, nullif(ltrim(v.value('@Grade_7', 'nvarchar(1)')),'')                                          [Grade_7]
, nullif(ltrim(v.value('@Grade_8', 'nvarchar(1)')),'')                                          [Grade_8]
, nullif(ltrim(v.value('@Grade_9', 'nvarchar(1)')),'')                                          [Grade_9]
, nullif(ltrim(v.value('@Grade_10', 'nvarchar(1)')),'')                                         [Grade_10]
, nullif(ltrim(v.value('@Grade_11', 'nvarchar(1)')),'')                                         [Grade_11]
, nullif(ltrim(v.value('@Grade_12', 'nvarchar(1)')),'')                                         [Grade_12]
, nullif(ltrim(v.value('@Admin', 'nvarchar(1)')),'')                              [Admin]
, nullif(ltrim(v.value('@Other', 'nvarchar(1)')),'')                              [Other]
, nullif(ltrim(v.value('@Total_Days_Absence', 'int')),'')                               [Total_Days_Absence]
, nullif(ltrim(v.value('@Maths', 'int')),'')                              [Maths]
, nullif(ltrim(v.value('@Science', 'int')),'')                            [Science]
, nullif(ltrim(v.value('@Language', 'int')),'')                                         [Language]
, nullif(ltrim(v.value('@Competency', 'int')),'')                                       [Competency]
, @filereference
from @xml.nodes('ListObject/row') as V(v)

----- DEal with the DOC staff up front
----- Delete them!
----- to do - further discussion about handling these

-- find a better way to do this...
-- the teacher location on this page is a 'pay point', more general than a school


DELETE
FROM @ndoeStaff
WHERE School_No like '%%%DOE'


-- clean up the names, defining the midle name if the name includes the surname

-- clean up the dob


UPDATE @ndoeStaff
SET Date_Of_Birth = convert(date, inDate_Of_Birth)
WHERE ISNUMERIC(inDate_Of_Birth) = 0

UPDATE @ndoeStaff
SET Date_Of_Birth = cast( convert(int, inDate_Of_Birth) - 2 as datetime)
WHERE ISNUMERIC(inDate_Of_Birth) = 1


UPDATE @ndoeStaff
SET Date_Of_Hire = convert(date, inDate_Of_Hire)
WHERE ISNUMERIC(inDate_Of_Hire) = 0

UPDATE @ndoeStaff
SET Date_Of_Hire = cast( convert(int, inDate_Of_Hire) - 2 as datetime)
WHERE ISNUMERIC(inDate_Of_Hire) = 1


UPDATE @ndoeStaff
SET Date_Of_Exit = convert(date, inDate_Of_Exit)
WHERE ISNUMERIC(inDate_Of_Exit) = 0

UPDATE @ndoeStaff
SET Date_Of_Exit = cast( convert(int, inDate_Of_Exit) - 2 as datetime)
WHERE ISNUMERIC(inDate_Of_Exit) = 1

UPDATE @ndoeStaff
SET Gender = case Gender when 'Male' then 'M' when 'Female' then 'F' else Gender end

Select Date_of_Birth, inDate_of_Birth
from @ndoeStaff
-- apply the ssId to @ndeoStaff

UPDATE @ndoeStaff
SET ssID = SS.ssID

from @ndoeStaff
	LEFT JOIN SchoolSurvey SS
		ON [@ndoeStaff].School_No = SS.schNo
		AND SS.svyYear = @SurveyYear

-- get the minimum and maximum levels taught
UPDATE @ndoeStaff
 set minYearTaught =
	case
	 when ECE = 'X' then 0
	 when Grade_1 = 'X' then 1
	 when Grade_2 = 'X' then 2
	 when Grade_3 = 'X' then 3
	 when Grade_4 = 'X' then 4
	 when Grade_5 = 'X' then 5
	 when Grade_6 = 'X' then 6
	 when Grade_7 = 'X' then 7
	 when Grade_8 = 'X' then 8
	 when Grade_9 = 'X' then 9
	 when Grade_10 = 'X' then 10
	 when Grade_11 = 'X' then 11
	 when Grade_12 = 'X' then 12
	 else null
	end,
	maxYearTaught =
	case
	 when Grade_12 = 'X' then 12
	 when Grade_11 = 'X' then 11
	 when Grade_10 = 'X' then 10
	 when Grade_9 = 'X' then 9
	 when Grade_8 = 'X' then 8
	 when Grade_7 = 'X' then 7
	 when Grade_6 = 'X' then 6
	 when Grade_5 = 'X' then 5
	 when Grade_4 = 'X' then 4
	 when Grade_3 = 'X' then 3
	 when Grade_2 = 'X' then 2
	 when Grade_1 = 'X' then 1
	 when ECE = 'X' then 0
	 else null
	end
-- add the teachers into teacher identity
-- a match is:

UPDATE @ndoeStaff
Set tID = TI.tID
FROM @ndoeStaff
, TeacherIdentity TI

WHERE
-- a match is:
-- FSM_SSN = tPayroll + surname + dob
-- surname + given name + dob + Gender fsm_ssn not known
(
	FSM_SSN = TI.tPayroll AND
	Last_Name = TI.tSurname AND
	Date_of_Birth = TI.tDOB
)
OR
( TI.tPayroll is null AND
	Last_Name = TI.tSurname AND
	First_Name = TI.tGiven AND
	Date_of_Birth = TI.tDOB AND
	Gender = tSex
)

-- second pass at tagging tIDs - pick up match on surname, given and appointment
-- at school - tId not otherwise assigned

UPDATE @ndoeStaff
Set tID = TI.tID
FROM @ndoeStaff
, TeacherIdentity TI
WHERE
	[@ndoeStaff].tID is null AND
	(
		( Last_Name = TI.tSurname AND
			First_Name = TI.tGiven) OR
		( Last_Name = TI.tSurname AND
			Date_of_Birth = TI.tDOB) OR
		( First_Name = TI.tGiven AND
		Date_of_Birth = TI.tDOB)
	)
	and TI.tID in
	(
		-- probably should specify a time period for this appointment,
		-- but for the prupose of matching, should be ok
		select tID from pTeacherRead.TeacherListByAppointment
		WHERE schNo = [@ndoeStaff].School_No
	)

-- we have tagged teachers with tID - we don;t want duplicates
Select tID
from @ndoeStaff
GROUP BY tID
HAVING count(*) > 1

if (@@ROWCOUNT > 0 )
		return

-- update an aggregate total for teachers at the school - we may not get further than this....
UPDATE SchoolSurvey
SET ssNumTeachers = NumTeachers
FROM SchoolSurvey
INNER JOIN (
	Select ssID
	, count(*) NumTeachers
	FROM @NdoeStaff
	GROUP BY ssID) S
	ON SchoolSurvey.ssID = S.ssID


Select 'Tagged', @@ROWCOUNT
Select rowIndex, tID from @ndoeStaff
-- for those teachers identified, update their TeacherIdentity record

UPDATE TeacherIdentity
SET tGiven = First_Name
, tSurname = Last_Name
, tDoB = Date_of_Birth
, tDatePSAppointed = Date_of_Hire
, tDatePSClosed = Date_of_Exit

, tSrc = @filereference
, tSrcID = rowIndex
FROM TeacherIdentity
INNER JOIN @ndoeStaff
ON TeacherIdentity.tId = [@ndoeStaff].tID

SELECT
	tID
	, FSM_SSN
	, First_Name
	, Middle_Name
	, Last_Name
	, Date_of_Birth
	, Gender
	, Date_of_Hire
	, Date_Of_Exit
	, @filereference
	, rowIndex

FROM @ndoeStaff
WHERE tID is null

Select @@ROWCOUNT

-- CREATE the missing Teachers
INSERT INTO TeacherIdentity
(
	tPayroll
	, tGiven
	, tMiddleNames
	, tSurname
	, tDoB
	, tSex
	, tDatePSAppointed
	, tDatePSClosed
	, tSrc
	, tSrcID
)
SELECT
	ltrim(FSM_SSN)
	, First_Name
	, Middle_Name
	, Last_Name
	, Date_of_Birth
	, Gender
	, Date_of_Hire
	, Date_Of_Exit
	, @filereference
	, rowIndex

FROM @ndoeStaff
WHERE tID is null


Select @@ROWCOUNT
-- get back these tIDs
-- filereference and rowIndex define uniquely

Select tID, tSrcID from TEacherIdentity
WHERE tSRc is not null


UPDATE @ndoeStaff
Set tID = TI.tID
FROM @ndoeStaff N
, TeacherIdentity TI
WHERE N.rowIndex = TI.tSrcID
AND TI.tSrc = @filereference

Select rowIndex, tID from @ndoeStaff
-- write the teacher survey record
-- by DELETing and reinserting

-- Delete everything for all affected teachers??

DELETE from TeacherSurvey
From TeacherSurvey
	INNER JOIN @ndoeStaff N
		on N.tId = TeacherSurvey.tID
	INNER JOIN SchoolSurvey SS
		ON TeacherSurvey.ssID = SS.ssID
WHERE SS.svyYEar = @SurveyYear


-- REINSERT
INSERT INTO TeacherSurvey
(
tID
, ssID
, tchFirstName
, tchMiddleNames
, tchFamilyName
, tchDOB
, tchGender
, tcheSource

)
SELECT
tID
, ssID
, First_Name
, Middle_Name
, Last_Name
, Date_of_Birth
, Gender
, @filereference
FROM @ndoeStaff

end try

begin catch
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

