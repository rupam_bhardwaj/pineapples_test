SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 11 2009
-- Description:	Create a new pay scale
-- =============================================
CREATE PROCEDURE [pEstablishmentOps].[CreatePayRateTable]
	-- Add the parameters for the stored procedure here
	@EffectiveDate datetime
	, @CopyFromDate datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO SalaryPointRates
		(spCode
		, spEffective
		, spSalaryFN
		, spHAllowFN
		, spSalary
		, spHAllow)
	SELECT
		SP.spCode
		, @EffectiveDate
		, R.spSalaryFN
		, R.spHAllowFN
		, R.spSalary
		, R.spHAllow

	FROM
		lkpSalaryPoints SP
		LEFT JOIN (Select * from SalaryPointRates WHERE spEffective = @CopyFromDate) R
			ON SP.spCode = R.spCode
END
GO

