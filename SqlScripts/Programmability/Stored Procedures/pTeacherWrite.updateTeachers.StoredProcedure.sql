SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 12 2007
-- Description:	update teachers from smis xml format
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[updateTeachers]
	-- Add the parameters for the stored procedure here
	@SurveyID int,
	@xml xml,
	@BatchID nvarchar(50) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
----	SET NOCOUNT ON;

	declare @Teachers table (
	TeacherID int IDENTITY,
	[tchUnion] [nvarchar](50) NULL,
	[tchFirstName] [nvarchar](50) NULL,
	[tchFamilyName] [nvarchar](50) NULL,
	[tchDOB] [datetime] NULL,
	[tchGender] [nvarchar](1) NULL,
	[tchCivilStatus] [nvarchar](50) NULL,
	[tchNumDep] [int] NULL DEFAULT ((0)),
	[tchCitizenship] [nvarchar](50) NULL,
	[tchLangMajor] [nvarchar](5) NULL,
	[tchLangMinor] [nvarchar](5) NULL,
	[tchIsland] [nvarchar](2) NULL,
	[tchDistrict] [nvarchar](5) NULL,
-- spouse
	[tchSpousetID] [int] NULL,
	[tchSpouseFirstName] [nvarchar](50) NULL,
	[tchSpouseFamilyName] [nvarchar](50) NULL,
	[tchSpouseOccupation] [nvarchar](50) NULL,
	[tchSpouseEmployer] [nvarchar](50) NULL,
-- employment
	[tchFullPart] [nvarchar](1) NULL,
	[tchFTE] [float] NULL DEFAULT ((0)),
	[tchSponsor] [nvarchar](50) NULL,
	[tchEmplNo] [nvarchar](50) NULL,
	[tchProvident] [nvarchar](50) NULL,
	[tchSalaryScale] [nvarchar](50) NULL,
	[tchSalary] [money] NULL DEFAULT ((0)),
	[tchHouse] [int] NULL,

-- duties
	[tchRole] [nvarchar](50) NULL,
	[tchTAM] [nvarchar](1) NULL,
	[tchSubjectMajor] [nvarchar](50) NULL,
	[tchSubjectMinor] [nvarchar](50) NULL,
	[tchSubjectMinor2] [nvarchar](50) NULL,
	[tchClass] [nvarchar](50) NULL,
	[tchClassMax] [nvarchar](50) NULL,
--	[tchJoint] [bit] NULL DEFAULT ((0)),
--	[tchComposite] [bit] NULL DEFAULT ((0)),
	[tchEdQual] [nvarchar](50) NULL,
	[tchSubjectTrained] [nvarchar](50) NULL,
	[tchQual] [nvarchar](20) NULL,
	[tchStatus] [nvarchar](2) NULL,
	[tchRegister] [nvarchar](20) NULL,
	[tchYearStarted] [int] NULL,
	[tchYears] [int] NULL ,
	[tchYearsSchool] [int] NULL,
	[tchInserviceYear] [int] NULL ,
	[tchInservice] [nvarchar](50) NULL,
	[tchTrained] [nvarchar](10) NULL,
	[tchECE] [nvarchar](50) NULL,
	[tID] [int] NULL ,
	[tchsmisID] [int] NULL
--	[tchSector] [nvarchar](3) NULL,
)


	if @BatchID is null
		select @BatchId = convert(nvarchar(50),newid())
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml


	INSERT @Teachers
		(
		tID,
		tchsmisID,
		tchFamilyName,tchFirstName,
		tchDOB, tchGender,
		tchCivilStatus,
		tchNumDep,
		tchCitizenship,
		tchLangMajor,
		tchLangMinor,
		tchIsland,
		tchDistrict,
-- spouse
		tchSpousetID,
		tchSpouseFamilyName,
		tchSpouseFirstName,
		tchSpouseOccupation,
		tchSpouseEmployer,

--employment
		tchFullPart,
		tchFTE,
		tchSponsor,
		tchEmplNo,
		tchProvident,
		tchSalaryScale,
		tchSalary,
		tchHouse,
-- duties
		tchRole,
		tchTAM,
		tchSubjectMajor,
		tchSubjectMinor,
		tchSubjectMinor2,
		tchClass,
		tchClassMax,
--qualifications
		tchEdQual,
		tchSubjectTrained,
		tchQual,
		tchRegister,
		tchYearStarted,
		tchYears,
		tchYearsSchool,
		tchInserviceYear,
		tchInService,
		tchTrained

		)
		Select

		tID,
		smisID,
		FamilyName, GivenName,
		DOB, Sex,
		MaritalStatus ,
		NumDependentChild ,
		Nationality ,
		LanguageMajor ,
		LanguageMinor,
		HomeIsland ,
		HomeDistrict,
--spouse
		spousetID,
		spouseFamilyName,
		spouseGivenName,
		spouseOccupation,
		spouseEmployer,

-- employment
		FullPart,
		FTE,
		SalaryPaidBy,
		EmploymentNo,
		ProvidentFundNo ,
		SalaryScale,
		Salary,
		HouseProvided,
-- duties
		[Role],
		TeachingDuty,
		SubjectMajor,
		SubjectMinor,
		SubjectMinor2,
		LevelTaughtMin,
		LevelTaughtMax,
-- qualifications
		EdQual,
		SubjectTrained,
		Qual,
		RegistrationNo ,
		YearFirstTeaching,
		YearsTeaching,
		YearsAtSchool,
		YearLastInservice,
		LastInserviceSubject,
		Accreditation
		from OPENXML (@idoc, '/Teachers/Teacher',2)
		WITH (
-- identifiers
				smisID int '@smisID',
				tID int '@PineapplesID',
-- personal
				FamilyName nvarchar(50) '@FamilyName',
				GivenName nvarchar(50)  '@GivenName',
				DOB datetime '@DOB',
				Sex nvarchar(1) '@Sex',
				MaritalStatus nvarchar(1) '@MaritalStatus',
				NumDependentChild int	'@NumDependentChild',
				Nationality nvarchar(10) '@Nationality',
				LanguageMajor nvarchar(10) '@LanguageMajor',
				LanguageMinor nvarchar(10) '@LanguageMinor',
				HomeIsland nvarchar(10) '@HomeIsland',
				HomeDistrict nvarchar(10) '@HomeDistrict',

-- spouse
				spousetID int '@SpousePineapplesID',
				spouseFamilyName nvarchar(50) '@SpouseFamilyName',
				spouseGivenName nvarchar(50) '@SpouseGivenName',
				spouseOccupation nvarchar(50) '@SpouseOccupation',
				spouseEmployer nvarchar(50)		'@SpouseEmployer',

-- employment conditions
				FullPart nvarchar(1)	 '@FullPart',
				FTE float '@FTE',
				SalaryPaidBy nvarchar(20) '@SalaryPaidBy',
				EmploymentNo nvarchar(50) '@EmploymentNo',
				ProvidentFundNo nvarchar(50) '@ProvidentFundNo',
				SalaryScale		nvarchar(10) '@SalaryScale',
				Salary	money '@Salary',
				HouseProvided int '@HouseProvided',
				[Role]	nvarchar(10) '@Role',
-- duties
				TeachingDuty nvarchar(1) '@TeachingDuty',
				SubjectMajor nvarchar(25) '@SubjectMajor',
				SubjectMinor nvarchar(25) '@SubjectMinor',
				SubjectMinor2 nvarchar(25) '@SubjectMinor2',
				LevelTaughtMin nvarchar(10) '@LevelTaughtMin',
				LevelTaughtMax nvarchar(10) '@LevelTaughtMax',
-- qualifications
				EdQual nvarchar(10) '@EdQual',
				Qual nvarchar(10)	'@Qual',
				SubjectTrained nvarchar(25) '@SubjectTrained',
				RegistrationNo nvarchar(50) '@RegistrationNo',
				YearFirstTeaching int '@YearFirstTeaching',
				YearsTeaching int '@YearsTeaching',
				YearsAtSchool int '@YearsAtSchool',
				YearLastInservice int '@YearLastInservice',
				LastInserviceSubject nvarchar(50) '@LastInserviceSubject',
				Accreditation nvarchar(10) '@Accreditation'

			)

-- the teacher survey record may already have a pineapplesID ie tId associated with it
-- if so, clear it if it si not valid
UPDATE @Teachers
	SET tId = null
WHERE tID not in (select tID from TeacherIdentity)

-- now attempt to find the tID from the name
print 'before update tId from name'
-- endeavour to find a match for the teacher
UPDATE @Teachers
	set tID = TI.tID
from @Teachers T INNER JOIN TeacherIdentity TI
	ON T.tchFamilyName = TI.tSurname
		AND T.tchFirstName = TI.tGiven
		AND T.tchGender = TI.tSex
		AND T.tchDOB = TI.tDOB
WHERE T.tID is null
print 'after update tid from name'

-- any remaining null tIDs mean we didn;t find an identity - create one now
INSERT INTO TeacherIdentity
	(
	tSurname, tGiven,
	tDOB, tSex,
	tSrcID, tSrc
	)
SELECT

	tchFamilyName, tchFirstName,
	tchDOB, tchGender,
	TeacherID, @BatchID
FROM @Teachers
WHERE tID is null

-- drag back the new tID
update @Teachers
SET tID = TI.tID
FROM @Teachers tmp

	INNER JOIN TeacherIdentity TI
		ON tmp.TeacherID = TI.tSrcID
WHERE TI.tSrc = @BatchID

-- update all the required fields on TeacherIdentity affected by the survey
UPDATE TeacherIdentity
set
	tSurname = tchFamilyName,
	tGiven = tchFirstName,
	tRegister = tchRegister,
	tPayroll = tchEmplNo,
	tProvident = tchProvident,
	tDOB = tchDOB,
	tSex = tchGender,
	tYearStarted = tchYearStarted
from
TeacherIdentity TI
INNER JOIN @Teachers T
ON TI.tID = T.tID
-- delete exisitng TeacherSurvey and replace

select tID, tchsmisID, tchFamilyName from @Teachers

DELETE from PrimaryClassTeacher WHERE tchsID in
(select tchsID from TeacherSurvey WHERE ssID = @SurveyID)

DELETE from TeacherSurvey WHERE ssID = @SurveyID
-- insert newly required TeacherSurvey
INSERT INTO TeacherSurvey
		(tID,
		ssID,
		tchsmisID,

		tchFamilyName,tchFirstName,
		tchDOB, tchGender,
		tchCivilStatus,
		tchNumDep,
		tchCitizenship,
		tchLangMajor,
		tchLangMinor,
		tchIsland,
		tchDistrict,
-- spouse
		tchSpousetID,
		tchSpouseFamilyName,
		tchSpouseFirstName,
		tchSpouseOccupation,
		tchSpouseEmployer,
-- employment
		tchFullPart,
		tchFTE,
		tchSponsor,
		tchEmplNo,
		tchProvident,
		tchSalaryScale,
		tchSalary,
		tchHouse,

-- duties
		tchRole,
		tchTAM,
		tchSubjectMajor,
		tchSubjectMinor,
		tchSubjectMinor2,
		tchClass,
		tchClassMax,
--qualifications
		tchEdQual,
		tchSubjectTrained,
		tchQual,
		tchRegister,
		tchYearStarted,
		tchYears,
		tchYearsSchool,
		tchInserviceYear,
		tchInService,
		tchTrained

)
SELECT
		tID,
		@SurveyID,
		tchsmisID,
		tchFamilyName,tchFirstName,
		tchDOB, tchGender,
		tchCivilStatus,
		tchNumDep,
		tchCitizenship,
		tchLangMajor,
		tchLangMinor,
		tchIsland,
		tchDistrict,
-- spouse
		tchSpousetID,
		tchSpouseFamilyName,
		tchSpouseFirstName,
		tchSpouseOccupation,
		tchSpouseEmployer,
-- employment
		tchFullPart,
		tchFTE,
		tchSponsor,
		tchEmplNo,
		tchProvident,
		tchSalaryScale,
		tchSalary,
		tchHouse,
-- duties
		tchRole,
		tchTAM,
		tchSubjectMajor,
		tchSubjectMinor,
		tchSubjectMinor2,
		tchClass,
		tchClassMax,
--qualifications
		tchEdQual,
		tchSubjectTrained,
		tchQual,
		tchRegister,
		tchYearStarted,
		tchYears,
		tchYearsSchool,
		tchInserviceYear,
		tchInService,
		tchTrained
FROM @Teachers


select * FROM @Teachers tmp

	INNER JOIN TeacherIdentity TI
		ON tmp.TeacherID = TI.tSrcID
WHERE TI.tSrc = @BatchID    -- Insert statements for procedure here

select * from TeacherIdentity where tSrc = @BatchId
SELECT * from @Teachers
Select * from TeacherSurvey WHERE ssId = @surveyId
END
GO

