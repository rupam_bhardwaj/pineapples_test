SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 12 2017
-- Description:	Refresh the warehouse school cohort table
-- =============================================
CREATE PROCEDURE [warehouse].[buildSchoolCohort]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
---- school level flow model

		begin transaction
		IF OBJECT_ID('warehouse.schoolCohort', 'U') IS NULL begin
			-- this query
			Select schNo
			, surveyYear
			, avg(Estimate) Estimate
			, min(SurveyDimensionID) SurveyDimensionID

			, YearOfEd
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(RepNY) RepNY
			, sum(TroutNY) TroutNY
			, sum(EnrolNYNextLevel) EnrolNYNextLevel
			, sum(RepNYNextLevel) RepNYNextLevel
			, sum(TrinNYNextLevel) TrinNYNextLevel
			into warehouse.SchoolCohort
			FROM
			(
				select schNo
				, SurveyYear
				, Estimate
				, SurveyDimensionID
				, lvlYear YearOfEd
				, GenderCode
				, Enrol
				, Rep
				, null RepNY
				, null TroutNY
				, null EnrolNYNextLevel
				, null RepNYNextLevel
				, null TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
				UNION ALL
				select schNo
				, SurveyYear - 1
				, null Estimate
				, null SurveyDimensionID
				, lvlYear YearOfEd
				, GenderCode
				, null Enrol
				, null Rep
				, Rep RepNY
				, Trout TroutNY
				, null EnrolNYNextLevel
				, null RepNYNextLevel
				, null TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
				WHERE Rep is not null or Trout is not null
				UNION ALL
				select schNo
				, SurveyYear - 1
				, null Estimate
				, null SurveyDimensionID
				, lvlYear - 1 YearOfEd
				, GenderCode
				, null Enrol
				, null Rep
				, null RepNY
				, null TroutNY
				, Enrol EnrolNYNextLevel
				, Rep RepNYNextLevel
				, Trin TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
					WHERE (Enrol is not null or Rep is not null or Trin is not null)
			) U
			GROUP BY
			U.schNo
			, U.surveyYear
			, U.YearOfEd

			print 'warehouse.schoolCohort created - rows:' + convert(nvarchar(10), @@rowcount)

		end else begin
			DELETE from warehouse.schoolCohort
			WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
			print 'warehouse.schoolCohort deletes - rows:' + convert(nvarchar(10), @@rowcount)


			INSERT
			into warehouse.SchoolCohort
			Select schNo
			, surveyYear
			, avg(Estimate) Estimate
			, min(SurveyDimensionID) SurveyDimensionID

			, YearOfEd
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(RepNY) RepNY
			, sum(TroutNY) TroutNY
			, sum(EnrolNYNextLevel) EnrolNYNextLevel
			, sum(RepNYNextLevel) RepNYNextLevel
			, sum(TrinNYNextLevel) TrinNYNextLevel
			FROM
			(
				select schNo
				, SurveyYear
				, Estimate
				, SurveyDimensionID
				, lvlYear YearOfEd
				, GenderCode
				, Enrol
				, Rep
				, null RepNY
				, null TroutNY
				, null EnrolNYNextLevel
				, null RepNYNextLevel
				, null TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
				UNION ALL
				select schNo
				, SurveyYear - 1
				, null Estimate
				, null SurveyDimensionID
				, lvlYear YearOfEd
				, GenderCode
				, null Enrol
				, null Rep
				, Rep RepNY
				, Trout TroutNY
				, null EnrolNYNextLevel
				, null RepNYNextLevel
				, null TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
				WHERE Rep is not null or Trout is not null
				UNION ALL
				select schNo
				, SurveyYear - 1
				, null Estimate
				, null SurveyDimensionID
				, lvlYear - 1 YearOfEd
				, GenderCode
				, null Enrol
				, null Rep
				, null RepNY
				, null TroutNY
				, Enrol EnrolNYNextLevel
				, Rep RepNYNextLevel
				, Trin TrinNYNextLevel
				From warehouse.enrol E
					LEFT JOIN lkpLevels L
						ON E.ClassLevel = L.codeCode
					WHERE (Enrol is not null or Rep is not null or Trin is not null)
			) U
			WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
			GROUP BY
			U.schNo
			, U.surveyYear
			, U.YearOfEd
			print 'warehouse.schoolCohort inserts - rows:' + convert(nvarchar(10), @@rowcount)

		end
		commit transaction

END
GO

