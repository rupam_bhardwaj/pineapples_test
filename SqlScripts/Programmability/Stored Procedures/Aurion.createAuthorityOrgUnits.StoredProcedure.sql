SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 9 2009
-- Description:	Allocates OrgUnit numbers to Authorities, as part of the Aurion interface
-- =============================================
CREATE PROCEDURE [Aurion].[createAuthorityOrgUnits]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
	, @Nested int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try


	declare @AurionModel int	-- we'll preserve support for the deep model (Auth Type/Auth/SchoolType/School/Sector)
								-- while adding support for the shallow model (Auth Type/Auth/Sector)
	declare @OrgLevel int		-- the level in the hierarchy for these units . Determined by the model.
								-- In fact, these units are only in the Hierarchy if Model = 0 (thin)
	declare @TopOrgUnit int
	declare @TopOrgLevel int

	Select @TopOrgUnit = common.SysParamInt('Aurion_TopLevelOrgNumber',0)
	Select @TopOrgLevel = common.SysParamInt('Aurion_TopLevelOrgLevel',4)
	Select @AurionModel = common.SysParamInt('Aurion_Model',0)


	Select @OrgLevel =
		@TopOrgLevel +
			Case @AurionModel
				when 0 then 1
				when 1 then 2
			end


	If @Nested = 0
		create table #OrgUnits
		(
				OrgUnitNumber int
				, OrgUnitLevel int
				, OrgUnitName nvarchar(25)
				, OrgUnitShortName nvarchar(8)
				, OrgUnitLongDescription nvarchar(50)
				, OrgUnitParent int
				, OrgUnitReference nvarchar(10)
		)


	If (@AurionModel = 1)
		-- don;t need authority type in model 0
		exec Aurion.createAuthorityTypeOrgUnits @SendAll, 1		-- nested

	begin transaction
	-- list the authorities that need an org unit number
	-- check estpAuth in case we have a supernumerary in the authority
		Select DISTINCT Auth.AuthCode authCode
		INTO #tmpAuth
		FROM Establishment E
			LEFT JOIN Schools S
				ON E.schNo = S.SchNo
			LEFT JOIN Authorities AUTH
				ON isnull(E.estpAuth,S.schAuth) = Auth.AuthCode
		WHERE
			authOrgUnitNumber is null


		declare @ToAllocate int

		-- this is the number of org unit numbers we need
		Select @ToAllocate = count(*)
		from #tmpAuth

		-- this is where we'll put them
		declare @counters table
		(
		counter int
		, maxCounters int
		, seq int
		, char nvarchar(6)
		)

		IF (@ToAllocate > 0 ) begin


		-- get the numbers

			insert into @counters
			exec getCounter 'Aurion_OrgUnit', @ToAllocate


			UPDATE Authorities
				SET authOrgUnitNumber = counter
			FROM
				Authorities A
					INNER JOIN
						(Select row_number() over (ORDER BY authCode) Pos
						, authCode
						from #tmpAuth
						) ORDERED
						ON A.authCode = ORDERED.authCode
					INNER JOIN @counters C
						ON ORDERED.Pos = C.seq


-- supernumerary org unit
			DELETE FROM @counters
			insert into @counters
			exec getCounter 'Aurion_OrgUnit', @ToAllocate

			UPDATE Authorities
				SET authSuperOrgUnitNumber = counter

			FROM
				Authorities A
					INNER JOIN
						(Select row_number() over (ORDER BY authCode) Pos
						, authCode
						from #tmpAuth
						) ORDERED
						ON A.authCode = ORDERED.authCode
					INNER JOIN @counters C
						ON ORDERED.Pos = C.seq

		end


	commit transaction


	INSERT INTO #OrgUnits
	(
		OrgUnitNumber
		, OrgUnitLevel
		, OrgUnitName
		, OrgUnitShortName
		, ORgUnitLongDescription
		, OrgUnitParent
		, OrgUnitReference
	)
	SELECT
		authOrgUnitNumber
		, @OrgLevel
		, left(authName,25)
		, left(A.authCode,8)
		, left(authName,50)
		, case @AurionModel
				when 0 then @TopOrgUnit
				when 1 then AT.atOrgUnitNumber
		  end
		, A.authCode

	FROM Authorities A
		LEFT JOIN lkpAuthorityType AT
			ON A.authType = AT.codeCode
		LEFT JOIN #tmpAuth
			ON A.authCode = #tmpAuth.authCode
	WHERE
		authOrgUnitNumber is not null
		AND
		(#tmpAuth.authCode is not null
			OR @SendAll = 1)


-- every authority needs a child node for supernumerary
--
-- get another set of numbers

	Select @OrgLevel = @OrgLevel + 1

	INSERT INTO #OrgUnits
	(
		OrgUnitNumber
		, OrgUnitLevel
		, OrgUnitName
		, OrgUnitShortName
		, ORgUnitLongDescription
		, OrgUnitParent
		, OrgUnitReference
	)
	SELECT
		authSuperOrgUnitNumber
		, @OrgLevel
		, 'Supernumerary'
		, 'Super'
		, A.authCode + ' Supernumerary'
		, authOrgUnitNumber
		, A.authCode + 'S'

	FROM Authorities A
		LEFT JOIN lkpAuthorityType AT
			ON A.authType = AT.codeCode
		LEFT JOIN #tmpAuth
			ON A.authCode = #tmpAuth.authCode
	WHERE
		authOrgUnitNumber is not null
		AND
		(#tmpAuth.authCode is not null
			OR @SendAll = 1)


	-- return the ones we added
	-- return the ones we added
	If (@Nested = 0) begin
		Select *
		FROM
			#OrgUnits

		drop table #OrgUnits
	end

	drop table #tmpAuth

end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

