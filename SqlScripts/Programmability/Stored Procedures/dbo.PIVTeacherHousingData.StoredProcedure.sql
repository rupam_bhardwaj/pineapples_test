SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Return the estimated resources from the selected category
-- 21 10 2012 use table variable
-- =============================================
CREATE PROCEDURE [dbo].[PIVTeacherHousingData]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @ebs table
(
	schNo nvarchar(50),
	LifeYear int,
	resName nvarchar(50),
	ActualssID int,
	bestssID int,
	bestYear int,
	Offset int,
	bestssqLevel int,
	ActualssqLevel int,
	SurveyDimensionssID int,
	Estimate smallint
)


create table #th
(
	schNo nvarchar(50),
	[Survey Year] int,
	surveyDimensionssID int,
	HousingType nvarchar(50),
	Materials nvarchar(50),
	Condition nvarchar(1),
	NumHouses int,
	MaritalStatus nvarchar(50),			-- 21 10 2012
	HouseProvided int,
	HouseProvidedYN nvarchar(1),
	HouseStatus nvarchar(20),
	NumTeachers int,
	PartnerIsTeacher int,
	PartnerIsTeacherYN nvarchar(1),
	NumHouseRequired int,
	HouseEstimate int,
	AgeOfHousingData int,
	TeacherEstimate int,
	AgeOfTeacherData int
)

begin try

		insert into @ebs
		select *
		from ESTIMATE_BestSurveyResourceCategory EBSRC
		WHERE				(EBSRC.resName like 'Staff Housing%'
						or EBSRC.resName = 'Teacher Housing')


		SELECT *
		INTO #ebst
		from ESTIMATE_BestSurveyTeachers
	-- get the teacher housing records to put in the table
	--
	INSERT INTO #th
	( schNo
		, [Survey Year]
		, surveyDimensionssID
		, HousingType
		, Materials
		, Condition
		, NumHouses
		, MaritalStatus
		, HouseProvided
		, HouseProvidedYN
		, HouseStatus
		, NumTeachers
		, PartnerIsTeacher
		, NumHouseRequired
		, HouseEstimate
		, AgeOfHousingData
		, TeacherEstimate
		, AgeOfTeacherData
	)
	SELECT ebs.schNo, ebs.LifeYear
		, isnull(#ebst.surveyDimensionssID, ebs.surveyDimensionssID)
		, HousingType
		, Materials
		, HousingCondition
		, resNumber
		, null, null, null, null
		, null, null, null
		, ebs.Estimate, ebs.Offset, #ebst.Estimate, #ebst.Offset
	FROM @ebs ebs
		INNER JOIN vtblTeacherHousing
		ON ebs.bestssID = vtblTeacherHousing.ssID
		LEFT JOIN #ebst
			ON ebs.schNo = #ebst.schNo
			AND ebs.LifeYear = #ebst.LifeYear


	-- Now get the teacher data
	INSERT INTO #th
		( schNo
		, [Survey Year]
		, surveyDimensionssID
		, HousingType
		, Materials
		, Condition
		, NumHouses
		, MaritalStatus
		, HouseProvided
		, HouseProvidedYN
		, HouseStatus
		, NumTeachers
		, PartnerIsTeacher
		, NumHouseRequired
		, HouseEstimate
		, AgeOfHousingData
		, TeacherEstimate
		, AgeOfTeacherData
	)
	Select
		#ebst.schNo
		, #ebst.LifeYear
		, isnull(#ebst.surveyDimensionssID, ebs.surveyDimensionssID)

		, '<>', null, null, null
		, TS.tchCivilStatus
		, case TS.tchHouse
				when  null then null
				when -1 then 1
				when 1 then 1			-- on site
				when 2 then 1			-- off site
				else 0
			end
		, 	case  TS.tchHouse
				when  null then null
				when -1 then 'Y'
				when 1 then 'Y'			-- on site
				when 2 then 'Y'			-- off site
				else 'N'
			end
		, 	case  TS.tchHouse
				when  null then null
				when -1 then 'Y'
				when 0 then 'N'
				when 1 then 'OnSite'			-- on site
				when 2 then 'OffSite'			-- off site
				when 3 then 'Rented'
				when 4 then 'Owned'
			end  HouseStatus

		, count(TS.tchsID) NumTeachers
		, count(SpouseTS.tchsID) PartnerIsTeacher
		, count(TS.tchsID) - (count(TS.tchsID)/ 2 )
		, ebs.Estimate, ebs.Offset, #ebst.Estimate, #ebst.Offset
	FROM #ebst
		INNER JOIN TeacherSurvey TS
			ON #ebst.bestssID = TS.ssID
		LEFT JOIN TeacherSurvey SpouseTS
			ON TS.tchSpousetID = SpouseTS.tID
			AND TS.ssID = SpouseTS.ssID
		LEFT JOIN @ebs ebs
			ON #ebst.schNo = ebs.SchNo
			AND #ebst.LifeYear = ebs.LifeYear
	GROUP BY
		#ebst.schNo, #ebst.LifeYear, TS.tchCivilStatus	, Ts.tchHouse
		, #ebst.surveyDimensionssID, ebs.surveyDimensionssID
		, ebs.Estimate, ebs.Offset, #ebst.Estimate, #ebst.Offset


	SELECT * from #th

	DROP TABLE #ebst
	DROP Table #th
end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
return 0


END
GO

