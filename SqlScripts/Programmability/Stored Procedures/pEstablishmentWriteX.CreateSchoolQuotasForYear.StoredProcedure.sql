SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-18
-- Description:	Calculate and write the school establishment quota from the formula tables
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[CreateSchoolQuotasForYear]
	-- Add the parameters for the stored procedure here
	@EstYear int
	, @SchoolNo nvarchar(50) = null
	, @ScenarioID int = null -- scenario Id for enrolment projection
AS
BEGIN


	-- begin by pulling out the schools enrolment projection

	SET NOCOUNT ON;
	begin try
	-- 20 2 2010 allow a ScenarioId to be passed to this routine to
	-- overwrite the one assocaited with the establishment plan
	--
		if (@ScenarioID is null) begin
			Select @ScenarioID = escnID
			FROM
				EstablishmentControl
			WHERE
				estcYear = @EstYear
			-- all in a trnasaction
		end


		If (@ScenarioID is null)
			Raiserror('No projection scenario is defined for this establishment year',16,1)

		begin transaction

		-- first get the total of the projection by level, for each school
		-- at this point we aggregate based on the Establishment Point
		-- this will then work whether we have enrolment at the Extension level or not


		------Select EP.SchNo, epdLevel levelCode,
		------sum(isnull(epdM,0) + isnull(epdF,0) + isnull(epdU,0)) Enrol

		------into #enrol
		------from
		------	EnrolmentScenario S
		------	INNER JOIN EnrolmentProjection EP on S.escnId = EP.escnID
		------	INNER JOIN EnrolmentProjectionData EPD on EP.epID = EPD.epID
		------WHERE
		------	S.escnID = @ScenarioID
		------	and (EP.schNo = @SchoolNo or @SchoolNo is null)
		------	and (EP.epYEar = @EstYear)

		------group by EP.SchNo, epdLevel

-- first we have to make sure the SchoolEstablishment (ie ShoolYearHistory)
-- records are there for every school that is not closed before that year
INSERT INTO SchoolYearHistory
(schNo, syYear, syAuth, syParent, systCode, syDormant)
SELECT
S.schNo
, @estYear
, syAuth
, syParent
, systCode
, syDormant
FROM (
Select *
, row_number() OVER (PARTITION BY schNo ORDER BY syYear DESC) ROW
FROM
SchoolYearHistory SYH
WHERE syYear <= @EstYear
) SYH2
INNER JOIN Schools S
ON SYH2.schNo = S.schNo
WHERE SYH2.Row = 1
AND SYH2.syYear <> @EstYear		-- this handily takes care of not adding a dup
AND S.schClosed = 0 or S.schClosed > @EstYear


		Select SE.estEstablishmentPoint SchNo
		, epdLevel levelCode
		, sum(isnull(epdM,0) + isnull(epdF,0) + isnull(epdU,0)) Enrol
		, count(DISTINCT SE.SchNo) NumSchools		-- just for debugging really
		into #enrol
		from
			SchoolEstablishment SE

			INNER JOIN EnrolmentProjection EP
				on SE.schNo = EP.schNo
				and SE.estYear = EP.epYear
			INNER JOIN EnrolmentProjectionData EPD on EP.epID = EPD.epID
		WHERE
			EP.escnID = @ScenarioID
			and (SE.estEstablishmentPoint = @SchoolNo or @SchoolNo is null)
			and (SE.estYear = @EstYear)

		group by SE.estEstablishmentPoint, epdLevel

		-- first we have to work out what formula tables apply
		-- here's the strategy
		-- get all possible combinations of schools and tables
		-- rank the tables in their groups according to the number of levels - smallest is best
		-- find all combinations of school and table WHERE
		-- there exists at least one level for the school in the group
		-- there are no levels for the school in the group that are not in this table
		-- then, because the tables may be subsets of each other, we take the smallest one by rank


		-- a simple helper query
		-- gives us all the levels that may apply in a group
		Select efGroup, eflLevel
		into #groupLevels
		from EFTAbles INNER JOIN EFTableLevels on EFTables.efTable = EFTableLevels.efTable
		group by efGRoup, eflLevel


		Select schNo, efTable
		into #SchoolEFTables
		from
		(
		-- schools and applicable tables
		-- the tables in each table group are ranked from smallest to largest: we only want the smallest one
		Select SchNo, T.efGRoup, T.EFTable, row_number() OVER(PARTITION BY schNo,T.efGroup ORDER BY TableLevels) NUM
		from
			-- this query returns the school and all the tables that apply to it
			-- at this stage there may be multiple tables in each table group
			-- this cross join schools and EFTables
			(Select DISTINCT schNo from #enrol) E,
			-- subquery T is the number of level in each table in each group
			-- at the top level, we rank by the number of levels to select the smallest applicable table
			(Select efGRoup, EFTables.efTable, count(eflLevel) TableLevels
				from EFTAbles
				INNER JOIN EFTableLevels ON efTables.efTable = efTableLevels.efTable
				GROUP BY efGRoup, EFTables.efTable) T

			WHERE
				-- this condition exlcudes any table in T , where that Table
				-- does not contain a Class Level for which this school has an enrolment
				-- where that class level is included in the grop of table T
				-- e.g. if the Table is a Secondary Table
				-- it must include all the Secondary levels for which this class has an enrolment
				-- Becuae the T may be nested, there may be multiple tables matching this condition for any Group
				not exists
				(Select levelCode from #enrol EE
					inner join
					#groupLevels MM on MM.eflLevel = EE.levelCode


					WHERE levelCode not in (Select eflLevel from EFTableLevels WHERE efTable = T.EFTable)
					and E.schNo = EE.schNo
					and T.efGroup = MM.efGroup
				)
			and exists
				-- we want to be sure that the school is represented in the group
				-- ie without this clause , we would get a secondary Table from the previous condition if the
				-- school had no secondary enrolments
				(Select levelCode from #enrol EEE
					inner join
					#groupLevels MMM on MMM.eflLevel = EEE.levelCode
					and E.schNo = EEE.schNo
					and T.efGroup = MMM.efGroup

				)
			) sub

		WHERE Num = 1


		-- after the above, #schoolEFTables contains each school
		-- and the tables that apply to it. There is at most one table from each table group,
		-- and that is the smallest table in the table group to contain all the levels for the school


-- the update strategy is to preserve the Current User values by writing them first into the temp table
-- then the calculated values are written into the temp table
-- all the SchooleEstablishmentRole records for the year are deleted and re-added


		create table #tempSER
		(
		schNo nvarchar(50)
		, rgCode nvarchar(10)
		, Quota int
		, CountUser int
		)

		-- save any exisitng user overrides into the temp table

		INSERT INTO #tempSER

		SELECT schNo
			, estrRoleGrade
			, null
			, estrCountUser
		FROM
			SchoolEstablishment SE
				INNER JOIN SchoolEStablishmentRoles SER
				ON SE.estID = SER.estID
		WHERE SE.estYear = @estYEar
			AND isnull(estrCountUser,0) > 0			-- don't put any nulls in there


		-- add the establishment header record for the school and year
		-- if its not already there
		INSERT INTO SchoolEstablishment
		(schNo, estYear, eststCode, estAuth, estParent, syDormant)
		SELECT SchNo, @EstYear, schType, schAuth, schParent, schDormant
		from Schools
		i
		WHERE (schNo IN (Select schNo from #enrol))
		and schNo not in (Select Schno from SchoolEstablishment WHERE estYear = @EstYear)


		-- get the calculated quota into the temp table

		INSERT INTO #tempSER

		Select schNo
		,  rgCode
		, sum(efaAlloc) Quota
		, null
		from EFAllocations
			inner join EFTableBands on EFAllocations.efbID = EFTableBands.efbID

		-- for each selected table
			-- get the total enrolment for the levels in that table
		inner join
		-- subquery TableEnrol finds the total enrol at the school for all level included in the table
		(Select #enrol.schNo, #SchoolEFTables.efTable,  sum(Enrol) TotEnrol
		from #enrol
			INNER JOIN EFTableLevels on EFTAbleLevels.eflLevel = #enrol.levelCode
			INNER JOIN #SchoolEFTables ON #SchoolEFTables.schNo = #enrol.SchNo
			WHERE #SchoolEFTables.efTable = efTableLevels.efTable
		group by #enrol.schNo, #SchoolEFTables.efTable
		) TableEnrol on TableEnrol.efTable = EFTableBAnds.efTable

		-- the table band that applies is the one that includes the school total enrolment
		WHERE TableEnrol.TotEnrol between EFTableBands.efbMin and EFTableBands.efbMax

		group by SchNo, rgCode


		-- now apply this temp table to update the SchoolEstablishmentRole records

		-- delete any exisitng role records
		-- 31 5 2010 fix a bug where these were not deleted for schools with no enrolment
		Delete from SchoolEstablishmentRoles
		WHERE estID in
			(Select estID from SchoolEstablishment
					WHERE estYear = @EstYear
					-- this formulation for the affected schools is correct
					AND (estEstablishmentPoint = @SchoolNo or @SchoolNo is null)
			)

		-- add themn all back

		INSERT INTO SchoolEStablishmentRoles
		(estID, estrRoleGrade, estrQuota, estrCountUser)

		SELECT
			syID
			, #tempSER.rgCode
			, sum(Quota)
			, case when SE.syLocked = 1 then isnull(sum(CountUser),0)
				else sum(CountUser)
			 end
		FROM
			SchoolYearHistory SE
			INNER JOIN #tempSER
			ON #tempSER.schNo = SE.schNo
		WHERE
			SE.syYear = @estYear
			AND SE.syDormant = 0			-- 24 5 2010
			--AND syParent is null
		GROUP BY
			SE.syID, SE.syLocked, #tempSER.rgCode


		commit transaction
	end try

	begin catch
	    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


END
GO

