SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16/11/2007
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVExamScoresData]

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- create the temp table for estimates
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

-- join the exams data

-- can we treat enrol differently so it can work as  a detail field?
-- 14 11 2009 SRVU0025
-- union the exam scores, null enrol
Select *
INTO #exam
FROM
(
Select
	X.exYear [Survey Year],
	ET.exCode [Exam Code],
	ET.exName [Exam Name],
	ET.exLevel [Exam Level],
	X.exYear [Exam Year],
	XS.exsBand  [Score Band],
	XS.exsGender,
	XS.exsCandidates Candidates,
	XS.exsScore TotalScore,
	XS.exsMin MinScore,
	XS.exsMax MaxScore,
	XS.schNo [Exam SchNo],
	Schools.schName  [Exam SchName],
	EBSE.surveydimensionssID,
	EBSE.Estimate,
	null LevelEnrol
FROM
	lkpExamTypes ET
	INNER JOIN Exams X
		ON ET.exCode = X.exCode
	INNER JOIN ExamScores XS
		ON X.exID = XS.exID
	INNER JOIN Schools
		ON XS.schNo = Schools.schNo
	LEFT JOIN #ebse EBSE
		ON X.exYear = EBSE.LifeYear AND XS.schNo = EBSE.schNo

    -- Insert statements for procedure here


UNION ALL
-- enrol
-- also try to insert placeholders for schools that report enrolments at the level, but no results
-- don't want every school in here, just those with a result, or an enrolment
Select
	X.exYear [Survey Year],
	ET.exCode [Exam Code],
	ET.exName [Exam Name],
	ET.exLevel [Exam Level],
	X.exYear [Exam Year],
	null  [Score Band],
	lkpGender.codeCode,
	null Candidates,
	null TotalScore,
	null MinScore,
	null MaxScore,
	Schools.schNo [Exam SchNo],
	Schools.schName  [Exam SchName],
	EBSE.surveydimensionssID,
	EBSE.Estimate,
	case lkpGender.codeCode when 'M' then SEL.enrolM else SEL.enrolF end LevelEnrol
FROM
	lkpExamTypes ET
	INNER JOIN Exams X
		ON ET.exCode = X.exCode
	CROSS JOIN Schools

	LEFT JOIN (Select DISTINCT exID, schNo FROM ExamScores) XS
		ON X.exID = XS.exID and XS.schNo = Schools.schNo

	LEFT JOIN #ebse EBSE
		ON X.exYear = EBSE.LifeYear AND Schools.schNo = EBSE.schNo
	LEFT JOIN pEnrolmentRead.ssIDEnrolmentLevel SEL
		ON SEL.ssID = EBSE.bestssID and SEL.LevelCode = ET.exLevel

	CROSS JOIN
			lkpGender

WHERE
	SEL.ssID is not null   -- there is enrolment
	OR XS.exID is not null	-- there is a score
) U


SELECT
	EX.*
	, R.[District Rank]
	, R.[District Decile]
	, R.[District Quartile]
	, R.[Rank]
	, R.Decile
	, R.Quartile

FROM #exam EX
	LEFT JOIN DimensionRank R
	ON	EX.[Survey Year] = R.svyYEar and
		EX.[Exam SchNo] = R.schNo

drop table #exam
drop TABLE #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVExamScoresData] TO [pExamRead] AS [dbo]
GO

