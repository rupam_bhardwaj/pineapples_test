SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: Armageddon + 1 (10 11 2016)
-- Description:	This produces the flat table that can be used to produce the LevelER nodes in WERMPAF
-- =============================================
CREATE PROCEDURE [warehouse].[buildEdLevelER]
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- enrolment related pupil tables

DELETE from Warehouse.EdLevelERDistrict
	WHERE SurveyYear >= @StartFromYEar or @StartFromYEar is null

DELETE from Warehouse.EdLevelER
-- don;t need the filter - rebuild from EDLEvelERDistrict

--build the distrct level totals
INSERT INTO warehouse.edLevelERDistrict
Select S.*
, POP.popM
, POP.popF
, POP.pop
, EL.edlMinYear firstYear
, EL.edlMaxYear lastYear
, EL.edlMaxYear - EL.edlMinYear + 1 numYears
FROM
(
Select SurveyYear
		, edLevelCode
		, districtCode
		, sum(enrolM) enrolM
		, sum(enrolF) enrolF
		, sum(enrol) enrol

		, sum(repM) repM
		, sum(repF) repF
		, sum(rep) rep

		, sum(intakeM) intakeM
		, sum(intakeF) intakeF
		, sum(intake) intake

		, sum(nEnrolM) nEnrolM
		, sum(nEnrolF) nEnrolF
		, sum(nEnrol) nEnrol

		, sum(nRepM) nRepM
		, sum(nRepF) nRepF
		, sum(nRep) nRep

		, sum(nIntakeM) nIntakeM
		, sum(nIntakeF) nIntakeF
		, sum(nIntake) nIntake
FROM
(
	Select SurveyYear
		, edLevelCode
		, DistrictCode
		, Enrol enrolM
		, null enrolF
		, Enrol enrol

		, Rep repM
		, null repF
		, Rep rep

		, case when EL.edlMinYear = ER.yearOfEd then isnull(Enrol,0) - isnull(Rep,0) end intakeM
		, null intakeF
		, case when EL.edlMinYear = ER.yearOfEd then isnull(Enrol,0) - isnull(Rep,0) end intake

		, case when edLevelOfficialAge = '=' then Enrol end nEnrolM
		, null nEnrolF
		, case when edLevelOfficialAge = '=' then Enrol end nEnrol

		, case when edLevelOfficialAge = '=' then Rep end nRepM
		, null nRepF
		, case when edLevelOfficialAge = '=' then Rep end nRep

		, case when EL.edlMinYear = ER.yearOfEd and ER.classLevelOfficialAge = '=' then isnull(Enrol,0) - isnull(Rep,0)  end nIntakeM
		, null nIntakeF
		, case when EL.edlMinYear = ER.yearOfEd and ER.classLevelOfficialAge = '=' then isnull(Enrol,0) - isnull(Rep,0)  end nIntake

	from warehouse.enrolmentRatios ER
		INNER JOIN lkpEducationLevels EL
			ON ER.edLevelCode = EL.codeCode
	WHERE GenderCode = 'M'
		AND (SurveyYear >= @StartFromYear or @StartFromYear is null )
	UNION ALL
		Select SurveyYear
		, edLevelCode
		, DistrictCode
		, null enrolM
		, Enrol enrolF
		, Enrol enrol

		, null repM
		, Rep repF
		, Rep rep

		, null intakeM
		, case when EL.edlMinYear = ER.yearOfEd then isnull(Enrol,0) - isnull(Rep,0) end  intakeF
		, case when EL.edlMinYear = ER.yearOfEd then isnull(Enrol,0) - isnull(Rep,0) end intake

		, null nEnrolM
		, case when edLevelOfficialAge = '=' then Enrol end nEnrolF
		, case when edLevelOfficialAge = '=' then Enrol end nEnrol

		, null nRepM
		, case when edLevelOfficialAge = '=' then Rep end nRepF
		, case when edLevelOfficialAge = '=' then Rep end nRep

		, null nIntakeF
		,  case when EL.edlMinYear = ER.yearOfEd and ER.classLevelOfficialAge = '=' then isnull(Enrol,0) - isnull(Rep,0)  end nIntakeF
		, case when EL.edlMinYear = ER.yearOfEd and ER.classLevelOfficialAge = '=' then isnull(Enrol,0) - isnull(Rep,0)  end nIntake

	from warehouse.enrolmentRatios ER
			INNER JOIN lkpEducationLevels EL
			ON ER.edLevelCode = EL.codeCode
	WHERE GenderCode = 'F'
		AND (SurveyYear >= @StartFromYear or @StartFromYear is null )
) U
GROUP BY SurveyYear, edLevelCode, districtCode

) S
LEFT JOIN
	lkpEducationLevels EL
	ON S.edLevelCode = EL.codeCode
LEFT join
(
	--population totals
	SELECT popYear
	, EL.codeCode edLevelCode
	, P.dID
	, sum(case when GenderCode = 'M' then pop end) PopM
	, sum(case when GenderCode = 'F' then pop end) PopF
	, sum(pop) Pop
	from warehouse.measurePopG P
		INNER JOIN Survey SVY
			ON SVY.svyYear = P.popYear
		INNER JOIN lkpEducationLevels EL
	ON (popAge - SVY.svyPSAge + 1) between edlMinYear and edlMaxYear
	GRoup by popYear, EL.codeCode, P.dID
) POP
	ON S.SurveyYEar = POP.PopYear
	AND S.edLevelCode = POP.edLevelCode
	AND S.districtCode = POP.dID

ORDER BY SurveyYear, EL.edlMinYear, DistrictCode

-- now aggregate the distict totals into National Totals
INSERT INTO warehouse.edLevelER
SELECT
		SurveyYEar
		, edLevelCode
		, sum(enrolM) enrolM
		, sum(enrolF) enrolF
		, sum(enrol) enrol

		, sum(repM) repM
		, sum(repF) repF
		, sum(rep) rep

		, sum(intakeM) intakeM
		, sum(intakeF) intakeF
		, sum(intake) intake

		, sum(nEnrolM) nEnrolM
		, sum(nEnrolF) nEnrolF
		, sum(nEnrol) nEnrol

		, sum(nRepM) nRepM
		, sum(nRepF) nRepF
		, sum(nRep) nRep

		, sum(nIntakeM) nIntakeM
		, sum(nIntakeF) nIntakeF
		, sum(nIntake) nIntake

		, sum(popM) popM
		, sum(popF) popF
		, sum(pop) pop

		, firstYear
		, lastYear
		, numYears

FROM warehouse.EdLevelERDistrict
GROUP BY SurveyYEar
		, edLevelCode
		, firstYear
		, lastYear
		, numYears
ORDER BY SurveyYEar
		, firstYear

END
GO

