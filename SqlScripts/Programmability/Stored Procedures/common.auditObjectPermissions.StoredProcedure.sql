SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 3 2010
-- Description:	audit the permissions assocaited to each object
-- =============================================
CREATE PROCEDURE [common].[auditObjectPermissions]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
CREATE TABLE #tmp
(
	objectID int
	, permType nvarchar(2)
	, R1 nvarchar(1000)
	, R2 nvarchar(1000)
	, R3 nvarchar(1000)
	, R4 nvarchar(1000)
	, R5 nvarchar(1000)
)

Select SS.object_ID objectID
      , P.[type] PermissionType
      , u.Name roleName

	INTO #perms
  FROM sys.objects SS
  LEFT JOIN [sys].[database_permissions] P
	ON Major_id = SS.object_id
  LEFT JOIN sys.database_principals U
	on U.principal_id = P.grantee_principal_id
	WHERE ss.type in ( 'U', 'V','P','FN','IF','TF')


INSERT INTO #tmp
(objectID
, permType
, R1
, R2
, R3
, R4
, R5
)

Select objectID
, permissionType
, cast([1] as nvarchar(1000)) Y1
, cast ([2] as nvarchar(1000)) Y2
, cast ([3] as nvarchar(1000)) Y3
, cast ([4] as nvarchar(1000)) Y4
, cast ([5] as nvarchar(1000)) Y5
FROM
(
Select objectID
, permissionType
, RoleName
, row_number() OVER (PARTITION BY objectID, permissionType order By roleName) POS
from #perms
--WHERE roleName is not null
) P
pivot
(
max(roleName)
for POS in ([1],[2],[3],[4],[5])
) pvt


Select obj.Name objectName
, sch.Name schemaName
, obj.type objectTypeCode
, obj.type_desc objectType
, SL SL
, [IN]
, [UP]
, [DL]
, [EX]
from
(
Select objectID
, [SL]
, [IN]
, [UP]
, [DL]
, [EX]
from
(
Select objectID
, permType
, isnull((R1 + ' '),'')
	+ isnull((R2 + ' '),'')
	+ isnull((R3 + ' '),'')
	+ isnull((R4 + ' '),'')
	+ isnull((R5 + ' '),'')		roleName

FROM #tmp
) P
pivot
(max(roleName)
for permType in ([SL],[IN],[UP],[DL],[EX])
) piv
) SUB
INNER JOIN sys.objects OBJ
	ON sub.objectID = OBJ.object_id
INNER JOIN sys.schemas SCH
	ON OBJ.schema_ID = SCH.schema_id
ORDER BY obj.Type_desc, obj.Name, sch.Name


DROP TABLE #tmp
DROP TABLE #perms
END
GO

