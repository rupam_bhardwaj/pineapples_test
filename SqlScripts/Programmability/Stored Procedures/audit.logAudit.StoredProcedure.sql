SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 5 2010
-- Description:	Create the audit log header record
-- =============================================
CREATE PROCEDURE [audit].[logAudit]
	-- Add the parameters for the stored procedure here
	@TableName nvarchar(50)
	, @Context nvarchar(100)
	, @Action nvarchar(1)
	, @NumRowsAffected int
	, @user nvarchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   begin try
   Select @user = isnull(@user, original_login())
   INSERT INTO audit.AuditLog
   (
	auditTable
	, auditUser
	, auditDateTime
	, auditContext
	, auditAction
	, auditAffected
	)
	VALUES
	(
		@TableName
		, @user
		, getdate()
		,@Context
		, @Action
		, @NumRowsAffected
	)
	RETURN @@IDENTITY

   end try
	begin catch

		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch
END
GO

