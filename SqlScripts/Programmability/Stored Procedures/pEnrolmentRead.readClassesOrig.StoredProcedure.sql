SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 1 2009
-- Description:	REad XML for Classes
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[readClassesOrig]
	-- Add the parameters for the stored procedure here
	@SurveyID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

declare @minYearOfEd int

select @minYEarOfEd = min(lvlYear)
from metaSchooltypelevelmap M
inner join SchoolSurvey SS
on M.stCode = SS.ssSchType
inner join lkpLevels L
on M.tlmLevel = L.codeCode
where SS.ssID = @surveyID


Select
Tag
, Parent
, ssID as [Classes!1!SurveyID]

, pcID as [Class!2!ID]
, pcHrsWeek as [Class!2!WeeklyHours]
, pcLang as [Class!2!langCode]
, subjCode as [Class!2!subjectCode]
, rmID as [Class!2!roomID]

, Teachers as [Teachers!3!ID!Hide]
, tchsID as [Teacher!4!ID]
, pctHrsWeek as [Teacher!4!HoursPerWeek]

, Levels as [Levels!5!ID!Hide]
, levelCode as [Level!6!Level]
, offset as [Level!6!yearOffset]
, pclNum as [Level!6!Enrol]
, pclM as [Level!6!EnrolM]
, pclF as [Level!6!EnrolF]
from
(
Select DISTINCT 1 as Tag
, NULL as Parent
, ssID as ssID

, null as pcID
, null as pcHrsWeek
, null as rmID
, null as pcLang
, null as subjCode

, null as Teachers
, null as tchsID
, null as pctHrsWeek

, null as Levels
, null as levelCode
, null as offset
, null as pclNum
, null as pclM
, null as pclF
from Classes
union all
Select 2 as Tag
, 1 as Parent
, ssID
, pcID
, pcHrsWeek
, rmID
, pcLang
, subjCode

, null
, null
, null

, null
, null
, null
, null
, null
, null

from Classes
union all
Select 3 as Tag
, 2 as Parent
, ssID
, pcID
, null
, null
, null
, null

, pcID
, null
, null

, null
, null
, null
, null
, null
, null

from Classes

union all
-- levels header
Select 5 as Tag
, 2 as Parent
, ssID
, pcID
, null
, null
, null
, null

, null
, null
, null

, pcID
, null
, null
, null
, null
, null

from Classes


union all

Select 6 as Tag
, 5
, ssID
, C.pcID
, null
, null
, null
, null

, null
, null
, null

, CL.pcID
, pclLevel
, lvlYear - @minYearOfEd
, pclNum
, pclM
, pclF

from Classes C Inner join ClassLevel CL
on C.pcID = CL.pcID
inner join lkpLevels L
on CL.pclLevel = l.codeCode

union all

Select 4 as Tag
, 3 as Parent
, ssID
, C.pcID
, null
, null
, null
, null

, Ct.pcID
, tchsID
, pctHrsWeek

, null
, null
, null
, null
, null
, null

from Classes C Inner join ClassTeacher CT
on C.pcID = CT.pcID

) sub

where ssId = @surveyID

ORDER By
[Classes!1!SurveyID]
,[Class!2!ID]
, [Teachers!3!ID!Hide]
, [Teacher!4!ID]
, [Levels!5!ID!Hide]
, [Level!6!Level]
, Parent
, Tag


for xml explicit

END
GO

