SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[setupPermissionRole]
	-- Add the parameters for the stored procedure here
	@roleName sysname ,
	@Owner sysname = 'dbo'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	if  exists(select p.name from sys.database_principals p where p.name = @roleName)
		print 'Role ' + @roleName + ' already exists'
	else begin
		begin try
			exec sp_addrole @roleName, @Owner
			print 'Role ' + @roleName + ' added'
		end try
		begin catch
			print 'Unable to add role ' + @roleName+ ': ' + ERROR_MESSAGE()
		end catch
	end

	if exists(select  u.Name
			from sys.database_principals u
			INNER JOIN sys.extended_properties P
				on u.principal_id = P.Major_ID and P.class = 4
				and P.[name] = 'PineapplesUser'
			WHERE u.Name = @RoleNAme)
		print 'Extended property already exists'
	else begin
		begin try
			exec sp_addextendedproperty  N'PineapplesUser', N'CoreRole', 'User', @roleName
			print 'Extended property added to ' + @roleName
		end try
		begin catch
			IF (@@TRANCOUNT > 1 )
				rollback
			print 'Error adding extended property to ' + @roleName + ': ' + ERROR_MESSAGE()
		end catch
	end

	-- create the related schema owned by this role
	exec setupSchema @roleName, @RoleName
END
GO

