SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[createLinkedInspectionSetID]
	@SurveyId int
	, @InspType nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
begin try


	declare @inspSetID int
	declare @SurveyYEar int

	select @SurveyYear = svyYear
		FROM SchoolSurvey SS
			WHERE ssId = @SurveyID

	Select @inspsetID = inspsetID
	from SurveyInspectionSet SIS
	INNER JOIN SchoolSurvey SS
		ON SIS.svyYear = SS.svyYear
	WHERE SS.ssID = @SurveyID
		AND SIS.intyCode = @InspType

	if @inspSetId is null begin

		begin transaction
		-- insert the Inspection set first
		INSERT INTO InspectionSet
		 ([inspsetName]
           ,[inspsetType]
           ,[inspsetYear]
		 )
		 Values ('School Survey ' + cast(@SurveyYear as nchar(4))
			, @InspType
			, @SurveyYear
		)

		select @inspsetID = SCOPE_IDENTITY()	-- 1 3 2010 i learned a new word

		INSERT INTO SurveyInspectionSet
		(svyYEar
		, intyCode
		, inspSetID
		)
		VALUES (@SurveyYear
		, @InspType
		, @inspsetID
		)
		commit transaction

    end
	Select @inspSetID
	return @inspSetID
end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

