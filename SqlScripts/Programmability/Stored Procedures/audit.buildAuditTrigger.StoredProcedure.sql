SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 05 2010
-- Description:	generate code for an audit trigger
-- =============================================
CREATE PROCEDURE [audit].[buildAuditTrigger]
	-- Add the parameters for the stored procedure here
	@TableName sysname
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @SQL nvarchar(4000)
declare @SQLD nvarchar(4000)
declare @SQLI nvarchar(4000)

select @Sql = ''
SELECT @SQLD = ''
Select @SqlI = ''


SELECT
	@SQL = @SQL + 'when ' + cast(ORDINAL_POSITION as nvarchar(2)) + ' then ' + quotename(COLUMN_NAME,'''') + char(13) + char(10)
	, @SQLD = @SQLD + 'when ' + cast(ORDINAL_POSITION as nvarchar(2)) + ' then cast(D.' + COLUMN_NAME +' as nvarchar(50))' + char(13) + char(10)
	, @SQLI = @SQLI + 'when ' + cast(ORDINAL_POSITION as nvarchar(2)) + ' then cast(I.' + COLUMN_NAME +' as nvarchar(50))' + char(13) + char(10)

from information_schema.columns
WHERE TABLE_NAME = @TAbleName

Select @SQL = 'case num' + char(13) + char(10) + @SQL + ' end ColumnName'
Select @SQLD = 'case num' + char(13) + char(10) + @SQLD + ' end dValue'
Select @SQLI = 'case num' + char(13) + char(10) + @SQLI + ' end IValue'

Select aSQL = 'Select '
print @SQL

print @SQLD

print @SQLI

END
GO

