SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Aurion].[sendAppointments]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
	, @SendBatch int = 0
	, @OverrideStartDate datetime = null
	, @Authority nvarchar(10) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	-- we need to find the last pay rate for the employee we are appointing
	-- if the salary rate on the appointment is null
	-- if an override start date has been specified,
	-- find the last pay period that starts before that date
	-- if no override start, find the last payperiod that starts before the
	-- appointment date

	SET NOCOUNT ON;

	Select
	tpsPayroll
	, tpsPeriodStart
	, tpsPeriodEnd
	, tpsSalaryPoint
	INTO #payrates
	FROM
	(Select
		tpsPayroll
		, tpsPeriodStart
		, tpsPeriodEnd
		, tpsSalaryPoint
		, row_number() OVER (Partition BY tpsPayroll ORDER BY tpsPeriodEnd DESC) row
	FROM TeacherPayslips
	) sub
	WHERE row = 1


	Select
		TI.tPayroll
		, TA.*
		--, 'L' + spExternal spExternal
		, case when SP.spCode like '1.%' then 'TL'
				when SP.spCode like '2.%' then 'TL'
				else 'L'
			end +  SP.spExternal spExternal
		, isnull(@OverrideStartdate, taDate) ApptDate
		, case E.estpScope when 'A' then estpAuth else schAuth end authCode
	FROM TeacherIDentity TI
		INNER JOIN TeacherAppointment TA
			ON TI.tID = TA.tID
		INNER JOIN Establishment E
			ON TA.estpNo = E.estpNo
		LEFT JOIN Schools S
			ON E.schNo = S.schNo
		LEFT JOIN #payrates P
			ON TI.tPayroll = P.tpsPayroll
		LEFT JOIN lkpSalaryPoints SP
			on SP.spCode = isnull(P.tpsSalaryPoint,TA.spCode)
	WHERE
		(E.estpFlag is not null or @SendAll = 1)
		and taEndDate is null
		and TI.tPayroll is not null			-- sad but true
		and (@Authority is null
			or @Authority = case E.estpScope when 'A' then estpAuth else schAuth end)
	ORDER BY TA.taID	-- output them in the order they were processed, in case of sequential issues with vacancy

END
GO

