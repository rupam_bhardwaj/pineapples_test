SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 02 2009
-- Description:	Return the XML grid representing the resources
-- =============================================
CREATE PROCEDURE [dbo].[xmlResourceProvision]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) = '',
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select R.*
into #R
from ResourceProvision R
		inner join SchoolSurvey S
			on R.ssId = S.ssID
	WHERE S.schNo = @schNo and S.svyYEar = @SurveyYEar

	order by rpType, rpSubject, rpLevel


declare @xmlResult xml

set @xmlResult =
(
Select Tag
, Parent
, Category as [ResourceProvision!1!category]
, SchoolLevel as [ResourceProvision!1!schoolLevel]
, Item as [Resource!2!item]
, ClassLevel as [data!3!classLevel]

, yearSupplied as [data!3!yearSupplied]
, number as [data!3!number]

from

(Select DISTINCT
	1 as Tag
	, null as Parent
	, rpType as Category
	, null as Item
	, null as classLevel
	, rpSchLevel as SchoolLevel
	, null as yearSupplied
	, null as number

	from #r

UNION ALL
Select DISTINCT
	2 as Tag
	, 1 as Parent
	, rpType as Category
	, rpSubject as Item
	, null as classLevel
	, rpSchLevel as SchoolLevel
	, null as yearSupplied
	, null as number

	from #r

UNION ALL
Select
	3 as Tag
	, 2 as Parent
	, rpType as Category
	, rpSubject as Item
	, rpLevel as classLevel
	, rpSchLevel as SchoolLevel
	, rpYear as yearSupplied
	, rpNum as number

	from #r

) u
ORDER BY

  [ResourceProvision!1!Category]
,  [ResourceProvision!1!schoolLevel]
, [Resource!2!Item]
, [data!3!classLevel]
, Tag

for xml explicit
)


select @xmlResult
drop table #R
END
GO
GRANT EXECUTE ON [dbo].[xmlResourceProvision] TO [pSchoolRead] AS [dbo]
GO

