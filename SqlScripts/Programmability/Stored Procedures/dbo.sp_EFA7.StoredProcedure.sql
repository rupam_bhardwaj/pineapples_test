SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 8 2009
-- Description:	EFA7 calculation
-- =============================================
CREATE PROCEDURE [dbo].[sp_EFA7]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- get estimated enrolments into a table - we need these later
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()


SELECT      EE.LifeYear AS [Survey Year]
			, L.secCode
			, sum(enM) as EnrolM
			, sum(enF) as EnrolF
			, isnull(sum(enM) ,0) + isnull(sum(enF),0) as Enrol

into #sectorTotals
			from
            #ebse EE INNER JOIN
                      dbo.Enrollments E ON EE.bestssID = E.ssID INNER JOIN
                      dbo.lkpLevels L ON E.enLevel = L.codeCode

			group by
			EE.LifeYEar
			, secCode

select [Survey Year]
	, sum(S.EnrolM) EnrolM
	, sum(S.EnrolF) EnrolF
	, sum(S.Enrol) Enrol

into #yearTotals

from #sectorTotals S
	group by [Survey Year]


/*
-- begin by collecting the cost centre amounts
-- th union query handles 3 cases:
-- 1) specific postings to a sector
-- 2) pro-rated across all sectors
-- 3) not posted to any sector
*/
-- the outer level of the query ormalises Actual and Budget
Select
	[Survey Year]
	, [CostCentreCode]
	, [Cost Centre]
	, SectorCode
	, ProRated
	, GNP
	, GNPPerCapita
	, case num when 1 then 'Actual' else 'Budget' end [View]
	, case num when 1 then GovtExpActual else GovtExpBudget end [Total Govt Exp]
	, case num when 1 then [Actual Total] else [Budget Total] end [Sector Total]
	, case num when 1 then [Actual Recurrent] else [Budget Recurrent] end [Sector Recurrent]
from
	(Select num from MetaNumbers WHERE num in (1,2)) M
CROSS JOIN
(
-- this level of the query groups and total the union query
Select U.[Survey Year]
	, U.costCentrecode
	, U.[Cost Centre]
	, U.sectorCode
	, U.ProRated
	, G.fnmGNP GNP
	, case when fnmGNPCurrency is null then 1 else fnmGNPXChange end * G.fnmGNPCapita GNPPerCapita
	, G.fnmExpTotA GovtExpActual
	, G.fnmExpTotB GovtExpBudget
	, G.fnmGNPCurrency
	, G.fnmGNPXChange
	, sum(U.[Actual Recurrent]) [Actual Recurrent]
	, sum(U.[Budget Recurrent]) [Budget Recurrent]
	, sum(U.[Actual Total]) [Actual Total]
	, sum(U.[Budget Total]) [Budget Total]
	, sum(U.SectorEnrolment) [Sector Enrolment]
	, sum(U.SectorEnrolmentPerc) [Sector Alloc]


from
-- case 1 - all goes to one sector
(
	SELECT
		EdExpenditure.xedYear as [Survey Year]
		, ccCode as [CostCentreCode]
		, ccDescription as [Cost Centre]
		, ccSector as SectorCode
		, ccProRate as ProRated
		, EdExpenditure.xedCurrentA as [Actual Recurrent]
		, EdExpenditure.xedCurrentB as [Budget Recurrent]
		, xedTotA as [Actual Total]
		, xedTotB as [Budget Total]
		, null as SectorEnrolment
		, null as SectorEnrolmentPerc
	FROM EdExpenditure
		INNER JOIN CostCentres ON EdExpenditure.xedEdLevel = CostCentres.ccCode
	WHERE ccSector is not null

UNION

-- case 2 - prorated
	SELECT EdExpenditure.xedYear AS [Year]
		, ccCode
		, ccDescription
		, ccSector AS Sector
		, ccProRate
		, [xedCurrentA]*[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] AS CurrentA
		, [xedCurrentB]*[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] AS CurrentB
		, xedTotA*[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] as [Actual Total]
		, xedTotB *[sectorEnrolments].[enrol]/[yearEnrolments].[enrol] as [Budget Total]
		, null as SectorEnrolment
		, null as SectorEnrolmentPerc
	FROM EdExpenditure
		INNER JOIN CostCentres ON EdExpenditure.xedEdLevel = CostCentres.ccCode
		INNER JOIN #sectorTotals sectorEnrolments ON EdExpenditure.xedYear = sectorEnrolments.[Survey Year]
		INNER JOIN #YearTotals yearEnrolments ON EdExpenditure.xedYear = yearEnrolments.[Survey Year]
	WHERE
		ccSector Is Null AND ccProRate=1


UNION

-- case 3 ignored becuase ccPRoRAte is 2 and the sector code is null, so sector is left blank in the output

	Select EdExpenditure.xedYear as [Survey Year]
		, ccCode as [CostCentreCode]
		, ccDescription as [Cost Centre]
		, null as Sector
		, ccProRate as ProRated
		, EdExpenditure.xedCurrentA as [Actual Recurrent]
		, EdExpenditure.xedCurrentB as [Budget Recurrent]
		, xedTotA as [Actual Total]
		, xedTotB as [Budget Total]
		, null as SectorEnrolment
		, null as SectorEnrolmentPerc
	FROM EdExpenditure
		INNER JOIN CostCentres ON EdExpenditure.xedEdLevel = CostCentres.ccCode
	WHERE
	ccSector Is Null AND ccProRate=2

UNION

-- add on to the end of this a record for the enrolment

	Select S.[Survey Year]
		, null as CostCentre
		, null as CostCentreDescription
		, secCode sectorCode
		, null as ProRated
		, null as AR
		, null as BR
		, null as AT
		, null as BT
		, S.Enrol
		, S.Enrol / Y.Enrol

	from #SectorTotals S
		INNER JOIN #yearTotals Y on S.[Survey Year] = Y.[Survey Year]
) U
INNER JOIN GovtExpenditure G on U.[Survey Year] = G.fnmYear
group by
	U.[Survey Year]
	, U.costCentrecode
	, U.[Cost Centre]
	, U.sectorCode
	, U.ProRated
	, G.fnmGNP
	, G.fnmGNPCapita
	, G.fnmExpTotA
	, G.fnmExpTotB
	, G.fnmGNPCurrency
	, G.fnmGNPXChange
) main

drop table #sectorTotals
drop table #yearTotals
drop table #ebse

END
GO

