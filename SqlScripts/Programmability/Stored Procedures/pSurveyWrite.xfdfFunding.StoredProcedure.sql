SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	create or update a school survey from xfdf
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfFunding]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Funding block

	<field name="Funding">
		<field name="Rev">
			<field name="Fundraising" />
			<field name="DonationPriv" />
			<field name="DonationChurch" />
			<field name="DonationOther" />
			<field name="GovGrant" />
			<field name="Total" />
		</field>
		<field name="Exp">
			<field name="Supplies" />
			<field name="Books" />
			<field name="Other" />
			<field name="Total" />
		</field>
		<field name="Total" />
	</field>

this is updated directly onto the SchoolSurvey Table
*/
UPDATE schoolSurvey
	SET ssFundRaising = RevFundraising
	, ssDonationPriv = RevDonationPriv
	, ssDonationChurch = RevDonationChurch
	, ssDonationOther = RevDonationOther
	, ssGovtGrant = RevGovtGrant

	, ssExpSupplies = ExpSupplies
	, ssExpBooks = ExpBooks
	, ssExpOther = ExpOther
FROM OPENXML(@idoc, '/x:field',2)		-- base is the Funding Node
WITH
(
	RevFundraising		money			'x:field[@name="Rev"]/x:field[@name="Fundraising"]/x:value'
	, RevDonationPriv	money			'x:field[@name="Rev"]/x:field[@name="DonationPriv"]/x:value'
	, RevDonationChurch	money			'x:field[@name="Rev"]/x:field[@name="DonationChurch"]/x:value'
	, RevDonationOther	money			'x:field[@name="Rev"]/x:field[@name="DonationOther"]/x:value'
	, RevGovtGrant		money			'x:field[@name="Rev"]/x:field[@name="GovtGrant"]/x:value'
	, ExpSupplies		money			'x:field[@name="Exp"]/x:field[@name="Supplies"]/x:value'
	, ExpBooks			money			'x:field[@name="Exp"]/x:field[@name="Books"]/x:value'
	, ExpOther			money			'x:field[@name="Exp"]/x:field[@name="Other"]/x:value'
) X
WHERE SchoolSurvey.ssID = @SurveyID


exec audit.xfdfInsert @SurveyID, 'Survey updated','Funding'
END
GO

