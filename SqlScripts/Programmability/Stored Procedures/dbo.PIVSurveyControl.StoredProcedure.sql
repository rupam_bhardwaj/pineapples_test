SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Data used by PIVSurveyControl pivot table
-- =============================================
CREATE PROCEDURE [dbo].[PIVSurveyControl]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT SurveyControl.saYear AS [Survey Year],
 Survey.svyCensusDate AS CensusDate,
Survey.svyCollectionDate AS CollectionDate,
SurveyControl.schNo AS School,
Schools.schName AS [Name of School],
SurveyControl.saSent AS SentDate,
case when saSent is null then 0 else 1 end Sent,

-- collected
saCollected AS CollectedDate,
case when saCollected is null then 0 else 1 end Collected,
case when saCollected <= svyCollectionDate then 1 else 0 end CollectedOnTime,

-- received
saReceived AS ReceivedDate,
convert(nvarchar(7), saReceived , 120) as ReceivedDateYYYYMM,
case when saReceived is null then 0 else 1 end Received,
case when saReceived <= saReceivedTarget then 1 else 0 end ReceivedOnTime,
case when saReceived>saReceivedTarget then datediff(d,saReceivedTarget, saReceived) else null end ReceivedDaysLate,
datediff(d,saSent, saReceived) AS ReceivedDaysSinceSent,
-- entered
saEntered AS EnteredDate,
convert(nvarchar(7), saEntered , 120) AS EnteredDateYYYYMM,
case when saEntered is null then 0 else 1 end Entered,
datediff(d,saSent, saEntered) AS EnteredDaysSinceSent,
datediff(d,saReceived, saEntered) AS EnteredDaysSinceReceived,
--audit
case when saAudit = 1 then 1 else 0 end as AuditReq,
saAuditDate AS AuditedDate,
case when saAuditDate is null then 0 else 1 end Audited,
saAuditResult AS AuditResult,
saFileLocn AS FileLocn,
ebse.surveyDimensionssID,
DSSNY.*
FROM Schools
INNER JOIN SurveyControl
	ON Schools.schNo = SurveyControl.schNo
INNER JOIN Survey
	ON Survey.svyYear = SurveyControl.saYear
LEFT JOIN #ebse ebse
ON (SurveyControl.saYear = ebse.LifeYear) AND (SurveyControl.schNo = ebse.schNo)
LEFT JOIN DimensionSchoolSurveyNoYear DSSNY
	ON Schools.schNo = DSSNY.[School No]


drop table #ebse
END
GO

