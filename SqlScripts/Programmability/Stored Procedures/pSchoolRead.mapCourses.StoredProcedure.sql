SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1/6/2009
-- Description:	Course Map
-- =============================================
CREATE PROCEDURE [pSchoolRead].[mapCourses]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50) = '',
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- first, figure out the school type in the year

	declare @paramSchoolType nvarchar(5)
	declare @SurveyForm nvarchar(50)

-- figure out the school type
--
	Select @paramSchoolType = ssSchType from SchoolSurvey WHERE schNo = @SchoolNo AND svyYear = @SurveyYear
	if @paramSchoolType is null
		Select @paramSchoolType = schType from Schools WHERE schNo = @SchoolNo

	-- now the survey that is used for the school type in the year

	Select @SurveyForm = ytForm
		from SurveyYearSchoolTypes
		where stCode = @paramSchoolType
			and svyYear = @SurveyYear;

    -- Now we have the school type year and survey form, we look for matches

		with X as
		( Select top 1 sjSurvey
		, stCode
		, case when sjSurvey = @SurveyForm then 8 else 0 end
		+
		case when stCode = @paramSchoolType then 4 else 0 end
		+ case when sjSurvey is null then 2 else 0 end
		+ case when stCode is null then 1 else 0 end
		Score


		from metaSchoolTypeCoursemap
		order by score desc
		)
		Select M.sjSubject as codeCode, M.sjSubject as codeDescription
		from metaSchoolTypeCoursemap M, X
		where isnull(M.sjSurvey,'') = isnull(X.sjSurvey,'')
		and  isnull(M.stCode,'') = isnull(X.stCode,'')
		and X.Score > 2
		order by M.sjSort
END
GO

