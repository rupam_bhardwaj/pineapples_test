SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 11 2009
-- Description:	get the data area access levels - has to be done with elevated permissions
-- =============================================
CREATE PROCEDURE [common].[getDataAreaAccessLevels]
	-- Add the parameters for the stored procedure here

WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select  RN.DataArea
			, max(case when DataAccess in ('Read','Write') then 1 else 0 end) hasB
			, max(case when DataAccess in ('ReadX','WriteX') then 1 else 0 end) hasX
			, max(case when DataAccess in ('ReadXX','WriteXX') then 1 else 0 end) hasXX
			, max(case when DataAccess in ('Admin') then 1 else 0 end) hasA
			, max(case when DataAccess in ('Ops') then 1 else 0 end) hasO
			from sys.database_principals u
			inner join sys.extended_properties P
				on u.principal_id = P.Major_ID and P.class = 4
			cross apply common.ParsePineapplesRoleName(U.name) RN
			WHERE P.name = 'PineapplesUser' and P.value = 'CoreRole'
				AND rn.dataArea > ''
	GROUP BY RN.DataArea
END
GO

