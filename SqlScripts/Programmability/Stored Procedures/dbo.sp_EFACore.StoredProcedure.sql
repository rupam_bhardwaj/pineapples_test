SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Underlying data for calculation of efa12
-- =============================================
-- 16 10 2012 bdl change temp table to table var to protect against collation conflict
CREATE PROCEDURE [dbo].[sp_EFACore]
(
	@PopulateNY int
	, @SingleYear int = null
	, @SummaryOnly int = 0
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- build estimate best survey enrolments as a temp table
Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()
WHERE (LifeYear in (@SingleYear, @SingleYear + 1) or @SingleYear is null)
-- current years enrolments by level and gender


-----------------------------------------------------------
-- Enrolments
-- 3 groups- This Year, NextYear, Next Year Next Level
-----------------------------------------------------------

declare @efa table
(
	schNo nvarchar(50),
	svyYear int,
	LevelCode nvarchar(20),
	YearOfEd int,
	EnrolM int,
	EnrolF int,
	RepM int,
	RepF int,
	EnrolNYM int,
	EnrolNYF int,
	RepNYM int,
	RepNYF int,
	EnrolNYNextLevelM int,
	EnrolNYNextLevelF int,
	RepNYNextLevelM int,
	RepNYNextLevelF int,
	Estimate smallint,
	[Year Of Data] int,
	[Age of Data] int,
	surveyDimensionssID int,
	[Estimate NY] smallint,
	[Year Of Data NY] int,
	[Age of Data NY] int,
	surveyDimensionssIDNY int
)
-- enrolments this year
INSERT INTO @efa
(
	schNo, svyYear,
	surveyDimensionssID,
	levelCode,
	enrolM, enrolF,
	Estimate, [Year of Data],[Age of Data]
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	EBSE.surveyDimensionssID,
	EL.LevelCode,
	EL.EnrolM,
	EL.enrolF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset
FROM #ebse EBSE
inner join pEnrolmentRead.ssidEnrolmentLevel EL
on EBSE.bestssID = El.ssID

if (@populateNY= 1)
	BEGIN
		-- next year's enrolment at the same level
		INSERT INTO @efa
		(
			schNo, svyYear, levelCode,
			enrolNYM, enrolNYF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveyDimensionssIDNY
		)
		SELECT
			EBSE.schNo,
			EBSE.LifeYear - 1 svyYear,
			EL.LevelCode,
			EL.enrolM AS EnrolNYM,
			EL.EnrolF AS EnrolNYF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveydimensionssID
		FROM #ebse EBSE
		inner join pEnrolmentRead.ssidEnrolmentLevel EL
		on EBSE.bestssID = El.ssID
		--INNER JOIN SchoolLifeYears SLY
		--	ON EBSE.schNo = SLY.schNo AND EBSE.LifeYear = SLY.svyYear


		-- enrolments next year next level
		INSERT INTO @efa
		(
			schNo, svyYear, levelCode,
			enrolNYNextLevelM, enrolNYNextLevelF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveyDimensionssIDNY
		)

		SELECT
			EBSE.schNo,
			EBSE.LifeYear-1 svyYear,
			L2.codeCode as LevelCode,
			EL.enrolM AS EnrolNYM,
			EL.EnrolF AS EnrolNYF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveyDimensionssID
		FROM #ebse EBSE
			inner join pEnrolmentRead.ssidEnrolmentLevel EL
				on EBSE.bestssID = El.ssID
			inner join lkpLevels L1
				on L1.codeCode = EL.LevelCode,
		-- find the 'next level' based on lvlYear, the year of education
		-- default path levels is a way to get a unique level
		-- needs to be considered how this would work in a school type not using the default path level levels

			common.LISTDefaultPathLevels LDPL
				inner join lkpLevels L2
				on L2.codeCode = LDPL.levelCode
		WHERE L2.lvlYear = L1.lvlYear-1
	END
-----------------------------------------------
-- Repeaters
-- this year, next year, next year next level
-----------------------------------------------

INSERT INTO @efa
(
	schNo, svyYear, levelCode,
	repM, repF,
	Estimate, [Year of Data],[Age of Data],
	surveyDimensionssID
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	RL.ptLevel,
	RL.repM,
	RL.repF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset,
	EBSE.surveydimensionssID
FROM #ebse EBSE
inner join pEnrolmentRead.ssidRepeatersLevel RL
on EBSE.bestssID = RL.ssID

if (@PopulateNY = 1)
	BEGIN
		INSERT INTO @efa
		(
			schNo, svyYear, levelCode,
			repNYM, repNYF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveyDimensionssIDNY
		)
		SELECT
			EBSE.schNo,
			EBSE.LifeYear-1 svyYear,
			RL.ptLevel,
			RL.repM,
			RL.repF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveyDimensionssID

		FROM #ebse EBSE
		inner join pEnrolmentRead.ssidRepeatersLevel RL
		on EBSE.bestssID = RL.ssID

		-- Repeaters next year next level
		INSERT INTO @efa
		(
			schNo, svyYear, levelCode,
			repNYNextLevelM, repNYNextLevelF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveydimensionssIDNY
		)
		SELECT
			EBSE.schNo,
			EBSE.LifeYear-1 svyYear,
			L2.codeCode as LevelCode,
			RL.repM,
			RL.repF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveyDimensionssID

		FROM #ebse EBSE
			inner join pEnrolmentRead.ssidRepeatersLevel RL
				on EBSE.bestssID = RL.ssID
			inner join lkpLevels L1
				on L1.codeCode = RL.ptLevel,
		-- find the 'next level' based on lvlYear, the year of education
		-- default path levels is a way to get a unique level
		-- needs to be considered how this would work in a school type not using the default path level levels

			common.LISTDefaultPathLevels LDPL
				inner join lkpLevels L2
				on L2.codeCode = LDPL.levelCode
		WHERE L2.lvlYear = L1.lvlYear-1
	END

-- now return the sum across these

if @SummaryOnly = 0 begin
	Select
		D.schNo,
		D.svyYear [Survey Year],
		-- we have to make sure there is a surveydimensionssid, even if there is only NY data
		isnull(max(surveyDimensionssID),max(surveyDimensionssIDNY)) surveyDimensionssID,
		LevelCode,
		lvlYear YearOfEd,
		sum(enrolM) as EnrolM,
		sum(enrolF) as enrolF,
		sum(EnrolNYM) AS EnrolNYM,
		sum(EnrolNYF) AS EnrolNYF,
		sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
		sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,

		--( sum(enrolM) + sum(enrolNYM) * max([Age Of Data])) / ( max([Age of Data]) + 1 ) SlopedEnrolM,

		sum(RepM) AS RepM,
		sum(RepF)  AS RepF,
		sum(RepNYM) AS RepNYM,
		sum(RepNYF)  AS RepNYF,
		sum(RepNYNextLevelM)  AS RepNYNextLevelM,
		sum(RepNYNextLevelF)  AS RepNYNextLevelF,
		max(Estimate) as Estimate,
		max([Year of Data]) as [Year of Data],
		max([Age of Data]) as [Age of Data],
		max([Estimate NY]) as [Estimate NY],
		max([Year of Data NY]) as [Year of Data NY],
		max([Age of Data NY]) as [Age of Data NY]
	from @efa D
	INNER JOIN lkpLevels L
	on D.levelCode = L.codeCode
	WHERE (D.svyYear = @SingleYear or @SingleYear is null)
	group by  schNo, svyYear, LevelCode,lvlYear
end

if @SummaryOnly = 1 begin
	print 'before slope'
	SELECT svyYear [Survey Year]
		, yearOfEd
		, sum(SlopedEnrolM) slopedEnrolM
		, sum(slopedEnrolF) slopedEnrolF
		, sum(isnull(slopedEnrolM,0) + isnull(slopedEnrolF,0)) slopedEnrol
	INTO #slope
	FROM
	(
		SELECT
			D.schNo,
			D.svyYear ,
			sum(enrolM) M,
			-- we have to make sure there is a surveydimensionssid, even if there is only NY data
			lvlYear yearOfEd,


			round(
				isnull(
						case when max([Age of Data]) > 0 and max([Estimate NY]) = 0 and min([Estimate NY]) = 0 then
							( isnull(sum(enrolM),0) + isnull(sum(enrolNYM),0) * isnull(max([Age Of Data]),0)) / cast(( isnull(max([Age of Data]),0) + 1 ) as float)
						else sum(enrolM)
					end
				, 0) --isnull
			,0) -- round
			 SlopedEnrolM,
			round(
				isnull(
						case when max([Age of Data]) > 0 and max([Estimate NY]) = 0 and min([Estimate NY]) = 0 then
							( isnull(sum(enrolF),0) + isnull(sum(enrolNYF),0) * isnull(max([Age Of Data]),0)) / cast(( isnull(max([Age of Data]),0) + 1 ) as float)
						else sum(enrolF)
					end
				, 0) --isnull
			,0) -- round
			 SlopedEnrolF
	FROM
		 @efa D
		 INNER JOIN lkpLevels L
		 on D.levelCode = L.codeCode
		WHERE (D.svyYear = @SingleYear or @SingleYear is null)
		group by  schNo, svyYear, lvlYear
	) sub
	GROUP BY svyYear, YearOfEd

	print 'after slope'


	SELECT sub.*
	, slopedEnrolM
	, slopedEnrolF
	, slopedEnrol
	FROM
	(
		Select
			D.svyYear [Survey Year],
			-- we have to make sure there is a surveydimensionssid, even if there is only NY data
			lvlYear YearOfEd,
			sum(enrolM) as EnrolM,
			sum(enrolF) as enrolF,
			sum(isnull(enrolM,0) + isnull(enrolF,0)) Enrol,

			sum(EnrolNYM) AS EnrolNYM,
			sum(EnrolNYF) AS EnrolNYF,
			sum(isnull(enrolNYM,0) + isnull(enrolNYF,0)) EnrolNY,

			sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
			sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,

			sum(RepM) AS RepM,
			sum(RepF)  AS RepF,
			sum(isnull(RepM,0) + isnull(RepF,0)) as Rep,

			sum(RepNYM) AS RepNYM,
			sum(RepNYF)  AS RepNYF,
			sum(isnull(RepNYM,0) + isnull(RepNYF,0)) as RepNY,

			sum(RepNYNextLevelM)  AS RepNYNextLevelM,
			sum(RepNYNextLevelF)  AS RepNYNextLevelF,
			sum(isnull(RepNYNextLevelM,0) + isnull(RepNYNextLevelF,0)) as RepNYNextLevel,


	-- work out the intakes for current year, Next Year, and Next Year next level

			sum(isnull(EnrolNYM,0) - isnull(repNYM,0)) intakeNYM,
			sum(isnull(EnrolNYF,0) - isnull(repNYF,0)) intakeNYF,
			sum(isnull(EnrolNYM,0) - isnull(repNYM,0)
				+ isnull(EnrolNYF,0) - isnull(repNYF,0)
				) intakeNY,

			sum(isnull(EnrolNYNextLevelM,0) - isnull(repNYNextLevelM,0)) intakeNYNextLevelM,
			sum(isnull(EnrolNYNextLevelF,0) - isnull(repNYNextLevelF,0)) intakeNYNextLevelF,
			sum(isnull(EnrolNYNextLevelM,0) - isnull(repNYNextLevelM,0)
				+ isnull(EnrolNYNextLevelF,0) - isnull(repNYNextLevelF,0)
				) intakeNYNextLevel,


			sum(case when Estimate=1 then enrolM else null end) as EstimateEnrolM,
			sum(case when Estimate=1 then enrolF else null end) as EstimateEnrolF,
			sum(case when Estimate=1 then isnull(enrolM,0) + isnull(EnrolF,0) else null end) as EstimateEnrol

		from @efa D
			INNER JOIN lkpLevels L
			on D.levelCode = L.codeCode
			INNER JOIN Survey S
			ON D.svyYEar = S.svyYEar
			WHERE (D.svyYear = @SingleYear or @SingleYear is null)
			group by  D.svyYear, lvlYEar
		) sub
		INNER JOIN #slope S
		ON sub.[Survey Year] = S.[Survey Year]
		AND sub.YearOfEd = S.YearOfED

end

DROP TABLE #ebse

END
GO
GRANT EXECUTE ON [dbo].[sp_EFACore] TO [pSchoolRead] AS [dbo]
GO

