SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 02 2010
-- Description:	Save a regession projection
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[makeScenarioTrend]
	-- Add the parameters for the stored procedure here
	@ScenarioID int = null
	, @ScenarioName nvarchar(50) = null
	, @StartYear int
	, @EndYear int
	, @IgnoreBefore int = null
	, @SchoolNo nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	CREATE TABLE #saveRegr

	(
		schNo nvarchar(50)
		, svyYear int
		, Slope float
		, Intercept float
		, Projection int
		, Enrol int
		, EnrolEstimate int
	)

	INSERT INTO #saveRegr
	EXEC pEnrolmentRead.regressionProjection @SchoolNo, @EndYEar, @IgnoreBefore

	declare @Params as XML

	Select @Params =
	(
	Select
		'Trend' [@Tool]
		,@StartYear [@StartYear]
		, @EndYear [@EndYear]
		, isnull(@IgnoreBefore,0) [Params/@ignoreBefore]
	FOR XML PATH('Projection')
	)
	-- are we operating on an existing scenario?
	If @ScenarioID is null begin
		INSERT INTO EnrolmentScenario
		(escnName
		, escnSplit
		, escnLevelSplit
		, escnYearStart
		, escnYearEnd
		, escnParams
		, escnSource
		)
		Select @ScenarioName
		, 'S'
		, 'L'		-- has a level split
		, @StartYear
		, @EndYear
		, @Params
		, 'Trend'
		Select @ScenarioID = @@IDENTITY
	end


	-- DELETE any EnrolmentProjection records matching the SCenarioID, selected schools

	DELETE from EnrolmentProjection
	WHERE escnID = @ScenarioID
		AND (schNo = @SchoolNo or @schoolNo is null)

	INSERT INTO EnrolmentProjection
	( escnID
		, epYEar
		, SchNo
		, epAggregate
	)
	SELECT @ScenarioID
	, svyYear
	, SchNo
	, Projection
	FROM #saveRegr
	WHERE svyYear <= @EndYEar
	AND (svyYear >= @StartYEar or @StartYear is null)


	-- now to apportion between levels... must be relatively hit /miss
	-- but we'll take the level breakdown of the last year of actuals, and keep those %
	-- on all projections


	Select schNo, ssEnrol, enLevel, sum(isnull(enM,0) + isnull(enF,0)) LevelEnrol
	INTO #levelPErc
	from
	(
	Select schNo, svyYear,
	ssID, ssEnrol
	, row_number() OVER (PARTITION by schNo ORDER BY svyYEar DESC) Row
	from SchoolSurvey
	WHERE ssID in (Select ssID from Enrollments)
	) sub
	INNER JOIN Enrollments E
	ON sub.ssID = E.ssID
	WHeRE row = 1
	GROUP BY schNo, ssEnrol, enLevel

	INSERT INTO EnrolmentProjectionData
	(epID
	, epdU
	, epdLevel
	)
	SELECT epID
	, round(cast((Projection * LevelEnrol) as float)/ ssEnrol, 0)
	, enLevel
	FRom #saveRegr
	INNER JOIN EnrolmentProjection
		ON #saveRegr.schNo = EnrolmentProjection.schNo
		AND #saveRegr.svyYear = EnrolmentProjection.epYear
		AND EnrolmentProjection.escnID = @ScenarioID
	INNER JOIN #levelPerc
		ON #levelPErc.schNo = #saveRegr.schNo

	Select EP.schNo, EP.epYear, EP.epID, EP.epAggregate, sum(epdU) LevelTotals,EP.epAggregate - sum(epdU) Adjustment
	INTO #toAdjust
	FROM EnrolmentProjection EP
	INNER JOIN  EnrolmentProjectionData EPD
		ON EPD.epID = EP.epID
	WHERE  EP.escnID = @ScenarioID
	GRoup BY EP.schNo, EP.epYear, EP.epID, EP.epAggregate
	HAVING sum(epdU) <> EP.epAggregate

	UPDATE EnrolmentProjectionData
	SET epdU = epdU + Adjustment
	FROM
	EnrolmentProjectionData
		INNER JOIN #toAdjust
			ON EnrolmentProjectionData.epID = #toAdjust.epID
	INNER JOIN
	(Select  epdID, Row_number() OVER(PARTITION BY epID ORDER BY epdU DESC) Big
	from EnrolmentProjectionData
	) Biggest
	ON EnrolmentProjectionData.epdID = Biggest.epdID
	AND Biggest.Big = 1


	DROP TABLE #toAdjust
	DROP TABLE #levelPErc
	DROP TABLE #saveRegr

--- OUTPUTS -------------------------------------------------
	-- recordset returns some stats about the scenario

	Select ES.escnID, escnName
		, escnSource
		, escnSplit
		, escnLevelSplit
		, escnYearStart
		, escnYearEnd
		, count(DISTINCT EP.schNo) NumSchools
		, sum(case when epYEar = escnYearStart then epdSum else null end) enrolStart
		, sum(case when epYEar = escnYearEnd then epdSum else null end) enrolEnd		-- last year
	FROM EnrolmentScenario ES
		INNER JOIN EnrolmentProjection EP
			ON ES.escnID = EP.escnID
		INNER JOIN EnrolmentProjectionData EPD
			ON EP.epID = EPD.epID
	WHERE ES.escnID = @ScenarioID
	GROUP BY ES.escnID, escnName
		, escnSource
		, escnSplit
		, escnLevelSplit
		, escnYearStart
		, escnYearEnd

end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

