SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 05 2010
-- Description:
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[PlanAuthorityTotals]
	-- Add the parameters for the stored procedure here
	@EstYear int = 0,
	@ColumnType int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @str nvarchar(4000)
	declare @strTot nvarchar(4000)
	declare @strSum nvarchar(4000)

	declare @ColumnField sysname
	declare @BlankToken nvarchar(20)

	Select @BlankToken = '(blank)'

if @EstYear = 0
	Select @EstYear = Year(common.today())

CREATE TABLE #extbColumnNames
(
	col nvarchar(50)
	, sorter int
)
if (@ColumnType = 0) begin

	INSERT INTO #extbColumnNames
	Select DISTINCT rgSalaryLevelRange
	, 1
	FROM RoleGrades
	ORDER BY
	rgSalaryLevelRange

	Select @ColumnField = 'rgSalaryLevelRange'
end

if (@ColumnType = 1) begin
	INSERT INTO #extbColumnNames
	Select DISTINCT codeCode
	, row_number() OVER (ORDER BY secSort, R.secCode, R.codeSort, codeCode)
	FROM lkpTeacherRole R
	LEFT JOIN EducationSectors ES
		ON R.secCode = ES.secCode

	WHERE codeCode in
	(Select roleCode from RoleGrades

		WHERE rgCode in (Select estpRoleGrade from Establishment)
		OR rgCode in (Select estrRoleGrade from SchoolEstablishmentRoles)
	)

	Select @ColumnField = 'roleCode'

end

if (@ColumnType = 2) begin
	INSERT INTO #extbColumnNames
	Select DISTINCT secCode
	, row_number() OVER (ORDER BY secSort)
	FROM EducationSectors

	Select @ColumnField = 'secCode'
end


INSERT INTO #extbColumnNames
VALUES (@BlankToken, 0)


	Select  @str = isnull(@str,'') + quotename(col,'[') + ', '
		, @strTot = isnull(@strTot,'') +' sum('+ quotename(col,'[') + ')' + quotename(col,'[') + ', '
		, @strSum = isnull(@strSum,'') + 'isnull(' + quotename(col,'[')  + ',0) + '
	from #extbColumnNames
	ORDER BY sorter, col


-- get rid of the trailing spaces and commas
	Select @str = left(@str, len(rtrim(@str)) - 1)
	Select @strTot = left(@strTot, len(rtrim(@strTot)) - 1)
	Select @strSum = left(@strSum, len(rtrim(@strSum)) - 1)

	declare @strSQL nvarchar(4000)
	Select @strSQL =
	'

Select isnull(pt.schAuth,#####) rowCode, isnull(authName,#####) rowName, ***** Total, pt.*
INTO #planAuthTotals
from
(
Select isnull(estAuth, schAuth) schAuth, isnull([@@@@@],#####) [@@@@@], estrCount from SchoolEstablishment SE
INNER JOIN SchoolEstablishmentRoles SER
on SE.estID = SER.estID
LEFT JOIN Schools S
	ON S.schNo = SE.schNo
LEFT JOIN RoleGrades RG
ON RG.rgCode = SER.estrRoleGrade
LEFT JOIN lkpTeacherRole R
	ON RG.roleCode = R.codeCode
WHERE estYear = @P1
) R
PIVOT
(
	sum(estrCount)
FOR [@@@@@] in (%%%%%)
) pt
LEFT JOIN TRAuthorities A
ON pt.schAuth = A.authCode
ORDER BY rowName;
Select * from #PlanAuthTotals;
Select $$$$$ , sum(Total) Total
FROM #PlanAuthTotals

'
select @strSQL = replace(@strSQL,'#####', quotename(@BlankToken,''''))

select @strSQL = replace(@strSQL,'@@@@@', @ColumnField)

select @strSQL = replace(@strSQL,'%%%%%', @str)

select @strSQL = replace(@strSQL,'$$$$$', @strTot)

select @strSQL = replace(@strSQL,'*****', @strSum)

print @strSQL

declare @ParmDefs nvarchar(400)

Select @ParmDefs = N'@P1 int'
exec sp_executesql @strSQL, @ParmDefs, @P1 = @EstYear


Select col from #extbColumnNames ORDER BY sorter, col
DROP TABLE #extbColumnNames

END
GO

