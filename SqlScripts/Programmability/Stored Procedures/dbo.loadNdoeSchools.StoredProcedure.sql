SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of student data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeSchools]
@xml xml
, @filereference uniqueidentifier
, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- totals by education level

declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- CONVENTION: Use the last year
Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),3,4) as int) + 1
From @xml.nodes('ListObject') as V(v)

Select @districtID = dID
from Districts WHERE dName = @districtName

print @SurveyYear
print @DistrictID

declare @ndoeSchools TABLE
(
RowIndex int
    , SchoolYear					nvarchar(100) NULL
	, State							nvarchar(100) NULL
	, School_Name					nvarchar(100) NULL
    , School_No						nvarchar(100) NULL
    , School_Type					nvarchar(100) NULL
    , School_Budget					nvarchar(100) NULL
    , Date_School_Begins			nvarchar(100) NULL
    , Num_Buildings						int NULL
    , Num_Classrooms					int NULL
    , Num_Classrooms_in_poor_condition  int NULL
    , Electricity					nvarchar(100) NULL
    , Electricity_Source			nvarchar(100) NULL
    , Telephone						nvarchar(100) NULL
    , Fax							nvarchar(100) NULL
    , Copier						nvarchar(100) NULL
    , Internet						nvarchar(100) NULL
    , Computer_Lab					nvarchar(100) NULL
    , Num_Computers					nvarchar(100) NULL
    , Use_of_Technology             nvarchar(100) NULL
	, fileReference					uniqueidentifier

	--
	, ssID							int
)
INSERT INTO @ndoeSchools
(
RowIndex
, SchoolYear
, State
, School_Name
, School_No
, School_Type
, School_Budget
, Date_School_Begins
, Num_Buildings
, Num_Classrooms
, Num_Classrooms_in_poor_condition
, Electricity
, Electricity_Source
, Telephone
, Fax
, Copier
, Internet
, Computer_Lab
, Num_Computers
, Use_of_Technology
, fileReference
)
SELECT
v.value('@Index', 'int') [ RowIndex]
, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                         [SchoolYear]
, nullif(ltrim(v.value('@State', 'nvarchar(100)')),'')                              [State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                        [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                          [School_No]
, nullif(ltrim(v.value('@School_Type', 'nvarchar(100)')),'')                        [School_Type]
, nullif(ltrim(v.value('@School_Budget', 'nvarchar(100)')),'')                      [School_Budget]
, nullif(ltrim(v.value('@Date_School_Begins', 'nvarchar(100)')),'')                 [Date_School_Begins]
, nullif(ltrim(v.value('@Num_Buildings', 'int')),'')								[Num_Buildings]
, nullif(ltrim(v.value('@Num_Classrooms', 'int')),'')								[Num_Classrooms]
, nullif(ltrim(v.value('@Num_Classrooms_in_poor_condition', 'int')),'')				[Num_Classrooms_in_poor_condition]
, nullif(ltrim(v.value('@Electricity', 'nvarchar(100)')),'')                        [Electricity]
, nullif(ltrim(v.value('@Electricity_Source', 'nvarchar(100)')),'')                 [Electricity_Source]
, nullif(ltrim(v.value('@Telephone', 'nvarchar(100)')),'')                          [Telephone]
, nullif(ltrim(v.value('@Fax', 'nvarchar(100)')),'')                                [Fax]
, nullif(ltrim(v.value('@Copier', 'nvarchar(100)')),'')                             [Copier]
, nullif(ltrim(v.value('@Internet', 'nvarchar(100)')),'')                           [Internet]
, nullif(ltrim(v.value('@Computer_Lab', 'nvarchar(100)')),'')                       [Computer_Lab]
, nullif(ltrim(v.value('@Num_Computers', 'nvarchar(100)')),'')                      [#_Computers]
, nullif(ltrim(v.value('@Use_of_Technology', 'nvarchar(100)')),'')                  [Use_of_Technology]
, @fileReference
from @xml.nodes('ListObject/row') as V(v)


-- create any records required in SchoolSurvey
-- but they should not be needed if we have already processed Schools


UPDATE @ndoeSchools
SET ssID = SS.ssID
FROM @ndoeSchools
	INNER JOIN SchoolSurvey SS
		ON SS.schNo = [@ndoeSchools].School_No
		AND SS.svyYear = @SurveyYear

UPDATE SchoolSurvey
SET
ssSchType = schType
, ssAuth = schAuth
, ssElectN = schElectN
, ssElectL = schElectL
, ssNumBuildings = Num_Buildings
, ssNumClassrooms = Num_Classrooms
, ssNumClassroomsPoor = Num_Classrooms_in_poor_condition
, ssSource = @filereference
FROm SchoolSurvey
INNER JOIN @ndoeSchools N
	ON SchoolSurvey.ssID = N.ssID
INNER JOIN Schools S
	ON SchoolSurvey.schNo = S.schNo


INSERT INTO SchoolSurvey
(
svyYear
, schNo
, ssSchType
, ssAuth
, ssElectN
, ssElectL
, ssNumBuildings
, ssNumClassrooms
, ssNumClassroomsPoor
, ssSource
)
Select DISTINCT @SurveyYear
, NDOE.School_No
, schType
, schAuth
, schElectN
, schElectL
, Num_Buildings
, Num_Classrooms
, Num_Classrooms_in_poor_condition
, @filereference

FROM @ndoeSchools NDOE
INNER JOIN Schools S
	ON NDOE.School_No = S.schNo

WHERE NDOE.ssID is null

-- for convenience, put the ssID back on the @ndoeSchools table

UPDATE @ndoeSchools
SET ssID = SS.ssID
FROM @ndoeSchools
	INNER JOIN SchoolSurvey SS
		ON SS.schNo = [@ndoeSchools].School_No
		AND SS.svyYear = @SurveyYear


		-- UPDATE the resource for Power

		DELETE from Resources
		WHERE ssID in (Select ssID from @ndoeSchools)
		AND resName = 'Power Supply'

		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		)
		SELECT ssID
		, 'Power Supply'
		, Electricity_Source
		, case Electricity when 'Yes' then -1 when 'Y' then -1
				when 'No' then 0 when 'N' then 0
				else null end
		FROM @ndoeSchools
		WHERE Electricity is not null or Electricity_Source is not null

		-- ict items
		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		)
		SELECT ssID
		, 'ICT'
		, 'Telephone'
		, case Telephone when 'Yes' then -1 when 'Y' then -1
				when 'No' then 0 when 'N' then 0
				else null end
		FROM @ndoeSchools
		WHERE Telephone is not null

		-- fax
		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		)
		SELECT ssID
		, 'ICT'
		, 'Fax'
		, case Fax when 'Yes' then -1 when 'Y' then -1
				when 'No' then 0 when 'N' then 0
				else null end
		FROM @ndoeSchools
		WHERE Fax is not null

		-- copier
		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		)
		SELECT ssID
		, 'ICT'
		, 'Copier'
		, case Copier when 'Yes' then -1 when 'Y' then -1
				when 'No' then 0 when 'N' then 0
				else null end
		FROM @ndoeSchools
		WHERE Copier is not null

		-- Internet
		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		)
		SELECT ssID
		, 'ICT'
		, 'Internet'
		, case Internet when 'Yes' then -1 when 'Y' then -1
				when 'No' then 0 when 'N' then 0
				else null end
		FROM @ndoeSchools
		WHERE Internet is not null

		-- Computer Lab
		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		)
		SELECT ssID
		, 'ICT'
		, 'Computer Lab'
		, case Computer_Lab when 'Yes' then -1 when 'Y' then -1
				when 'No' then 0 when 'N' then 0
				else null end
		FROM @ndoeSchools
		WHERE Computer_Lab is not null

		-- no of computers alos set resNumber
		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		, resNumber
		)
		SELECT ssID
		, 'ICT'
		, 'Computers'
		, case when Num_Computers > 0 then -1
				else 0 end
		, Num_Computers
		FROM @ndoeSchools
		WHERE Num_Computers is not null

		-- Use of technology is Y/N
		INSERT INTO Resources
		(ssID
		, resName
		, resSplit
		, resAvail
		)
		SELECT ssID
		, 'ICT'
		, 'Use of technology'
		, case Use_of_Technology when 'Yes' then -1 when 'Y' then -1
				when 'No' then 0 when 'N' then 0
				else null end
		FROM @ndoeSchools
		WHERE Use_of_Technology is not null
END
GO

