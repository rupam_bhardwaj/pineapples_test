SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 05 2010
-- Description:	Audit log view
-- =============================================
CREATE PROCEDURE [common].[AuditLogFilter]
	-- Add the parameters for the stored procedure here
	@SummaryLevel int = 0
	,@TableName sysname
	, @AuditID int = null
	, @User nvarchar(100) = null
	, @DateStart datetime = null
	, @DateEnd datetime = null
	, @Action nvarchar(1) = null
	, @Context nvarchar(100) = null
	, @KeyS nvarchar(50) = null
	, @Key int = null
	, @ColumnName sysname = null

WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Role nvarchar(128)
	declare @InRole int

	Select @Role =
		case @TAbleName
			when 'Schools' then 'pSchoolReadX'
			when 'Establishment' then 'pEstablishmentReadX'
			when 'TeacherAppointment' then 'pEstablishmentReadX'
			when 'TeacherIdentity' then 'pTeacherReadX'
			else 'pAdminAdmin'
		end
	EXECUTE AS CALLER -- go dwon from the elevated pineapples security to the original login
	Select @InRole = common.fnIs_MemberEx(@Role)
	REVERT

	if @InRole = 0 begin
		RAISERROR('permission denied',16,0)
		return
	end
	if @SummaryLevel = 0

		SELECT  aLog.[auditID]
		  ,[auditTable]
		  ,[auditUser]
		  ,[auditDateTime]
		  ,[auditContext]
		  ,[auditAction]
		  ,[auditAffected]
		  , auditDate
		  , arowKeys
		  , arowKey
		  , isnull(arowKeys, cast(arowKey as nvarchar(50))) rowPK
		  , acolName
		  , acolBefore
		  , aColAfter
	  FROM [audit].[AuditLog] aLog
	  INNER JOIN audit.auditRow aRow
		on aLog.auditID = aRow.auditID
	  LEFT JOIN audit.auditColumn aColumn
  		on aColumn.arowID = aRow.arowID
		WHERE auditTable = @TableName
		AND (@AuditID = aLog.auditID or @AuditID is null)
		AND (auditUser = @User or @User is null)
		AND (auditDate >= @DateStart or @DateStart is null)
		AND (auditDate <= @DateEnd or @DateEnd is null)
		AND (auditAction = @Action or @Action is null)
		AND (auditContext = @Context or @Context is null)
		AND (arowKeyS = @KeyS or @KeyS is null)
		AND (arowKey = @Key or  @Key is null)
		AND (acolName = @ColumnName or @ColumnName is null)

	if @SummaryLevel = 1

		SELECT DISTINCT aLog.[auditID]
		  ,[auditTable]
		  ,[auditUser]
		  ,[auditDateTime]
		  ,[auditContext]
		  ,[auditAction]
		  ,[auditAffected]
		  , auditDate
		  , arowKeys
		  , arowKey
		  , isnull(arowKeys, cast(arowKey as nvarchar(50))) rowPK

	  FROM [audit].[AuditLog] aLog
	  INNER JOIN audit.auditRow aRow
		on aLog.auditID = aRow.auditID
	  LEFT JOIN audit.auditColumn aColumn
  		on aColumn.arowID = aRow.arowID
		WHERE auditTable = @TableName
		AND (@AuditID = aLog.auditID or @AuditID is null)
		AND (auditUser = @User or @User is null)
		AND (auditDate >= @DateStart or @DateStart is null)
		AND (auditDate <= @DateEnd or @DateEnd is null)
		AND (auditAction = @Action or @Action is null)
		AND (auditContext = @Context or @Context is null)
		AND (arowKeyS = @KeyS or @KeyS is null)
		AND (arowKey = @Key or  @Key is null)
		AND (acolName = @ColumnName or @ColumnName is null)

	if @SummaryLevel = 2

		SELECT DISTINCT aLog.[auditID]
		  ,[auditTable]
		  ,[auditUser]
		  ,[auditDateTime]
		  ,[auditContext]
		  ,[auditAction]
		  ,[auditAffected]
		  , auditDate

	  FROM [audit].[AuditLog] aLog
	  INNER JOIN audit.auditRow aRow
		on aLog.auditID = aRow.auditID
	  LEFT JOIN audit.auditColumn aColumn
  		on aColumn.arowID = aRow.arowID
		WHERE auditTable = @TableName
		AND (@AuditID = aLog.auditID or @AuditID is null)
		AND (auditUser = @User or @User is null)
		AND (auditDate >= @DateStart or @DateStart is null)
		AND (auditDate <= @DateEnd or @DateEnd is null)
		AND (auditAction = @Action or @Action is null)
		AND (auditContext = @Context or @Context is null)
		AND (arowKeyS = @KeyS or @KeyS is null)
		AND (arowKey = @Key or  @Key is null)
		AND (acolName = @ColumnName or @ColumnName is null)
END
GO

