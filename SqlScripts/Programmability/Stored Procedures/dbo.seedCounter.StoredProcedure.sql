SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-11
-- Description:	Set an incremented value for the next available value of a counter
-- =============================================
CREATE PROCEDURE [dbo].[seedCounter]
(
	-- Add the parameters for the function here
	@counterName nvarchar(20)
	, @seedValue int
	, @CounterRetrieved int = 0
)

AS
BEGIN
	-- Declare the return variable here

	begin try
		begin transaction
			declare @CounterCurrent int

			Select @CounterCurrent = counterValue
			from sysCounters
			WHERE counterName = @CounterName

			if (@CounterCurrent is null)
				INSERT INTO sysCounters (counterName, counterValue)
				SELECT @counterName, @SeedValue
				WHERE not exists (Select counterName from sysCounters WHERE counterName = @counterName)

			if (@CounterCurrent = @CounterRetrieved)
				UPDATE sysCounters
					SET counterValue = @SeedValue
					WHERE counterName = @counterName
			else
				raiserror ('The counter has been modified since it was read. ', 16,0)

		commit transaction
		Select @SeedValue
		RETURN @SeedValue
	end try

	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


	-- Return the result of the function

END
GO

