SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 10 2013
-- Description:	Default handling of a pivot on pupilTables,
-- that only requires grouping by Gender ( ie no row or column item or level items)
-- =============================================
CREATE PROCEDURE [dbo].[PIVPupilTable_Gender]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) ,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	print 'PupilTable Gender'
   Select *
   INTO #tmpPIVCols
   FROM PIVColsPupilTablesGender
		WHERE Category = @group or @group is null

	exec dbo.PIVPupilTable_Default_EXEC

   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear
END
GO

