SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [common].[TeacherRoleStructure]
AS


SELECT
	ES.secDesc
	, ES.secSort
	, TR.*
	, RG.*
	, EFUse.MinSize
	, EFUse.MaxSize
	from TrEducationSEctors ES
	INNER JOIN TRTeacherRole TR
		ON ES.secCode = TR.secCode
	INNER JOIN TRRoleGRades RG
		ON RG.roleCode = TR.codeCode
	LEFT JOIN
		(Select rgCode
			, case when min(efbMin) = 0 then null else min(efbMin) end MinSize
			, case when max(efbMax) = 99999 then null else max(efbmax) end  MaxSize
			from EFTableBands TB
			INNER JOIN EFAllocations FA
				ON TB.efbID = FA.efbID
			GROUP BY rgCode
		) EFUse
		ON RG.rgCode = EFUse.rgCode
ORDER BY secSort, rgSort, rgSalaryLevelMax DESC, rgSalaryLevelMin DESC
GO

