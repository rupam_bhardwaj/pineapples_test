SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 12 2010
-- Description:	Nation-level summary of enrolment ratios at the class level
-- , rather than by level of education
-- Output is Xml, creates the levelERs node in the VERMPAF
-- =============================================
CREATE PROCEDURE [warehouse].[_vermLevelER]
	-- Add the parameters for the stored procedure here
	@SendASXML int = 0
	, @xmlOut xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @schoolNo nvarchar(50)
declare @SurveyYear int

declare @checkPop int		-- number of population models
declare @popModCode nvarchar(10)			-- the pop model we will use
declare @popModName nvarchar(50)
declare @popModDefault int

Select @checkPop = count(*)
, @popModCode = min(popModCode)
From PopulationModel


if (@checkPop=0) begin
	raiserror('There is no Population Model to use. ',
				16,10)
end

if (@checkPop > 1) begin
	declare @checkEFA int
	Select @checkEFA = count(*)

	-- SRSI0045 20 9 2010 get the one that is the default
	, @popModCode = min(popModCode)
	From PopulationModel

	WHERE popModEFA = 1

	if (@checkEFA <> 1) begin
		raiserror('You must specify exactly one Population Model to use for EFA reporting. ',
					16,10)
	end
end


Select *
	into #ebse
	from dbo.tfnESTIMATE_BestSurveyenrolments()
	WHERE (schNo = @schoolNo or @SchoolNo is null)
	AND (LifeYear between @SurveyYear and @SurveyYEar + 1
			or @SurveyYEar is null)
	   -- NER 1 to 6 this can come from EnrolmentRatios

IF @SendAsXML = 0

   Select
   P.popYear [Survey Year]
   , P.popAge [Official Age]
   , P.popM
   , P.popF
   , P.pop
   , N.YearOfEd
   , DL.levelCode
   , enrolM
   , enrolF
   , enrol
   , nEnrolM
   , nEnrolF
   , nEnrol
   , isnull(enrolM,0)/popM gerM
   , isnull(enrolF,0)/popF gerF
   , isnull(enrol,0)/pop ger
   , isnull(nEnrolM,0)/popM nerM
   , isnull(nEnrolF,0)/popF nerF
   , isnull(nEnrol,0)/pop ner

   FROM
   (
	Select

		P.popYear
		, P.popAge
		, cast(sum(P.popM) as float) popM
		, cast(sum(P.popF) as float) popF
		, cast(sum(P.popM + P.popF) as float) pop
	FROM
		dbo.Population P
		WHERE popModCode = @PopModCode or @popModCode is null
		GROUP BY popYear, popAge
	) P
   INNER JOIN
   (
   Select
    LifeYear
   , N.YearOfEd
   , N.OfficialAge
   , sum(enrolM) enrolM
   , sum(enrolF) enrolF
   , sum(enrol)  enrol
   , sum(nEnrolM) nEnrolM
   , sum(nEnrolF) nEnrolF
   , sum(nEnrol)  nEnrol
	   , sum(repM) repM
	   , sum(repF) repF
	   , sum(rep)  rep
	   , sum(nrepM) nRepM
	   , sum(nrepF) nRepF
	   , sum(nrep)  nRep
	FROM #ebse
	INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelN N
		On #ebse.bestssID = N.ssID
	LEFT JOIN pEnrolmentRead.ssIDRepeatersLevelN RN
		On N.ssID = RN.ssID
		AND N.YearOfEd = RN.YearOfEd

	group by LifeYear, N.YearOfEd, N.OfficialAge
   ) N
   On P.popYear = N.LifeYear
	AND P.popAge = N.OfficialAge
	INNER JOIN common.ListDefaultPathLevels DL
	ON N.YEarOFEd = DL.YearOfEd
	ORDER BY P.PopYear, P.popAge

if @SendASXML <> 0  begin

   declare @XML xml
   declare @XML2 xml
   SELECT @XML =
   (
   Select
   SurveyYear		[@year]
   , OfficialAge		[@officialAge]
   , YearOfEd		[@yearOfEd]
   , ClassLevel	[@levelCode]
   , popM				--[pop/@M]
   , popF				--[pop/@F]
   , pop				--[pop/@T]
   , enrolM				--[enrol/@M]
   , enrolF				--[enrol/@F]
   , enrol				--[enrol/@T]
   , nEnrolM			--[nEnrol/@M]
   , nEnrolF			--[nEnrol/@F]
   , nEnrol				--[nEnrol/@T]
   , repM				--[rep/@M]
   , repF				--[rep/@F]
   , rep				--[rep/@T]
   , nRepM				--[nRep/@M]
   , nRepF				--[nRep/@F]
   , nRep				--[nRep/@T]
   , intakeM			--[intake/@M]
   , intakeF			--[intake/@F]
   , intake				--[intake/@T]
   , nIntakeM			--[nIntake/@M]
   , nIntakeF			--[nIntake/@F]
   , nIntake			--[nIntake/@T]
   , cast(psaM as int)								psaM	--[psa/@M]
   , cast(psaF as int)								psaF	--[psa/@F]
   , cast(psa as int)								psa		--[psa/@T]

   , convert(decimal(8,5),isnull(enrolM,0)/cast(popM as decimal(13,5)))		gerM	--[ger/@M]
   , convert(decimal(8,5),isnull(enrolF,0)/cast(popF as decimal(13,5)))		gerF	--[ger/@F]
   , convert(decimal(8,5),isnull(enrol,0)/cast(pop as decimal(13,5)))		ger		--[ger/@T]
   , convert(decimal(8,5),isnull(nEnrolM,0)/cast(popM as decimal(13,5)))	nerM	--[ner/@M]
   , convert(decimal(8,5),isnull(nEnrolF,0)/cast(popF as decimal(13,5)))	nerF	--[ner/@F]
   , convert(decimal(8,5),isnull(nEnrol,0)/cast(pop as decimal(13,5)))		ner		--[ner/@T]

   , convert(decimal(8,5),isnull(intakeM,0)/cast(popM as decimal(13,5)))	girM	--[gir/@M]
   , convert(decimal(8,5),isnull(intakeF,0)/cast(popF as decimal(13,5)))	girF	--[gir/@F]
   , convert(decimal(8,5),isnull(intake,0)/cast(pop as decimal(13,5)))		gir		--[gir/@T]
   , convert(decimal(8,5),isnull(nIntakeM,0)/cast(popM as decimal(13,5)))	nirM	--[nir/@M]
   , convert(decimal(8,5),isnull(nIntakeF,0)/cast(popF as decimal(13,5)))	nirF	--[nir/@F]
   , convert(decimal(8,5),isnull(nIntake,0)/cast(pop as decimal(13,5)))		nir		--[nir/@T]

   , case when YearOfEd = 1 then
			case when isnull(nIntakeM,0) = 0 then 0 else cast(psaM/ IntakeM  as decimal(18,5)) end
		else null end								psaPercM	--[psaPerc/@M]

   , case when YearOfEd = 1 then
			case when isnull(nIntakeF,0) = 0 then 0 else cast(psaF / IntakeF as decimal(8,5)) end
		else null end								psaPercF	--[psaPerc/@F]

    , case when YearOfEd = 1 then
			case when isnull(nIntake,0) = 0 then 0 else cast(psa / Intake as decimal(8,5)) end
		else null end 								psaPerc		--[psaPerc/@T]
   FROM
		warehouse.classLevelER
 	ORDER BY SurveyYear, YearOfEd
	FOR XML PATH('LevelER')
	)

	SElect @xmlOut =
		(Select @xml
			FOR XML PATH('LevelERs')
		)
	-- send the xml as the result set
	if @SendAsXML = 1
		SELECT @xmlOut LevelNers
end
   drop table #ebse
END
GO

