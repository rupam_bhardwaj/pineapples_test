SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 2 2010
-- Description:	regression projection of enrolment
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[RegressionProjection]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50) = null,
	@EndYEar int = null,
	@IgnoreBefore int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ThisYEar int
	select @thisyear = year(getdate())

	if(@EndYear is null)
		Select @EndYear = @ThisYEar + 10


Select schNo
		, M Slope
		, (sumY - M * sumX) / N Intercept
		, FirstEnrolmentYear
INTO #R
from
(
Select schNo
		, (N* sumXY - sumX*sumY) / (N*sumXX - sumX* sumX) M
		, N
		, sumY
		,sumX
		, FirstEnrolmentYear

from
(
	-- first get the number of terms, ΣX, ΣY, ΣXY, ΣX2.
	select schNo, count(*) N
		, min(svyYear) FirstEnrolmentYear
		,sum(YearOffset) sumX
		, sum(ssEnrol) sumY
		, sum(YearOffset * ssEnrol) sumXY
		, sum(YearOffset * YearOffset) sumXX

	from
	(Select schNo
		, svyYear
		, svyYear - @ThisYear YearOffset		-- avoid overflows this way rather than deal with year
		, ssEnrol
	from SchoolSurvey

	WHERE ssEnrol is not null
	AND (schNo = @SchoolNo or @SchoolNo is null)		-- other params may go here
	AND (svyYear >= @IgnoreBefore or @IgnoreBefore is null)
	) SS
	GROUP BY schNo
)	sub1
WHERE (N*sumXX - sumX* sumX) <> 0
) sub2


CREATE TABLE #Output
(
	schNo nvarchar(50)
	, Slope float
	, Intercept float
	, svyYear int
	, Projection int
	, Enrol int
	, Estimate int
)
INSERT INTO #output

Select #R.schNo
	, Slope
	, Intercept
	, @ThisYear + num svyYear
	, num * Slope + Intercept Projection
	,null
	,null
from #R
INNER JOIN Schools
ON #R.schNo = Schools.schNo
CROSS JOIN metaNumbers
WHERE num between (FirstEnrolmentYear - @ThisYear) and (@EndYEar - @ThisYear)
AND (num * Slope + Intercept) > 0
AND (@ThisYear + num < schClosed or schClosed = 0)

INSERT INTO #output
Select EE.schNo
, null
, null
, LifeYear
, null
, EE.bestEnrol
, null
FRom dbo.tfnEstimate_BestSurveyEnrolments() EE
INNER JOIN SchoolSurvey
	ON EE.bestssID = SchoolSurvey.ssID
WHERE ssEnrol is not null
AND EE.Estimate = 0
AND (SchoolSurvey.schNo = @SchoolNo or @SchoolNo is null)		-- other params may go here

INSERT INTO #output
Select EE.schNo
, null
, null
, LifeYear
, null
, null
, EE.bestEnrol
FRom dbo.tfnEstimate_BestSurveyEnrolments() EE
INNER JOIN SchoolSurvey
	ON EE.bestssID = SchoolSurvey.ssID
WHERE ssEnrol is not null
AND (SchoolSurvey.schNo = @SchoolNo or @SchoolNo is null)		-- other params may go here

Select schNo, svyYear
,sum(Slope) Slope
,sum(Intercept) Intercept
, sum(Projection) Projection
, sum(Enrol) Enrol
, sum(Estimate) EstimateEnrol
FRom #output
GROUP BY schNo, svyYear
ORDER BY SchNo, svyYear

DROP TABLE #output
DROP TABLE #r


END
GO

