SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [warehouse].[BuildRank]
AS

BEGIN
/*
	This procedure rebuilds the ranking table showing the rank, dectile and quartile for
	each school in each year, within its district and school type, and nationally within school type

*/
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ranktmp TABLE
(
	svyYear int not null,
	schNo nvarchar(50) not null,
	District nvarchar(10) null,
	SchoolType nvarchar(10) not null,
	Enrol int,
	EnrolM int,
	EnrolF int,
	Estimate bit
	, rankD int
	, rankN int

)

	declare @ds TABLE
(

	svyYear int,
	District nvarchar(10),
	SchoolType nvarchar(10),
	numSchools int
)

	declare @dsn TABLE
(

	svyYear int,
	SchoolType nvarchar(10),
	numSchools int
)

select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()
WHERE LifeYear in (Select svyYear from Survey)

-- this temp table is the enrolment for each school in each life year
-- it includee the enroloment value, (now staged on Schoolsurvey)
-- and the estimate flag

insert into @ranktmp(svyYear, schNo, District, SchoolType, Enrol, EnrolM, EnrolF, Estimate, rankD, rankN)


select E.[LifeYear],
E.schNo,
I.iGroup,
ss2.ssSchType,
SS.ssEnrol,
SS.ssEnrolM,
SS.ssEnrolF,
E.Estimate
, ROW_NUMBER()  OVER (PARTITION BY [LifeYear], ss2.ssSchType, iGRoup  ORDER BY ss.ssEnrol DESC)
, ROW_NUMBER()  OVER (PARTITION BY [LifeYear], ss2.ssSchType  ORDER BY ss.ssEnrol DESC)
from
#ebse E
INNER JOIN
SchoolSurvey SS
on E.bestssID = ss.ssID
inner join SchoolSurvey SS2
on E.surveydimensionssID = SS2.ssID
INNER JOIN Schools S
on SS.schNo = s.schNo
inner join Islands I on S.iCode = i.iCode


INSERT INTO @ds
Select svyYear, District, SchoolType
		, count(rankD) numSchools
from @rankTmp
group by svyYear, District, SchoolType


INSERT INTO @dsn
Select svyYear,  SchoolType
		, sum(numSchools)
from  @ds
group by svyYear,  SchoolType


IF OBJECT_ID('warehouse.SurveyYearRank', 'U') IS NOT NULL
		DROP TABLE warehouse.SurveyYearRank;

--insert into SurveyYearRank(svyYear, schNo,
--	rankDistrict, rankSchType,
--	rankEnrol, rankEnrolM, rankEnrolF, rankEstimate,
--	rankDistrictSchoolType, rankDistrictSchoolTypeDec, rankDistrictSchoolTypeQtl,
--	rankSchoolType, rankSchoolTypeDec, rankSchoolTypeQtl
--)
select E.svyYear SurveyYear
, E.schNo
, E.District rankDistrict
, E.SchoolType rankSchType
, E.Enrol Enrol
, E.enrolM EnrolM
, E.enrolF EnrolF
, E. Estimate Estimate
, E.rankD rankDistrictSchoolType
, 		right('0' + convert(nvarchar(2),case
			when DS.numSchools < 10 then rankD
			else floor((rankD-1) * 10 / DS.numSchools)+1
		end),2) rankDistrictSchoolTypeDec
,		'Q' + convert(nchar(1),case
			when DS.numSchools < 4 then rankD
			else floor((rankD-1) * 4 / DS.numSchools)+1
		end) rankDistrictSchoolTypeQtl


, E.rankN rankSchoolType
, 		right('0' + convert(nvarchar(2),case
			when DSN.numSchools < 10 then rankN
			else floor((rankN-1) * 10 / DSN.numSchools)+1
		end),2) rankSchoolTypeDec
,		'Q' + convert(nchar(1),case
			when DSN.numSchools < 4 then rankN
			else floor((rankN-1) * 4 / DSN.numSchools)+1
		end) rankSchoolTypeQtl
INTO warehouse.SurveyYearRank
from
@rankTmp E
inner join @ds DS on
	E.svyYear = DS.svyyear and
	E.District = DS.District and
	E.SchoolType = DS.SchoolType
inner join @dsn DSN on
	E.svyYear = DSN.svyyear and
	E.SchoolType = DSN.SchoolType


END
GO

