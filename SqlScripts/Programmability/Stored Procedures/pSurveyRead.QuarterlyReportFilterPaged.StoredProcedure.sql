SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis and Ghislain Hachey
-- Create date: 18 6 2016
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyRead].[QuarterlyReportFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@QRID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50) = null, --'e.g. GHS100'
	@InspQuarterlyReport nvarchar(50) = null, --- e.g. '2016/Q4'

	@XmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		qrID int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pSurveyRead.QuarterlyReportFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	@QRID,
	@School,
	@SchoolNo,
	@InspQuarterlyReport,

	@xmlFilter


-------------------------------------
-- return results

Select QuarterlyReports.*
FROM pInspectionRead.QuarterlyReports AS QuarterlyReports
INNER JOIN @Keys AS K ON QuarterlyReports.qrID = K.qrID

-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

