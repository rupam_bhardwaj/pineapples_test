SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PIVEnrolmentEstPROC]
AS
exec dbo.estenrol
SELECT  LVL.*,
		G.*,
		E.[Survey Year],
		E.schNo,
		E.enAge,
		E.enLevel,
        E.Estimate,
		E.[Age of Data],
		E.[Year of Data],
		E.DataQualityLevel,
        E.SurveyYearQualityLevel,
		E.AgeOffset,
		E.YearOfBirth,
		CASE GenderCode
			WHEN 'M' then
				enM
			ELSE
				enF
		END Enrol
FROM

(SELECT     EE.schNo, EE.LifeYear AS [Survey Year], dbo.Enrollments.enAge, dbo.Enrollments.enLevel,
                      dbo.Enrollments.enM, dbo.Enrollments.enF,
					EE.bestssID,
					EE.Estimate,
                      EE.Offset AS [Age of Data], EE.bestYear AS [Year of Data],
                      EE.bestssqLevel AS DataQualityLevel, EE.ActualssqLevel AS SurveyYearQualityLevel,
                      dbo.Enrollments.enAge - (dbo.Survey.svyPSAge + dbo.lkpLevels.lvlYear - 1) AS AgeOffset, dbo.Survey.svyYear - dbo.Enrollments.enAge AS YearOfBirth
FROM         ##estenrol EE INNER JOIN
                      dbo.Enrollments ON EE.bestssID = dbo.Enrollments.ssID INNER JOIN
                      dbo.lkpLevels ON dbo.Enrollments.enLevel = dbo.lkpLevels.codeCode INNER JOIN
                      dbo.Survey ON EE.LifeYear = dbo.Survey.svyYear

) E
				INNER JOIN DimensionLevel LVL
					on E.enLevel = LVL.LevelCode
				CROSS JOIN
                      dbo.DimensionGender G
GO

