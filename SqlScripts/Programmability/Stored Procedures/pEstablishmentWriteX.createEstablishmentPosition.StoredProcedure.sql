SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 10 2009
-- Description:	Insert a new position into the establishment
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[createEstablishmentPosition]
	-- Add the parameters for the stored procedure here
	@PositionNo nvarchar(20) ,
	@SchNo nvarchar(50),
	@Authority nvarchar(10),
	@RoleGrade nvarchar(20),
	@ActiveDate datetime,
	@Title nvarchar(50),
	@Description nvarchar(65),
	@Subject nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	INSERT INTO Establishment
	(estpNo, schNo, estpAuth, estpRoleGrade, estpActiveDate, estpTitle, estpDescription, estpSubject)
	VALUES
		(@PositionNo
		,@schNo
		,@Authority
		,@RoleGrade
		,@ActiveDate
		,@Title
		,@Description
		,@Subject)
   Select * from Establishment WHERE estpID = @@IDENTITY
END
GO

