SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- 6 11 2012 added collate to temp tables
-- added primary key to @keys table
-- =============================================
CREATE PROCEDURE [pSchoolRead].[BookFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@BookCode nvarchar(20) = null,
    @Title nvarchar(100) = null,
    @PublisherCode  nvarchar(20) = null,
    @ISBN  nvarchar(20) = null,
	@TG nvarchar(10) = null,

	@Subject nvarchar(20) = null,
	@Level nvarchar(20) = null,
	@EdLevelCode nvarchar(10) = null,
	@SchoolType nvarchar(20) = null,
	@CurriculumYear int = null,

	@XmlFilter xml = null


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		bkCode nvarchar(50) PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pSchoolRead.BookFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	@BookCode,
    @Title,
    @PublisherCode,
    @ISBN ,
	@TG,

	@Subject,
	@Level,
	@EdLevelCode,
	@SchoolType,
	@CurriculumYear,
	@xmlFilter


-------------------------------------
-- return results

Select Books.*
, QOH
, QOHReplace
from pSchoolRead.BooksView Books
INNER JOIN @Keys K
	ON Books.bkCode = K.bkCode
LEFT JOIN pSchoolRead.BooksQOH QOH
	ON Books.bkCode = QOH.bkCode

-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

