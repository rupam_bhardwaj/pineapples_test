SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-01-18
-- Description:	integrity to update totals on SchoolSurvey
-- =============================================
CREATE PROCEDURE [dbo].[integritySchoolSurveyEnrolment]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;

if @schNo is null
	update SchoolSurvey
		set ssEnrolM = totM,
		ssEnrolF = totF,
		ssEnrol = isnull(totF,0)+isnull(totM,0)
		from SchoolSurvey INNER JOIN
			(Select ssID, sum(enM) totM, sum(enF) totF from Enrollments group by ssID) E
			on SchoolSurvey.ssID = E.ssID
    -- Insert statements for procedure here
else
	update SchoolSurvey
		set ssEnrolM = totM,
		ssEnrolF = totF
		from SchoolSurvey INNER JOIN
			(Select ssID, sum(enM) totM, sum(enF) totF from Enrollments group by ssID) E
			on SchoolSurvey.ssID = E.ssID
		where SchoolSurvey.schNo = @schNo
END
GO

