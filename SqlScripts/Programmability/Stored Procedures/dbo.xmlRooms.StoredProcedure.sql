SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-02-07
-- Description:	xml representation of rooms
-- =============================================
CREATE PROCEDURE [dbo].[xmlRooms]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) = 0,
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Select R.*
into #r
from Rooms R
	inner join SchoolSurvey S
	on R.ssId = S.ssID
	WHERE S.SchNo = @schNo
		and S.svyYear = @SurveyYear


declare @xmlResult xml

set @xmlResult =
(Select
	Tag
,	Parent
,   QualityCode as [Rooms!1!qualityCode]
,	SchoolLevel as [Rooms!1!sector]
,	Location as [Rooms!1!location]
,	roomType as [Room!2!roomType]
,	roomID as [Room!2!roomID]
,	roomName as [Room!2!roomName]
,   buildingReviewID as [Room!2!buildingReviewID]
,   roomLevel as [Room!2!classLevel]
,	roomLength as [Room!2!roomLength]
,	roomWidth as [Room!2!roomWidth]
,	roomArea as [Room!2!roomArea]
,	roomSeq as  [Room!2!displaySeq]
,   roomYear as [Room!2!yearConstruction]
,   roomMaterials as [Room!2!materials]
,   roomMaterialRoof as [Room!2!materialRoof]
,   roomMaterialFloor as [Room!2!materialFloor]
,   roomCondition as [Room!2!condition]
,   roomSecureDoor as [Room!2!secureDoor]
,   roomSecureWindows as [Room!2!secureWindows]

,	roomCapF as [Room!2!capF]
,	roomCapM as [Room!2!capM]
,	roomShowersF as [Room!2!showersF]
,	roomShowersM as [Room!2!showersM]
,	roomToiletsF as [Room!2!toiletsF]
,	roomToiletsM as [Room!2!toiletsM]
,	roomLaundry as [Room!2!laundry]
,	roomKitchen as [Room!2!kitchen]

,	roomBeds as [Room!2!beds]
,	roomMatresses as [Room!2!matresses]
,	roomCupboards as [Room!2!cupboards]

,	roomWoodStove as [Room!2!woodStove]
,	roomGasStove as [Room!2!gasStove]
,	roomElecStove as [Room!2!elecStove]
,	roomLockedStorage as [Room!2!lockedStorage]
,	roomColdStorage as [Room!2!coldStorage]
,	roomLunchDay as [Room!2!lunchDay]
,	roomLunchBoarder as [Room!2!lunchBoarder]

,   furnRoomID as [Furniture!3!ID!Hide]
,   furnType as [Item!4!furnType]
,   furnNum as [Item!4!num]
,   furnCondition as [Item!4!condition]

from
(
Select DISTINCT
	1 as Tag
, null as Parent
, rmQualityCode as QualityCode
, rmSchLevel as SchoolLevel
, rmLocation as Location
, null as roomType
, null as roomID
, null as roomLevel
, null as roomName
, null as buildingReviewID			-- 16 03 2010
, null as roomLength
, null as roomWidth
, null as roomArea
, null as roomSeq
, null as roomYear
, null as roomMaterials
, null as roomMaterialRoof
, null as roomMaterialFloor
, null as roomCondition
, null as roomSecureDoor
, null as roomSecureWindows

, null as roomCapF
, null as roomCapM
, null as roomShowersF
, null as roomShowersM
, null as roomToiletsF
, null as roomToiletsM
, null as roomLaundry
, null as roomKitchen

, null as roomBeds
, null as roomMatresses
, null as roomCupboards

, null as roomWoodStove
, null as roomGasStove
, null as roomElecStove
, null as roomLockedStorage
, null as roomColdStorage
, null as roomLunchDay
, null as roomLunchBoarder


, null as furnRoomID
, null as furnType
, null as furnNum
, null as furnCondition

from #r

UNION ALL

Select DISTINCT
2 as Tag
, 1 as Parent
, rmQualityCode as QualityCode
, rmSchLevel as SchoolLevel
, rmLocation as Location
, rmType as roomType
, rmID as roomID
, rmLevel as roomLevel
, rmTitle as roomName
, brevID as buildingReviewID
, cast(rmLength as decimal(18,2)) as roomLength
, cast(rmWidth as decimal(18,2)) as roomWidth
, cast(rmSize as decimal(18,2)) as roomArea
,
	case when rmNo = 0 then null else RmNo end as roomSeq
, rmYear as roomYear
, rmMaterials as roomMaterials
, rmMaterialRoof as roomMaterialRoof
, rmMaterialFloor as roomMaterialFloor
, rmCondition as roomCondition
, rmSecureDoor as roomSecureDoor
, rmSecureWindows as roomSecureWindows

, rmcapF as roomCapF
, rmcapM as roomCapM
, rmShowersF as roomShowersF
, rmShowersM as roomShowersM
, rmToiletsF as roomToiletsF
, rmToiletsM as roomToiletsM
, rmLaundry as roomLaundry
, rmKitchen as roomKitchen

, rmBeds as roomBeds
, rmMatresses as roomMatresses
, rmCupboards as roomCupboards

, rmStoveWood as roomWoodStove
, rmStoveGas as roomGasStove
, rmStoveElec as roomElecStove
, rmLockedStore as roomLockedStorage
, rmRefrig as roomColdStorage
, rmLunchDay as roomLunchDay
, rmLunchBoarder as roomLunchBoarder

, null as furnRoomID
, null as furnType
, null as furnNum
, null as furnCondition

from #r

UNION ALL

Select DISTINCT
3 as Tag
, 2 as Parent
, rmQualityCode as QualityCode
, rmSchLevel as SchoolLevel
, rmLocation as Location
, rmType as roomType
, #r.rmID as roomID
, rmLevel as roomLevel
, rmTitle as roomName
, brevID as buildingReviewID			-- 16 03 2010

, cast(rmLength as decimal(18,2)) as roomLength
, cast(rmWidth as decimal(18,2)) as roomWidth
, cast(rmSize as decimal(18,2)) as roomArea
,
	case when rmNo = 0 then null else RmNo end as roomSeq
, rmYear as roomYear
, rmMaterials as roomMaterials
, rmMaterialRoof as roomMaterialRoof
, rmMaterialFloor as roomMaterialFloor
, rmCondition as roomCondition
, rmSecureDoor as roomSecureDoor
, rmSecureWindows as roomSecureWindows

, null as roomCapF
, null as roomCapM
, null as roomShowersF
, null as roomShowersM
, null as roomToiletsF
, null as roomToiletsM
, null as roomLaundry
, null as roomKitchen

, null as roomBeds
, null as roomMatresses
, null as roomCupboards

, null as roomWoodStove
, null as roomGasStove
, null as roomElecStove
, null as roomLockedStorage
, null as roomColdStorage
, null as roomLunchDay
, null as roomLunchBoarder


, f.rmID as furnRoomID
, null as furnType
, null as furnNum
, null as furnCondition

from #r inner join Furniture F on #r.rmID = F.rmID

UNION ALL

Select DISTINCT
4 as Tag
, 3 as Parent
, rmQualityCode as QualityCode
, rmSchLevel as SchoolLevel
, rmLocation as Location
, rmType as roomType
, #r.rmID as roomID
, rmLevel as roomLevel
, rmTitle as roomName
, brevID as buildingReviewID			-- 16 03 2010

, cast(rmLength as decimal(18,2)) as roomLength
, cast(rmWidth as decimal(18,2)) as roomWidth
, cast(rmSize as decimal(18,2)) as roomArea

, case when rmNo = 0 then null else RmNo end as roomSeq
, rmYear as roomYear
, rmMaterials as roomMaterials
, rmMaterialRoof as roomMaterialRoof
, rmMaterialFloor as roomMaterialFloor
, rmCondition as roomCondition
, rmSecureDoor as roomSecureDoor
, rmSecureWindows as roomSecureWindows

, null as roomCapF
, null as roomCapM
, null as roomShowersF
, null as roomShowersM
, null as roomToiletsF
, null as roomToiletsM
, null as roomLaundry
, null as roomKitchen

, null as roomBeds
, null as roomMatresses
, null as roomCupboards

, null as roomWoodStove
, null as roomGasStove
, null as roomElecStove
, null as roomLockedStorage
, null as roomColdStorage
, null as roomLunchDay
, null as roomLunchBoarder

, f.rmID as furnRoomID
, fCode as furnType
, fNum as furnNum
, fCond as furnCondition

from #r inner join Furniture F on #r.rmID = F.rmID


) u
ORDER BY
	[Rooms!1!qualityCode]
,	[Rooms!1!sector]
,	[Rooms!1!Location]
,	[Room!2!roomType]
,	[Room!2!displaySeq]
,   [Furniture!3!ID!Hide]
,	[Item!4!furnType]

for xml explicit
)
Select @xmlResult
END
GO
GRANT EXECUTE ON [dbo].[xmlRooms] TO [pSchoolRead] AS [dbo]
GO

