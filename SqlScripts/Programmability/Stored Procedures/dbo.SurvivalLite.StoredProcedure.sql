SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Underlying data for calculation of efa12
-- =============================================
CREATE PROCEDURE [dbo].[SurvivalLite]
(
	@Consolidation int = 0
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- build estimate best survey enrolments as a temp table
Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()

-- current years enrolments by level and gender


-----------------------------------------------------------
-- Enrolments
-- 3 groups- This Year, NextYear, Next Year Next Level
-----------------------------------------------------------

create table #efa
(
	schNo nvarchar(50),
	svyYear int,
	Age int,
	LevelCode nvarchar(20),
	YearOfEd int,
	EnrolM int,
	EnrolF int,
	RepM int,
	RepF int,
	EnrolNYM int,
	EnrolNYF int,
	RepNYM int,
	RepNYF int,
	EnrolNYNextLevelM int,
	EnrolNYNextLevelF int,
	RepNYNextLevelM int,
	RepNYNextLevelF int,
	Estimate smallint,
	[Year Of Data] int,
	[Age of Data] int,
	surveyDimensionssID int,
	[Estimate NY] smallint,
	[Year Of Data NY] int,
	[Age of Data NY] int,
	surveyDimensionssIDNY int
)
-- enrolments this year
INSERT INTO #EFA
(
	schNo, svyYear,
	Age,
	levelCode,
	enrolM, enrolF,
	Estimate, [Year of Data],[Age of Data],
	surveyDimensionssID
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	El.enAge,
	EL.enLevel,
	EL.enM,
	EL.enF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset,
	EBSE.surveyDimensionssID
FROM #ebse EBSE
inner join Enrollments EL
on EBSE.bestssID = El.ssID

		-- next year's enrolment at the same level
		INSERT INTO #efa
		(
			schNo, svyYear, Age, levelCode,
			enrolNYM, enrolNYF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveyDimensionssIDNY
		)
		SELECT
			EBSE.schNo,
			EBSE.LifeYear - 1 svyYear,
			EL.enAge - 1,
			EL.enLevel,		-- becuase we are pairing this with the record from the previous year
			EL.enM AS EnrolNYM,
			EL.enF AS EnrolNYF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveydimensionssID
		FROM #ebse EBSE
		inner join Enrollments EL
		on EBSE.bestssID = El.ssID
		--INNER JOIN SchoolLifeYears SLY
		--	ON EBSE.schNo = SLY.schNo AND EBSE.LifeYear = SLY.svyYear


		-- enrolments next year next level
		INSERT INTO #efa
		(
			schNo, svyYear, Age, levelCode,
			enrolNYNextLevelM, enrolNYNextLevelF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveyDimensionssIDNY
		)

		SELECT
			EBSE.schNo,
			EBSE.LifeYear-1 svyYear,
			El.enAge - 1,				-- next year record
			L2.codeCode as LevelCode,
			EL.enM AS enrolNYNextLevelM,
			EL.enF AS enrolNYNextLevelF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveyDimensionssID
		FROM #ebse EBSE
			inner join Enrollments EL
				on EBSE.bestssID = El.ssID
			inner join lkpLevels L1
				on L1.codeCode = EL.enLevel,
		-- find the 'next level' based on lvlYear, the year of education
		-- default path levels is a way to get a unique level
		-- needs to be considered how this would work in a school type not using the default path level levels

			LISTDefaultPathLevels LDPL
				inner join lkpLevels L2
				on L2.codeCode = LDPL.levelCode
		WHERE L2.lvlYear = L1.lvlYear-1
-----------------------------------------------
-- Repeaters
-- this year, next year, next year next level
-----------------------------------------------

INSERT INTO #EFA
(
	schNo, svyYear, Age, levelCode,
	repM, repF,
	Estimate, [Year of Data],[Age of Data],
	surveyDimensionssID
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	RL.ptAge,
	RL.ptLevel,
	RL.ptM,
	RL.ptF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset,
	EBSE.surveydimensionssID
FROM #ebse EBSE
inner join (Select * from PupilTables WHERE ptCode = 'REP') RL
on EBSE.bestssID = RL.ssID

		INSERT INTO #EFA
		(
			schNo, svyYear, Age, levelCode,
			repNYM, repNYF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveyDimensionssIDNY
		)
		SELECT
			EBSE.schNo,
			EBSE.LifeYear-1 svyYear,
			RL.ptAge - 1,
			RL.ptLevel,
			RL.ptM,
			RL.ptF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveyDimensionssID

		FROM #ebse EBSE
		inner join (Select * from PupilTables WHERE ptCode = 'REP')  RL
		on EBSE.bestssID = RL.ssID

		-- Repeaters next year next level
		INSERT INTO #EFA
		(
			schNo, svyYear, Age, levelCode,
			repNYNextLevelM, repNYNextLevelF,
			[Estimate NY], [Year of Data NY],[Age of Data NY],
			surveydimensionssIDNY
		)
		SELECT
			EBSE.schNo,
			EBSE.LifeYear-1 svyYear,
			RL.ptAge - 1 Age,
			L2.codeCode as LevelCode,
			RL.ptM,
			RL.ptF,
			EBSE.Estimate,
			EBSE.[bestYear],
			EBSE.offset,
			EBSE.surveyDimensionssID

		FROM #ebse EBSE
			inner join (Select * from PupilTables WHERE ptCode = 'REP')  RL
				on EBSE.bestssID = RL.ssID
			inner join lkpLevels L1
				on L1.codeCode = RL.ptLevel,
		-- find the 'next level' based on lvlYear, the year of education
		-- default path levels is a way to get a unique level
		-- needs to be considered how this would work in a school type not using the default path level levels

			LISTDefaultPathLevels LDPL
				inner join lkpLevels L2
				on L2.codeCode = LDPL.levelCode
		WHERE L2.lvlYear = L1.lvlYear-1


---------------------------------------------------------------------------
-- end of buld temp table - it's a biggie!
---------------------------------------------------------------------------
-- other temp tables for dimensions
-- take a subset of these
SELECT
	LevelCode
	, [Level]
	, [Year of Education]
	, SectorCode
	, Sector
	, edLevelCode
	, [Education Level]
	, edLevelAltCode
	, [Education Level Alt]
	, edLevelAlt2Code
	, [Education Level Alt2]
	, [ISCED Level Name]
	, [ISCED Level]
	, [ISCED SubClass]

into #dl
FROM        DimensionLevel ORDER BY [Year of Education],levelCode


Select ssId [Survey ID]
	, ssSchType SchoolTypeCode
	, ssAuth AuthorityCode
	, iGroup [District Code]
INTO #dsSmall
from DimensionSchoolSurveySub S
	INNER JOIN Islands I
	on s.iCode = I.iCode;


-----------------------------------------------------------------
-- Output formats

-- now return the sum across these

if @consolidation = -1
	BEGIN


		Select
			D.schNo,
			D.svyYear [Survey Year],
			-- we have to make sure there is a surveydimensionssid, even if there is only NY data
			isnull(max(surveyDimensionssID),max(surveyDimensionssIDNY)) surveyDimensionssID,
			LevelCode,
			YearOfEd,
			sum(enrolM) as EnrolM,
			sum(enrolF) as enrolF,
			sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYM]+[EnrolM])/cast(([Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM,
			sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYF]+[EnrolF])/cast(([Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF,
			sum(EnrolNYM) AS EnrolNYM,
			sum(EnrolNYF) AS EnrolNYF,
			sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
			sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,
			sum(RepM) AS RepM,
			sum(RepF)  AS RepF,
			sum(RepNYM) AS RepNYM,
			sum(RepNYF)  AS RepNYF,
			sum(RepNYNextLevelM)  AS RepNYNextLevelM,
			sum(RepNYNextLevelF)  AS RepNYNextLevelF,
			max(Estimate) as Estimate,
			max([Year of Data]) as [Year of Data],
			max([Age of Data]) as [Age of Data],
			max([Estimate NY]) as [Estimate NY],
			max([Year of Data NY]) as [Year of Data NY],
			max([Age of Data NY]) as [Age of Data NY]
		from #efa D
		group by schNo, svyYear, LevelCode,YearOfEd
	END

if @Consolidation = 0
	BEGIN
		print ''
	END

if @Consolidation = 1
		BEGIN


		with X as
		(
		Select
			D.svyYear [Survey Year],
			-- we have to make sure there is a surveydimensionssid, even if there is only NY data
			D.Estimate,
			D.[Estimate NY],
			Age,
			LevelCode,
			SchoolTypeCode,
			AuthorityCode,
			[District Code],
			sum(enrolM) as EnrolM,
			sum(enrolF) as enrolF,
			sum(
				case when D.[Age of Data] > 0
					then (D.[Age of Data]*isnull([EnrolNYM],0)+isnull([EnrolM],0))/cast((D.[Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM,
			sum(
				case when D.[Age of Data] > 0
					then (D.[Age of Data]*isnull([EnrolNYF],0)+isnull([EnrolF],0))/cast((D.[Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF,
			sum(EnrolNYM) AS EnrolNYM,
			sum(EnrolNYF) AS EnrolNYF,
			sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
			sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,
			sum(RepM) AS RepM,
			sum(RepF)  AS RepF,
			sum(RepNYM) AS RepNYM,
			sum(RepNYF)  AS RepNYF,
			sum(RepNYNextLevelM)  AS RepNYNextLevelM,
			sum(RepNYNextLevelF)  AS RepNYNextLevelF,

			max([Year of Data]) as [Year of Data],
			max([Age of Data]) as [Age of Data],

			max([Year of Data NY]) as [Year of Data NY],
			max([Age of Data NY]) as [Age of Data NY]
		from #efa D
			inner join #dsSmall DS
			on 	isnull(D.surveyDimensionssID,D.surveyDimensionssIDNY) = DS.[Survey ID]

		group by svyYear,
			Estimate,
			[Estimate NY],
			SchoolTypeCode,
			AuthorityCode,
			[District Code],
			Age,  LevelCode
		)
		Select [Survey Year]
			, Estimate
			, SchoolTypeCode
			, AuthorityCode
			, [District Code]
			, D.dName District
			, DL.*
			, Age
			, G.*
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.SlopedEnrolM else X.SlopedEnrolF end) SlopedEnrol
			, (case when G.GenderCode = 'M' then X.enrolNYM else X.enrolNYF end) EnrolNY
			, (case when G.GenderCode = 'M' then X.enrolNYNextLevelM else X.enrolNYNextLevelF end) EnrolNYNextLevel
			, (case when G.GenderCode = 'M' then X.RepM else X.RepF end) Rep
			, (case when G.GenderCode = 'M' then X.RepNYM else X.RepNYF end) RepNY
			, (case when G.GenderCode = 'M' then X.RepNYNextLevelM else X.RepNYNextLevelF end) RepNYNextLevel

		from X
			INNER JOIN #DL DL
			on X.levelCode = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode

		CROSS JOIN DimensionGender G

	END

if @Consolidation = 2
		BEGIN


		Select
			D.SchNo,
			D.svyYear [Survey Year],
			-- we have to make sure there is a surveydimensionssid, even if there is only NY data
			Age,
			LevelCode,
			sum(enrolM) as EnrolM,
			sum(enrolF) as enrolF,
			sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYM]+[EnrolM])/cast(([Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM,
			sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYF]+[EnrolF])/cast(([Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF,

			sum(EnrolNYM) AS EnrolNYM,
			sum(EnrolNYF) AS EnrolNYF,
			sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
			sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,
			sum(RepM) AS RepM,
			sum(RepF)  AS RepF,
			sum(RepNYM) AS RepNYM,
			sum(RepNYF)  AS RepNYF,
			sum(RepNYNextLevelM)  AS RepNYNextLevelM,
			sum(RepNYNextLevelF)  AS RepNYNextLevelF,
			max(Estimate) as Estimate,
			max([Year of Data]) as [Year of Data],
			max([Age of Data]) as [Age of Data],
			max([Estimate NY]) as [Estimate NY],
			max([Year of Data NY]) as [Year of Data NY],
			max([Age of Data NY]) as [Age of Data NY],
			isnull(max(D.surveyDimensionssID),max(D.surveyDimensionssIDNY)) surveyDimensionssID
		into #Agg
		from #efa D
--			on 	isnull(D.surveyDimensionssID,D.surveyDimensionssIDNY) = DS.[Survey ID]

		group by schNo, svyYear,
			Age,  LevelCode

			Select * from #Agg
/*		Select [Survey Year]
			, Estimate
			, SchoolTypeCode
			, [District Code]
			, D.dName District
			, DL.*
			, Age
			, G.*
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.SlopedEnrolM else X.SlopedEnrolF end) SlopedEnrol
			, (case when G.GenderCode = 'M' then X.enrolNYM else X.enrolNYF end) EnrolNY
			, (case when G.GenderCode = 'M' then X.enrolNYNextLevelM else X.enrolNYNextLevelF end) EnrolNYNextLevel
			, (case when G.GenderCode = 'M' then X.RepM else X.RepF end) Rep
			, (case when G.GenderCode = 'M' then X.RepNYM else X.RepNYF end) RepNY
			, (case when G.GenderCode = 'M' then X.RepNYNextLevelM else X.RepNYNextLevelF end) RepNYNextLevel

		from X
			INNER JOIN #DL DL
			on X.levelCode = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode

		CROSS JOIN DimensionGender G
*/
	END
if @Consolidation = 3
		BEGIN


		with X as
		(
		Select
			D.svyYear [Survey Year],
			-- we have to make sure there is a surveydimensionssid, even if there is only NY data
			Age,
			LevelCode,
			SchoolTypeCode,
			[District Code],
			sum(enrolM) as EnrolM,
			sum(enrolF) as enrolF,
			sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYM]+[EnrolM])/cast(([Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM,
			sum(
				case when [Age of Data] > 0
					then ([Age of Data]*[EnrolNYF]+[EnrolF])/cast(([Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF,

			sum(EnrolNYM) AS EnrolNYM,
			sum(EnrolNYF) AS EnrolNYF,
			sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
			sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,
			sum(RepM) AS RepM,
			sum(RepF)  AS RepF,
			sum(RepNYM) AS RepNYM,
			sum(RepNYF)  AS RepNYF,
			sum(RepNYNextLevelM)  AS RepNYNextLevelM,
			sum(RepNYNextLevelF)  AS RepNYNextLevelF,
			max(Estimate) as Estimate,
			max([Year of Data]) as [Year of Data],
			max([Age of Data]) as [Age of Data],
			max([Estimate NY]) as [Estimate NY],
			max([Year of Data NY]) as [Year of Data NY],
			max([Age of Data NY]) as [Age of Data NY]
		from #efa D
			inner join #dsSmall DS
			on 	isnull(D.surveyDimensionssID,D.surveyDimensionssIDNY) = DS.[Survey ID]

		group by svyYear,
			SchoolTypeCode,
			[District Code],
			Age,  LevelCode
		)
			Select * from X
/*		Select [Survey Year]
			, Estimate
			, SchoolTypeCode
			, [District Code]
			, D.dName District
			, DL.*
			, Age
			, G.*
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.SlopedEnrolM else X.SlopedEnrolF end) SlopedEnrol
			, (case when G.GenderCode = 'M' then X.enrolNYM else X.enrolNYF end) EnrolNY
			, (case when G.GenderCode = 'M' then X.enrolNYNextLevelM else X.enrolNYNextLevelF end) EnrolNYNextLevel
			, (case when G.GenderCode = 'M' then X.RepM else X.RepF end) Rep
			, (case when G.GenderCode = 'M' then X.RepNYM else X.RepNYF end) RepNY
			, (case when G.GenderCode = 'M' then X.RepNYNextLevelM else X.RepNYNextLevelF end) RepNYNextLevel

		from X
			INNER JOIN #DL DL
			on X.levelCode = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode

		CROSS JOIN DimensionGender G
*/
	END
IF @Consolidation = 4
-- aggregates up to level and rank
	BEGIN


	WITH X AS
	(


		Select E.svyYear [Survey Year]

			, E.Estimate

			, levelCode
			, DS.SchoolTypeCode
			, DS.[District Code]

			, R.[District Dectile]
			, R.[District Quartile]

			, R.Dectile
			, R.Quartile


			, sum(E.EnrolM) EnrolM
			, sum(E.EnrolF) EnrolF
			, sum(E.EnrolNYM) EnrolNYM
			, sum(E.EnrolNYF) EnrolNYF
			, sum(E.EnrolNYNextLevelM) EnrolNYNextLevelM
			, sum(E.EnrolNYNextLevelF) EnrolNYNextLevelF
			, sum(E.RepM) RepM
			, sum(E.RepF) RepF
			, sum(E.RepNYM) RepNYM
			, sum(E.RepNYF) RepNYF
			, sum(E.RepNYNextLevelM) RepNYNextLevelM
			, sum(E.RepNYNextLevelF) RepNYNextLevelF
			, sum(
				case when E.[Age of Data] > 0
					then (E.[Age of Data]*isnull([EnrolNYM],0)+[EnrolM])/cast((E.[Age Of Data]+1) as float)
					else enrolM
				end ) SlopedEnrolM
			, sum(
				case when E.[Age of Data] > 0
					then (E.[Age of Data]*isnull([EnrolNYF],0)+[EnrolF])/cast((E.[Age Of Data]+1) as float)
					else enrolF
				end ) SlopedEnrolF
		from #efa E
			INNER JOIN #dsSmall DS
			on DS.[Survey ID] = e.surveyDimensionssID
			left join Dimensionrank R
			on E.svyYear = R.[svyYear] and E.schNo = R.schNo

		group by E.svyYear
			, E.levelCode
			, E.Estimate
			, DS.SchoolTypeCode
			, DS.[District Code]
			, R.[District Dectile]
			, R.[District Quartile]

			, R.Dectile
			, R.Quartile

	)
		Select X.[Survey Year]
			, X.Estimate
			, G.*
			, X.SchoolTypeCode
			, ST.stDescription SchoolType
			, X.[District Code]
			, D.dName District
			, DL.*
			, [District Dectile]
			, [District Quartile]

			, Dectile
			, Quartile
			, (case when G.GenderCode = 'M' then X.enrolM else X.enrolF end) Enrol
			, (case when G.GenderCode = 'M' then X.SlopedEnrolM else X.SlopedEnrolF end) SlopedEnrol
			, (case when G.GenderCode = 'M' then X.enrolNYM else X.enrolNYF end) EnrolNY
			, (case when G.GenderCode = 'M' then X.enrolNYNextLevelM else X.enrolNYNextLevelF end) EnrolNYNextLevel
			, (case when G.GenderCode = 'M' then X.RepM else X.RepF end) Rep
			, (case when G.GenderCode = 'M' then X.RepNYM else X.RepNYF end) RepNY
			, (case when G.GenderCode = 'M' then X.RepNYNextLevelM else X.RepNYNextLevelF end) RepNYNextLevel


		from X
			INNER JOIN #DL DL
			on X.levelCode = DL.[LevelCode]
			INNER JOIN Districts D
			on X.[District Code] = D.dID
			INNER JOIN SchoolTypes ST
			on X.SchoolTypeCode = ST.stcode
			CROSS JOIN #G G
	END
DROP TABLE #ebse
DROP TABLE #efa
END
GO

