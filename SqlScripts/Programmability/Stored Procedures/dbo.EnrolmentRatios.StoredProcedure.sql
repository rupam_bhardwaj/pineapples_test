SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 2 2010
-- Description:	Lite version of EFA5 query
-- =============================================
CREATE PROCEDURE [dbo].[EnrolmentRatios]
	-- Add the parameters for the stored procedure here
	@SendASXML int = 0,
	@xmlout xml = null OUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #erData
(
[Survey Year] int
, Estimate int
, Age int
, LevelCode nvarchar(10)
, enrolM int
, enrolF int
, repM int
, repF int
, popM int
, popF int
, intakeM int
, intakeF int
, popModCode nvarchar(10)
, popModName nvarchar(50)
, popModDefault int
, atAgeLevel int
, atEdLevelAge int
, atEdLevelAltAge int
, atEdLevelAlt2Age int
, edLevelCode nvarchar(10)
, edLevelAltCode nvarchar(10)
, edLevelAlt2Code nvarchar(10)
)

declare @FirstYear int


SELECT @FirstYear = min(popYear)
From dbo.Population P
INNER JOIN PopulationModel PM
	ON P.popmodCode = PM.popModCode
WHERE PM.popmodEFA = 1


select @FirstYear = case when @FirstYEar < year(getdate()) - 5 then year(getdate()) - 5
						else @FirstYear end

INSERT INTO #erData
EXEC dbo.sp_EFA5Data 1, @FirstYear

Select *
, case
	when isnull(popM , 0 ) = 0 then null
	else cast(enrolM as float)/popM
	end gerM
, case
	when isnull(popF , 0 ) = 0 then null
	else cast(enrolF as float)/popF
	end gerF
, case
	when isnull(pop , 0 ) = 0 then null
	else cast(enrol as float)/pop
	end ger
, case
	when isnull(popM , 0 ) = 0 then null
	else cast(nEnrolM as float)/popM
	end nerM

, case
	when isnull(popF , 0 ) = 0 then null
	else cast(nEnrolF as float)/popF
	end nerF
, case
	when isnull(pop , 0 ) = 0 then null
	else cast(nEnrol as float)/pop
	end ner

, case
	when isnull(nEnrolM,0) = 0 then null
	when isnull(popM,0) = 0 then null
	else (cast(nEnrolf as float)/popF)/(cast(nEnrolM as float)/popM)
  end nerGPI
, case
	when isnull(nEnrolM,0) = 0 then null
	else (cast(Enrolf as float)/popF)/(cast(EnrolM as float)/popM)
	end gerGPI

, case
	when isnull(intakePopM, 0) = 0 then null
	else cast(intakeM as float)/IntakepopM
	end girM
, case
	when isnull(intakePopF, 0) = 0 then null
	else cast(IntakeF as float)/IntakePopF
	end girF
,  case
	when isnull(intakePop, 0) = 0 then null
	else cast(Intake as float)/IntakePop
	end	gir

, case
	when isnull(intakePopM, 0) = 0 then null
	else cast(nintakeM as float)/IntakepopM
	end nirM
, case
	when isnull(intakePopF, 0) = 0 then null
	else cast(nIntakeF as float)/IntakePopF
	end nirF
, case
	when isnull(intakePop, 0) = 0 then null
	else cast(nIntake as float)/IntakePop
	end	nir

, case
	when isnull(popLastLevelM, 0) = 0 then null
	else cast(intakeLastLevelM as float)/popLastLevelM
	end girLastLevelM
, case
	when isnull(popLastLevelF, 0) = 0 then null
	else cast(intakeLastLevelF as float)/popLastLevelF
	end girLastLevelF
, case
	when isnull(popLastLevel, 0) = 0 then null
	else cast(intakeLastLevel as float)/popLastLevel
	end girLastLevel

, case
	when isnull(Enrol,0) = 0 then null
	else cast(EstimateEnrol as float)/Enrol
	end	EstimatePerc
INTO #erData2
from
(
Select [Survey Year]
, edLevelCode
, edlMinYear MinYoE
, edlMaxYear MaxYoE
, edlMaxYear - edlMinYear + 1 edlevelYears
, svyPSAge + edlMinYear - 1 startAge
, sum(enrolM) enrolM
, sum(enrolF) enrolF
, sum(isnull(enrolM,0)+isnull(enrolF,0)) Enrol
, sum(repM) repM
, sum(repF) repF
, sum(isnull(repM,0) + isnull(repF,0)) rep
, sum(popM) popM
, sum(popF) popF
, sum(isnull(popM,0) + isnull(popF,0)) pop

, sum(case when L.lvlYear = EL.edlMinYear then popM else null end) intakePopM
, sum(case when L.lvlYear = EL.edlMinYear then popF else null end) intakePopF
, sum(case when L.lvlYear = EL.edlMinYear then isnull(popM,0) + isnull(popF,0)  else null end) intakePop

, sum(case when L.lvlYear = EL.edlMaxYear then popM else null end) PopLastLevelM
, sum(case when L.lvlYear = EL.edlMaxYear then popF else null end) PopLastLevelF
, sum(case when L.lvlYear = EL.edlMaxYear then isnull(popM,0) + isnull(popF,0)  else null end) PopLastLevel

, sum(case when L.lvlYear = EL.edlMinYear then intakeM else null end) intakeM
, sum(case when L.lvlYear = EL.edlMinYear then intakeF else null end) intakeF
, sum(case when L.lvlYear = EL.edlMinYear then isnull(intakeM,0) + isnull(intakeF,0) else null end) intake


, sum(case when L.lvlYear = EL.edlMinYear and atAgeLevel = 1 then intakeM else null end) nintakeM
, sum(case when L.lvlYear = EL.edlMinYear and atAgeLevel = 1 then intakeF else null end) nintakeF
, sum(case when L.lvlYear = EL.edlMinYear  and atAgeLevel = 1 then isnull(intakeM,0) + isnull(intakeF,0) else null end) nIntake

, sum(case when L.lvlYear = EL.edlMaxYear then intakeM else null end) intakeLastLevelM
, sum(case when L.lvlYear = EL.edlMaxYear then intakeF else null end) intakeLastLevelF
, sum(case when L.lvlYear = EL.edlMaxYear then isnull(intakeM,0) + isnull(intakeF,0) else null end) intakeLastLevel


, sum(case when atEdLevelAge = 1 then enrolM else null end) nEnrolM
, sum(case when atEdLevelAge = 1 then enrolF else null end) nEnrolF
, sum(case when atEdLevelAge = 1 then isnull(enrolM,0) + isnull(enrolF,0) else null end) nEnrol

, sum(case when Estimate = 1 then isnull(enrolM,0)+isnull(enrolF,0) else null end) EstimateEnrol
from #erData
INNER JOIN lkpLevels L
	ON #erData.levelCode = L.codeCode
INNER JOIN lkpEducationLevels EL
	ON #erData.edLevelCode = EL.codeCode
INNER JOIN Survey S
	ON #erData.[Survey Year] = S.svyYear
GROUP BY [Survey YEar], edLevelCode
, edlMinYear, edlMaxYear, svyPSAge

UNION
Select [Survey Year]
, edLevelAltCode
, edlMinYear MinYoE
, edlMaxYear MaxYoE
, edlMaxYear - edlMinYear + 1 edlevelYears
, svyPSAge + edlMinYear - 1 startAge
, sum(enrolM) enrolM
, sum(enrolF) enrolF
, sum(isnull(enrolM,0)+isnull(enrolF,0)) Enrol
, sum(repM) repM
, sum(repF) repF
, sum(isnull(repM,0) + isnull(repF,0)) rep
, sum(popM) popM
, sum(popF) popF
, sum(isnull(popM,0) + isnull(popF,0)) pop

, sum(case when L.lvlYear = EL.edlMinYear then popM else null end) intakePopM
, sum(case when L.lvlYear = EL.edlMinYear then popF else null end) intakePopF
, sum(case when L.lvlYear = EL.edlMinYear then isnull(popM,0) + isnull(popF,0) else null end) intakePop

, sum(case when L.lvlYear = EL.edlMaxYear then popM else null end) PopLastLevelM
, sum(case when L.lvlYear = EL.edlMaxYear then popF else null end) PopLastLevelF
, sum(case when L.lvlYear = EL.edlMaxYear then isnull(popM,0) + isnull(popF,0) else null end) PopLastLevel

, sum(case when L.lvlYear = EL.edlMinYear then intakeM else null end) intakeM
, sum(case when L.lvlYear = EL.edlMinYear then intakeF else null end) intakeF
, sum(case when L.lvlYear = EL.edlMinYear then isnull(intakeM,0) + isnull(intakeF,0) else null end) intake


, sum(case when L.lvlYear = EL.edlMinYear and atAgeLevel = 1 then intakeM else null end) nintakeM
, sum(case when L.lvlYear = EL.edlMinYear and atAgeLevel = 1 then intakeF else null end) nintakeF
, sum(case when L.lvlYear = EL.edlMinYear  and atAgeLevel = 1 then isnull(intakeM,0) + isnull(intakeF,0) else null end) nIntake

, sum(case when L.lvlYear = EL.edlMaxYear then intakeM else null end) intakeLastLevelM
, sum(case when L.lvlYear = EL.edlMaxYear then intakeF else null end) intakeLastLevelF
, sum(case when L.lvlYear = EL.edlMaxYear then isnull(intakeM,0) + isnull(intakeF,0) else null end) intakeLastLevel


, sum(case when atEdLevelAltAge = 1 then enrolM else null end) nEnrolM
, sum(case when atEdLevelAltAge = 1 then enrolF else null end) nEnrolF
, sum(case when atEdLevelAltAge = 1 then isnull(enrolM,0) + isnull(enrolF,0) else null end) nEnrol

, sum(case when Estimate = 1 then isnull(enrolM,0)+isnull(enrolF,0) else null end) EstimateEnrol

from #erData
INNER JOIN lkpLevels L
	ON #erData.levelCode = L.codeCode
INNER JOIN lkpEducationLevelsAlt EL
	ON #erData.edLevelAltCode = EL.codeCode
INNER JOIN Survey S
	ON #erData.[Survey Year] = S.svyYear
GROUP BY [Survey YEar], edLevelAltCode
, edlMinYear, edlMaxYear, svyPSAge

UNION
Select [Survey Year]
, edLevelAlt2Code
, edlMinYear MinYoE
, edlMaxYear MaxYoE
, edlMaxYear - edlMinYear + 1 edlevelYears
, svyPSAge + edlMinYear - 1 startAge
, sum(enrolM) enrolM
, sum(enrolF) enrolF
, sum(isnull(enrolM,0)+isnull(enrolF,0)) Enrol
, sum(repM) repM
, sum(repF) repF
, sum(isnull(repM,0) + isnull(repF,0)) rep
, sum(popM) popM
, sum(popF) popF
, sum(isnull(popM,0) + isnull(popF,0)) pop

, sum(case when L.lvlYear = EL.edlMinYear then popM else null end) intakePopM
, sum(case when L.lvlYear = EL.edlMinYear then popF else null end) intakePopF
, sum(case when L.lvlYear = EL.edlMinYear then isnull(popM,0) + isnull(popF,0) else null end) intakePop

, sum(case when L.lvlYear = EL.edlMaxYear then popM else null end) PopLastLevelM
, sum(case when L.lvlYear = EL.edlMaxYear then popF else null end) PopLastLevelF
, sum(case when L.lvlYear = EL.edlMaxYear then isnull(popM,0) + isnull(popF,0) else null end) PopLastLevel

, sum(case when L.lvlYear = EL.edlMinYear then intakeM else null end) intakeM
, sum(case when L.lvlYear = EL.edlMinYear then intakeF else null end) intakeF
, sum(case when L.lvlYear = EL.edlMinYear then isnull(intakeM,0) + isnull(intakeF,0) else null end) intake


, sum(case when L.lvlYear = EL.edlMinYear and atAgeLevel = 1 then intakeM else null end) nintakeM
, sum(case when L.lvlYear = EL.edlMinYear and atAgeLevel = 1 then intakeF else null end) nintakeF
, sum(case when L.lvlYear = EL.edlMinYear  and atAgeLevel = 1 then isnull(intakeM,0) + isnull(intakeF,0) else null end) nIntake

, sum(case when L.lvlYear = EL.edlMaxYear then intakeM else null end) intakeLastLevelM
, sum(case when L.lvlYear = EL.edlMaxYear then intakeF else null end) intakeLastLevelF
, sum(case when L.lvlYear = EL.edlMaxYear then isnull(intakeM,0) + isnull(intakeF,0) else null end) intakeLastLevel

, sum(case when atEdLevelAlt2Age = 1 then enrolM else null end) nEnrolM
, sum(case when atEdLevelAlt2Age = 1 then enrolF else null end) nEnrolF
, sum(case when atEdLevelAlt2Age = 1 then isnull(enrolM,0) + isnull(enrolF,0) else null end) nEnrol

, sum(case when Estimate = 1 then isnull(enrolM,0)+isnull(enrolF,0) else null end) EstimateEnrol

from #erData
INNER JOIN lkpLevels L
	ON #erData.levelCode = L.codeCode
INNER JOIN lkpEducationLevelsAlt2 EL
	ON #erData.edLevelAlt2Code = EL.codeCode
INNER JOIN Survey S
	ON #erData.[Survey Year] = S.svyYear
GROUP BY [Survey YEar], edLevelAlt2Code
, edlMinYear, edlMaxYear, svyPSAge
) sub
WHERE edLevelCode is not null
ORDER BY [Survey Year],edLevelCode
--SELECT * from #erData


IF @SENDASXML = 0 begin

	SELECT * from #erData2

end

if @SendAsXML <> 0 begin
	declare @xml xml
	declare @xml2 xml


	SELECT @xml =
	(
		SELECT [Survey Year] [@year]
		, edLevelCode			[@edLevelCode]
		, MinYoE				[@firstYear]
		, MaxYoE				[@lastYear]
		, edlevelYears			[@numYears]
		, startAge				[@startAge]
		, popM
		, popF
		, pop

		, enrolM
		, enrolF
		, enrol
		, nEnrolM
		, nEnrolF
		, nEnrol

		, intakeM
		, intakeF
		, intake
		, nIntakeM
		, nIntakeF
		, nIntake

		, repM
		, repF
		, rep
		, cast(gerM as decimal(8,5)) gerM
		, cast(gerF as decimal(8,5)) gerF
		, cast(ger as decimal(8,5)) ger
		, cast(nerM as decimal(8,5)) nerM
		, cast(nerF as decimal(8,5)) nerF
		, cast(ner as decimal(8,5)) ner
		, cast(girM as decimal(8,5)) girM
		, cast(girF as decimal(8,5)) girF
		, cast(gir as decimal(8,5)) gir
		, cast(nirM as decimal(8,5)) nirM
		, cast(nirF as decimal(8,5)) nirF
		, cast(nir as decimal(8,5)) nir
		,cast(EstimatePerc as decimal(7,4)) est
	FROM #erData2
	FOR XML PATH('ER')
	)

	SELECT @xmlout
	=
	(SELECT @Xml
	 FOR XML PATH('ERs')
	 )

	 if @SendAsXML = 1
			SELECT @xmlout		-- don't return a confusing resultset if we only want the output param
end

END
GO
GRANT EXECUTE ON [dbo].[EnrolmentRatios] TO [public] AS [dbo]
GO

