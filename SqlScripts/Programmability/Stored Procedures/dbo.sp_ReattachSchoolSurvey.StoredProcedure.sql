SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 12 2007
-- Description:	reattach a survey to a differnet school
-- =============================================
CREATE PROCEDURE [dbo].[sp_ReattachSchoolSurvey]
	-- Add the parameters for the stored procedure here
	@SurveyID int,
	@NewSchool nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- don;t reattach if the school typeon the survey does not match the school type
-- of the target school

declare @SurveyType nvarchar(10)
declare @TargetType nvarchar(10)

select @SurveyType = ssSchType
from SchoolSurvey
WHERE ssID = @SurveyID

select @TargetType =schType
from Schools
WHERE schNo = @NewSchool

if @TargetType <> @SurveyType
	raiserror (N'School type does not match',10,1)

UPDATE SchoolSurvey
	set schNo = @NewSchool
WHERE ssID = @SurveyID


END
GO

