SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	Update classes information
--					version 1 will use the simpler Kiribati format
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[xfdfClass]
	-- Add the parameters for the stored procedure here
	@surveyID int,
	@grid xml
AS
BEGIN

/*

	<field name="Class">
			<field name="C">
				<field name="00">
					<field name="V">
						<value>Class 1</value>
					</field>
					<field name="K">
						<value>P1</value>
					</field>
				</field>
				...
			</field>
			<field name="D">			-- data node
				<field name="00">
					<field name="T0">
						<field name="ID">
							<value>2001034</value>
						</field>
						<field name="Given">
							<value>Fred</value>
						</field>
						<field name="Family">
							<value>Green</value>
						</field>
					</field>
					<field name="00">
						<field name="All">
							<value>40</value>
						</field>
					</field>
					<field name="01">
						<field name="All" />
					</field>
					<field name="02">
						<field name="All" />
					</field>

*/

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- this process wi will write to classes, class level , and class teacher
--

	declare @classes TABLE
	(
		seq			int
		, r			nvarchar(2)
		, category	nvarchar(1)
		, hrsWeek	numeric(18,2)
		, lang		nvarchar(5)
		, roomID	int
		, subjCode	nvarchar(10)
		, shiftCode	nvarchar(10)
	)

	declare @levels TABLE
	(
		r			nvarchar(2)					-- ties the levels to the class
		, c			nvarchar(2)			-- pointer to the current column, from which we get the level code
		, Tot		int					-- support non-gender split ( e.g, KI or gender split formats)
		, M			int
		, F			int
	)

	declare @teachers TABLE
	(
		r			nvarchar(2)
		,tseq		int
		,employnum	nvarchar(20)
		,firstname	nvarchar(50)
		,familyname	nvarchar(50)
		, tID		int						-- we'll find this and update it
	)


	declare @colSet table(

	c nvarchar(2)
	, ck nvarchar(20)
	, cv nvarchar(100)
							)

	declare @idoc int

	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'

	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid, @xmlns


declare @category nvarchar(1)

	Select @category = case when rootnode = 'ClassJ' then 'J' else 'C' end
	FROM OPENXML(@idoc,'/x:field',2)		-- base node is the row seq node
	WITH
	(

		rootnode		nvarchar(10)		'@name'
	)

declare @tag nvarchar(10)
select @tag = 'Classes:' + @category
-- populate the table of classes
	INSERT INTO @classes
	(
		seq
		, r
		, category
		, hrsWeek
		, lang

		, subjCode
		, shiftCode
	)
	Select
	convert(int, r)
	, r
	, @category
	, hrsWeek
	, lang
	, subjCode
	, shiftCode

	FROM OPENXML(@idoc,'/x:field/x:field[@name="D"]/x:field',2)		-- base node is the row seq node
	WITH
	(

		r			nvarchar(2)		'@name'

		, hrsWeek	numeric(18,2)	'x:field[@name="HrsWeek"]/x:value'
		, lang		nvarchar(5)		'x:field[@name="Language"]/x:value'
		, subjCode	nvarchar(10)	'x:field[@name="Subject"]/x:value'
		, shiftCode	nvarchar(10)	'x:field[@name="Shift"]/x:value'
		)

	INSERT INTO @levels
		(
		r
		, c
		, Tot
		, M
		, F
	)
	SELECT *
	FROM OPENXML(@idoc,'/x:field/x:field[@name="D"]/x:field/x:field',2)		-- base node is the col seq node
	WITH
	(
		r	nvarchar(2)			'../@name'
		, c	  nvarchar(2)		'@name'
		, tot int				'x:field[@name="All"]/x:value'
		, m int					'x:field[@name="M"]/x:value'
		, f int					'x:field[@name="F"]/x:value'
	)
	WHERE c not in ('T0','T1')		-- the teacher nodes hand under the row node directly
	AND (tot is not null or m is not null or f is not null)


	INSERT INTO @teachers
		(
		r
		,tseq
		,employnum
		,firstname
		,familyname
	)
	SELECT r, 0, employnum, firstname, familyname
	FROM OPENXML(@idoc,'/x:field/x:field[@name="D"]/x:field/x:field[@name="T0"]',2)		-- Teacher 0 node
	WITH
	(
		r					nvarchar(2)			'../@name'
		, employnum			nvarchar(50)		'x:field[@name="ID"]/x:value'
		, firstname			nvarchar(50)		'x:field[@name="Given"]/x:value'
		, familyname		nvarchar(50)		'x:field[@name="Family"]/x:value'
	)
	WHERE (employnum is not null or firstname is not null or familyname is not null)
	-- repeat for the second teacher
	INSERT INTO @teachers
		(
		r
		,tseq
		,employnum
		,firstname
		,familyname
	)
	SELECT r, 1, employnum, firstname, familyname
	FROM OPENXML(@idoc,'/x:field/x:field[@name="D"]/x:field/x:field[@name="T1"]',2)		-- Teacher 1 node
	WITH
	(
		r					nvarchar(2)			'../@name'
		, employnum			nvarchar(50)		'x:field[@name="ID"]/x:value'
		, firstname			nvarchar(50)		'x:field[@name="Given"]/x:value'
		, familyname		nvarchar(50)		'x:field[@name="Family"]/x:value'
	)
	WHERE (employnum is not null or firstname is not null or familyname is not null)

	-- collect the column headers, whioch are class levels
	INSERT INTO @colSet
	Select *
	FROM OPENXML(@idoc ,'/x:field/x:field[@name="C"]/x:field',2)
	WITH
	(
		c			nvarchar(2)			'@name'
		, ck			nvarchar(2)			'x:field[@name="K"]/x:value'
		, cv			nvarchar(20)			'x:field[@name="V"]/x:value'
	)


Select * from @Classes
Select * from @Levels
Select * from @Teachers
Select * from @ColSet

-- a bit of cleaning up - we can now remove all the classes that don't have a level or a teacher
DELETE FROM @Classes
FROM @Classes
	LEFT JOIN @Levels L
		ON [@Classes].r = L.r
	LEFT JOIN @Teachers T
		ON [@Classes].r = T.r
WHERE T.r is null and L.r is null

Select * from @Classes
-- now try to identify the teacher, based on what we know - the surveyID, employnum and name
--
	UPDATE @teachers
	SET tID = TI.tID
	FROM @teachers
		INNER JOIN TeacherIdentity TI
			ON [@teachers].employnum = TI.tPayroll
			AND [@teachers].familyname = TI.tSurname
	WHERE TI.tID in (Select tID from TeacherSurvey WHERE ssID = @SurveyID)
	AND [@teachers].tID is null

	UPDATE @teachers
	SET tID = TI.tID
	FROM @teachers
		INNER JOIN TeacherIdentity TI
			ON [@teachers].firstname = TI.tGiven
			AND [@teachers].familyname = TI.tSurname
	WHERE TI.tID in (Select tID from TeacherSurvey WHERE ssID = @SurveyID)
	AND [@teachers].tID is null

	UPDATE @teachers
	SET tID = TI.tID
	FROM @teachers
		INNER JOIN TeacherIdentity TI
			ON [@teachers].employnum = TI.tPayroll
	WHERE TI.tID in (Select tID from TeacherSurvey WHERE ssID = @SurveyID)
	AND [@teachers].tID is null

-- if not in TeacherSurvey, may still be a match if a Satellite / parent school arrangement
-- match on everything...

	UPDATE @teachers
	SET tID = TI.tID
	FROM @teachers
		INNER JOIN TeacherIdentity TI
			ON coalesce([@teachers].employnum,'') = coalesce(TI.tPayroll,'')
			AND [@teachers].firstname = TI.tGiven
			AND [@teachers].familyname = TI.tSurname
	WHERE TI.tID in (Select tID from TeacherSurvey WHERE ssID = @SurveyID)
	AND [@teachers].tID is null


-- start the transaction

begin transaction

begin try

	-- delete only specified rows and columns

	DELETE from Classes
	WHERE ssID = @SurveyID
		AND pcCat = @Category

	INSERT INTO Classes
	(
		ssID
		, pcCat
		, pcSeq
		, pcHrsWeek
		, pcLang
		, subjCode
		, shiftCode
	)
	SELECT
		@surveyID
		, @category
		, seq
		, hrsWeek
		, lang
		, subjCode
		, shiftCode

	FROM @Classes
	exec audit.xfdfInsert @SurveyID, 'classes inserted',@tag,@@rowcount
-- now add the levels
	INSERT INTO ClassLevel
	(
		pcID
		, pclLevel
		, pclNum
		, pclF
		, pclM
	)
	Select
		Classes.pcID
		, COLS.ck			-- the class level key
		, L.Tot
		, L.F
		, L.M
	FROM Classes
		INNER JOIN @Classes CC			-- needed to get the row ID back for this sequence number
		ON Classes.pcSeq = CC.seq
			AND Classes.pcCat = @category
			AND Classes.ssID = @surveyID
		INNER JOIN @levels L			-- class level data
			ON CC.r = L.r
		INNER JOIN @colset COLS
			ON L.c = COLS.c

	exec audit.xfdfInsert @SurveyID, 'class levels inserted',@tag,@@rowcount
	-- teachers?
	-- ??

	INSERT INTO ClassTeacher
	(
		pcID
		, pctSeq
		, TT.tID
	)
	SELECT Classes.pcID
		, TT.tSeq
		, TT.tID
	FROM Classes
		INNER JOIN @Classes CC			-- needed to get the row ID back for this sequence number
		ON Classes.pcSeq = CC.seq
			AND Classes.pcCat = @category
			AND Classes.ssID = @surveyID
		INNER JOIN @teachers TT
			ON cc.r = TT.r
	exec audit.xfdfInsert @SurveyID, 'class teachers inserted',@tag,@@rowcount


end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,@tag
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit
if @@trancount > 0
	commit transaction

return


END
GO

