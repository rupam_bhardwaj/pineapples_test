SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 12 2007
-- Description:	update teachers from smis xml format
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[updateTeacherIdentity]
	-- Add the parameters for the stored procedure here

	@ID int ,
	@Payroll nvarchar(50) ,
	@NamePRefix nvarchar(50),
	@Given nvarchar(50) ,
	@MiddleNames nvarchar(50),
	@Surname nvarchar(50) ,
	@NameSuffix nvarchar(50),
	@Sex nvarchar(1) ,
	@DOB datetime ,
	@Register nvarchar(50) ,
	@Provident nvarchar(50) ,
	@YearStarted int ,
	@YearFinished int ,
	@DatePSAppointed datetime ,
	@DatePSClosed datetime ,
	@SalaryPoint nvarchar(10) ,
	@SalaryPointDate datetime ,
	@payptCode nvarchar(10) ,
	@EditContext nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
----	SET NOCOUNT ON;

begin try
	UPDATE TeacherIdentity
	SET tPayroll = @PAyroll
	, tNamePrefix = @NamePrefix
	, tGiven = 	@Given
	, tMiddleNames = @MiddleNames
	, tSurname = 	@Surname
	, tNameSuffix = @NameSuffix
	, tSex = 	@Sex
	, tDoB = @DOB
	, tRegister = @Register
	, tPRovident = 	@Provident
	, tYearStarted = 	@YearStarted
	, tYearFinished = 	@YearFinished
	, tDatePSAppointed = 	@DatePSAppointed
	, tDatePSClosed = 	@DatePSClosed
	, tSalaryPoint = 	@SalaryPoint
	, tSalaryPointDate = 	@SalaryPointDate
	, tPayPtCode = 	@payptCode
	, pEditContext = @EditContext

	WHERE tID = @ID
	if @@ROWCOUNT <> 1
		RAISERROR('The teacher identity was not found', 16,1)

end try
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction


-- update all the required fields on TeacherIdentity affected by the survey
END
GO

