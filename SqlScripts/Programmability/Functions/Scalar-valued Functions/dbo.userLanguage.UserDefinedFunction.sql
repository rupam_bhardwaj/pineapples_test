SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[userLanguage]
(
	-- Add the parameters for the function here

)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int


	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = uLang from UserInfo WHERE uLogin = dbo.currentUser()

	-- Return the result of the function
	RETURN isnull(@Result,0)

END
GO
GRANT EXECUTE ON [dbo].[userLanguage] TO [public] AS [dbo]
GO

