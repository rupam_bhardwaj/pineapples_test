SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [pSchoolRead].[fnSchoolPayAllowances]
(
	@SchoolNo nvarchar(400)
	, @Year int
)
RETURNS nvarchar(400)
AS
BEGIN


	RETURN
  STUFF(
    (
    SELECT

      ' ' + estaCode
    FROM EstablishmentAllowance A
    INNER JOIN ListSchools
		ON A.lstName = ListSchools.lstName
			AND ( A.estaListValue = ListSchools.lstValue or A.estaListValue is null)
    WHERE schNo = @SchoolNo
    AND A.estaYear = @Year
    FOR XML PATH('')
    ), 1, 1, ''
  )


END
GO

