SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 04 2010
-- Description:	Generic function to return a recordset based on std ManpowerFilter
-- =============================================
CREATE FUNCTION [pEstablishmentRead].[readManpower]
(
	 @asAtDate datetime = null
	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null
	, @RoleGrade nvarchar(10) = null
	, @Role nvarchar(10) = null
	, @Sector nvarchar(10) = null
	, @Unfilled bit = 0
	, @PayslipExceptions bit = 0
	, @SurveyExceptions bit = 0
	, @SalaryPointExceptions bit = 0
	, @PositionFlag nvarchar(10) = null
	, @PositionFlagOp bit = 1

)
RETURNS
@wf TABLE
(
	-- Add the column definitions for the TABLE variable here
	estpNo nvarchar(20)
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	if (@AsAtDate is null)
		Select @AsAtDate = common.Today()

	declare @LastSurvey int
	declare @LastSurveyDate datetime

	Select @LastSurvey = max(svyYear)
			, @LastSurveyDate = max(svyCensusDate)
	from Survey
	WHERE svyCensusDate <= @AsAtDate

INSERT INTO @wf
Select E.estpNo

from Establishment E
	INNER JOIN
		-- does it make a slight optimisation pushing these criteria to the top,
		-- instead of at the end?
		(Select schNo, schName, schType, schAuth
			from Schools
			WHERE
				(SchAuth = @Authority or @Authority is null)
				AND (schType = @SchoolType or @SchoolType is null)

		) Schools
		ON E.schNo = Schools.schNo
	INNER JOIN RoleGrades ON RoleGrades.rgCode = E.estpRoleGrade
	INNER JOIN lkpTeacherRole Roles ON RoleGRades.roleCode = Roles.codeCode
	LEFT JOIN
			( Select *
				from TeacherAppointment  Appt
				WHERE (Appt.taDate <= @asAtDate)
				AND (Appt.taEndDate >= @AsAtDate or Appt.taEndDate is null)
			) Appts
		ON Appts.estpNo = E.estpNo
	LEFT JOIN TEacherIdentity TI
		ON TI.tID = Appts.tID
	LEFT JOIN
			( Select *
				from TeacherPaySlips
				WHERE tpsPeriodStart <= @AsAtDate
					AND tpsPeriodEnd >= @AsAtDate
			) Slips
		   ON Slips.tpsPayroll = TI.tPayroll
		-- ON Slips.tID = TI.tID

	LEFT JOIN (Select tID, SchoolSurvey.SchNo , SchoolSurvey.svyYear, TeacherSurvey.tchRole
					from TeacherSurvey INNER JOIN SchoolSurvey
						ON TeacherSurvey.ssID = SchoolSurvey.ssID
				WHERE SvyYear = @LastSurvey) TS
		ON TS.tID = TI.tID
	LEFT JOIN (Select schNo, max(svyYear) lastTeacherInfo
					from TeacherSurvey INNER JOIN SchoolSurvey
						ON TeacherSurvey.ssID = SchoolSurvey.ssID
				GROUP BY schNo ) LastTeacherInfoYear

		ON LastTeacherInfoYear.schNo = E.schNo

	LEFT JOIN lkpSalaryPoints SalPt
		ON SalPt.spCode = slips.tpsSalaryPoint

	WHERE
		(E.schNo = @SchoolNo or @SchoolNo is null)
		-- condiotions on authority and school type moved into the FROM
		AND (E.estpRoleGrade = @RoleGrade OR @roleGrade is null)
		AND (RoleGrades.roleCode = @Role or @Role is null)
		AND (Roles.secCode = @Sector or @Sector is null)
		AND (E.estpActiveDate <= @asAtDate)
		AND (E.estpClosedDate >= @AsAtDate or E.estpClosedDate is null)
		AND ((@PayslipExceptions = 1
				AND
				(( Slips.tpsPosition <> E.estpNo)
				OR (Slips.tpsID is null and TI.tID is not null))		-- there is no payslip....
			  )
			 OR
				(@Unfilled = 1 AND Appts.tID is null)
			 OR
				(@Unfilled = 0 and 	@PayslipExceptions = 0)
			)
		AND (@SurveyExceptions = 0
				-- a survey exception is when
				-- the teacher has appeared some place else
				OR (@LastSurveyDate between Appts.taDate and isnull(Appts.taEndDate,'2999-12-31')
					AND
						(
						TS.schNo <> E.schNo
						-- or the teacher has not appeared anywhere, but the school survey is in
						OR (TS.tID is null and lastTeacherInfo = @LastSurvey and TI.tID is not null)
						)
					)
			)
		AND ( @SalaryPointExceptions = 0
				-- salary point is outside the range for the position
				OR SalPt.salLevel  < RoleGrades.rgSalaryLevelMin
				OR SalPt.salLevel > RoleGRades.rgSalaryLevelMax
			)
		AND ( @PositionFlag is null
				OR (@positionFlagOp = 1 and estpFlag = @PositionFlag)
				OR (@positionFlagOp = 0 and ( estpFlag <> @PositionFlag or estpFlag is null))
			)

	RETURN
END
GO

