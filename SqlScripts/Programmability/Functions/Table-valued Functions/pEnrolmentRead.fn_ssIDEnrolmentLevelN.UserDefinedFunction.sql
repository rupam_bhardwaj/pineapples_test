SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [pEnrolmentRead].[fn_ssIDEnrolmentLevelN]
(
	@LifeYear int
)
RETURNS TABLE
AS
RETURN
(
	-- Add the SELECT statement with parameter references here
	SELECT     Enrollments.ssID,
enLevel AS LevelCode,
lvlYear as YearOfEd,
SUM(enM) AS EnrolM,
SUM(enF) AS enrolF,
SUM(isnull(enM,0) + isnull(enF,0)) AS Enrol,
sum(case when enAge = svyPSAge + L.lvlYear - 1 then enM else null end) nEnrolM,
sum(case when enAge = svyPSAge + L.lvlYear - 1 then enF else null end) nEnrolF,
sum(case when enAge = svyPSAge + L.lvlYear - 1
		then isnull(enM,0) + isnull(enF,0) else null end
	) nEnrol,
svyPSAge + L.lvlYear - 1 OfficialAge

FROM   dbo.Enrollments
INNER JOIN lkpLevels L
ON Enrollments.enLevel = L.codecode
INNER JOIN SchoolSurvey SS
ON SS.ssID = Enrollments.ssID
LEFT JOIN Survey S
ON S.svyYear = @LifeYear
GROUP BY Enrollments.ssID, enLevel, lvlYear, svyPSAge
)
GO

