SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[tfnQualityIssues]
(
	-- Add the parameters for the function here
	@Item nvarchar(50),
	@SubItem nvarchar(50)
)
RETURNS TABLE
AS
RETURN
(
	-- Add the SELECT statement with parameter references here
	SELECT ssID,ssqLevel  , ssqDataItem, ssqSubITem
	from SchoolSurveyQuality WHERE ssqDataItem = @Item
AND (ssqSubItem = @subItem OR @SubItem is null)
)
GO
GRANT SELECT ON [dbo].[tfnQualityIssues] TO [public] AS [dbo]
GO

