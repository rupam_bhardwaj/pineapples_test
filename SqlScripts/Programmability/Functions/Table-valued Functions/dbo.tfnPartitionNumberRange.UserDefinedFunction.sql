SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[tfnPartitionNumberRange]
(
	-- Add the parameters for the function here
	@Partition nvarchar(50)
)
RETURNS TABLE
AS
RETURN
(

	SELECT metaNumbers.num, Partitions.ptName
	FROM metaNumbers, Partitions
		WHERE metaNumbers.num Between [ptMin] And [ptMax]
		 AND Partitions.ptSet=@Partition
)
GO
GRANT SELECT ON [dbo].[tfnPartitionNumberRange] TO [public] AS [dbo]
GO

