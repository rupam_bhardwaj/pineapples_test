SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-11
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[MakeEstablishmentNumbers]
(
	-- Add the parameters for the function here
	@SchoolNo nvarchar(50)
	, @numPositions int = 1
)
RETURNS @PositionNumbers TABLE( posNo nvarchar(50)
								, counter int)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(50)

	declare @Prefix nvarchar(20)
	declare @seq int
	declare @MaxNo nvarchar(20)


	Set @Prefix = '272'
	-- Add the T-SQL statements to compute the return value here

	select @MaxNo = isnull(max(estpNo),0) from Establishment
						-- if there are school merges, position numbers can get misaligned with school numbers
						-- allocate only from thse that match this school no.
						--WHERE SchNo = @SchoolNo
						WHERE estpNo like @Prefix + '-' + @SchoolNo + '%'

	Select @seq = cast(right(@MaxNo,3) as int)


	INSERT INTO @PositionNumbers
	SELECT
		@Prefix + '-' + @SchoolNo + '' + replace(str(@seq+num,3),' ','0')
		, num
	FROM metaNumbers
	WHERE num between 1 and @NumPositions
	-- Return the result of the function


	RETURN
END
GO

