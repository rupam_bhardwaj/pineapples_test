SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 12 2007
-- Description:	Values for transfer codes. Transfers may be disaggregated by district, or island, or other...
-- =============================================
CREATE FUNCTION [dbo].[tfnTransferCodes]
(
)
RETURNS
@codes TABLE
(
	-- Add the column definitions for the TABLE variable here
	trfrCode nvarchar(10),
	trfrName nvarchar(50),
	sortKey nvarchar(50)
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	declare @Param nvarchar(20)
	Select @Param = paramText from sysParams WHERE paramName = 'TRANSFER_SPLIT'

if @Param= 'Islands'
	INSERT INTO @codes

	Select
		iCode as TrfrCode,
		iName as TrfrName,
		iCode as SortKey
	from Islands ORDER BY iCode

if @Param = 'Districts'
	INSERT INTO @codes
	SELECT
		dID as TrfrCode,
		dName as TrfrName,
		dName as SortKey
	from Districts ORDER BY dName

INSERT INTO @codes
Select '<OS>' as TrfrCode, 'Overseas' as TrfrName,'ZZ' as SortKey

INSERT INTO @codes

Select '<??>' as TrfrCode, 'Unknown' as TrfrrName,'ZZZ' as SortKey

RETURN
END
GO
GRANT SELECT ON [dbo].[tfnTransferCodes] TO [public] AS [dbo]
GO

