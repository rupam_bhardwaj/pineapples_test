SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 06 2009
-- Description:	Return the set of pay rates in force at the given pay date
-- =============================================
CREATE FUNCTION [dbo].[SalaryPointRatesAsAt]
(
	-- Add the parameters for the function here
	@payDate datetime
)
RETURNS TABLE
AS
RETURN
(
-- Add the SELECT statement with parameter references here
Select SPR.*, @payDate payDate
from SalaryPointRates SPR
	inner join
		(Select spCode, max(spEffective) maxDate from SalaryPointRates
			WHERE spEffective <= @payDate
			group by spCode) SPM
	on SPR.spCode = SPM.spCode
		and SPR.spEffective = SPM.MaxDate
)
GO

