CREATE SYNONYM [dbo].[PrimaryClassTeacher] FOR [dbo].[ClassTeacher]
GO
GRANT SELECT ON [dbo].[PrimaryClassTeacher] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[PrimaryClassTeacher] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[PrimaryClassTeacher] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PrimaryClassTeacher] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PrimaryClassTeacher] TO [public] AS [dbo]
GO

