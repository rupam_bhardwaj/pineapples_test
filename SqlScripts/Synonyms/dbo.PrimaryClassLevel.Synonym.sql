CREATE SYNONYM [dbo].[PrimaryClassLevel] FOR [dbo].[ClassLevel]
GO
GRANT SELECT ON [dbo].[PrimaryClassLevel] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[PrimaryClassLevel] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[PrimaryClassLevel] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PrimaryClassLevel] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PrimaryClassLevel] TO [public] AS [dbo]
GO

