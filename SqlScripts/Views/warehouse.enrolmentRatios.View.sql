SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Enrolment Ratios
--
-- this view assembles data points needed for calulation of Enrolment Ratios.
-- In particular, it makes both Population and Enrolment available in one view,
-- although these are not on the same record.
-- consumers of this view will GROUP to get pop and enrol on the same output record.
-- this is an imtermediate view used in the construction of the following warehouse items:
-- [NationEdLevelAge]
--
-- =============================================
CREATE VIEW [warehouse].[enrolmentRatios]
 WITH VIEW_METADATA
 AS
 Select E.SurveyYear
 , E.Estimate
 , E.ClassLevel
 , E.Age
 , E.GenderCode
 , E.DistrictCode
 , E.AuthorityCode
 , E.SchoolTypeCode
 , E.Enrol
 , E.Rep
 , E.Trin
 , E.Trout
 , E.PSA
, null Pop
 , ELA.yearOfEd
 , ELA.edLevelCode
 , ELA.edLevel
 , ELA.EdLevelOfficialAge
 , ELA.classLevelOfficialAge
 , case when edLevelOfficialAge = '=' then Enrol end EdLevelOfficialAgeEnrol
 , case when ClassLevelOfficialAge = '=' then Enrol end ClassLevelOfficialAgeEnrol
 , case when ClassLevelOfficialAge = '=' then Rep end ClassLevelOfficialAgeRep

 from warehouse.tableEnrol E
	LEFT JOIN lkpLevels L
		ON E.ClassLevel = L.codeCode
	LEFT JOIN  dimensionEdLevelAge ELA
		ON E.SurveyYear = ELA.svyYear
		AND E.Age = ELA.Age
		AND L.lvlYear = ELA.yearOfEd

		WHERE (Enrol is not null or Rep is not null or PSA is not null)
		 AND edLevelClassification is null
UNION ALL
Select
popYear SurveyYear
, 0 Estimate
, DL.levelCode ClassLevel
, popAge Age
, genderCode
, dID DistrictCode
, null AuthorityCode
, null schoolTypeCode
, null Enrol
, null Rep
, null Trin
, null Trout
, null PSA
, pop Pop
 , ELA.yearOfEd
 , ELA.edLevelCode
 , ELA.edLevel
 , ELA.EdLevelOfficialAge
 , ELA.classLevelOfficialAge
 , null EdLevelOfficialAgeEnrol
 , null ClassLevelOfficialAgeEnrol
 , null ClassLevelOfficialAgeRep
from warehouse.measurePopG	P
	INNER JOIN dimensionEdLevelAge ELA
		ON P.popYear = ELA.svyYear
		AND P.popAge = ELA.Age
		AND ELA.ClassLevelOfficialAge = '='
		AND ELA.edLevelClassification is null
	-- note it may be possible to have more than one class level at the same year of ed
	-- we do not want to duplicate population in this circumstance,
	-- so we select only one class level at each year of education,
	-- by using ListDefaultPathLevels ie those classes that are in the default "path"
	LEFT JOIN ListDefaultPathLevels DL
		ON ELA.yearOfEd = DL.YearOfEd
GO

