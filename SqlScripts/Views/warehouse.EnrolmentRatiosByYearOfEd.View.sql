SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[EnrolmentRatiosByYearOfEd]
WITH VIEW_METADATA
AS
------------------------------------------------------------------------------------
-- warehouse.EnrolmentRatiosByYearOfEd
-- Replicates dbo.EnrolmentRatiosByYearOfEd
-- but drawing from warehouse rather than live data
--
-- this view allows enrolment ratios to be calculated for a single year of education.
-- This is in contrast to the general calculation which is based on
-- Educaiton Levels, and henace ranges of ages and years of education.
-- Here, a pupil is of official age if they are exactly the age for their current year of ed.
-- the Official age poipulation is just the population of the single year of age for that year of education.
------------------------------------------------------------------------------------
SELECT S.*
, case when isnull(popM,0) = 0 then null else convert(float, EnrolM ) / PopM end gerM
, case when isnull(popF,0) = 0 then null else convert(float, EnrolF ) / PopF end gerF
, case when isnull(pop,0) = 0 then null else convert(float, Enrol )/ Pop end ger
FROM
(
Select SurveyYear [Survey Year]
, min(case when ClassLevelOfficialAge = '=' then Age end) [Official Age]
, sum(case GenderCode when 'M' then Pop end) popM
, sum(case GenderCode when 'F' then Pop end) popF
, sum(Pop) pop

, L.lvlYear YearOfEd

, DF.level levelCode
, sum(case GenderCode when 'M' then Enrol end) enrolM
, sum(case GenderCode when 'F' then Enrol end) enrolF
, sum(Enrol) enrol
, sum(case when GenderCode = 'M' and ClassLevelOfficialAge = '=' then enrol end) nEnrolM
, sum(case when GenderCode = 'F' and ClassLevelOfficialAge = '=' then enrol end) nEnrolF
, sum(case when ClassLevelOfficialAge = '=' then enrol end) nEnrol

from warehouse.EnrolmentRatios ER
	INNER JOIN lkpLEvels L
		ON ER.ClassLevel = L.codeCode
	LEFT JOIN ListDefaultPathLevels DF
		ON l.lvlYear = DF.YearOfEd

GROUP BY
 SurveyYear
, L.lvlYear
, DF.level
) S
GO

