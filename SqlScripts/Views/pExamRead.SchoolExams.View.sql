SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pExamRead].[SchoolExams]
WITH VIEW_METADATA
AS
SELECT *
, case when isnull(Candidates,0) = 0 then null else convert(decimal(7, 2), Scores / Candidates) end AvgScore
, case when isnull(CandidatesM,0) = 0 then null else convert(decimal(7, 2), ScoresM / CandidatesM) end AvgScoreM
, case when isnull(CandidatesF,0) = 0 then null else convert(decimal(7, 2), ScoresF / CandidatesF) end AvgScoreF
FROM
(
Select DISTINCT
EXS.schNo
, schName
, X.exID
, X.exCode	ExamCode
, X.exYear	ExamYear
, sum(EXS.exsCandidates) Candidates
, sum(case when exsGender = 'M' then exsCandidates end) CandidatesM
, sum(case when exsGender = 'F' then exsCandidates end) CandidatesF
, sum(EXS.exsScore) Scores
, sum(case when exsGender = 'M' then exsScore end) ScoresM
, sum(case when exsGender = 'F' then exsScore end) ScoresF

FROM Exams X
	INNER JOIN ExamScores EXS
		ON X.exID = EXS.exID
	INNER JOIN Schools S
		ON EXS.schNo = S.schNo
GROUP BY EXS.schNo
, S.schName
, X.exID
, X.exCode
, X.exYear
) SUB
GO

