SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[SchoolSurveyQualityPupilTables]
as
SELECT SchoolSurveyQuality.*
FROM SchoolSurveyQuality
WHERE (((SchoolSurveyQuality.ssqDataItem)='Pupils'))
GO

