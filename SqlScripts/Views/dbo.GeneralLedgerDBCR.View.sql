SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[GeneralLedgerDBCR]
AS
SELECT
dbo.GeneralLedger.glCode,
Case
	When dbo.GeneralLedger.[glDBCR]='DB' Then 1
	Else -1
End AS DBCRSign

FROM dbo.GeneralLedger
GO

