SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [warehouse].[EnrolAndPop]
WITH VIEW_METADATA
AS
select *
, null Pop
from warehouse.tableEnrol

UNION ALL

Select
popYear SurveyYear
, 0 Estimate
, null ClassLevel
, popAge Age
, genderCode
, dID DistrictCode
, null AuthorityCode
, null schoolTypeCode
, null Enrol
, null Rep
, null Trin
, null Trout
, null Boarders
, null Disab
, null Dropout
, null PSA
, pop Pop
 from warehouse.measurePopG
GO

