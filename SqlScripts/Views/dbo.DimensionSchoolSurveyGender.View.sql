SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolSurveyGender]
AS
SELECT DimensionSchoolSurvey.*, g.*
FROM DimensionGender AS g, DimensionSchoolSurvey
GO

