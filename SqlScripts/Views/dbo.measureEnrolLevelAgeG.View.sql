SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Enrolments by class level and age at national aggregation
-- Normalised by gender
-- Used in the creation of data warehouse
-- =============================================
CREATE VIEW [dbo].[measureEnrolLevelAgeG]
AS
SELECT
SurveyYear
, Estimate
, ClassLevel
, Age
, G.genderCode
, case genderCode
	when 'M' then EnrolM
	when 'F' then EnrolF
 end Enrol

from measureEnrolLevelAge M
	CROSS JOIN dimensionGender G
GO

