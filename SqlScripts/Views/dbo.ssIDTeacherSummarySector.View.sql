SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2007
-- Description:	summary of teacher statistics by sector
-- =============================================
CREATE VIEW [dbo].[ssIDTeacherSummarySector]
as
SELECT
TS.ssID,
Q.secCode AS SectorCode,
Count(TS.tchsID) AS Total,

sum(case q.Qualified when 'Y' then 1 else 0 end) AS NumQualified,
sum(case when q.Qualified = 'Y' and tSex = 'M' then
	1 else 0 end) AS NumQualifiedM,
sum(case when q.Qualified = 'Y' and tSex = 'F' then
	1 else 0 end) AS NumQualifiedF,

sum(case q.Certified when 'Y' then 1 else 0 end) AS NumCertified,
sum(case when q.Certified = 'Y' and tSex = 'M' then
	1 else 0 end) AS NumCertifiedM,
sum(case when q.Certified = 'Y' and tSex = 'F' then
	1 else 0 end) AS NumCertifiedF,


sum(case q.Qualified when 'Y' then tchSalary else 0 end) AS  QualifiedSalary,
sum(case q.Certified when 'Y' then tchSalary else 0 end) AS CertifiedSalary,

Sum(case tSex when 'M' then 1 else 0 end) as MaleTeachers,
Sum(case tSex when 'F' then 1 else 0 end) as FemaleTeachers,

Sum(case tSex when 'M' then svyYEar - year(tDOB) else null end) as TotalAgeMale,
Sum(case tSex when 'F' then svyYEar - year(tDOB) else null end) as TotalAgeFemale,

sum(case when isnull(tchFullPart,'F') <> 'P' then 1 else 0 end) FullTime,
Sum(case tSex when 'M' then
			case when isnull(tchFullPart,'F') <> 'P' then 1 else 0 end
		else 0 end)  AS FullTimeM,
Sum(case tSex when 'F' then
			case when isnull(tchFullPart,'F') <> 'P' then 1 else 0 end
		else 0 end)  AS FullTimeF,

sum(case when isnull(tchFullPart,'F') <> 'P' then 1 else tchFTE end) FTE,
Sum(case tSex when 'M' then
			case when isnull(tchFullPart,'F') <> 'P' then 1 else tchFTE end
		else 0 end)  AS FTEM,
Sum(case tSex when 'F' then
			case when isnull(tchFullPart,'F') <> 'P' then 1 else tchFTE end
		else 0 end)  AS FTEF,

-- support for factoring TAM (teacher/Admin/Mixed duties) into PTR Mixed and T = 1 A = 0
sum(case when isnull(tchTAM,'T') = 'A' then 0 else 1 end) TAM,
Sum(case tSex when 'M' then
			case when isnull(tchTAM,'T') = 'A' then 0 else 1 end
		else 0 end)  AS TAMM,
Sum(case tSex when 'F' then
			case when isnull(tchTAM,'T') = 'A' then 0 else 1 end
		else 0 end)  AS TAMF,

sum(case when isnull(tchTAM,'T') = 'A' then 0 else
		case when isnull(tchFullPart,'F') <> 'P' then 1 else tchFTE end
	 end) TAMFTE,
Sum(case tSex when 'M' then
			case when isnull(tchTAM,'T') = 'A' then 0 else
				case when isnull(tchFullPart,'F') <> 'P' then 1 else tchFTE end
			end
		else 0
	end)  AS TAMFTEM,
Sum(case tSex when 'F' then
			case when isnull(tchTAM,'T') = 'A' then 0 else
				case when isnull(tchFullPart,'F') <> 'P' then 1 else tchFTE end
			end
		else 0
	end)  AS TAMFTEF,

sum(case when [Authoritygroupcode]='G' then 1 else 0 end) GovTeacher,
Sum(case tSex when 'M' then
			case when [Authoritygroupcode]='G' then 1 else 0 end
		else 0 end)  AS GovTeacherM,
Sum(case tSex when 'F' then
			case when [Authoritygroupcode]='G' then 1 else 0 end
		else 0 end)  AS GovTeacherF

FROM TeacherSurvey TS
	INNER JOIN TeacherIdentity TI
		ON TS.tID = TI.tID
	LEFT JOIN tchsIDQualifiedCertified AS q
		ON TS.tchsID = q.tchsID
	LEFT JOIN DimensionAuthority AS AUTH
		ON TS.tchSponsor = AUTH.AuthorityCode
GROUP BY TS.ssID, Q.secCode
GO

