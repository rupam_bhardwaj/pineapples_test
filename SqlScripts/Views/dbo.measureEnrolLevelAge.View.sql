SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Enrolments by class level and age at national aggregation
-- the purpose of this view is to Include estimates.
-- Used in the creation of data warehouse
-- Normalised by gender in measureEnrolLevelAgeG
-- =============================================
CREATE VIEW [dbo].[measureEnrolLevelAge]
AS
SELECT
LifeYear SurveyYear
, Estimate
, enLevel ClassLevel
, enAge Age
, sum(enM) EnrolM
, sum(enF) EnrolF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN Enrollments E
		ON B.bestssID = E.ssID
	INNER JOIN DimensionSchoolSurvey DSS
		ON DSS.[Survey ID] = B.surveyDimensionssID
GROUP BY LifeYear, Estimate, enAge, enLevel
GO

