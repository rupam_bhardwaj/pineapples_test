SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[lkpIslands]
AS
SELECT     TOP 100 PERCENT dbo.Islands.iCode, dbo.Islands.iName, dbo.Districts.dName, dbo.Islands.iGroup
FROM         dbo.Districts INNER JOIN
                      dbo.Islands ON dbo.Districts.dID = dbo.Islands.iGroup
ORDER BY dbo.Islands.iName
GO
GRANT DELETE ON [dbo].[lkpIslands] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpIslands] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpIslands] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpIslands] TO [public] AS [dbo]
GO

