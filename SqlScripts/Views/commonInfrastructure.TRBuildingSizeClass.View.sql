SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [commonInfrastructure].[TRBuildingSizeClass]
AS
Select bszCode
, isnull(CASE dbo.userLanguage()
	WHEN 0 THEN bszDescription
	WHEN 1 THEN bszDescriptionL1
	WHEN 2 THEN bszDescriptionL2

END,bszDescription) AS bszDescription
, bszMin
, bszMax
from lkpBuildingSizeClass;
GO

