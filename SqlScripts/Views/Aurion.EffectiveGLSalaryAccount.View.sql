SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Aurion].[EffectiveGLSalaryAccount]
AS
select ORG.*
	, S.schType
	, S.schAuth
	, SEC.secGLSalaries
	, TYP.stGLSalaries
	, S.schGLSalaries
	, isnull(schGLSalaries,isnull(secGLSalaries, stGLSalaries)) GLSalaryAccount
from Aurion.SectorCodeOrgUnits ORG
	INNER JOIN Schools S
	ON ORG.schNo = S.schNo
	INNER JOIN EducationSectors SEC
	ON SEC.secCode = ORG.secCode
	INNER JOIN SchoolTypes TYP
	ON TYP.stCode = S.schType
GO

