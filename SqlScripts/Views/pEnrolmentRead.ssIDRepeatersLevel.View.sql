SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [pEnrolmentRead].[ssIDRepeatersLevel]
as
/*

	Created: 11 10 2007

	Repeaters grouped by class level; ie ignores disaggregation by age
	Does not use estimates

*/


SELECT vtblRepeaters.ssID, vtblRepeaters.ptLevel, Sum(vtblRepeaters.ptM) AS RepM, Sum(vtblRepeaters.ptF) AS RepF
FROM vtblRepeaters
GROUP BY vtblRepeaters.ssID, vtblRepeaters.ptLevel
GO

