SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental list of enrolment at lowest aggregation level - school, year classLevel age gender
-- the purpose of this view is to Include estimates.
-- Used in the creation of data warehouse
-- Related higher aggregates are:
--   -- measureEnrolLevelAge (national totals)
--   -- measureEnrolSchool ( school totals)
-- =============================================
CREATE VIEW [dbo].[measureEnrol]
AS
SELECT schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, enLevel ClassLevel
, enAge Age
, enM EnrolM
, enF EnrolF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN Enrollments E
		ON B.bestssID = E.ssID
GO

