SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblLibraryResources]
AS
SELECT
Resources.resID,
Resources.ssID,
Resources.resName,
Resources.resSplit,
Resources.resAvail,
Resources.resNumber,
Resources.resQty,
Resources.resCondition
FROM dbo.Resources
WHERE (((Resources.resName)='Library Resources'))
GO

