SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRRoleGrades]
AS
SELECT     rgCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN rgDescription
	WHEN 1 THEN rgDescriptionL1
	WHEN 2 THEN rgDescriptionL2

END,rgDescription) AS rgDescription
,roleCode, rgSalaryLevelMin, rgSalaryLevelMax
, rgSalaryPointMedian
, rgSort

FROM         dbo.RoleGrades
GO
GRANT SELECT ON [dbo].[TRRoleGrades] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TRRoleGrades] TO [public] AS [dbo]
GO

