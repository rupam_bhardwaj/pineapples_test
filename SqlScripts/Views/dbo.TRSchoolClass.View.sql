SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSchoolClass]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codedescription
	WHEN 1 THEN codedescriptionL1
	WHEN 2 THEN codedescriptionL2

END,codedescription) AS codedescription

FROM         dbo.lkpSchoolClass
GO
GRANT SELECT ON [dbo].[TRSchoolClass] TO [public] AS [dbo]
GO

