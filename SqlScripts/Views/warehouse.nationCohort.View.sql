SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Nation Cohort
--
-- A wrapper around warehouse.cohort table to aggregate to national level.
-- Cohort views contain on the fields needed to calculate flow indicators using the
-- reconstructed cohort method on a single row. Specifcially, they contain This Year and Next Data data on the record.
-- This makes it much easier to calculate the required ratios across 2 years' data.
-- Related warehouse objects:
-- warehouse.cohort : school level cohort data (table). All other cohort objects are derived from this.
-- warehouse.schoolFlow: flow indicators calculated at school level
-- warehouse.districtCohort : district level cohort data
-- warehouse.districtFlow : flow indicator calculated at district level.
-- warehouse.nationCohort : nation level cohort data
-- warehouse.nationFlow : flow indicator calculated at nation level.
-- xxCohort version do not calculate the actual indicators - this is so you can do this calculation in
-- a cube (pivot table, tableau) where the ratio is based on an aggregate of rows
-- based on the cube configuration.
-- xxFlow versions do provide these ratios at the Row level. Use these for flat reporting (Jasper etc)
-- warehouse.cohort is built by warehouse.buildCohort as part of warehouse.buildwarehouse.
-- =============================================
CREATE VIEW [warehouse].[nationCohort]
WITH VIEW_METADATA
AS
Select SurveyYear
, YearOfEd
, GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.Cohort
GROUP BY SurveyYear
, YearOfEd
, GenderCode
UNION ALL
Select SurveyYear
, YearOfEd
, null GenderCode
, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(RepNY) RepNY
, sum(TroutNY) TroutNY
, sum(EnrolNYNextLevel) EnrolNYNextLevel
, sum(RepNYNextLevel) RepNYNextLevel
, sum(TrinNYNextLevel) TrinNYNextLevel
From warehouse.Cohort
GROUP BY SurveyYear
, YearOfEd
GO

