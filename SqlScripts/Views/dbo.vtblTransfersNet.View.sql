SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTransfersNet]
AS
SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptLevel,
PupilTables.ptRow AS OriginCode,
Case
	When [ptCode]='TROUT' Then -1 * [ptM]
	Else 1 * [ptM]
End AS trM,
Case
When [ptCode]='TROUT' Then -1 * [ptF]
	Else 1 * [ptF]
End AS trF
FROM dbo.PupilTables
WHERE (((PupilTables.ptCode)='TRIN'
Or (PupilTables.ptCode)='TROUT'))
GO

