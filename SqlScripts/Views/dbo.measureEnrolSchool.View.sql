SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental list of enrolment totals by gender by school by year
-- Includes estimates
-- Used in the creation of data warehouse
-- =============================================
CREATE VIEW [dbo].[measureEnrolSchool]
AS
SELECT schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, sum(enM) EnrolM
, sum(enF) EnrolF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN Enrollments E
		ON B.SurveyDimensionssID = E.ssID
GROUP BY SchNo, LifeYear, Estimate, Offset, SurveyDimensionssID
GO

