SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qryTeacherSurvey]
AS
SELECT     dbo.TeacherSurvey.tchsID, dbo.TeacherSurvey.ssID, dbo.TeacherSurvey.tchSort, dbo.TeacherSurvey.tchEmplNo, dbo.TeacherSurvey.tchRegister,
                      dbo.TeacherSurvey.tchProvident, dbo.TeacherSurvey.tchUnion, dbo.TeacherSurvey.tchFirstName, dbo.TeacherSurvey.tchFamilyName,
                      dbo.TeacherSurvey.tchSalaryScale, dbo.TeacherSurvey.tchSalary, dbo.TeacherSurvey.tchDOB, dbo.TeacherSurvey.tchGender, dbo.TeacherSurvey.tchCitizenship,
                      dbo.TeacherSurvey.tchIsland, dbo.TeacherSurvey.tchDistrict, dbo.TeacherSurvey.tchSponsor, dbo.TeacherSurvey.tchFTE, dbo.TeacherSurvey.tchFullPart,
                      dbo.TeacherSurvey.tchYears, dbo.TeacherSurvey.tchYearsSchool, dbo.TeacherSurvey.tchTrained, dbo.TeacherSurvey.tchEdQual, dbo.TeacherSurvey.tchQual,
                      dbo.TeacherSurvey.tchSubjectTrained, dbo.TeacherSurvey.tchCivilStatus, dbo.TeacherSurvey.tchNumDep, dbo.TeacherSurvey.tchHouse,
                      dbo.TeacherSurvey.tchSubjectMajor, dbo.TeacherSurvey.tchSubjectMinor, dbo.TeacherSurvey.tchSubjectMinor2, dbo.TeacherSurvey.tchClass,
                      dbo.TeacherSurvey.tchClassMax, dbo.TeacherSurvey.tchJoint, dbo.TeacherSurvey.tchComposite, dbo.TeacherSurvey.tchStatus, dbo.TeacherSurvey.tchRole,
                      dbo.TeacherSurvey.tchTAM, dbo.TeacherSurvey.tID, dbo.TeacherSurvey.tchECE, dbo.TeacherSurvey.tchInserviceYear, dbo.TeacherSurvey.tchInservice,
                      dbo.TeacherSurvey.tchSector, dbo.TeacherSurvey.tchYearStarted, dbo.TeacherSurvey.tchSpouseFirstName, dbo.TeacherSurvey.tchSpouseFamilyName,
                      dbo.TeacherSurvey.tchSpouseOccupation, dbo.TeacherSurvey.tchSpouseEmployer, dbo.TeacherSurvey.tchSpousetID, dbo.TeacherSurvey.tchLangMajor,
                      dbo.TeacherSurvey.tchLangMinor, dbo.SchoolSurvey.svyYear, dbo.Schools.schNo, dbo.Schools.schName, dbo.TeacherIdentity.tRegister,
                      dbo.TeacherIdentity.tPayroll, dbo.TeacherIdentity.tDOB, dbo.TeacherIdentity.tSex, dbo.TeacherIdentity.tSurname, dbo.TeacherIdentity.tGiven,
                      dbo.TeacherIdentity.tSrcID, dbo.TeacherIdentity.tSrc, dbo.TeacherIdentity.tComment
FROM         dbo.Schools INNER JOIN
                      dbo.TeacherIdentity INNER JOIN
                      dbo.SchoolSurvey INNER JOIN
                      dbo.TeacherSurvey ON dbo.SchoolSurvey.ssID = dbo.TeacherSurvey.ssID ON dbo.TeacherIdentity.tID = dbo.TeacherSurvey.tID ON
                      dbo.Schools.schNo = dbo.SchoolSurvey.schNo
GO

