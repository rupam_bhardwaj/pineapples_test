SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[lookupentryBuildingTypes]
AS
SELECT bdlgCode AS codeCode
, bdlgDescription AS codeDescription
, bdlgDescriptionL1 AS codeDescriptionL1
, bdlgDescriptionL2 AS codeDescriptionL2
FROM lkpBuildingTypes
GO
GRANT DELETE ON [dbo].[lookupentryBuildingTypes] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lookupentryBuildingTypes] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lookupentryBuildingTypes] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lookupentryBuildingTypes] TO [public] AS [dbo]
GO

