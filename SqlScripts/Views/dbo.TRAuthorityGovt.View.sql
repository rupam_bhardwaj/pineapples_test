SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRAuthorityGovt]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END,codeDescription) AS codeDescription

FROM         dbo.lkpAuthorityGovt
GO
GRANT SELECT ON [dbo].[TRAuthorityGovt] TO [public] AS [dbo]
GO

