SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paElements]
WITH VIEW_METADATA
AS
Select E.*
, C.pacomCode + '.' + E.paelmCode  elmID
, C.pafrmCode + '.' + C.pacomCode + '.' + E.paelmCode  elmFullID
, C.pafrmCode
, F.pafrmDescription
from paElements_ E
INNER JOIN paCompetencies_ C
	ON E.pacomID = C.pacomID
INNER JOIN paFrameworks_ F
	ON C.pafrmCode = F.pafrmCode
GO

