SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSubject]
as

Select subjCode as [Subject Code],
subjName as [Subject Name]
from TRSubjects
GO

