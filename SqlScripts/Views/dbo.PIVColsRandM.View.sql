SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsRandM]
AS
---------------------------------------
-- repairs and maintenance decoded
---------------------------------------
Select ssID
, case ssRandM

	when -1 then 1
	else ssRandM
end HasProgram
, case ssRandM
	when 1 then 'Y'
	when -1 then 'Y'
	when 0 then 'N'
end HasProgramYN
, ssRandMResp ResponsibilityCode
, RM.randmDescription Responsibilty
FROM SchoolSurvey
	LEFT JOIN TRRandM RM
		ON ssRandMResp = RM.randmCode
--WHERE ssRandM is not null or ssRandMResp is not null
GO

