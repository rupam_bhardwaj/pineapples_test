SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolYearHasDataChildProtection]
AS
SELECT     dbo.SchoolSurvey.svyYear,
		   dbo.SchoolSurvey.schNo,
		   dbo.SchoolSurvey.ssID,
		   ssqLevel

FROM     dbo.SchoolSurvey INNER JOIN dbo.vtblChildProtection
		 ON dbo.SchoolSurvey.ssID = dbo.vtblChildProtection.ssID
			LEFT OUTER JOIN
                      dbo.tfnQualityIssues('Pupils', 'CP') Q
					on schoolsurvey.ssID = q.ssID
GO

