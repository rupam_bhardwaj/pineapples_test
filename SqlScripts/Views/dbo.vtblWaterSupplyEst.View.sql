SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblWaterSupplyEst]
AS
SELECT
ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.schNo,
ESTIMATE.bestYear AS [Year OF Data],
ESTIMATE.Estimate,
ESTIMATE.Offset AS [Age Of Data],
ESTIMATE.ActualssID,
ESTIMATE.SurveyDimensionssID,
ESTIMATE.bestssID,
ESTIMATE.bestssqLevel AS [Data Quality Level],
ESTIMATE.ActualssqLevel AS [Survey Year Quality Level],
vtblWaterSupply.*
FROM
ESTIMATE_BestSurveyResourceCategory AS ESTIMATE
INNER JOIN vtblWaterSupply
ON ESTIMATE.bestssID = vtblWaterSupply.ssID
WHERE (((ESTIMATE.resName)='Water Supply'))
GO
GRANT SELECT ON [dbo].[vtblWaterSupplyEst] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[vtblWaterSupplyEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[vtblWaterSupplyEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[vtblWaterSupplyEst] TO [pSchoolWrite] AS [dbo]
GO

