SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paElementAvg]
WITH VIEW_METADATA
AS
Select paID
		, comFullID
		, elmFullID
		, comID
		, elmID
		, avg(PL) RawElmAvg
		, convert(decimal(4,1), avg(PL)) ElmAvg
		FROM
		(
		select
		A.paID
		, I.elmFullID
		, I.comFullID
		, I.indFullID
		, I.comID
		, I.elmID
		, I.paindID
		, L.paplCode
		, L.palComment
		, convert(decimal(4,1),IL.paplValue) PL

		from paAssessment_ A

		INNER JOIN paAssessmentLine_ L

			ON L.paID =A.paID
		INNER JOIN paIndicators I
			ON I.paindID = L.paindID
		LEFT JOIN paIndicatorLevels_ IL
			ON IL.paindID = I.paindID
			AND IL.paplCode = L.paplCode
		) I

		GROUP BY paID
		, comFullID
		, elmFullID
		, comID
		, elmID
GO

