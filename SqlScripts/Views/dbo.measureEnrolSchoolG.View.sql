SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--      Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental list of enrolment totals school by year. This is normalised by Gender
-- ie in contrast to measureSchoolEnrol, M and F totals are separate records
-- Includes estimates
-- Used in the creation of data warehouse
-- =============================================
CREATE VIEW [dbo].[measureEnrolSchoolG]
AS
SELECT schNo
, SurveyYear
, Estimate

, SurveyDimensionID
, G.genderCode
, case G.genderCode
	when 'M' then EnrolM
	when 'F' then EnrolF
end Enrol
from measureEnrolSchool E
	CROSS JOIN DimensionGender G
GO

