SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRISTCourse] AS
SELECT istcCode,
isnull(CASE dbo.userLanguage()
    WHEN 0 THEN istcDescription
    WHEN 1 THEN istcDescriptionL1
    WHEN 2 THEN istcDescriptionL2
 END,istcDescription) AS istcDescription
 FROM dbo.ISTCourse
GO
GRANT SELECT ON [dbo].[TRISTCourse] TO [public] AS [dbo]
GO

