SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSecondaryTransitions]
AS
/* TRSecondaryTransitions
-- translate the code description according to user language
-- Created: Brian Lewis 21 8 2007
-- Last Modified:
*/
SELECT     codeCode, codeSeq, CASE dbo.userLanguage() WHEN 0 THEN codes.codeDescription WHEN 1 THEN codes.codeDescriptionL1 END AS codeDescription
FROM         dbo.lkpSecondaryTransitions AS codes
GO
GRANT SELECT ON [dbo].[TRSecondaryTransitions] TO [public] AS [dbo]
GO

