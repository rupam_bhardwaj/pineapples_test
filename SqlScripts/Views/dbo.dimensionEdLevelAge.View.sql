SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Dimension EdLevel Age
--
-- Perform as the basic for any pupil age and EdLevel combination
-- is that age IN, OVER, or UNDER the offical age range for the ed level?
-- Note that this depends on survey year, since the offical age range is determined by the
-- offical start age, which is allowed to change from year to year
--
-- this view is fundamental to OVER , UNDER analysis in the warehouse.
-- It is used by warehouse.enrolmentRatios.
-- Generally to use this domension, you'll need to join it to some data source on
-- on survey year, age, and year of ed

-- Note that this view combines all the Education Levels - including Alt and Alt2
-- =============================================
CREATE VIEW [dbo].[dimensionEdLevelAge]
AS
Select U.*
FROM
(
SELECT D.svyYear
, Age
, yearOfEd
, AtEdLevelAge
, case
	when Age < edlMinYear + S.svyPSAge - 1 then 1
	else 0
	end UnderEdLevelAge
, case when Age > edlMaxYear + SvyPSAge - 1 then 1
	else 0
	end OverEdLevelAge
, case when Age < edlMinYear + S.svyPSAge - 1 then 'UNDER'
	   when Age > edlMaxYear + SvyPSAge - 1 then 'OVER'
	   else '='
	end EdLevelOfficialAge
, case when Age < YearOfEd + S.svyPSAge - 1 then 'UNDER'
	   when Age >  YearOfEd + SvyPSAge - 1 then 'OVER'
	   else '='
	end ClassLevelOfficialAge

, edLevelCode
, codeDescription edLevel
, null edLevelClassification
FROM DimensionAge D
	INNER JOIN lkpEducationLevels EL
		ON D.edLevelCode = EL.codeCode
	INNER JOIN Survey S
		ON S.svyYear = D.svyYear
	WHERE yearOfEd between EL.edlMinYear and EL.edlMaxYear

UNION
SELECT D.svyYear
, Age
, yearOfEd
, AtEdLevelAltAge
, case
	when Age < edlMinYear + S.svyPSAge - 1 then 1
	else 0
	end UnderEdLevelAge
, case when Age > edlMaxYear + SvyPSAge - 1 then 1
	else 0
	end OverEdLevelAge
, case when Age < edlMinYear + S.svyPSAge - 1 then 'UNDER'
	   when Age > edlMaxYear + SvyPSAge - 1 then 'OVER'
	   else '='
	end EdLevelOfficalAge
, case when Age < YearOfEd + S.svyPSAge - 1 then 'UNDER'
	   when Age >  YearOfEd + SvyPSAge - 1 then 'OVER'
	   else '='
	end ClassLevelOfficalAge
, edLevelAltCode
, codeDescription
, 'Alt' edLevelClassification
FROM DimensionAge D
	INNER JOIN lkpEducationLevelsAlt EL
		ON D.edLevelAltCode = EL.codeCode
	INNER JOIN Survey S
		ON S.svyYear = D.svyYear
	WHERE yearOfEd between EL.edlMinYear and EL.edlMaxYear
UNION
SELECT D.svyYear
, Age
, yearOfEd
, AtEdLevelAlt2Age
, case
	when Age < edlMinYear + S.svyPSAge - 1 then 1
	else 0
	end UnderEdLevelAge
, case when Age > edlMaxYear + SvyPSAge - 1 then 1
	else 0
	end OverEdLevelAge
, case when Age < edlMinYear + S.svyPSAge - 1 then 'UNDER'
	   when Age > edlMaxYear + SvyPSAge - 1 then 'OVER'
	   else '='
	end EdLevelOfficalAge
, case when Age < YearOfEd + S.svyPSAge - 1 then 'UNDER'
	   when Age >  YearOfEd + SvyPSAge - 1 then 'OVER'
	   else '='
	end ClassLevelOfficalAge
, edLevelAlt2Code
, codeDescription
, 'Alt2' edLevelClassification

FROM DimensionAge D
	INNER JOIN lkpEducationLevelsAlt2 EL
		ON D.edLevelAlt2Code = EL.codeCode
	INNER JOIN Survey S
		ON S.svyYear = D.svyYear
	WHERE yearOfEd between EL.edlMinYear and EL.edlMaxYear
) U
CROSS JOIN
(
Select max(enAge) MaxAge
From Enrollments
) X
WHERE U.Age <= X.maxAge
GO

