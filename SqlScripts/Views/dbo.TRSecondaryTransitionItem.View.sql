SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSecondaryTransitionItem]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codedescription
	WHEN 1 THEN codedescriptionL1
	WHEN 2 THEN codedescriptionL2

END,codedescription) AS codedescription,codeSeq

FROM         dbo.lkpSecondaryTransitionItem
GO
GRANT SELECT ON [dbo].[TRSecondaryTransitionItem] TO [public] AS [dbo]
GO

