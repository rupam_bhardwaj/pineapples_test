SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2007
-- Description:	Dimension Level
--
-- Dimension based on class level, allows aggregations to use
-- any characteristics of the class level, including ISCED classifications hierarchy
-- and any EducationLevel hierarchy.
-- =============================================
CREATE VIEW [dbo].[DimensionLevel]
AS
SELECT
			dbo.TRLevels.codeCode AS LevelCode, dbo.TRLevels.codeDescription AS [Level],
						dbo.TRLevels.lvlYear as [Year of Education],
						dbo.TRLevels.lvlGV AS LevelGV,
                      EducationLevels.codeCode AS edLevelCode, EducationLevels.codeDescription AS [Education Level],
					  EducationLevelsAlt.codeCode AS edLevelAltCode,	-- name changed to match jet
                      EducationLevelsAlt.codeDescription AS [Education Level Alt],
					  EducationLevelsAlt2.codeCode AS edLevelAlt2Code,  -- name changed to match jet
                      EducationLevelsAlt2.codeDescription AS [Education Level Alt2], dbo.TREducationSectors.secCode AS SectorCode, dbo.TREducationSectors.secDesc AS Sector,
                      EducationLevels.edlMinYear AS edLevelMin, EducationLevels.edlMaxYear AS edLevelMax, EducationLevelsAlt.edlMinYear AS edLevelAltMin,
                      EducationLevelsAlt.edlMaxYear AS edLevelAltMax, EducationLevelsAlt2.edlMinYear AS edLevelAlt2Min, EducationLevelsAlt2.edlMaxYear AS edLevelAlt2Max,
                      dbo.ISCEDLevelSub.ilsCode AS [ISCED SubClass]
			, dbo.ISCEDLevel.ilCode AS [ISCED Level], dbo.ISCEDLevel.ilName AS [ISCED Level Name]
-- put these twice in case there is any issue with changing the name?
-- removed after checking vU Stats digest workbooks - no risk in removing these
--					  , EducationLevelsAlt.codeCode AS edLevelAlt,
--					  EducationLevelsAlt2.codeCode AS edLevelAlt2

FROM         dbo.TREducationLevels AS EducationLevels CROSS JOIN
                      dbo.TREducationLevelsAlt AS EducationLevelsAlt CROSS JOIN
                      dbo.TREducationLevelsAlt2 AS EducationLevelsAlt2 CROSS JOIN
                      dbo.ISCEDLevelSub INNER JOIN
                      dbo.ISCEDLevel ON dbo.ISCEDLevelSub.ilCode = dbo.ISCEDLevel.ilCode INNER JOIN
                      dbo.TRLevels ON dbo.ISCEDLevelSub.ilsCode = dbo.TRLevels.ilsCode INNER JOIN
                      dbo.TREducationSectors ON dbo.TRLevels.secCode = dbo.TREducationSectors.secCode
WHERE     (dbo.TRLevels.lvlYear BETWEEN EducationLevels.edlMinYear AND EducationLevels.edlMaxYear) AND (dbo.TRLevels.lvlYear BETWEEN
                      EducationLevelsAlt.edlMinYear AND EducationLevelsAlt.edlMaxYear) AND (dbo.TRLevels.lvlYear BETWEEN EducationLevelsAlt2.edlMinYear AND
                      EducationLevelsAlt2.edlMaxYear)
GO
GRANT SELECT ON [dbo].[DimensionLevel] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[DimensionLevel] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[DimensionLevel] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[DimensionLevel] TO [pSchoolWrite] AS [dbo]
GO

