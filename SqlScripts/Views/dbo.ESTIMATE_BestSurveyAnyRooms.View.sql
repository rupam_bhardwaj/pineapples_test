SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ESTIMATE_BestSurveyAnyRooms]
AS
SELECT     schNo, LifeYear, subQ2.rmType, ActualssID,
			bestssID,
			bestYear, LifeYear - bestYear AS Offset,
			bestssqLevel,
			QI.ssqLevel ActualssqLevel,
			isnull(bestssID, ActualssID) SurveydimensionssID,
		CASE
			WHEN (BestYear IS NULL) THEN NULL
            WHEN LifeYear = BestYear THEN 0
			WHEN QI.ssqLevel = 2 then 2
			ELSE 1
		END AS Estimate
FROM
	(SELECT     schNo, LifeYear, subQ.rmType, subQ.rmQualityCode, ActualssID,
		MIN(xBestData) AS BestData,
		dbo.fnextractBestssID(MIN(xBestData)) AS bestssID,
		dbo.fnextractBestYear(MIN(xBestData)) AS bestYear,
		dbo.fnExtractBestSSQLevel(MIN(xBestData)) AS bestSSQLevel

     FROM  (SELECT     L.schNo, L.svyYear AS LifeYear, L.ActualssID,
			R.codecode as rmType, D.rmQualityCode, D.svyYear AS DataYear,
			dbo.fnMakeBestDataStr(L.svyYear, D.svyYear, D.ssID, D.ssqLevel) AS xBestData
			FROM dbo.SchoolLifeYears  AS L
				CROSS JOIN dbo.lkpRoomTypes as R
				LEFT OUTER JOIN
				dbo.schoolYearHasDataRooms AS D
    			ON L.schNo = D.schNo
				AND r.codeCode = d.rmType
				AND D.svyYear BETWEEN
                L.svyYear - dbo.sysParamInt(N'EST_ROOM_FILL_FORWARD') AND L.svyYear + dbo.sysParamInt(N'EST_ROOM_FILL_BACKWARD')
			) AS subQ
	 GROUP BY schNo, LifeYear, ActualssID, subQ.rmType, subQ.rmQualityCode
	) AS subQ2
	LEFT OUTER JOIN dbo.tfnQualityIssues('ROOMS',null) QI
	ON subQ2.ActualssID = QI.ssID and subq2.rmQualityCode = QI.ssqSubItem
GO

