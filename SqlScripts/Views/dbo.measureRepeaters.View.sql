SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental view of repeaters by school, year, age and class level
-- this view includes repeaters, and "virtualises" the raw repeaters data held in table PupilTables
-- Used in the creation of data warehouse
-- Normalised by gender in measureRepeatersG
-- =============================================
CREATE VIEW [dbo].[measureRepeaters]
AS
SELECT schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, ptLevel ClassLevel
, ptAge Age
, ptM RepM
, ptF RepF
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN vtblRepeaters R
		ON B.SurveyDimensionssID = R.ssID
GO

