SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ddSchoolType]
AS
SELECT     TOP 100 PERCENT SchoolType, SortOrder
FROM         (Select '(all)' as SchoolType, 0 as SortOrder From metaNumbers WHERE num = 0
			UNION Select stDescription, stAgeMin from TRSchoolTypes as SchoolTypes
) AS L
ORDER BY SortOrder
GO
GRANT DELETE ON [dbo].[ddSchoolType] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ddSchoolType] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ddSchoolType] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ddSchoolType] TO [public] AS [dbo]
GO

