SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 2 2010
-- Description:	the workorders where there is a duplication of school and item
-- =============================================
CREATE VIEW [pInfrastructureRead].[workorderAuditByType]
AS
SELECT WorkItems.[witmID]
      ,WorkItems.[schNo]
      ,Schools.[SchName]
      ,TotCost
      ,NotContingency
      ,Contingency
      ,WorkItems.[bldID]
      , Buildings.bldgTitle
      ,WorkItems.[witmType]
      , WT.codeDescription [WorkItem Type]
      ,[woRef] [WO Ref]
      ,[woStatus] [W/O Status]
      ,[witmProgress] Progress
      ,[witmQty]
      ,[witmDesc]
      ,[witmEstCost]
      ,[witmSourceOfFunds]
      ,[witmCostCentre]
      ,[witmContractValue]
      ,[witmProgressDate]
      ,[witmActualCost]
      ,[witmInspectedBy]
      ,[witmEstBaseCost]
      ,[witmEstBaseUnit]
      ,[witmBldCreateType]
      ,[witmRoomsClass]
      ,[witmRoomsOHT]
      ,[witmRoomsStaff]
      ,[witmRoomsAdmin]
      ,[witmRoomsStorage]
      ,[witmRoomsDorm]
      ,[witmRoomsKitchen]
      ,[witmRoomsDining]
      ,[witmRoomsLibrary]
      ,[witmRoomsSpecialTuition]
      ,[witmRoomsHall]
      ,[witmRoomsOther]
      , WorkOrders.[woID]
      ,[woDesc]	  [W/O Desc]
      ,[woPlanned]
      ,[woBudget]
      ,[woWorkApproved]
      ,[woFinanceApproved]
      ,[woSourceOfFunds]
      ,[woDonorManaged]
      ,[woCostCentre]
      ,[woTenderClose]
      ,[woContracted]
      ,[supCode]
      ,[woContractValue]
      ,[woCommenced]
      ,[woPlannedCompletion]
      ,[woCompleted]
      ,[woInvoiceValue]
      ,[woSignoff]
      ,[woSignOffUser]


  FROM WorkItems
	INNER JOIN Schools
		ON WorkItems.schNo = Schools.schNo
  INNER JOIN
		(
		Select schNo
			, sum(witmEstCost) TotCost
			, sum(case witmType when 'Contingency' then null else witmEstCost end) NotContingency

			, sum(case witmType when 'Contingency' then witmEstCost else null end) Contingency
		from WorkItems
		GROUP BY schNo
		) U
	ON WorkITems.schNo = U.schNo
  LEFT JOIN WorkOrders
	ON WorkOrders.woID = WorkITems.woID
  LEFT JOIN Buildings
	ON Buildings.bldID = WorkItems.bldID
  LEFT JOIN TRWorkItemType WT
	on WT.codeCode = WorkItems.witmType


WHERE NotContingency / Contingency > 20
OR (Contingency is null and NotContingency is not null)
GO

