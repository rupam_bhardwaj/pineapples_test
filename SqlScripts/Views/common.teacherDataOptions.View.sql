SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [common].[teacherDataOptions]
AS
SELECT        c, N, seq
FROM            (SELECT        'total' AS c, 'Total' AS N, 0 AS seq) AS S
GO

