SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblLeaversEST]
AS
SELECT
ESTIMATE_PupilTables.[Survey Year],
ESTIMATE_PupilTables.schNo,
ESTIMATE_PupilTables.[Year of Data],
ESTIMATE_PupilTables.Estimate,
ESTIMATE_PupilTables.[Age of Data],
ESTIMATE_PupilTables.bestssID,
ESTIMATE_PupilTables.ptID,
ESTIMATE_PupilTables.ssID,
ESTIMATE_PupilTables.ptCode,
ESTIMATE_PupilTables.ptLevel,
ESTIMATE_PupilTables.ptM,
ESTIMATE_PupilTables.ptF
FROM ESTIMATE_PupilTables
WHERE (((ESTIMATE_PupilTables.ptCode)='PNA'))
GO

