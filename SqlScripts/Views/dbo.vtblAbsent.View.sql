SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblAbsent]
AS

SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptLevel,
PupilTables.ptM,
PupilTables.ptF
FROM dbo.PupilTables
WHERE (((PupilTables.ptCode)='ABSENT'));
GO

