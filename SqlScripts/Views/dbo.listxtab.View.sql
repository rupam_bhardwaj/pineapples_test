SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[listxtab]

			AS
			SELECT schNo, [Boarding],[Effective CHS],[ESCC Monitoring],[Infrastructure I],[Infrastructure II],[LEAP Schools],[Renbel upgrade],[Temotu M&E],[XC],[Zones] FROM listschools PIVOT (MAX(lstValue) FOR lstName IN ([Boarding],[Effective CHS],[ESCC Monitoring],[Infrastructure I],[Infrastructure II],[LEAP Schools],[Renbel upgrade],[Temotu M&E],[XC],[Zones])) p
GO
GRANT SELECT ON [dbo].[listxtab] TO [pSchoolRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[listxtab] TO [public] AS [dbo]
GO

