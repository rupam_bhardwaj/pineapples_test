SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsPupilTablesLevelGender]
AS
Select
ssID,
ptCode Category,
genderCode,
Gender,
ptLevel LevelCode,
Level,
[Year Of Education],
LEvelGV GeneralVocational,
edLevelCode,
[Education Level],
SectorCode,
Sector,
[ISCED Level],
case when genderCode = 'M' then ptM else ptF end NumChildren
FROM PupilTables PT
	LEFT JOIN DimensionLevel L
		ON pt.ptLevel = L.levelCode
	CROSS JOIN DimensionGender G
GO

