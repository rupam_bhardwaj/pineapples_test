SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolLifeYears]
AS
	Select
		Schools.schNo
		, SchEst
		, syYear svyYear
		, schClosed
		, ssID ActualssID
		FROM Schools
		INNER JOIN SchoolYearHistory SYH
		ON Schools.SchNo = SYH.schNo
		LEFT JOIN SchoolSurvey SS
			ON SYH.schNo = SS.schNo
			AND SYH.syYear = SS.svyYear
		WHERE syDormant = 0
	UNION
		Select Schools.schNo
		, SchEst
		, svyYear
		, schClosed
		, ssID
		FROM Schools
		INNER JOIN SchoolSurvey SS
			ON Schools.schNo = SS.schNo
		LEFT JOIN SchoolYearHistory SYH
			ON SYH.schNo = SS.schNo
			AND SYH.syYear = SS.svyYear
		WHERE SYH.syID is null
GO
GRANT SELECT ON [dbo].[SchoolLifeYears] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolLifeYears] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolLifeYears] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolLifeYears] TO [pSchoolWrite] AS [dbo]
GO

