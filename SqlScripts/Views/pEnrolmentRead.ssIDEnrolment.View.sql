SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pEnrolmentRead].[ssIDEnrolment]
AS
SELECT     ssID, SUM(enM) AS EnrolM, SUM(enF) AS EnrolF, SUM(ISNULL(enM, 0) + ISNULL(enF, 0)) AS Enrol
FROM         dbo.Enrollments
GROUP BY ssID
GO

