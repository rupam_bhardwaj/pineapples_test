SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRConditionCodes]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END,codeDescription) AS codeDescription,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeInitial
	WHEN 1 THEN codeInitialL1
	WHEN 2 THEN codeInitialL2

END,codeInitial) AS codeInitial,
codeCodeN

FROM         dbo.lkpConditionCodes
GO
GRANT SELECT ON [dbo].[TRConditionCodes] TO [public] AS [dbo]
GO

