SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 11 2007
-- Description:	summary of Readers by level, by survey
-- =============================================
CREATE VIEW [dbo].[ssIDReadersLevelSubject]
AS
SELECT vtblReaders.ssID,
vtblReaders.resLevel,
resSplit [Subject],
Sum(vtblReaders.resNumber) AS NumReaders,
Sum(case when [rescondition]=1 then [resNumber] end) AS NumReadersGood,
Sum(case when [rescondition]=2 then [resNumber] end) AS NumReadersFair,
Sum(case when [rescondition]=3 then [resNumber] end) AS NumReadersPoor
FROM vtblReaders
GROUP BY vtblReaders.ssID, vtblReaders.resLevel, resSplit
GO

