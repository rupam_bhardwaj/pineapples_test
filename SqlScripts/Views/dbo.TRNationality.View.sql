SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRNationality]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END,codeDescription) AS codeDescription,codeSeq

FROM         dbo.lkpNationality
GO
GRANT SELECT ON [dbo].[TRNationality] TO [public] AS [dbo]
GO

