SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSiteHost]
AS
SELECT     hostCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN hostDescription
	WHEN 1 THEN hostDescriptionL1
	WHEN 2 THEN hostDescriptionL2

END,hostDescription) AS hostDescription
, hostSeq
FROM         dbo.lkpSiteHost
GO

