SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpWorkItemProgress](
	[codeCode] [nvarchar](15) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [lkpWorkItemProgress_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpWorkItemProgress] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpWorkItemProgress] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpWorkItemProgress] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpWorkItemProgress] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpWorkItemProgress] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [codeDescription] ON [dbo].[lkpWorkItemProgress]
(
	[codeDescription] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [codeDEscriptionL1] ON [dbo].[lkpWorkItemProgress]
(
	[codeDescriptionL1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [codeDescriptionL2] ON [dbo].[lkpWorkItemProgress]
(
	[codeDescriptionL2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System defined codes for work order progress. Foreign key  ( not mandatory) on Work Items.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWorkItemProgress'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Work Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWorkItemProgress'
GO

