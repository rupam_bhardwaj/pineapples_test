SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[ExamStateResults](
	[examID] [int] NOT NULL,
	[examCode] [nvarchar](10) NULL,
	[examYear] [int] NULL,
	[examName] [nvarchar](50) NULL,
	[StateID] [nvarchar](5) NULL,
	[State] [nvarchar](50) NULL,
	[Gender] [nvarchar](1) NULL,
	[standardID] [int] NULL,
	[standardCode] [nvarchar](20) NULL,
	[standardDesc] [nvarchar](100) NULL,
	[benchmarkID] [int] NULL,
	[benchmarkCode] [nvarchar](20) NULL,
	[achievementLevel] [int] NOT NULL,
	[achievementDesc] [nvarchar](200) NULL,
	[Candidates] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Exam Results
-- 
-- Exam results are based on the "standard-benchmark-achievement level" hierarchy.
-- Variants of presentation are
-- consolidation: national district (state) or school levelreporting by benchmark or consoilidated to standard
-- normalised or "crosstabbed" 
-- In the cross tabbed versions up to 10 achievement levels are presented on a single record.
-- This is more amenable to some chart presentations.
-- 
-- The datasets are:
-- ExamNationResults - national consolidation 
-- ExamNationResultsX - national consolidation crosstabbed
-- ExamNationStandards - national consolidation by standard
-- ExamNationStandardsX - national consolidation by standard - crosstabbed
-- ExamStateResults - district consolidation (table)
-- ExamStateResultsX - district consolidation crosstabbed
-- ExamStateStandards - district consolidation by standard
-- ExamStateStandardsX - district consolidation by standard - crosstabbed
-- ExamSchoolResults - school level reporting (table)
-- ExamSchoolResultsX - school level reporting crosstabbed
-- ExamSchooltandards - school level reporting by standard
-- ExamSchoolStandardsX - school level reporting by standard - crosstabbed
-- =============================================' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'ExamStateResults'
GO

