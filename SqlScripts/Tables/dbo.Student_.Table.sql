SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_](
	[stuID] [uniqueidentifier] NOT NULL,
	[stuCardID] [nvarchar](20) NULL,
	[stuNamePrefix] [nvarchar](50) NULL,
	[stuGiven] [nvarchar](50) NULL,
	[stuMiddleNames] [nvarchar](50) NULL,
	[stuFamilyName] [nvarchar](50) NULL,
	[stuNameSuffix] [nvarchar](20) NULL,
	[stuGivenSoundex] [nchar](10) NULL,
	[stuFamilySoundex] [nchar](10) NULL,
	[stuDoB] [date] NULL,
	[stuDoBEst] [int] NULL,
	[stuGender] [nvarchar](1) NULL,
	[stuEthnicity] [nvarchar](200) NULL,
	[stuCreateFileRef] [uniqueidentifier] NULL,
	[stuEditFileRef] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Student_] PRIMARY KEY CLUSTERED 
(
	[stuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

