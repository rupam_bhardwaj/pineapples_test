SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISTCourseGroup](
	[istgCode] [nvarchar](10) NOT NULL,
	[istgDescription] [nvarchar](50) NULL,
	[istgDescriptionL1] [nvarchar](50) NULL,
	[istgDescriptionL2] [nvarchar](50) NULL,
 CONSTRAINT [ISTCourseGroup_PK] PRIMARY KEY NONCLUSTERED 
(
	[istgCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ISTCourseGroup] TO [pISTRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISTCourseGroup] TO [pISTWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISTCourseGroup] TO [pISTWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISTCourseGroup] TO [pISTWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISTCourseGroup] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Categorisation of IST courses for reporting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourseGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'IST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourseGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inservice Training' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTCourseGroup'
GO

