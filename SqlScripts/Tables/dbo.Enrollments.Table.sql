SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enrollments](
	[enID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[enAge] [smallint] NULL,
	[enLevel] [nvarchar](10) NULL,
	[enOrigin] [nvarchar](10) NULL,
	[enM] [int] NULL,
	[enF] [int] NULL,
	[enSum]  AS (case when [enM] IS NULL AND [enF] IS NULL then NULL else isnull([enM],(0))+isnull([enF],(0)) end),
 CONSTRAINT [aaaaaEnrollments1_PK] PRIMARY KEY NONCLUSTERED 
(
	[enID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Enrollments] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Enrollments] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Enrollments] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Enrollments] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Enrollments] TO [pSchoolRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Enrollments] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Enrollments_SSID] ON [dbo].[Enrollments]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Enrollments] ADD  CONSTRAINT [DF__Enrollment__ssID__7EC1CEDB]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Enrollments] ADD  CONSTRAINT [DF__Enrollmen__enAge__7FB5F314]  DEFAULT ((0)) FOR [enAge]
GO
ALTER TABLE [dbo].[Enrollments] ADD  CONSTRAINT [DF__Enrollments__enM__00AA174D]  DEFAULT ((0)) FOR [enM]
GO
ALTER TABLE [dbo].[Enrollments] ADD  CONSTRAINT [DF__Enrollments__enF__019E3B86]  DEFAULT ((0)) FOR [enF]
GO
ALTER TABLE [dbo].[Enrollments]  WITH CHECK ADD  CONSTRAINT [Enrollments_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Enrollments] CHECK CONSTRAINT [Enrollments_FK00]
GO
ALTER TABLE [dbo].[Enrollments]  WITH CHECK ADD  CONSTRAINT [Enrollments_FK01] FOREIGN KEY([enLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[Enrollments] CHECK CONSTRAINT [Enrollments_FK01]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 8 2009
-- Description:	maintains the aggregates for enrolment on the schoolSurvey table
-- =============================================
CREATE TRIGGER [dbo].[enrolment_change] 
   ON  [dbo].[Enrollments] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE SchoolSurvey
	set ssEnrolM = sumM
	, ssEnrolF = sumF
	, ssEnrol = SumAll
	FROM SchoolSurvey SS
	LEFT JOIN 
	(
		Select 
		ssID
		,sum(enM) sumM
		, sum(enF) sumF
		, sum(enSum) sumAll
		from Enrollments
		GROUP BY ssID
	) E
	ON SS.ssID = E.ssID
	WHERE
		-- every affected ssID
		SS.ssID in (Select ssID from INSERTED union Select ssID from DELETED)
END
GO
ALTER TABLE [dbo].[Enrollments] ENABLE TRIGGER [enrolment_change]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'To faciliate readng the total f both genders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Enrollments', @level2type=N'COLUMN',@level2name=N'enSum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enrollments (sic) is the principal table used to stores enrolment data. 
By Age, level and Gender. 
Note that like all Pineapples tables, it is not normalised by gender, by has specific fields for M and F. 
Normalisation by Gender occurs when the data is moved to a pivot table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Enrollments'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'in Survey XML at /Survey/Grid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Enrollments'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Survey: ISurbveyComponent  fusbEnrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Enrollments'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrollments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Enrollments'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Enrollments'
GO

