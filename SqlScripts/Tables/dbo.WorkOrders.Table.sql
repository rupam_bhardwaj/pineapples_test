SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkOrders](
	[woID] [int] IDENTITY(1,1) NOT NULL,
	[woRef] [nvarchar](20) NULL,
	[woStatus] [nvarchar](15) NULL,
	[woDesc] [nvarchar](50) NULL,
	[woPlanned] [datetime] NULL,
	[woBudget] [money] NULL,
	[woWorkApproved] [datetime] NULL,
	[woFinanceApproved] [datetime] NULL,
	[woSourceOfFunds] [nvarchar](15) NULL,
	[woDonorManaged] [bit] NOT NULL,
	[woCostCentre] [nvarchar](10) NULL,
	[woTenderClose] [datetime] NULL,
	[woContracted] [datetime] NULL,
	[supCode] [nvarchar](10) NULL,
	[woContractValue] [money] NULL,
	[woCommenced] [datetime] NULL,
	[woPlannedCompletion] [datetime] NULL,
	[woCompleted] [datetime] NULL,
	[woInvoiceValue] [money] NULL,
	[woSignoff] [datetime] NULL,
	[woSignOffUser] [nvarchar](50) NULL,
 CONSTRAINT [WorkOrders_PK] PRIMARY KEY NONCLUSTERED 
(
	[woID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[WorkOrders] TO [pInfrastructureReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[WorkOrders] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[WorkOrders] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[WorkOrders] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[WorkOrders] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [lkpWorkOrderStatusWorkOrders] ON [dbo].[WorkOrders]
(
	[woStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [supCode] ON [dbo].[WorkOrders]
(
	[supCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkOrders] ADD  CONSTRAINT [DF_WorkOrders_woDonorManaged]  DEFAULT ((0)) FOR [woDonorManaged]
GO
ALTER TABLE [dbo].[WorkOrders]  WITH CHECK ADD  CONSTRAINT [lkpWorkOrderStatusWorkOrders] FOREIGN KEY([woStatus])
REFERENCES [dbo].[lkpWorkOrderStatus] ([codeCode])
GO
ALTER TABLE [dbo].[WorkOrders] CHECK CONSTRAINT [lkpWorkOrderStatusWorkOrders]
GO
ALTER TABLE [dbo].[WorkOrders]  WITH CHECK ADD  CONSTRAINT [SuppliersWorkOrders] FOREIGN KEY([supCode])
REFERENCES [dbo].[Suppliers] ([supCode])
GO
ALTER TABLE [dbo].[WorkOrders] CHECK CONSTRAINT [SuppliersWorkOrders]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A work order is an order on a supplier or subcontractor to perform infrastructure work at a school.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkOrders'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Work Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkOrders'
GO

