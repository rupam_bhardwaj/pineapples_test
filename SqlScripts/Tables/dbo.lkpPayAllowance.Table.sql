SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpPayAllowance](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[payItemType] [nvarchar](20) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_lkpPayAllowance] PRIMARY KEY CLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpPayAllowance] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpPayAllowance] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpPayAllowance] TO [pTeacherAdmin] AS [dbo]
GO
GRANT DELETE ON [dbo].[lkpPayAllowance] TO [pTeacherOps] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpPayAllowance] TO [pTeacherOps] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpPayAllowance] TO [pTeacherOps] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpPayAllowance] TO [public] AS [dbo]
GO

