SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpSalaryPoints](
	[spCode] [nvarchar](10) NOT NULL,
	[spSalary] [money] NULL,
	[spSort] [int] NULL,
	[salLevel] [nvarchar](5) NULL,
	[spExternalU] [nvarchar](10) NULL,
	[spExternal]  AS (isnull([spExternalU],[spCode])),
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpSalaryPoints1_PK] PRIMARY KEY NONCLUSTERED 
(
	[spCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpSalaryPoints] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSalaryPoints] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSalaryPoints] TO [pAdminWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[lkpSalaryPoints] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSalaryPoints] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSalaryPoints] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpSalaryPoints] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpSalaryPoints] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpSalaryPoints] ADD  CONSTRAINT [DF__lkpSalary__spSal__4B0D20AB]  DEFAULT ((0)) FOR [spSalary]
GO
ALTER TABLE [dbo].[lkpSalaryPoints] ADD  CONSTRAINT [DF__lkpSalary__spSor__4C0144E4]  DEFAULT ((0)) FOR [spSort]
GO
ALTER TABLE [dbo].[lkpSalaryPoints]  WITH CHECK ADD  CONSTRAINT [lkpSalaryPoints_FK00] FOREIGN KEY([salLevel])
REFERENCES [dbo].[lkpSalaryLevel] ([salLevel])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lkpSalaryPoints] CHECK CONSTRAINT [lkpSalaryPoints_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Salary Point Code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints', @level2type=N'COLUMN',@level2name=N'spCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unused - salaries related to points are maintained by date in SalaryPointRates' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints', @level2type=N'COLUMN',@level2name=N'spSalary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sort order for salary points. Should correspond to increasing pay ie lowest to highest. Need not be contiguous numbers.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints', @level2type=N'COLUMN',@level2name=N'spSort'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Salary Level to which this point belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints', @level2type=N'COLUMN',@level2name=N'salLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'override the code for searching ro loading from payroll system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints', @level2type=N'COLUMN',@level2name=N'spExternalU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'code for external payroll system = spExternalU, defaults to spCode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints', @level2type=N'COLUMN',@level2name=N'spExternal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Salary point futher divide Salary Levels. Each point is tied to a level. Salries are tied to Salary Points, and rates may vary over time.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryPoints'
GO

