SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CostCentres](
	[ccCode] [nvarchar](20) NOT NULL,
	[ccDescription] [nvarchar](100) NULL,
	[ccSector] [nvarchar](50) NULL,
	[ccProRate] [int] NULL,
	[ccDescriptionL1] [nvarchar](100) NULL,
	[ccDescriptionL2] [nvarchar](100) NULL,
 CONSTRAINT [aaaaaCostCentres1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ccCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[CostCentres] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[CostCentres] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[CostCentres] TO [pAdminWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[CostCentres] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[CostCentres] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[CostCentres] TO [pFinanceWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[CostCentres] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[CostCentres] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[CostCentres] ADD  CONSTRAINT [DF__CostCentr__ccPro__693CA210]  DEFAULT ((0)) FOR [ccProRate]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Part of the hgh level erporting of expenditure; for the calculation of EFA7 and 8. 

A cost centre is an item or subtotal in the Ministry''s General Ledger. Budget and Expenditure is recorded each year by cost centre. Typically, this will be rekeyed from some presentation of the ministry accounts.
To develop costs per sector each cost centre may be 
- mapped to a specific education sector, 
- prorated against all sectors, or 
- ignored.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CostCentres'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Education Expenditure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CostCentres'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Expenditure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CostCentres'
GO

