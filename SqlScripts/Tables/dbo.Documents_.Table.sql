SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Documents_](
	[docID] [uniqueidentifier] NOT NULL,
	[docTitle] [nvarchar](100) NULL,
	[docDescription] [nvarchar](400) NULL,
	[docSource] [nvarchar](100) NULL,
	[docDate] [datetime] NULL,
	[docRotate] [int] NOT NULL,
	[docTags] [nvarchar](200) NULL,
	[docType]  AS (case when [docTitle] like '%.%' then reverse(left(upper(reverse([docTitle])),charindex('.',reverse([docTitle]))-(1)))  end) PERSISTED,
	[pCreateUser] [nvarchar](100) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](100) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_Documents_] PRIMARY KEY CLUSTERED 
(
	[docID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Documents_] ADD  CONSTRAINT [DF_Documents__docID]  DEFAULT (newid()) FOR [docID]
GO
ALTER TABLE [dbo].[Documents_] ADD  CONSTRAINT [DF_Documents__docRotate]  DEFAULT ((0)) FOR [docRotate]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Document Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Document original name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Document description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Source of the document' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docSource'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of the document' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For images, rotation to apply on display' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docRotate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Search tags' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docTags'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'File Extension' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Documents_', @level2type=N'COLUMN',@level2name=N'docType'
GO

