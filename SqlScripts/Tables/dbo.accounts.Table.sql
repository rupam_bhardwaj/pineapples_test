SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[accounts](
	[schNo] [varchar](50) NULL,
	[name] [varchar](50) NULL,
	[vendorno] [varchar](50) NULL,
	[bank] [varchar](50) NULL,
	[account] [varchar](50) NULL
) ON [PRIMARY]
GO

