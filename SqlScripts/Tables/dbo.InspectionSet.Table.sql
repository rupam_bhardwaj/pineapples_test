SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InspectionSet](
	[inspsetID] [int] IDENTITY(1,1) NOT NULL,
	[inspsetName] [nvarchar](50) NULL,
	[inspsetType] [nvarchar](20) NULL,
	[inspsetYear] [int] NULL,
	[escnID] [int] NULL,
 CONSTRAINT [InspectionSet_PK] PRIMARY KEY NONCLUSTERED 
(
	[inspsetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[InspectionSet] TO [pInspectionRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[InspectionSet] TO [pInspectionWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[InspectionSet] TO [pInspectionWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[InspectionSet] TO [pInspectionWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[InspectionSet] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [escnID] ON [dbo].[InspectionSet]
(
	[escnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An inspection set is a set of inspections of the same type, carried out in the same year. 
The type of the set is an Inpection Type code defined in lkpInspectionTypes.
Any InspectionSet may have many related records in SchoolInspection - one for each school inspected in the Inspection set.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InspectionSet'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'linked table/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InspectionSet'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Inpection Set definitions are created and edited in
fgldInspectionSet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InspectionSet'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Assessments and Inspections' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InspectionSet'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Assements and Inspections' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InspectionSet'
GO

