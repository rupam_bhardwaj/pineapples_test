SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paCompetencies_](
	[pacomID] [int] IDENTITY(1,1) NOT NULL,
	[pacomCode] [nvarchar](10) NOT NULL,
	[pacomDescription] [nvarchar](50) NOT NULL,
	[pacomDescriptionL1] [nvarchar](50) NULL,
	[pacomDescriptionL2] [nvarchar](50) NULL,
	[pafrmCode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_paCompetencies] PRIMARY KEY CLUSTERED 
(
	[pacomID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_paCompetenciesfrm] ON [dbo].[paCompetencies_]
(
	[pafrmCode] ASC,
	[pacomCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paCompetencies_]  WITH CHECK ADD  CONSTRAINT [FK_paCompetencies_paFrameworks] FOREIGN KEY([pafrmCode])
REFERENCES [dbo].[paFrameworks_] ([pafrmCode])
GO
ALTER TABLE [dbo].[paCompetencies_] CHECK CONSTRAINT [FK_paCompetencies_paFrameworks]
GO

