SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaResourceCategories](
	[mresCatCode] [nvarchar](50) NOT NULL,
	[mresCatName] [nvarchar](150) NULL,
	[mresCatNameL1] [nvarchar](150) NULL,
	[mresCatNameL2] [nvarchar](150) NULL,
	[mresCatBuildingType] [nvarchar](10) NULL,
 CONSTRAINT [aaaaametaResourceCategories1_PK] PRIMARY KEY NONCLUSTERED 
(
	[mresCatCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaResourceCategories] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaResourceCategories] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaResourceCategories] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaResourceCategories] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaResourceCategories] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaResourceCategories] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'List of possible resource categories. Metadata? Or lookup? Somewhere between. Survey Resource grids a constructed by Category. Category code is foreign key on metaResourceDefs.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaResourceCategories'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaResourceCategories'
GO

