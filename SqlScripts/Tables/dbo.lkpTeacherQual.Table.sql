SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpTeacherQual](
	[codeCode] [nvarchar](20) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[codeSeq] [int] NULL,
	[codeTeach] [bit] NULL,
	[codeQualified] [bit] NULL,
	[codeCertified] [bit] NULL,
	[codeGroup] [nvarchar](20) NULL,
	[codeOld] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpTeacherQual1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpTeacherQual] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpTeacherQual] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherQual] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpTeacherQual] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherQual] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpTeacherQual] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpTeacherQual] ADD  CONSTRAINT [DF__lkpTeache__codeS__4222D4EF]  DEFAULT ((0)) FOR [codeSeq]
GO
ALTER TABLE [dbo].[lkpTeacherQual] ADD  CONSTRAINT [DF__lkpTeache__codeT__4316F928]  DEFAULT ((0)) FOR [codeTeach]
GO
ALTER TABLE [dbo].[lkpTeacherQual] ADD  CONSTRAINT [DF__lkpTeache__codeQ__440B1D61]  DEFAULT ((0)) FOR [codeQualified]
GO
ALTER TABLE [dbo].[lkpTeacherQual] ADD  CONSTRAINT [DF__lkpTeache__codeC__44FF419A]  DEFAULT ((0)) FOR [codeCertified]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Qualifications a teacher may have. Collected on the teacher survey. The qualifiactions are divided into Ed and nonEd. The teacher''s qualifications, and the secotr in which they are teacerhing, determine if the teacher is qualified to teach and/or certified to teach.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherQual'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherQual'
GO

