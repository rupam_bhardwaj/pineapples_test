SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Toilets](
	[toiID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[toiType] [nvarchar](25) NULL,
	[toiUse] [nvarchar](10) NULL,
	[toiNum] [int] NULL,
	[toiCondition] [nvarchar](1) NULL,
	[toiYN] [nvarchar](1) NULL,
	[toiNumUsable] [int] NULL,
 CONSTRAINT [aaaaaToilets1_PK] PRIMARY KEY NONCLUSTERED 
(
	[toiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Toilets] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Toilets] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Toilets] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Toilets] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Toilets] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Toilets_SSID] ON [dbo].[Toilets]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Toilets] ADD  CONSTRAINT [DF__Toilets__ssID__3BCADD1B]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Toilets] ADD  CONSTRAINT [DF__Toilets__toiNum__3CBF0154]  DEFAULT ((0)) FOR [toiNum]
GO
ALTER TABLE [dbo].[Toilets]  WITH CHECK ADD  CONSTRAINT [Toilets_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Toilets] CHECK CONSTRAINT [Toilets_FK00]
GO
ALTER TABLE [dbo].[Toilets]  WITH CHECK ADD  CONSTRAINT [Toilets_FK01] FOREIGN KEY([toiType])
REFERENCES [dbo].[lkpToiletTypes] ([ttypName])
GO
ALTER TABLE [dbo].[Toilets] CHECK CONSTRAINT [Toilets_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Toilet type. In WASH data, represents the most ''common'' toilet type. Not all the toilets numbered are therefore necessarily of this type.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Toilets', @level2type=N'COLUMN',@level2name=N'toiType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Male/Female/Boys/Girls/Common/Shared' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Toilets', @level2type=N'COLUMN',@level2name=N'toiUse'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of the toilets that are usable. this is an alternative to recording condition (WASH format)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Toilets', @level2type=N'COLUMN',@level2name=N'toiNumUsable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number, type and usage of toilets at a schoo. Collected o the Survey Form.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Toilets'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Toilets'
GO

