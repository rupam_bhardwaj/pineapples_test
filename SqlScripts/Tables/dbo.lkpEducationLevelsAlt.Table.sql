SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpEducationLevelsAlt](
	[codeCode] [nvarchar](50) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[edlMinYear] [int] NULL,
	[edlMaxYear] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpEducationLevelsAlt1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpEducationLevelsAlt] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpEducationLevelsAlt] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpEducationLevelsAlt] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpEducationLevelsAlt] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpEducationLevelsAlt] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpEducationLevelsAlt] ADD  CONSTRAINT [DF__lkpEducat__edlMi__3587F3E0]  DEFAULT ((0)) FOR [edlMinYear]
GO
ALTER TABLE [dbo].[lkpEducationLevelsAlt] ADD  CONSTRAINT [DF__lkpEducat__edlMa__367C1819]  DEFAULT ((0)) FOR [edlMaxYear]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Education Levels Alt and Alt2 are 2 alternative schmes for dividing the education system chronologically. This allows one schme to report Primary and Junior Secondary as separate level,s with another scheme reporting Basci Education as the union of these.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpEducationLevelsAlt'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpEducationLevelsAlt'
GO

