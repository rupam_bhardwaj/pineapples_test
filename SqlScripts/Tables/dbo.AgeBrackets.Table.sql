SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AgeBrackets](
	[bID] [int] IDENTITY(1,1) NOT NULL,
	[bName] [nvarchar](50) NULL,
	[bSort] [int] NULL,
	[bScheme] [nvarchar](5) NULL,
 CONSTRAINT [aaaaaAgeBrackets1_PK] PRIMARY KEY NONCLUSTERED 
(
	[bID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[AgeBrackets] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[AgeBrackets] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[AgeBrackets] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[AgeBrackets] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[AgeBrackets] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[AgeBrackets] ADD  CONSTRAINT [DF__AgeBracke__bSort__1332DBDC]  DEFAULT ((0)) FOR [bSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OBSELETE - all partitions are now managed in Partitions table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AgeBrackets'
GO

