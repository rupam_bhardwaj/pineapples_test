SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISTEnrol](
	[isteID] [int] IDENTITY(1,1) NOT NULL,
	[istsID] [int] NOT NULL,
	[tID] [int] NOT NULL,
	[isteConfirmed] [int] NULL,
	[isteResultA] [nvarchar](10) NULL,
	[isteResultN] [int] NULL,
	[isteAttendance] [int] NULL,
	[istePF] [nvarchar](1) NULL,
	[isteNote] [ntext] NULL,
 CONSTRAINT [ISTEnrol_PK] PRIMARY KEY NONCLUSTERED 
(
	[isteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ISTEnrol] TO [pISTRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISTEnrol] TO [pISTWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISTEnrol] TO [pISTWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISTEnrol] TO [pISTWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISTEnrol] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISTEnrol] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISTEnrol] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [ISTSessionISTEnrol] ON [dbo].[ISTEnrol]
(
	[istsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [TeacherIdentityISTEnrol] ON [dbo].[ISTEnrol]
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ISTEnrol] ADD  CONSTRAINT [DF__ISTEnrol__isteCo__6E8DC917]  DEFAULT ((0)) FOR [isteConfirmed]
GO
ALTER TABLE [dbo].[ISTEnrol]  WITH CHECK ADD  CONSTRAINT [ISTSessionISTEnrol] FOREIGN KEY([istsID])
REFERENCES [dbo].[ISTSession] ([istsID])
GO
ALTER TABLE [dbo].[ISTEnrol] CHECK CONSTRAINT [ISTSessionISTEnrol]
GO
ALTER TABLE [dbo].[ISTEnrol]  WITH CHECK ADD  CONSTRAINT [TeacherIdentityISTEnrol] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[ISTEnrol] CHECK CONSTRAINT [TeacherIdentityISTEnrol]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enrolment in an IST Session. tID is a foreign key from TeacherIdentity. istsID is the forign key from ISTSession.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTEnrol'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'IST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTEnrol'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inservice Training' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTEnrol'
GO

