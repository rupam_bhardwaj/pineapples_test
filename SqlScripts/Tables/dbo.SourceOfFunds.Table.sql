SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SourceOfFunds](
	[sofCode] [nvarchar](10) NOT NULL,
	[sofDesc] [nvarchar](50) NULL,
	[sofC] [bit] NULL,
	[sfgCode] [nvarchar](10) NULL,
	[sofDescL1] [nvarchar](50) NULL,
	[sofDescL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaSourceOfFunds1_PK] PRIMARY KEY NONCLUSTERED 
(
	[sofCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SourceOfFunds] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SourceOfFunds] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SourceOfFunds] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SourceOfFunds] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SourceOfFunds] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SourceOfFunds] ADD  CONSTRAINT [DF__SourceOfFu__sofC__0539C240]  DEFAULT ((0)) FOR [sofC]
GO
ALTER TABLE [dbo].[SourceOfFunds]  WITH CHECK ADD  CONSTRAINT [SourceOfFunds_FK00] FOREIGN KEY([sfgCode])
REFERENCES [dbo].[SourceOfFundsGroup] ([sfgCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SourceOfFunds] CHECK CONSTRAINT [SourceOfFunds_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sources of funding to schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SourceOfFunds'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Funding' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SourceOfFunds'
GO

