SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolAlias](
	[saID] [int] IDENTITY(1,1) NOT NULL,
	[saAlias] [nvarchar](100) NULL,
	[saSrc] [nvarchar](50) NULL,
	[schNo] [nvarchar](50) NOT NULL,
 CONSTRAINT [aaaaaSchoolAlias1_PK] PRIMARY KEY NONCLUSTERED 
(
	[saID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolAlias] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolAlias] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolAlias] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolAlias] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolAlias] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolAlias_schNo] ON [dbo].[SchoolAlias]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolAlias]  WITH CHECK ADD  CONSTRAINT [SchoolAlias_FK00] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolAlias] CHECK CONSTRAINT [SchoolAlias_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table of aliases - alternate names or numbers - for schools. Aliases can be identified by a source. This is how the Atlas numbers for schools are recorded (the source is the exam code). 
Aliases are created when schools are merged. Aliasses can also be manually defined.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolAlias'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolAlias'
GO

