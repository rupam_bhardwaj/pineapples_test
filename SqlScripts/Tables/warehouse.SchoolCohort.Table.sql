SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[SchoolCohort](
	[schNo] [nvarchar](50) NOT NULL,
	[surveyYear] [int] NULL,
	[Estimate] [int] NULL,
	[SurveyDimensionID] [int] NULL,
	[YearOfEd] [int] NULL,
	[Enrol] [int] NULL,
	[Rep] [int] NULL,
	[RepNY] [int] NULL,
	[TroutNY] [int] NULL,
	[EnrolNYNextLevel] [int] NULL,
	[RepNYNextLevel] [int] NULL,
	[TrinNYNextLevel] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Warehouse base table for reconsutrcted cohort model flow calculations. this table combines the require "This Year" NextYear" data on a single row, making the ratio calculations simple.

Reporting will generally come from views built on this table:

-- warehouse.schoolFlow
-- warehouse.districtCohort
-- warehouse.districtFlow
-- warehouse.nationCohort
-- warehouse.nationFlow


' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'SchoolCohort'
GO

