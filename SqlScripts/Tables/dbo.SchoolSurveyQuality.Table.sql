SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolSurveyQuality](
	[ssqID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[ssqDataItem] [nvarchar](50) NOT NULL,
	[ssqSubItem] [nvarchar](50) NULL,
	[ssqLevel] [int] NOT NULL,
	[ssqIssue] [nvarchar](50) NULL,
	[ssqDescription] [ntext] NULL,
	[ssqResolution] [ntext] NULL,
	[ssqUser] [nvarchar](50) NULL,
	[ssqDateTime] [datetime] NULL,
	[ssqCreateUser] [nvarchar](50) NULL,
	[ssqCreateDateTime] [datetime] NULL,
	[ssqRelay] [int] NOT NULL,
	[ssqRelayText] [nvarchar](400) NULL,
 CONSTRAINT [aaaaaSchoolSurveyQuality1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ssqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_SchoolSurveyQuality_SSID_Item_sub] UNIQUE NONCLUSTERED 
(
	[ssID] ASC,
	[ssqDataItem] ASC,
	[ssqSubItem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolSurveyQuality] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolSurveyQuality] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolSurveyQuality] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolSurveyQuality] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolSurveyQuality] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolSurveyQuality_ITem_Sub_Level] ON [dbo].[SchoolSurveyQuality]
(
	[ssqDataItem] ASC,
	[ssqSubItem] ASC,
	[ssqLevel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolSurveyQuality] ADD  CONSTRAINT [DF_SchoolSurveyQuality_ssqLevel]  DEFAULT ((1)) FOR [ssqLevel]
GO
ALTER TABLE [dbo].[SchoolSurveyQuality] ADD  CONSTRAINT [DF_SchoolSurveyQuality_ssqRelay]  DEFAULT ((0)) FOR [ssqRelay]
GO
ALTER TABLE [dbo].[SchoolSurveyQuality]  WITH CHECK ADD  CONSTRAINT [SchoolSurveyQuality_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolSurveyQuality] CHECK CONSTRAINT [SchoolSurveyQuality_FK00]
GO
ALTER TABLE [dbo].[SchoolSurveyQuality]  WITH CHECK ADD  CONSTRAINT [CK SchoolSurveyQuality ssqLevel] CHECK  (([ssqLevel]=(2) OR [ssqLevel]=(1) OR [ssqLevel]=(0)))
GO
ALTER TABLE [dbo].[SchoolSurveyQuality] CHECK CONSTRAINT [CK SchoolSurveyQuality ssqLevel]
GO
ALTER TABLE [dbo].[SchoolSurveyQuality]  WITH CHECK ADD  CONSTRAINT [CK_SchoolSurveyQuality_ssqRelay] CHECK  (([ssqRelay]=(2) OR [ssqRelay]=(1) OR [ssqRelay]=(0)))
GO
ALTER TABLE [dbo].[SchoolSurveyQuality] CHECK CONSTRAINT [CK_SchoolSurveyQuality_ssqRelay]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quality issues reported on a school survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurveyQuality'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurveyQuality'
GO

