SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpBuildingMaterials](
	[bmatCode] [nvarchar](10) NOT NULL,
	[bmatDescription] [nvarchar](50) NOT NULL,
	[bmatDescriptionL1] [nvarchar](50) NULL,
	[bmatDescriptionL2] [nvarchar](50) NULL,
	[bmatFloor] [bit] NOT NULL,
	[bmatWall] [bit] NOT NULL,
	[bmatRoof] [bit] NOT NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_lkpBuildingMaterials] PRIMARY KEY CLUSTERED 
(
	[bmatCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpBuildingMaterials] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpBuildingMaterials] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpBuildingMaterials] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpBuildingMaterials] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpBuildingMaterials] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpBuildingMaterials] ADD  CONSTRAINT [DF_lkpBuildingMaterials_bmatFloor]  DEFAULT ((1)) FOR [bmatFloor]
GO
ALTER TABLE [dbo].[lkpBuildingMaterials] ADD  CONSTRAINT [DF_lkpBuildingMaterials_bmatWall]  DEFAULT ((1)) FOR [bmatWall]
GO
ALTER TABLE [dbo].[lkpBuildingMaterials] ADD  CONSTRAINT [DF_lkpBuildingMaterials_bmatRoof]  DEFAULT ((1)) FOR [bmatRoof]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Detailed list of building materials that may be used for walls floor or roof.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingMaterials'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingMaterials'
GO

