SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstablishmentAllowance](
	[estaID] [int] IDENTITY(1,1) NOT NULL,
	[estaCode] [nvarchar](10) NOT NULL,
	[lstName] [nvarchar](20) NOT NULL,
	[estaListValue] [nvarchar](10) NULL,
	[estaYear] [int] NOT NULL,
	[estaValue] [money] NULL,
	[estaLimitPerSchool] [int] NOT NULL,
 CONSTRAINT [PK_EstablishmentAllowance] PRIMARY KEY CLUSTERED 
(
	[estaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EstablishmentAllowance] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[EstablishmentAllowance] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EstablishmentAllowance] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EstablishmentAllowance] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[EstablishmentAllowance] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EstablishmentAllowance] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EstablishmentAllowance] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[EstablishmentAllowance] TO [pFinanceRead] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_EstablishmentAllowance] ON [dbo].[EstablishmentAllowance]
(
	[estaCode] ASC,
	[estaYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EstablishmentAllowance] ADD  CONSTRAINT [DF_EstablishmentAllowance_estaLimitPerSchool]  DEFAULT ((0)) FOR [estaLimitPerSchool]
GO
ALTER TABLE [dbo].[EstablishmentAllowance]  WITH CHECK ADD  CONSTRAINT [FK_EstablishmentAllowance_lkpPayAllowance] FOREIGN KEY([estaCode])
REFERENCES [dbo].[lkpPayAllowance] ([codeCode])
GO
ALTER TABLE [dbo].[EstablishmentAllowance] CHECK CONSTRAINT [FK_EstablishmentAllowance_lkpPayAllowance]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EstablishmentAllowance holds details of allowances, and the schools to which they apply, to assist in costing the  Establishment Plan.
EstablishmentAllowance defines the year of the allowance, and a List Name. That list defines the schools to which the allowance applies.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstablishmentAllowance'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment Planning' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstablishmentAllowance'
GO

