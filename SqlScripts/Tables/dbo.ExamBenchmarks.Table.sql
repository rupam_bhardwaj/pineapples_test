SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamBenchmarks](
	[exbnchID] [int] IDENTITY(1,1) NOT NULL,
	[exbnchCode] [nvarchar](20) NOT NULL,
	[exID] [int] NOT NULL,
	[exbnchDescription] [nvarchar](200) NULL,
	[exstdID] [int] NULL,
 CONSTRAINT [PK_ExamBenchmarks] PRIMARY KEY CLUSTERED 
(
	[exbnchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ExamBenchmarks_exID] ON [dbo].[ExamBenchmarks]
(
	[exID] ASC,
	[exbnchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_examBnchCode] ON [dbo].[ExamBenchmarks]
(
	[exID] ASC,
	[exbnchCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExamBenchmarks]  WITH CHECK ADD  CONSTRAINT [FK_ExamBenchmarks_ExamStandards] FOREIGN KEY([exstdID])
REFERENCES [dbo].[ExamStandards] ([exstdID])
GO
ALTER TABLE [dbo].[ExamBenchmarks] CHECK CONSTRAINT [FK_ExamBenchmarks_ExamStandards]
GO
ALTER TABLE [dbo].[ExamBenchmarks]  WITH CHECK ADD  CONSTRAINT [FK_Exams_ExamBenchmarks] FOREIGN KEY([exID])
REFERENCES [dbo].[Exams] ([exID])
GO
ALTER TABLE [dbo].[ExamBenchmarks] CHECK CONSTRAINT [FK_Exams_ExamBenchmarks]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text code for benchmark as used in the Exam system. Unique within the exam instance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamBenchmarks', @level2type=N'COLUMN',@level2name=N'exbnchCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key of Exam' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamBenchmarks', @level2type=N'COLUMN',@level2name=N'exID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extended description of benchmark' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamBenchmarks', @level2type=N'COLUMN',@level2name=N'exbnchDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id of parent Standard.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamBenchmarks', @level2type=N'COLUMN',@level2name=N'exstdID'
GO

