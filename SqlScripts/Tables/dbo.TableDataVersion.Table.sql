SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableDataVersion](
	[TableName] [nvarchar](50) NULL,
	[TableVersion] [timestamp] NULL,
	[TableGroup] [int] NULL,
	[LastModifiedUser] [nvarchar](50) NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TableDataVersion] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TableDataVersion] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table to manage caching of lookups, by indicating what has changed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TableDataVersion'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'utility' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TableDataVersion'
GO

