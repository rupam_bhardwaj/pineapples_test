SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResourceProvision](
	[rpID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[rpType] [nvarchar](50) NULL,
	[rpSubject] [nvarchar](50) NULL,
	[rpYear] [int] NULL,
	[rpNum] [int] NULL,
	[rpSchLevel] [nvarchar](3) NULL,
	[rpLevel] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaResourceProvision1_PK] PRIMARY KEY NONCLUSTERED 
(
	[rpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ResourceProvision] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ResourceProvision] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ResourceProvision] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ResourceProvision] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ResourceProvision] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_ResourceProvision_ssID_Type] ON [dbo].[ResourceProvision]
(
	[ssID] ASC,
	[rpType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ResourceProvision] ADD  CONSTRAINT [DF__ResourcePr__ssID__0D0FEE32]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[ResourceProvision] ADD  CONSTRAINT [DF__ResourceP__rpYea__0E04126B]  DEFAULT ((0)) FOR [rpYear]
GO
ALTER TABLE [dbo].[ResourceProvision] ADD  CONSTRAINT [DF__ResourceP__rpNum__0EF836A4]  DEFAULT ((0)) FOR [rpNum]
GO
ALTER TABLE [dbo].[ResourceProvision]  WITH CHECK ADD  CONSTRAINT [FK_ResourceProvision_SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ResourceProvision] CHECK CONSTRAINT [FK_ResourceProvision_SchoolSurvey]
GO
ALTER TABLE [dbo].[ResourceProvision]  WITH CHECK ADD  CONSTRAINT [ResourceProvision_FK00] FOREIGN KEY([rpLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ResourceProvision] CHECK CONSTRAINT [ResourceProvision_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Recordse of resource provision on a survey. ssID is the foreign key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResourceProvision'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Resources' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ResourceProvision'
GO

