SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstablishmentSuperPos](
	[eppID] [int] IDENTITY(1,1) NOT NULL,
	[estcYear] [int] NOT NULL,
	[eppScope] [nvarchar](1) NOT NULL,
	[eppAuth] [nvarchar](10) NULL,
	[eppQuota] [int] NULL,
	[eppCountUser] [int] NULL,
	[eppCount]  AS (isnull([eppCountUser],[eppQuota])),
 CONSTRAINT [PK_EstablishmentSuperPos] PRIMARY KEY CLUSTERED 
(
	[eppID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[EstablishmentSuperPos] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[EstablishmentSuperPos] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[EstablishmentSuperPos] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EstablishmentSuperPos] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EstablishmentSuperPos] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_EstablishmentSuperPos_1] ON [dbo].[EstablishmentSuperPos]
(
	[estcYear] ASC,
	[eppScope] ASC,
	[eppAuth] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EstablishmentSuperPos] ADD  CONSTRAINT [DF_EstablishmentSuperPos_eppScope]  DEFAULT (N'A') FOR [eppScope]
GO
ALTER TABLE [dbo].[EstablishmentSuperPos]  WITH CHECK ADD  CONSTRAINT [FK_EstablishmentSuperPos_Authorities] FOREIGN KEY([eppAuth])
REFERENCES [dbo].[Authorities] ([authCode])
GO
ALTER TABLE [dbo].[EstablishmentSuperPos] CHECK CONSTRAINT [FK_EstablishmentSuperPos_Authorities]
GO
ALTER TABLE [dbo].[EstablishmentSuperPos]  WITH CHECK ADD  CONSTRAINT [FK_EstablishmentSuperPos_EstablishmentControl] FOREIGN KEY([estcYear])
REFERENCES [dbo].[EstablishmentControl] ([estcYear])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EstablishmentSuperPos] CHECK CONSTRAINT [FK_EstablishmentSuperPos_EstablishmentControl]
GO
ALTER TABLE [dbo].[EstablishmentSuperPos]  WITH CHECK ADD  CONSTRAINT [CK_EstablishmentSuperPos] CHECK  ((case [eppScope] when 'A' then [eppAuth] else 'A' end IS NOT NULL))
GO
ALTER TABLE [dbo].[EstablishmentSuperPos] CHECK CONSTRAINT [CK_EstablishmentSuperPos]
GO
ALTER TABLE [dbo].[EstablishmentSuperPos]  WITH CHECK ADD  CONSTRAINT [CK_EstablishmentSuperPos_1] CHECK  (([eppCountUser]>=(0)))
GO
ALTER TABLE [dbo].[EstablishmentSuperPos] CHECK CONSTRAINT [CK_EstablishmentSuperPos_1]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'unique index on super positions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EstablishmentSuperPos', @level2type=N'INDEX',@level2name=N'IX_EstablishmentSuperPos_1'
GO

