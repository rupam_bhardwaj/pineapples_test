SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GrantValues](
	[gvID] [int] IDENTITY(1,1) NOT NULL,
	[gvYear] [int] NOT NULL,
	[grCode] [nvarchar](20) NOT NULL,
	[gvSector] [nvarchar](3) NULL,
	[gvEdLevel] [nvarchar](10) NULL,
	[gvValue] [money] NOT NULL,
	[gvMaxPaymentNo] [int] NULL,
 CONSTRAINT [GrantValues_PK] PRIMARY KEY NONCLUSTERED 
(
	[gvID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[GrantValues] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[GrantValues] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[GrantValues] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[GrantValues] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[GrantValues] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [EducationSectorsGrantValues] ON [dbo].[GrantValues]
(
	[gvSector] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [GrantRulesGrantValues] ON [dbo].[GrantValues]
(
	[grCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GrantValues] ADD  CONSTRAINT [DF__GrantValu__gvMax__49322736]  DEFAULT ((0)) FOR [gvMaxPaymentNo]
GO
ALTER TABLE [dbo].[GrantValues]  WITH CHECK ADD  CONSTRAINT [EducationSectorsGrantValues] FOREIGN KEY([gvSector])
REFERENCES [dbo].[EducationSectors] ([secCode])
GO
ALTER TABLE [dbo].[GrantValues] CHECK CONSTRAINT [EducationSectorsGrantValues]
GO
ALTER TABLE [dbo].[GrantValues]  WITH CHECK ADD  CONSTRAINT [GrantRulesGrantValues] FOREIGN KEY([grCode])
REFERENCES [dbo].[GrantRules] ([grCode])
GO
ALTER TABLE [dbo].[GrantValues] CHECK CONSTRAINT [GrantRulesGrantValues]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Grant Values holds the value of a Grant Rule for each year. 
Grant Values also allow a different rate to be set for a rule for by Sector. Where a sector is specified , the Grant Calculator applies that rule to the enrolment or boarders in that sector only. Therefore, a rule may appear once in each grant payment for each sector taught at the school.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantValues'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'Linked table/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantValues'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Grant Values are maintained in a popup (frmSchoolGRantValues) launched from the Lookups page for Grant Rules.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantValues'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantValues'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GrantValues'
GO

