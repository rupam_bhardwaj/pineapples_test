SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[measurePopG](
	[popYear] [int] NULL,
	[dID] [nvarchar](5) NULL,
	[popAge] [int] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[pop] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Population by district, gender and age.
This is taken from the Deault Population Model.
If the population data is available by District, it is provided in this table. Otherwise, the dID field is null and the record is unique for year, age, and gendercode.

Becuase the population may be split by district, this table should be aggregated if you want national numbers.' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'measurePopG'
GO

