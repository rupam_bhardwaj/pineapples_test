SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicles](
	[vehID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[vehMake] [nvarchar](50) NULL,
	[vehModel] [nvarchar](50) NULL,
	[vehYearMade] [int] NULL,
	[vehDatePurchased] [datetime] NULL,
	[vehDealer] [nvarchar](50) NULL,
	[vehFunded] [nvarchar](50) NULL,
	[vehCondition] [nvarchar](1) NULL,
 CONSTRAINT [aaaaaVehicles1_PK] PRIMARY KEY NONCLUSTERED 
(
	[vehID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Vehicles] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Vehicles] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Vehicles] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Vehicles] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Vehicles] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Vehicles_SSID] ON [dbo].[Vehicles]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Vehicles]  WITH CHECK ADD  CONSTRAINT [Vehicles_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Vehicles] CHECK CONSTRAINT [Vehicles_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'List of vehicle assets at a school. SI TVET survey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vehicles'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'TVET' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vehicles'
GO

