SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuildingReview](
	[brevID] [int] IDENTITY(1,1) NOT NULL,
	[inspID] [int] NOT NULL,
	[bldID] [int] NOT NULL,
	[brevSeq] [int] NULL,
	[brevCode] [nvarchar](10) NOT NULL,
	[brevSubType] [nvarchar](10) NULL,
	[brevTitle] [nvarchar](50) NOT NULL,
	[brevDescription] [nvarchar](2000) NULL,
	[brevClosed] [bit] NOT NULL,
	[brevEndMode] [nvarchar](10) NULL,
	[brevL] [decimal](18, 2) NULL,
	[brevW] [decimal](18, 2) NULL,
	[brevA] [decimal](18, 2) NULL,
	[brevIrregular] [bit] NOT NULL,
	[brevH] [decimal](18, 2) NULL,
	[brevE] [decimal](18, 2) NULL,
	[brevR] [decimal](18, 2) NULL,
	[brevLevels] [int] NULL,
	[brevLat] [decimal](10, 6) NULL,
	[brevLong] [decimal](10, 6) NULL,
	[brevMaterials] [nvarchar](10) NULL,
	[brevMatW] [nvarchar](10) NULL,
	[brevNoteW] [nvarchar](200) NULL,
	[brevMatF] [nvarchar](10) NULL,
	[brevNoteF] [nvarchar](200) NULL,
	[brevMatR] [nvarchar](10) NULL,
	[brevNoteR] [nvarchar](200) NULL,
	[bvcCode] [nvarchar](10) NULL,
	[brevYear] [int] NULL,
	[brevCondition] [nvarchar](1) NULL,
	[brevNote] [nvarchar](2000) NULL,
	[brevRoomsClass] [int] NULL,
	[brevRoomsOHT] [int] NULL,
	[brevRoomsStaff] [int] NULL,
	[brevRoomsAdmin] [int] NULL,
	[brevRoomsStorage] [int] NULL,
	[brevRoomsDorm] [int] NULL,
	[brevRoomsDining] [int] NULL,
	[brevRoomsKitchen] [int] NULL,
	[brevRoomsLibrary] [int] NULL,
	[brevRoomsHall] [int] NULL,
	[brevRoomsSpecialTuition] [int] NULL,
	[brevRoomsOther] [int] NULL,
	[brevRoomsAll]  AS (((((((((((isnull([brevRoomsClass],(0))+isnull([brevRoomsOHT],(0)))+isnull([brevRoomsStaff],(0)))+isnull([brevRoomsAdmin],(0)))+isnull([brevRoomsStorage],(0)))+isnull([brevRoomsDorm],(0)))+isnull([brevRoomsKitchen],(0)))+isnull([brevRoomsDining],(0)))+isnull([brevRoomsLibrary],(0)))+isnull([brevRoomsHall],(0)))+isnull([brevRoomsSpecialTuition],(0)))+isnull([brevRoomsOther],(0))),
 CONSTRAINT [PK_BuildingREview] PRIMARY KEY CLUSTERED 
(
	[brevID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[BuildingReview] TO [pInfrastructureRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[BuildingReview] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[BuildingReview] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[BuildingReview] TO [pInfrastructureWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[BuildingReview] TO [pSchoolRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[BuildingReview] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[BuildingReview] ADD  CONSTRAINT [DF_BuildingReview_brevClosed]  DEFAULT ((0)) FOR [brevClosed]
GO
ALTER TABLE [dbo].[BuildingReview] ADD  CONSTRAINT [DF_BuildingReview_brevIrregular]  DEFAULT ((0)) FOR [brevIrregular]
GO
ALTER TABLE [dbo].[BuildingReview]  WITH CHECK ADD  CONSTRAINT [FK_BuildingReview_Building] FOREIGN KEY([bldID])
REFERENCES [dbo].[Buildings] ([bldID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BuildingReview] CHECK CONSTRAINT [FK_BuildingReview_Building]
GO
ALTER TABLE [dbo].[BuildingReview]  WITH CHECK ADD  CONSTRAINT [FK_BuildingReview_Inspection] FOREIGN KEY([inspID])
REFERENCES [dbo].[SchoolInspection] ([inspID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BuildingReview] CHECK CONSTRAINT [FK_BuildingReview_Inspection]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 1 2010
-- Description:	Insert a building review
-- =============================================
-- the instead of trigger allows a null bldID to get passed - indicating that we will create the building
-- record
CREATE TRIGGER [dbo].[BuildingReviewInsert] 
   ON  [dbo].[BuildingReview] 
   INSTEAD OF INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- some validations
	---- reject the whole batch if no inspID
	
	if exists(Select inspID from INSERTED WHERE inspID is null) begin
		raiserror('Inspection ID is missing',16,1)
	end 

	-- only allow a single inspID in any update that contains new buildings

	declare @InspCount int

	select @inspCount = count( distinct inspID) from INSERTED WHERE bldID is null

	if (@inspCount > 1)begin
		raiserror('Cannot create buildings from more than one inspection',16,1)
		
	end
    -- Insert statements for trigger here
	-- where the building id is null, insert new buildings , gettig the school code from the inspection

	declare @currentBldID int
	declare @inspID int

begin try

	begin transaction
		
		Select @currentBldID = isnull(max(bldID),0) from Buildings
		select @inspID = inspID from INSERTED		-- just required if we add buildings

--****************** adding buildings ********************************

-- 2 distinct cases 
-- closure is separate
--- this is a bit of an odd case - adding a building that is closed straight off the bat...
IF EXISTS (Select * from INSERTED WHERE brevClosed = 1)	
		INSERT INTO [Buildings]
			([bldgCode]	
			, bldgSubType
			, bldgCloseDate
			, bldgEndMode
			)
			Select
				INSERTED.brevCode
				, INSERTED.brevSubType
				, SI.inspStart		-- the close date is  the inspection date
				,INSERTED.brevEndMode
			FROM INSERTED
			INNER JOIN SchoolInspection SI
				on INSERTED.inspID = SI.inspID
			WHERE INSERTED.bldID is null
				AND INSERTED.brevClosed = 1

IF EXISTS (Select * from INSERTED WHERE brevClosed = 0)		
		INSERT INTO [Buildings]
          ([bldgCode]
           ,[bldgTitle]
           ,[bldgDescription]
           ,[bldgSubType]
           ,[bldgL]
           ,[bldgW]
           ,[bldgA]
           ,[bldgIrregular]
           ,[bldgH]
           ,[bldgE]
           ,[bldgR]
           ,[bldgLevels]
           ,[bldgLat]
           ,[bldgLong]
           
           ,[bldgMaterials]
           ,[bldgMatW]
           ,[bldgNoteW]
           ,[bldgMatF]
           ,[bldgNoteF]
           ,[bldgMatR]
           ,[bldgNoteR]
           
           ,[bvcCode]
           ,[bldgYear]
           ,[bldgCondition]
           ,[schNo]
           ,[bldgNote]
           ,[bldgRoomsClass]
           ,[bldgRoomsOHT]
           ,[bldgRoomsStaff]
           ,[bldgRoomsAdmin]
           ,[bldgRoomsStorage]
           ,[bldgRoomsDorm]
           ,[bldgRoomsKitchen]
           ,[bldgRoomsDining]
           ,[bldgRoomsLibrary]
           ,[bldgRoomsSpecialTuition]
           ,[bldgRoomsHall]
           ,[bldgRoomsOther]
           , bldgSeq)
		 SELECT 
	
			  [brevCode]
			  ,[brevTitle]
			  ,[brevDescription]
			  ,[brevSubType]
			  ,[brevL]
			  ,[brevW]
			  ,[brevA]
			  ,[brevIrregular]
			  ,[brevH]
			  ,[brevE]
			  ,[brevR]
			  ,[brevLevels]
			  ,[brevLat]
			  ,[brevLong]
			  
			  ,[brevMaterials]
			  ,[brevMatW]
			  ,[brevNoteW]
			  ,[brevMatF]
			  ,[brevNoteF]
			  ,[brevMatR]
			  ,[brevNoteR]
			  
			  ,[bvcCode]
			  ,[brevYear]
			  ,[brevCondition]
			  , schNo
			  ,[brevNote]
			  ,[brevRoomsClass]
			  ,[brevRoomsOHT]
			  ,[brevRoomsStaff]
			  ,[brevRoomsAdmin]
			  ,[brevRoomsStorage]
			  ,[brevRoomsDorm]
			  ,[brevRoomsDining]
			  ,[brevRoomsKitchen]
			  ,[brevRoomsLibrary]
			  ,[brevRoomsSpecialTuition]
			  ,[brevRoomsHall]
			  ,[brevRoomsOther]
				, brevSeq		
		 
		
		  FROM INSERTED
			INNER JOIN SchoolInspection SI
				on INSERTED.inspID = SI.inspID
			WHERE INSERTED.bldID is null

-- now we can add just those we created to the building review
	INSERT INTO [BuildingReview]
			   ([inspID]
			   , brevSeq
			   ,[bldID]
			   ,[brevCode]
			  ,[brevTitle]
			  ,[brevDescription]
			  ,[brevSubType]
			  
			   ,brevClosed
			   , brevEndMode
			   
			  ,[brevL]
			  ,[brevW]
			  ,[brevA]
			  ,[brevIrregular]
			  ,[brevH]
			  ,[brevE]
			  ,[brevR]
			  ,[brevLevels]
			  ,[brevLat]
			  ,[brevLong]
			  
			  ,[brevMaterials]
			  ,[brevMatW]
			  ,[brevNoteW]
			  ,[brevMatF]
			  ,[brevNoteF]
			  ,[brevMatR]
			  ,[brevNoteR]
			  
			  ,[bvcCode]
			  ,[brevYear]
			  ,[brevCondition]

			  ,[brevNote]
			  
			  ,[brevRoomsClass]
			  ,[brevRoomsOHT]
			  ,[brevRoomsStaff]
			  ,[brevRoomsAdmin]
			  ,[brevRoomsStorage]
			  ,[brevRoomsDorm]
			  ,[brevRoomsDining]
			  ,[brevRoomsKitchen]
			  ,[brevRoomsLibrary]
			  ,[brevRoomsSpecialTuition]
			  ,[brevRoomsHall]
			  ,[brevRoomsOther]
			 )

	  	SELECT 
			  @inspID
			  , bldgSeq
			, bldID
	        , [bldgCode]
           ,[bldgTitle]
           ,[bldgDescription]
           ,[bldgSubType]
           
           , bldgClosed
           , bldgEndMode
           
           ,[bldgL]
           ,[bldgW]
           ,[bldgA]
           ,[bldgIrregular]
           ,[bldgH]
           ,[bldgE]
           ,[bldgR]
           ,[bldgLevels]
           ,[bldgLat]
           ,[bldgLong]
           
           ,[bldgMaterials]
           ,[bldgMatW]
           ,[bldgNoteW]
           ,[bldgMatF]
           ,[bldgNoteF]
           ,[bldgMatR]
           ,[bldgNoteR]
           
           ,[bvcCode]
           ,[bldgYear]
           ,[bldgCondition]

           ,[bldgNote]
           
           ,[bldgRoomsClass]
           ,[bldgRoomsOHT]
           ,[bldgRoomsStaff]
           ,[bldgRoomsAdmin]
           ,[bldgRoomsStorage]
           ,[bldgRoomsDorm]
           ,[bldgRoomsKitchen]
           ,[bldgRoomsDining]
           ,[bldgRoomsLibrary]
            ,[bldgRoomsSpecialTuition]
          ,[bldgRoomsHall]
           ,[bldgRoomsOther]
		  FROM [Buildings]
			WHERE bldID > @currentbldID
	
if exists(Select * from INSERTED where bldID is not null) begin
-- avoid this statement altogther if it's not going to do anything
-- that allows the value of @@IDENTITY to point to the record actually added
-- which Access expects when this is invoked from a bound form

-- 
	INSERT INTO [BuildingReview]
			   ([inspID]
			   , brevSeq
			   ,[bldID]
			   ,[brevCode]
			  ,[brevTitle]
			  ,[brevDescription]
			  ,[brevSubType]

				, brevClosed
				, brevEndMode
			  
			  ,[brevL]
			  ,[brevW]
			  ,[brevA]
			  ,[brevIrregular]
			  ,[brevH]
			  ,[brevE]
			  ,[brevR]
			  ,[brevLevels]
			  ,[brevLat]
			  ,[brevLong]
			  
			  ,[brevMaterials]
			  ,[brevMatW]
			  ,[brevNoteW]
			  ,[brevMatF]
			  ,[brevNoteF]
			  ,[brevMatR]
			  ,[brevNoteR]
			  
			  ,[bvcCode]
			  ,[brevYear]
			  ,[brevCondition]

			  ,[brevNote]
			  
			  ,[brevRoomsClass]
			  ,[brevRoomsOHT]
			  ,[brevRoomsStaff]
			  ,[brevRoomsAdmin]
			  ,[brevRoomsStorage]
			  ,[brevRoomsDorm]
			  ,[brevRoomsDining]
			  ,[brevRoomsKitchen]
			  ,[brevRoomsLibrary]
			  ,[brevRoomsSpecialTuition]
			  ,[brevRoomsHall]
			  ,[brevRoomsOther]
			 )
	  	SELECT 
			  [inspID]
			  , brevSeq
			   ,[bldID]
			   ,[brevCode]

			  ,[brevTitle]
			  ,[brevDescription]
			  ,[brevSubType]
			  
			  , brevClosed
			  , brevEndMode
			  
			  ,[brevL]
			  ,[brevW]
			  ,[brevA]
			  ,[brevIrregular]
			  ,[brevH]
			  ,[brevE]
			  ,[brevR]
			  ,[brevLevels]
			  ,[brevLat]
			  ,[brevLong]
			  
			  ,[brevMaterials]
			  ,[brevMatW]
			  ,[brevNoteW]
			  ,[brevMatF]
			  ,[brevNoteF]
			  ,[brevMatR]
			  ,[brevNoteR]
			  
			  ,[bvcCode]
			  ,[brevYear]
			  ,[brevCondition]

			  ,[brevNote]
			  
			  ,[brevRoomsClass]
			  ,[brevRoomsOHT]
			  ,[brevRoomsStaff]
			  ,[brevRoomsAdmin]
			  ,[brevRoomsStorage]
			  ,[brevRoomsDorm]
			  ,[brevRoomsDining]
			  ,[brevRoomsKitchen]
			  ,[brevRoomsLibrary]
			  ,[brevRoomsSpecialTuition]
			  ,[brevRoomsHall]
			  ,[brevRoomsOther]

		FROM INSERTED
		WHERE bldID is not null

end
	-- if we are inserting a new review for an exisitng building, we should update the building fields
    -- if this is the most recent review for the building

	select BR.bldID,  max(inspStart) LastInspection
	into #maxDate 
	from SchoolInspection SI
		INNER JOIN BuildingReview BR
			ON BR.inspID = SI.inspID
		INNER JOIN INSERTED
			ON BR.bldID = INSERTED.bldID
	WHERE INSERTED.bldID is not null
	group by BR.bldID

-- for buildings that are closed by this REview,
-- only set the closed date
	UPDATE Buildings
		SET bldgCode = isnull(brevCode, bldgCode)
			, bldgTitle = isnull(brevTitle, bldgTitle)
			, bldgDescription = isnull(brevDescription, bldgDescription)
			, bldgSubType = isnull(brevSubType, bldgSubType)
			
			, bldgCloseDate = case brevClosed when 1 then SI.inspStart else null end
			, bldgEndMode = case brevClosed when 1 then brevEndMode else null end
			, bldgL = isnull(brevL, bldgL)
			, bldgW = isnull(brevW, bldgW)
			, bldgA = isnull(brevA, bldgA)
			, bldgIrregular = isnull(brevIrregular, bldgIrregular)
			, bldgH = isnull(brevH, bldgH)
			, bldgE = isnull(brevE, bldgE)
            , bldgLevels = isnull(brevLevels, bldgLevels)
			, bldgR = isnull(brevR, bldgR)
            , bldgMaterials = isnull(brevMaterials, bldgMaterials)
			, bldgMatW = isnull(brevMatW, bldgMatW)
			, bldgMatF = isnull(brevMatF, bldgMatF)
			, bldgMatR = isnull(brevMatR, bldgMatR)
			, bldgNoteW = isnull(brevNoteW, bldgNoteW)
			, bldgNoteF = isnull(brevNoteF, bldgNoteF)
			, bldgNoteR = isnull(brevNoteR, bldgNoteR)
           , bldgLat = isnull(brevLat, bldgLat)
           , bldgLong = isnull(brevLong, bldgLong)
           
 
           
           ,[bvcCode] = INSERTED.bvcCode
           ,[bldgYear] = isnull(brevYear, bldgYear)
           ,[bldgCondition] = isnull(brevCondition, bldgCondition)
           ,[bldgNote] = isnull(brevNote, bldgNote)
           ,[bldgRoomsClass] = brevRoomsClass
           ,[bldgRoomsOHT] = brevRoomsOHT
           ,[bldgRoomsStaff] = brevRoomsStaff
           ,[bldgRoomsAdmin] = brevRoomsAdmin
           ,[bldgRoomsStorage] = brevRoomsStorage
           ,[bldgRoomsDorm] = brevRoomsDorm
           ,[bldgRoomsKitchen] = brevRoomsKitchen
           ,[bldgRoomsDining] = brevRoomsDining
           ,[bldgRoomsLibrary] = brevRoomsLibrary
            ,[bldgRoomsSpecialTuition] = brevRoomsSpecialTuition
          ,[bldgRoomsHall] = brevRoomsHall
           ,[bldgRoomsOther] = brevRoomsOther
           
           , bldgSeq = brevSeq
			
	FROM Buildings
		INNER JOIN INSERTED
			ON Buildings.bldID = INSERTED.bldID
		INNER JOIN SchoolInspection SI
			ON INSERTED.inspID = SI.inspID
		INNER JOIN #maxDate 
			ON #maxDate.bldID = INSERTED.bldID
	WHERE SI.inspStart = #maxDate.LastInspection
	
	commit transaction

end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch
	
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000), 
        @ErrorSeverity INT, 
        @ErrorState INT; 
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin 
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end 

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 
end catch
END
GO
ALTER TABLE [dbo].[BuildingReview] ENABLE TRIGGER [BuildingReviewInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 3 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[BuildingReviewUpdate] 
   ON  [dbo].[BuildingReview] 
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- if this is the most recent review, we can update the building data
    
		select BR.bldID,  max(inspStart) LastInspection
		into #maxDate 
		from SchoolInspection SI
			INNER JOIN BuildingReview BR
				ON BR.inspID = SI.inspID
			INNER JOIN INSERTED
				ON BR.bldID = INSERTED.bldID
		WHERE INSERTED.bldID is not null
		group by BR.bldID
	
	
    	UPDATE Buildings
		SET bldgCode = isnull(brevCode, bldgCode)
			, bldgTitle = isnull(brevTitle, bldgTitle)
			, bldgDescription = isnull(brevDescription, bldgDescription)
			, bldgSubType = isnull(brevSubType, bldgSubType)
			
			, bldgCloseDate = case brevClosed when 1 then SI.inspStart else null end
			, bldgEndMode = case brevClosed when 1 then brevEndMode else null end

			, bldgL = isnull(brevL, bldgL)
			, bldgW = isnull(brevW, bldgW)
			, bldgA = isnull(brevA, bldgA)
			, bldgIrregular = isnull(brevIrregular, bldgIrregular)
			, bldgH = isnull(brevH, bldgH)
			, bldgE = isnull(brevE, bldgE)
            , bldgLevels = isnull(brevLevels, bldgLevels)
			, bldgR = isnull(brevR, bldgR)
            , bldgMaterials = isnull(brevMaterials, bldgMaterials)
			, bldgMatW = isnull(brevMatW, bldgMatW)
			, bldgMatF = isnull(brevMatF, bldgMatF)
			, bldgMatR = isnull(brevMatR, bldgMatR)
			, bldgNoteW = isnull(brevNoteW, bldgNoteW)
			, bldgNoteF = isnull(brevNoteF, bldgNoteF)
			, bldgNoteR = isnull(brevNoteR, bldgNoteR)
           , bldgLat = isnull(brevLat, bldgLat)
           , bldgLong = isnull(brevLong, bldgLong)
           
 
           
           ,[bvcCode] = INSERTED.bvcCode
           ,[bldgYear] = isnull(brevYear, bldgYear)
           ,[bldgCondition] = isnull(brevCondition, bldgCondition)
           ,[bldgNote] = isnull(brevNote, bldgNote)
           ,[bldgRoomsClass] = brevRoomsClass
           ,[bldgRoomsOHT] = brevRoomsOHT
           ,[bldgRoomsStaff] = brevRoomsStaff
           ,[bldgRoomsAdmin] = brevRoomsAdmin
           ,[bldgRoomsStorage] = brevRoomsStorage
           ,[bldgRoomsDorm] = brevRoomsDorm
           ,[bldgRoomsKitchen] = brevRoomsKitchen
           ,[bldgRoomsDining] = brevRoomsDining
           ,[bldgRoomsLibrary] = brevRoomsLibrary
            ,[bldgRoomsSpecialTuition] = brevRoomsSpecialTuition
          ,[bldgRoomsHall] = brevRoomsHall
           ,[bldgRoomsOther] = brevRoomsOther
			
	FROM Buildings
		INNER JOIN INSERTED
			ON Buildings.bldID = INSERTED.bldID
		INNER JOIN SchoolInspection SI
			ON INSERTED.inspID = SI.inspID
		INNER JOIN #maxDate 
			ON #maxDate.bldID = INSERTED.bldID
	WHERE SI.inspStart = #maxDate.LastInspection
	

END
GO
ALTER TABLE [dbo].[BuildingReview] ENABLE TRIGGER [BuildingReviewUpdate]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Building subtype' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BuildingReview', @level2type=N'COLUMN',@level2name=N'brevSubType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A Building Review is a review of a single building. Building review stores most of the same data as the buildings table. A building Review is a particular typ[e of SchoolInspcetion, where the InspectionType = ''BLDG''. The Inspection ID inspID is the foreign key on this table, linking reviews of individual buildings to the school''s overall Building Review inspection.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BuildingReview'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Building Reviews' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BuildingReview'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Buildings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BuildingReview'
GO

