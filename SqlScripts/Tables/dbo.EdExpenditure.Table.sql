SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EdExpenditure](
	[xedID] [int] IDENTITY(1,1) NOT NULL,
	[xedYear] [int] NULL,
	[xedEdLevel] [nvarchar](20) NULL,
	[xedItem] [nvarchar](50) NULL,
	[xedXtrn] [bit] NULL,
	[sofCode] [nvarchar](50) NULL,
	[xedTotA] [money] NULL,
	[xedCurrentA] [money] NULL,
	[xedTotB] [money] NULL,
	[xedCurrentB] [money] NULL,
 CONSTRAINT [aaaaaEdExpenditure1_PK] PRIMARY KEY NONCLUSTERED 
(
	[xedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EdExpenditure] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EdExpenditure] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EdExpenditure] TO [pFinanceWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[EdExpenditure] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EdExpenditure] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[EdExpenditure] ADD  CONSTRAINT [DF__EdExpendi__xedXt__1F98B2C1]  DEFAULT ((0)) FOR [xedXtrn]
GO
ALTER TABLE [dbo].[EdExpenditure] ADD  CONSTRAINT [DF__EdExpendi__xedTo__208CD6FA]  DEFAULT ((0)) FOR [xedTotA]
GO
ALTER TABLE [dbo].[EdExpenditure] ADD  CONSTRAINT [DF__EdExpendi__xedCu__2180FB33]  DEFAULT ((0)) FOR [xedCurrentA]
GO
ALTER TABLE [dbo].[EdExpenditure] ADD  CONSTRAINT [DF__EdExpendi__xedTo__22751F6C]  DEFAULT ((0)) FOR [xedTotB]
GO
ALTER TABLE [dbo].[EdExpenditure] ADD  CONSTRAINT [DF__EdExpendi__xedCu__236943A5]  DEFAULT ((0)) FOR [xedCurrentB]
GO
ALTER TABLE [dbo].[EdExpenditure]  WITH CHECK ADD  CONSTRAINT [FK_EdExpenditure_CostCentres] FOREIGN KEY([xedEdLevel])
REFERENCES [dbo].[CostCentres] ([ccCode])
GO
ALTER TABLE [dbo].[EdExpenditure] CHECK CONSTRAINT [FK_EdExpenditure_CostCentres]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Education Expenditure holds actual and budgets by Cost Centre for current and capital expenditure. This is distributed between sectors according to the settings for the cost centre, so as to construct a cost per pupil for each sector.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EdExpenditure'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Education Expenditure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EdExpenditure'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EdExpenditure'
GO

