SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeTeacherRoleMap](
	[typroleID] [int] IDENTITY(1,1) NOT NULL,
	[stCode] [nvarchar](10) NULL,
	[codeCode] [nvarchar](50) NULL,
	[typroleSort] [int] NULL,
 CONSTRAINT [aaaaametaSchoolTypeTeacherRoleMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[typroleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeTeacherRoleMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeTeacherRoleMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeTeacherRoleMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeTeacherRoleMap] TO [pTeacherAdmin] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeTeacherRoleMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherRoleMap] ADD  CONSTRAINT [DF__metaSchoo__typro__0CDAE408]  DEFAULT ((0)) FOR [typroleSort]
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherRoleMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeTeacherRole_FK00] FOREIGN KEY([codeCode])
REFERENCES [dbo].[lkpTeacherRole] ([codeCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherRoleMap] CHECK CONSTRAINT [metaSchoolTypeTeacherRole_FK00]
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherRoleMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeTeacherRole_FK01] FOREIGN KEY([stCode])
REFERENCES [dbo].[SchoolTypes] ([stCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[metaSchoolTypeTeacherRoleMap] CHECK CONSTRAINT [metaSchoolTypeTeacherRole_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teacher roles that may be required in a given school type. Foreign keys are Teacher Role and School type.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeTeacherRoleMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeTeacherRoleMap'
GO

