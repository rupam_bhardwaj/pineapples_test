SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyYearRank](
	[rankID] [int] IDENTITY(1,1) NOT NULL,
	[svyYear] [smallint] NULL,
	[schNo] [nvarchar](50) NULL,
	[rankDistrict] [nvarchar](10) NULL,
	[rankSchtype] [nvarchar](50) NULL,
	[rankEnrol] [smallint] NULL,
	[rankEstimate] [smallint] NULL,
	[rankDistrictSchoolType] [smallint] NULL,
	[rankDistrictSchoolTypeDec] [nvarchar](2) NULL,
	[rankDistrictSchoolTypeQtl] [nvarchar](2) NULL,
	[rankSchoolType] [smallint] NULL,
	[rankSchoolTypeDec] [nvarchar](2) NULL,
	[rankSchoolTypeQtl] [nvarchar](2) NULL,
	[rankEnrolF] [smallint] NULL,
	[rankEnrolM] [smallint] NULL,
 CONSTRAINT [aaaaaSurveyYearRank1_PK] PRIMARY KEY NONCLUSTERED 
(
	[rankID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyYearRank] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyYearRank] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyYearRank] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyYearRank] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyYearRank] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SurveyYearRank]  WITH CHECK ADD  CONSTRAINT [SurveyYearRank_FK00] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyYearRank] CHECK CONSTRAINT [SurveyYearRank_FK00]
GO
ALTER TABLE [dbo].[SurveyYearRank]  WITH CHECK ADD  CONSTRAINT [SurveyYearRank_FK01] FOREIGN KEY([svyYear])
REFERENCES [dbo].[Survey] ([svyYear])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SurveyYearRank] CHECK CONSTRAINT [SurveyYearRank_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the Rank position of a school by type and province , or by type nationally, Allows grouping into quartile and decile.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearRank'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Reporting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearRank'
GO

