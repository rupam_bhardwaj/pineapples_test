SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolLedger](
	[schlgID] [int] IDENTITY(1,1) NOT NULL,
	[schNo] [nvarchar](50) NULL,
	[svyYear] [int] NULL,
	[schlgView] [nvarchar](50) NULL,
	[schlgAccount] [nvarchar](20) NULL,
	[schlgBudget] [int] NULL,
	[schlgActual] [int] NULL,
	[schlgYearOffset] [int] NULL,
 CONSTRAINT [aaaaaSchoolLedger1_PK] PRIMARY KEY NONCLUSTERED 
(
	[schlgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolLedger] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolLedger] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolLedger] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolLedger] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolLedger] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolLedger_SchNo_Year] ON [dbo].[SchoolLedger]
(
	[schNo] ASC,
	[svyYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolLedger] ADD  CONSTRAINT [DF__SchoolLed__schlg__60FC61CA]  DEFAULT ((0)) FOR [schlgBudget]
GO
ALTER TABLE [dbo].[SchoolLedger] ADD  CONSTRAINT [DF__SchoolLed__schlg__61F08603]  DEFAULT ((0)) FOR [schlgActual]
GO
ALTER TABLE [dbo].[SchoolLedger] ADD  CONSTRAINT [DF__SchoolLed__schlg__62E4AA3C]  DEFAULT ((0)) FOR [schlgYearOffset]
GO
ALTER TABLE [dbo].[SchoolLedger]  WITH CHECK ADD  CONSTRAINT [SchoolLedger_FK00] FOREIGN KEY([schlgAccount])
REFERENCES [dbo].[GeneralLedger] ([glCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SchoolLedger] CHECK CONSTRAINT [SchoolLedger_FK00]
GO
ALTER TABLE [dbo].[SchoolLedger]  WITH CHECK ADD  CONSTRAINT [SchoolLedger_FK01] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolLedger] CHECK CONSTRAINT [SchoolLedger_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In Vanuatu, derived from Cahs Book and Budgets prepared as part of the school survey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolLedger'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolLedger'
GO

