SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Population](
	[popID] [int] IDENTITY(1,1) NOT NULL,
	[popmodCode] [nvarchar](10) NULL,
	[dID] [nvarchar](5) NULL,
	[iCode] [nvarchar](2) NULL,
	[elN] [nvarchar](10) NULL,
	[elL] [nvarchar](10) NULL,
	[popEA] [nvarchar](10) NULL,
	[popYear] [int] NULL,
	[popAge] [int] NULL,
	[popM] [int] NULL,
	[popF] [int] NULL,
	[popSum]  AS (case when [popM] IS NULL AND [popF] IS NULL then NULL else isnull([popM],(0))+isnull([popF],(0)) end),
 CONSTRAINT [aaaaaPopulation1_PK] PRIMARY KEY NONCLUSTERED 
(
	[popID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Population] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Population] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Population] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Population] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Population] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Population] ADD  CONSTRAINT [DF__Population__popM__6F4A8121]  DEFAULT ((0)) FOR [popM]
GO
ALTER TABLE [dbo].[Population] ADD  CONSTRAINT [DF__Population__popF__703EA55A]  DEFAULT ((0)) FOR [popF]
GO
ALTER TABLE [dbo].[Population]  WITH CHECK ADD  CONSTRAINT [Population_FK00] FOREIGN KEY([popmodCode])
REFERENCES [dbo].[PopulationModel] ([popmodCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Population] CHECK CONSTRAINT [Population_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Population data; either from census or projections. May be grouped at various levels of aggregation. Often only at the national level. Must be in 1 year age ranges - apply sprague multipliers to any data not in this format.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Population'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Population' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Population'
GO

