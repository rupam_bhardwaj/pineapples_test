SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaPupilTableDefs](
	[tdefCode] [nvarchar](20) NOT NULL,
	[tdefName] [nvarchar](50) NULL,
	[tdefDataCode] [nvarchar](20) NULL,
	[tdefRows] [nvarchar](50) NULL,
	[tdefCols] [nvarchar](50) NULL,
	[tdefGen] [bit] NULL,
	[tdefRowTotals] [bit] NULL,
	[tdefColTotals] [bit] NULL,
	[tdefHideCol1] [bit] NULL,
	[tdefSrc] [nvarchar](50) NULL,
	[tdefNameL1] [nvarchar](50) NULL,
	[tdefNameL2] [nvarchar](50) NULL,
	[tdefShading] [nvarchar](50) NULL,
	[tdefPermissionsReqR] [nvarchar](50) NULL,
	[tdefPermissionsReqW] [nvarchar](50) NULL,
 CONSTRAINT [aaaaametaPupilTableDefs1_PK] PRIMARY KEY NONCLUSTERED 
(
	[tdefCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaPupilTableDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaPupilTableDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaPupilTableDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaPupilTableDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaPupilTableDefs] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaPupilTableDefs] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaPupilTableDefs] ADD  CONSTRAINT [DF__metaPupil__tdefG__16CE6296]  DEFAULT ((0)) FOR [tdefGen]
GO
ALTER TABLE [dbo].[metaPupilTableDefs] ADD  CONSTRAINT [DF__metaPupil__tdefR__17C286CF]  DEFAULT ((1)) FOR [tdefRowTotals]
GO
ALTER TABLE [dbo].[metaPupilTableDefs] ADD  CONSTRAINT [DF__metaPupil__tdefC__18B6AB08]  DEFAULT ((1)) FOR [tdefColTotals]
GO
ALTER TABLE [dbo].[metaPupilTableDefs] ADD  CONSTRAINT [DF__metaPupil__tdefH__19AACF41]  DEFAULT ((0)) FOR [tdefHideCol1]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If not null, a Role name. The user must be a member of this role to show this data. NOT intended to control the access, but provides a means of knowing that the access wil be denied before reading, so attempting to hide.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaPupilTableDefs', @level2type=N'COLUMN',@level2name=N'tdefPermissionsReqR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Definitions of the Pupil Tables - grids of data that may be part of a survey form. The Tabledefs defines:
- the code and name
- data code, to identify these records in PupilTables
- code of SchoolTableItems for the row domain of the grid
- code of school table item for the column domain of the grid
- various other setup options
Rendered as XML as part of the SchoolSurvey XML.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaPupilTableDefs'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'MetaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaPupilTableDefs'
GO

