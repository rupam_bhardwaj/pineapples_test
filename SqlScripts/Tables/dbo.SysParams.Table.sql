SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysParams](
	[paramName] [nvarchar](50) NOT NULL,
	[paramUse] [nvarchar](250) NULL,
	[paramText] [nvarchar](150) NULL,
	[paramInt] [int] NULL,
	[paramMemo] [ntext] NULL,
	[paramObject] [image] NULL,
 CONSTRAINT [aaaaaSysParams1_PK] PRIMARY KEY NONCLUSTERED 
(
	[paramName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[SysParams] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SysParams] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SysParams] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[SysParams] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SysParams] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System parameters in various categories, have a code, description, and optionally numeric, text or image value. 
See System Configuraiton documentation for full descriptions.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysParams'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Configuration' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SysParams'
GO

