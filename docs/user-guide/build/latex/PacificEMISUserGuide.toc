\select@language {english}
\contentsline {chapter}{\numberline {1}Pacific EMIS Overview}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Brief History}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Accessing and Login}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Overview of Features (Modules)}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Education Indicators (At a Glance)}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Managing Schools}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Managing Teachers}{9}{chapter.4}
\contentsline {chapter}{\numberline {5}Managing Performance Assessment}{11}{chapter.5}
\contentsline {chapter}{\numberline {6}Managing Surveys}{13}{chapter.6}
\contentsline {section}{\numberline {6.1}Online Web Application Survey Submission}{13}{section.6.1}
\contentsline {section}{\numberline {6.2}PDF eSurvey Submission}{13}{section.6.2}
\contentsline {chapter}{\numberline {7}Indices and tables}{15}{chapter.7}
