﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


using Pineapples.Data.DB;
using Softwords.DataTools;

namespace Pineapples.Data.DataLayer
{
    internal class DSLookups : BaseRepository, IDSLookups
    {
        Dictionary<string, string> lkpCommands;

        public DSLookups(DB.PineapplesEfContext cxt) : base(cxt)
        {
            lkpCommands = new Dictionary<string, string>();
            lkpCommands.Add("schoolTypes", "SELECT stCode C, stDescription N from TRSchooltypes;");
            lkpCommands.Add("districts", "SELECT dID C, [dName] N from Districts ORDER BY dName;");
            lkpCommands.Add("authorities", "SELECT authCode C, authName N, authType T from TRAuthorities;");
            lkpCommands.Add("authorityTypes",
                            "SELECT [codeDescription] N, [codeCode] C, codeGroup G from TRAuthorityType;");
            lkpCommands.Add("authorityGovt",
                            "SELECT [codeDescription] N, [codeCode] C from TRAuthorityGovt;");
            lkpCommands.Add("islands", 
                            "SELECT [iName] N, [iCode] C, iGroup D FROM lkpIslands ORDER BY [iName];");
            lkpCommands.Add("electoraten",
                            "SELECT [codeDescription] N, [codeCode] C FROM lkpElectorateN ORDER BY [codeDescription];");
            lkpCommands.Add("electoratel",
                            "SELECT [codeDescription] N, [codeCode] C FROM lkpElectorateL ORDER BY [codeDescription];");
            lkpCommands.Add("examTypes",
                            "SELECT exCode C, exName N FROM lkpExamTypes ORDER BY exName");
            lkpCommands.Add("languages",
                            "SELECT [langName] N, [langCode] C FROM TRLanguage ORDER BY [langName];");
            lkpCommands.Add("listNames",
                            "SELECT [lstDescription] N, [lstName] C FROM Lists ORDER BY [lstName];");
            lkpCommands.Add("schoolClasses",
                            "SELECT [codeCode] C, [codedescription] N FROM TRSchoolClass ORDER BY [codedescription];");
            lkpCommands.Add("schoolNames",
                            "SELECT [schName] N, [schNo] C FROM Schools ORDER BY [schName];");
            // pa lookups
            lkpCommands.Add("paFrameworks",
                            "SELECT [pafrmDescription] N, [pafrmCode] C FROM paFrameworks_ ORDER BY [pafrmDescription];");
            lkpCommands.Add("paCompetencies",
                            "SELECT [pacomDescription] N, [comFullID] C, paComID ID FROM paCompetencies ORDER BY [comFullID];");
            lkpCommands.Add("paElements",
                            "SELECT [paelmDescription] N, [elmFullID] C, paelmID ID FROM paElements ORDER BY [elmFullID];");
            lkpCommands.Add("paIndicators",
                            "SELECT [paindText] N, [indFullID] C, paindID ID " +
                             " FROM paIndicators ORDER BY[indFullID]");
            lkpCommands.Add("paFields",
                            "SELECT C FROM paFields ORDER BY seq");
            lkpCommands.Add("examFieldOptions",
                            "SELECT f C, g from common.examFieldOptions ORDER BY seq;");
            lkpCommands.Add("examDataOptions",
                            "SELECT C, N from common.examDataOptions ORDER BY seq;");
            lkpCommands.Add("schoolFieldOptions",
                            "SELECT f C, g from common.schoolFieldOptions ORDER BY seq;");
            lkpCommands.Add("schoolDataOptions",
                            "SELECT C, N from common.schoolDataOptions ORDER BY seq;");            
            lkpCommands.Add("teacherFieldOptions",
                            "SELECT f C, g from common.teacherFieldOptions ORDER BY seq;");
            lkpCommands.Add("teacherDataOptions",
                            "SELECT C, N from common.teacherDataOptions ORDER BY seq;");
            lkpCommands.Add("studentFieldOptions",
                            "SELECT f C, g from common.studentFieldOptions ORDER BY seq;");
            lkpCommands.Add("studentDataOptions",
                            "SELECT C, N from common.studentDataOptions ORDER BY seq;");
            lkpCommands.Add("appDataOptions",
                            "SELECT C, N from common.appDataOptions ORDER BY seq;");

            lkpCommands.Add("vocab",
                            "SELECT  isnull(vocabTerm, vocabName) N, vocabName C FROM sysVocab");
            lkpCommands.Add("sysParams",
                            "SELECT paramName C, paramText N, paramInt I from sysParams");
            lkpCommands.Add("schoolRegistration",
                            "SELECT rxCode C, rxDescription N FROM TRSchoolRegistration");
            lkpCommands.Add("surveyYears",
                            "SELECT svyYear C, svyYear N FROM Survey ORDER BY svyYear DESC");
            // WHERE clause used to only retrieve table lookups that already work; in future either add to where clause of remove when they're all supported
            lkpCommands.Add("tableDefs",
                            "SELECT tdefCode C, tdefName N FROM metaPupilTableDefs WHERE tdefCode = 'ENROL_PRI' OR tdefCode = 'ENROL_SEC' OR tdefCode = 'REP' OR tdefCode = 'DIS' OR tdefCode = 'TRIN' OR tdefCode = 'TROUT' UNION SELECT 'ENROL', 'Enrolments';");
            lkpCommands.Add("subjects",
                            "SELECT subjCode C, subjName N FROM TRSubjects ORDER BY subjName");
            lkpCommands.Add("publishers",
                            "SELECT pubCode C, pubName N FROM Publishers ORDER BY pubName");
            lkpCommands.Add("bookTG",
                            "SELECT codeCode C, codeDescription N FROM TRBookTG ORDER BY codeDescription");
            lkpCommands.Add("levels",
                            "SELECT codeCode C, codeDescription N, lvlYear YoEd from TrLevels ORDER BY lvlYear");
            lkpCommands.Add("gender",
                            "SELECT codeCode C, codeDescription N FROM TRGender ORDER BY codeDescription");
            lkpCommands.Add("teacherRegStatus",
                            "SELECT statusCode C, statusDesc N FROM lkpTeacherStatus ORDER BY statusDesc");
            lkpCommands.Add("ethnicities", // Move this to a configurable lkpEthnicity and make sure it matches in workbook
                            "SELECT 'Carolinian' C, 'Carolinian' N UNION SELECT 'Caucasian' C, 'Caucasian' N UNION SELECT 'Chamorro' C, 'Chamorro' N UNION SELECT 'Chinese' C, 'Chinese' N UNION SELECT 'Chuukese' C, 'Chuukese' N UNION SELECT 'Faraulup' C, 'Faraulup' N UNION SELECT 'Fechailapese' C, 'Fechailapese' N UNION SELECT 'Filipino' C, 'Filipino' N UNION SELECT 'Hawaiian' C, 'Hawaiian' N UNION SELECT 'Ifalikese' C, 'Ifalikese' N UNION SELECT 'Japanese' C, 'Japanese' N UNION SELECT 'Kapingese' C, 'Kapingese' N UNION SELECT 'Kiribatese' C, 'Kiribatese' N UNION SELECT 'Korean' C, 'Korean' N UNION SELECT 'Kosraean' C, 'Kosraean' N UNION SELECT 'Lamotrek' C, 'Lamotrek' N UNION SELECT 'Marshallese' C, 'Marshallese' N UNION SELECT 'Mokilese' C, 'Mokilese' N UNION SELECT 'Mortlockese' C, 'Mortlockese' N UNION SELECT 'Ngatikese' C, 'Ngatikese' N UNION SELECT 'Nukuoroan' C, 'Nukuoroan' N UNION SELECT 'Other' C, 'Other' N UNION SELECT 'Palauan' C, 'Palauan' N UNION SELECT 'Pingelapese' C, 'Pingelapese' N UNION SELECT 'Pohnpeian' C, 'Pohnpeian' N UNION SELECT 'Samoan' C, 'Samoan' N UNION SELECT 'Satawalese' C, 'Satawalese' N UNION SELECT 'Sonsorolese' C, 'Sonsorolese' N UNION SELECT 'Tobian' C, 'Tobian' N UNION SELECT 'Ulithian' C, 'Ulithian' N UNION SELECT 'Waabese' C, 'Waabese' N UNION SELECT 'Woleian' C, 'Woleian' N UNION SELECT 'Yapese' C, 'Yapese' N UNION");

            // documents and links
            lkpCommands.Add("teacherLinkTypes",
                            "SELECT codeCode C, codeDescription N FROM TRTeacherLinkTypes ORDER BY codeSort");
            lkpCommands.Add("schoolLinkTypes",
                            "SELECT codeCode C, codeDescription N FROM TRSchoolLinkTypes ORDER BY codeSort");


        }
        public DataSet CoreLookups()
        {
            string[] lookups = new string[]
                        {
                            "schoolTypes",
                            "districts",
                            "authorities",
                            "authorityTypes",
                            "authorityGovt",
                            "islands",
                            "electoraten",
                            "electoratel",
                            "examTypes",
                            "languages",
                            "listNames",
                            "schoolClasses",
                            "schoolNames",
                            "vocab",
                            "sysParams",
                            "paFields",
                            "paFrameworks",
                            "examFieldOptions",
                            "examDataOptions",
                            "schoolFieldOptions",
                            "schoolDataOptions",
                            "teacherFieldOptions",
                            "teacherDataOptions",
                            "studentFieldOptions",
                            "studentDataOptions",
                            "schoolRegistration",
                            "surveyYears",
                            "tableDefs",
                            "subjects",
                            "publishers",
                            "bookTG",
                            "levels",
                            "gender",
                            "teacherRegStatus",
                            "teacherLinkTypes",
                            "schoolLinkTypes"
                        };
            return getLookupSets(lookups);
        }

        public DataSet PaLookups()
        {
            string[] lookups = new string[]
                         {
                            "paCompetencies",
                            "paElements",
                            "paIndicators"

                         };
            return getLookupSets(lookups);
        }

        public DataSet getLookupSets(string[] lookups)
        {
            string lkpSql = string.Empty;
            string cmdSql = string.Empty;
            StringBuilder sb = new StringBuilder(String.Empty);

            foreach (string lkpName in lookups)
            {
                if (lkpCommands.TryGetValue(lkpName, out lkpSql))
                {
                    sb.AppendLine(lkpSql);
                }
                else
                {
                    throw new Exception(String.Format(@"Lookup table {0} is not defined", lkpName));
                }
            }

            // all were identified
            SqlCommand cmd = new SqlCommand();

            cmd.CommandType = CommandType.Text;
            // !!!!!!
            // IMPORTANT - this method should only be used when sql is a LITERAL
            cmd.CommandText = sb.ToString();
            DataSet ds = cmdToDataset(cmd);
            for (int i = 0; i < lookups.Length; i++)
            {
                ds.Tables[i].TableName = lookups[i];
            }
            return ds;
        }


        private IDataResult getLookups(string sql)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandType = CommandType.Text;
            // !!!!!!
            // IMPORTANT - this method should only be used when sql is a LITERAL
            cmd.CommandText = sql;
            return sqlExec(cmd, false);

        }

        public void SaveEntry(string lookupName, Models.LookupEntry entry)
        {

        }
    }
}
