﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;

namespace Pineapples.Data.DataLayer
{
    public class DSQuarterlyReport : BaseRepository, IDSQuarterlyReport
    {
        public DSQuarterlyReport(DB.PineapplesEfContext cxt) : base(cxt)
        {

        }

        #region CRUD methods
        public IDataResult Create(QuarterlyReportBinder qr, ClaimsIdentity identity)
        {
            qr.Create(cxt, identity);
            return Read((int)qr.qrID);
        }

        public IDataResult Read(int QRID)
        {
            SqlCommand cmd = readCmd(QRID);
            //return sqlExec(cmd, false, "quarterlyreport");
            return structuredResult(cmd, "quarterlyreport", new string[] { });
        }

        public QuarterlyReportBinder Update(QuarterlyReportBinder qr, ClaimsIdentity identity)
        {
            qr.Update(cxt, identity);
            return qr;
        }

        public QuarterlyReportBinder Delete(int QRID, ClaimsIdentity identity)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Collection Methods
        public IDataResult Filter(QuarterlyReportFilter fltr)
        {
            SqlCommand cmd = filterCmd(fltr);
            return sqlExec(cmd);
        }
        #endregion

        #region Commands
        private SqlCommand filterCmd(QuarterlyReportFilter fltr)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyRead.QuarterlyReportFilterPaged";

            cmd.Parameters.Add("@ColumnSet", SqlDbType.Int).Value = (object)(fltr.ColumnSet) ?? DBNull.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = (object)(fltr.PageSize) ?? DBNull.Value;
            cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = (object)(fltr.PageNo) ?? DBNull.Value;
            cmd.Parameters.Add("@SortColumn", SqlDbType.NVarChar, 128).Value = (object)(fltr.SortColumn) ?? DBNull.Value;
            cmd.Parameters.Add("@SortDir", SqlDbType.Int).Value = (object)(fltr.SortDesc) ?? DBNull.Value;

            cmd.Parameters.Add("@QRID", SqlDbType.Int).Value = (object)fltr.QRID ?? DBNull.Value;
            cmd.Parameters.Add("@School", SqlDbType.NVarChar, 50).Value = (object)fltr.School ?? DBNull.Value;
            cmd.Parameters.Add("@SchoolNo", SqlDbType.NVarChar, 50).Value = (object)fltr.SchoolNo ?? DBNull.Value;
            cmd.Parameters.Add("@InspQuarterlyReport", SqlDbType.NVarChar, 50).Value = (object)fltr.InspQuarterlyReport ?? DBNull.Value;

            return cmd;
        }

        /// <summary>
        /// read command read a single quarterlayreport
        /// </summary>
        private SqlCommand readCmd(int QRID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyRead.QuarterlyReportRead";

            cmd.Parameters.Add("@QRID", SqlDbType.Int)
                .Value = QRID;
            return cmd;
        }
        #endregion
    }
}
