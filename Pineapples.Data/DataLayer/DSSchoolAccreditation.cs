﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;

namespace Pineapples.Data.DataLayer
{
    public class DSSchoolAccreditation : BaseRepository, IDSSchoolAccreditation
    {
        public DSSchoolAccreditation(DB.PineapplesEfContext cxt) : base(cxt)
        {

        }

        #region CRUD methods
        public IDataResult Create(SchoolAccreditationBinder sa, ClaimsIdentity identity)
        {
            sa.Create(cxt, identity);
            return Read((int)sa.saID);
        }

        public IDataResult Read(int SAID)
        {
            SqlCommand cmd = readCmd(SAID);
            //return sqlExec(cmd, false, "schoolaccreditation");
            return structuredResult(cmd, "schoolaccreditation", new string[] { });
        }

        public SchoolAccreditationBinder Update(SchoolAccreditationBinder sa, ClaimsIdentity identity)
        {
            sa.Update(cxt, identity);
            return sa;
        }

        public SchoolAccreditationBinder Delete(int SAID, ClaimsIdentity identity)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Collection Methods
        public IDataResult Filter(SchoolAccreditationFilter fltr)
        {
            SqlCommand cmd = filterCmd(fltr);
            return sqlExec(cmd);
        }
        #endregion

        #region Commands
        private SqlCommand filterCmd(SchoolAccreditationFilter fltr)
        {
            SqlCommand cmd = new SqlCommand();
            // generated by a tool
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyRead.SchoolAccreditationFilterPaged";

            cmd.Parameters.Add("@ColumnSet", SqlDbType.Int).Value = (object)(fltr.ColumnSet) ?? DBNull.Value;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = (object)(fltr.PageSize) ?? DBNull.Value;
            cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = (object)(fltr.PageNo) ?? DBNull.Value;
            cmd.Parameters.Add("@SortColumn", SqlDbType.NVarChar, 128).Value = (object)(fltr.SortColumn) ?? DBNull.Value;
            cmd.Parameters.Add("@SortDir", SqlDbType.Int).Value = (object)(fltr.SortDesc) ?? DBNull.Value;

            cmd.Parameters.Add("@SAID", SqlDbType.Int).Value = (object)fltr.SAID ?? DBNull.Value;
            cmd.Parameters.Add("@School", SqlDbType.NVarChar, 50).Value = (object)fltr.School ?? DBNull.Value;
            cmd.Parameters.Add("@InspYear", SqlDbType.NVarChar, 50).Value = (object)fltr.InspYear ?? DBNull.Value;

            return cmd;
        }

        /// <summary>
        /// read command read a single school accreditation
        /// </summary>
        private SqlCommand readCmd(int SAID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pSurveyRead.SchoolAccreditationRead";

            cmd.Parameters.Add("@SAID", SqlDbType.Int).Value = SAID;
            return cmd;
        }
        #endregion
    }
}
