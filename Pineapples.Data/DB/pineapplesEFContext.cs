﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace Pineapples.Data.DB
{
    public partial class PineapplesEfContext : DbContext
    {
        public PineapplesEfContext(string databaseorconnection) : base(databaseorconnection)
        {
        }

        public static PineapplesEfContext CurrentContext()
        {
            string server = System.Web.Configuration.WebConfigurationManager.AppSettings["server"];
            string database = System.Web.Configuration.WebConfigurationManager.AppSettings["database"];
            string appname = System.Web.Configuration.WebConfigurationManager.AppSettings["appname"] ?? "Pineapples.Data";
            return CurrentContext(server, database, appname);
        }

        public static PineapplesEfContext CurrentContext(string server, string database, string appname)
        {
            string conn = String.Format("Data Source={0};Initial Catalog={1};Application Name={2};Integrated Security=True", server, database, appname);
            return new PineapplesEfContext(conn);
        }
        public static PineapplesEfContext CurrentContext(string server, string database)
        {
            return CurrentContext(server, database, "pineapples.Injected");
        }

        #region data io methods

        public DataSet execCommand(SqlCommand cmd)
        {
            cmd.Connection = this.Database.Connection as SqlConnection;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public async Task<DataSet> execCommandAsync(SqlCommand cmd)
        {
            if (this.Database.Connection.State == ConnectionState.Closed)
            {
                await this.Database.Connection.OpenAsync();
            }
            // canot make the async code using ExecuteReaderAsync work when there are more than 2 results
            // so the only part of this nominally 'async' is the opening the connection
            return execCommand(cmd);
        }

        public object execScalar(SqlCommand cmd)
        {
            if (this.Database.Connection.State == ConnectionState.Closed)
            {
                this.Database.Connection.Open();
            }
            cmd.Connection = this.Database.Connection as SqlConnection;
            return cmd.ExecuteScalar();
        }

        public async Task<object> execScalarAsync(SqlCommand cmd)
        {
            if (this.Database.Connection.State == ConnectionState.Closed)
            {
                await this.Database.Connection.OpenAsync();
            }
            cmd.Connection = this.Database.Connection as SqlConnection;
            return await cmd.ExecuteScalarAsync();
        }

        #endregion

        #region Entity Sets
        public virtual DbSet<Models.School> Schools { get; set; }
        public virtual DbSet<Models.TeacherIdentity> TeacherIdentities { get; set; }
        public virtual DbSet<Models.Book> Books { get; set; }
        public virtual DbSet<Models.QuarterlyReport> QuarterlyReports { get; set; }
        public virtual DbSet<Models.PerfAssessment> PerfAssessments { get; set; }
        public virtual DbSet<Models.PerfAssessmentIndicator> PerfAssessmentIndicators { get; set; }
        public virtual DbSet<Models.SchoolAccreditation> SchoolAccreditations { get; set; }
        public virtual DbSet<Models.TeacherLink> TeacherLinks { get; set; }
        public virtual DbSet<Models.SchoolLink> SchoolLinks { get; set; }
        public virtual DbSet<Models.Document> Documents { get; set; }
        public virtual DbSet<Models.Exam> Exams { get; set; }
        public virtual DbSet<Models.Student> Students { get; set; }
        // lookup tables
        public virtual DbSet<Models.Gender> Genders { get; set; }
        public virtual DbSet<Models.Disability> Disabilities { get; set; }

        public virtual DbSet<Models.Authority> Authorities { get; set; }
        public virtual DbSet<Models.AuthorityType> AuthorityTypes { get; set; }
        public virtual DbSet<Models.AuthorityGovt> AuthorityPublicPrivate { get; set; }


        public virtual DbSet<Models.Island> Islands { get; set; }
        public virtual DbSet<Models.District> Districts { get; set; }

        public virtual DbSet<Models.EducationLevel> EducationLevels { get; set; }
        public virtual DbSet<Models.EducationLevelAlt> EducationLevelsAlt { get; set; }
        public virtual DbSet<Models.EducationLevelAlt2> EducationLevelsAlt2 { get; set; }

        public virtual DbSet<Models.Bank> Banks { get; set; }
        public virtual DbSet<Models.BookTG> BookTGs { get; set; }
        public virtual DbSet<Models.BuildingMaterial> BuildingMaterials { get; set; }
        public virtual DbSet<Models.BuildingSizeClass> BuildingSizeClasses { get; set; }
        public virtual DbSet<Models.BuildingSubType> BuildingSubTypes { get; set; }
        public virtual DbSet<Models.BuildingType> BuildingTypes { get; set; }
        public virtual DbSet<Models.BuildingValuationClass> BuildingValuationClasses { get; set; }
        public virtual DbSet<Models.ChildProtection> ChildProtections { get; set; }
        public virtual DbSet<Models.CivilStatus> CivilStatusValues { get; set; }
        public virtual DbSet<Models.ConditionCode> ConditionCodes { get; set; }
        public virtual DbSet<Models.Currency> Currencies { get; set; }
        public virtual DbSet<Models.DistanceCode> DistanceCodes { get; set; }
        public virtual DbSet<Models.LocalElectorate> LocalElectorates { get; set; }
        public virtual DbSet<Models.NationalElectorate> NationalElectorates { get; set; }
        public virtual DbSet<Models.ExamType> ExamTypes { get; set; }
        public virtual DbSet<Models.InspectionType> InspectionTypes { get; set; }
        public virtual DbSet<Models.LandOwnership> LandOwnerships { get; set; }
        public virtual DbSet<Models.LandUse> LandUses { get; set; }
        public virtual DbSet<Models.Language> Languages { get; set; }
        public virtual DbSet<Models.LanguageGroup> LanguageGroups { get; set; }
        public virtual DbSet<Models.MaterialType> MaterialTypes { get; set; }
        public virtual DbSet<Models.Nationality> Nationalities { get; set; }
        public virtual DbSet<Models.PayAllowance> PayAllowances { get; set; }
        public virtual DbSet<Models.PNAReason> PNAReasons { get; set; }
        public virtual DbSet<Models.PrimaryTransition> PrimaryTransitions { get; set; }
        public virtual DbSet<Models.PupilTableType> PupilTableTypes { get; set; }
        public virtual DbSet<Models.QualityIssue> QualityIssues { get; set; }
        public virtual DbSet<Models.RandM> RandMs { get; set; }
        public virtual DbSet<Models.Region> Regions { get; set; }
        public virtual DbSet<Models.RoomFunction> RoomFunctions { get; set; }

        public virtual DbSet<Models.RoomType> RoomTypes { get; set; }
        public virtual DbSet<Models.RubbishDisposal> RubbishDisposalMethods { get; set; }
        public virtual DbSet<Models.SalaryLevel> SalaryLevels { get; set; }
        public virtual DbSet<Models.SalaryPoint> SalaryPoints { get; set; }
        public virtual DbSet<Models.SchoolBuildingType> SchoolBuildingTypes { get; set; }
        public virtual DbSet<Models.SchoolClass> SchoolClasses { get; set; }
        public virtual DbSet<Models.SchoolLinkType> SchoolLinkTypes { get; set; }
        public virtual DbSet<Models.SchoolRegistration> SchoolRegistrations { get; set; }
        public virtual DbSet<Models.SecondaryTransition> SecondaryTransitions { get; set; }
        public virtual DbSet<Models.SecondaryTransitionItem> SecondaryTransitionItems { get; set; }
        public virtual DbSet<Models.ServiceType> ServiceTypes { get; set; }
        public virtual DbSet<Models.Shift> Shifts { get; set; }
        public virtual DbSet<Models.SiteHost> SiteHosts { get; set; }
        public virtual DbSet<Models.Subject> Subjects { get; set; }
        public virtual DbSet<Models.TeacherHousingType> TeacherHousingTypes { get; set; }
        public virtual DbSet<Models.TeacherLinkType> TeacherLinkTypes { get; set; }
        public virtual DbSet<Models.TeacherQual> TeacherQuals { get; set; }
        public virtual DbSet<Models.TeacherRole> TeacherRoles { get; set; }
        public virtual DbSet<Models.TeacherStatusValue> TeacherStatusValues { get; set; }
        public virtual DbSet<Models.TeacherTrainingType> TeacherTrainingTypes { get; set; }
        public virtual DbSet<Models.ToiletType> ToiletTypes { get; set; }
        public virtual DbSet<Models.WaterSupplyType> WaterSupplyTypes { get; set; }
        public virtual DbSet<Models.WorkItemProgressValue> WorkItemProgressValues { get; set; }
        public virtual DbSet<Models.WorkItemType> WorkItemTypes { get; set; }
        public virtual DbSet<Models.WorkOrderStatusValue> WorkOrderStatusValues { get; set; }

        // allow to edit navigiations in IdentitiesP via a synonym
        public virtual DbSet<Models.Navigation> Navigations { get; set; }

        #endregion
    }
}
