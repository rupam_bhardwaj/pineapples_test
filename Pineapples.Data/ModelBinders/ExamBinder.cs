﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.Exam>))]
    public class ExamBinder : Softwords.Web.Models.Entity.ModelBinder<Models.Exam>
    {
        public int exID
        {
            get
            {
                return (int)getProp("exID");
            }
            set
            {
                definedProps.Add("exID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.Exam e = null;

            IQueryable<Models.Exam> qry = from exam in cxt.Exams
                                                 where exam.exID == exID
                                        select exam;

            if (qry.Count() == 0)
            {
                throw new RecordNotFoundException(exID);
            }

            e = qry.First();
            // Is concurrency needed?
            //if (Convert.ToBase64String(e.pRowversion.ToArray()) != this.RowVersion)
            //{
            //    // optimistic concurrency error
            //    // we return the most recent version of the record
            //    FromDb(e);
            //    throw new ConcurrencyException(this.definedProps);
            //}
            ToDb(cxt, identity, e);
        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.Exam e = null;

            IQueryable<Models.Exam> qry = from exam in cxt.Exams
                                      where exam.exID == exID
                                      select exam;

            if (qry.Count() != 0)
            {
                throw new DuplicateKeyException(exID);
            }

            e = new Models.Exam();
            cxt.Exams.Add(e);
            ToDb(cxt, identity, e);
        }
    }
}
