﻿using System;
using Newtonsoft.Json;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.Student>))]
    public class StudentBinder : Softwords.Web.Models.Entity.ModelBinder<Models.Student>
    {
        public Guid? stuID
        {
            get
            {
                return getProp("stuID") as Guid?;
            }
            set
            {
                definedProps.Add("stuID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            // currently working directy with Student_ table so we can use the primary key
            Models.Student s = cxt.Students.Find(stuID);

            if (s == null)
            {
                throw new RecordNotFoundException(stuID);
            }
            // to do concurrency?
            //if (Convert.ToBase64String(e.Rowversion.ToArray()) != RowVersion)
            //{
            //    // optimistic concurrency error
            //    // we return the most recent version of the record
            //    FromDb(e);
            //    throw new ConcurrencyException(this.definedProps);
            //}
            ToDb(cxt, identity, s);
        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.Student s = null;

            if (getProp("stuID") != null)
            {
                throw new DuplicateKeyException(ID);
            }

            s = new Models.Student(); ;
            cxt.Students.Add(s);
            ToDb(cxt, identity, s);
        }
    }
}
