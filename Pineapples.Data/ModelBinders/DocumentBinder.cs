﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;


namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.Document>))]
    public class DocumentBinder : Softwords.Web.Models.Entity.ModelBinder<Models.Document>
    {
        public Guid? ID
        {
            get
            {
                return getProp("docID") as Guid?;
            }
            set
            {
                definedProps.Add("docID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            // currently working direct;y with documents_ table, not Dopcuments view
            // s we can use the primary key
            Models.Document e = cxt.Documents.Find(ID);

            if (e == null)
            {
                throw new RecordNotFoundException(ID);
            }
            // to do concurrency?
            //if (Convert.ToBase64String(e.Rowversion.ToArray()) != RowVersion)
            //{
            //    // optimistic concurrency error
            //    // we return the most recent version of the record
            //    FromDb(e);
            //    throw new ConcurrencyException(this.definedProps);
            //}
            ToDb(cxt, identity, e);
        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.Document e = new Models.Document(); ;
            cxt.Documents.Add(e);
            ToDb(cxt, identity, e);



        }
    }
}
