﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;



namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.School>))]
    public class SchoolBinder : Softwords.Web.Models.Entity.ModelBinder<Models.School>
    {
        //public School() : base() { }
        public string schNo
        {
            get
            {
                return getProp("schNo") as string;
            }
            set
            {
                definedProps.Add("schNo", value);
            }
        }

        //public string schVillage { get; set; }

        //public string schAuth { get; set; }

        public decimal? Lat { get; set; }
        public decimal? Lng { get; set; }

        //public string schReg { get; set; }
        //public string schRegStatus { get; set; }
        //public DateTime? schRegStatusDate { get; set; }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            // entity framework offers a slightly simpler way of getting the record compared to linq2sql
            Models.School s = cxt.Schools.Find(schNo);
            
            //IQueryable<Models.School> qry = from sch in cxt.Schools
            //                             where sch.schNo == schNo
            //                             select sch;

            //if (qry.Count() == 0)
            if (s == null)
            {
                throw new RecordNotFoundException(schNo);
            }
            
            if (this.RowVersion != null)
            {
                if (Convert.ToBase64String(s.pRowversion.ToArray()) != this.RowVersion)
                {
                    // optimistic concurrency error
                    // we return the most recent version of the record
                    FromDb(s);
                    throw new ConcurrencyException(this.definedProps);
                }
            }
            ToDb(cxt, identity, s);


        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.School s = null;

            IQueryable<Models.School> qry = from sch in cxt.Schools
                                        where sch.schNo == schNo
                                        select sch;

            if (qry.Count() != 0)
            {
                throw new DuplicateKeyException(schNo);
            }

            s = new Models.School();
            cxt.Schools.Add(s);
            ToDb(cxt, identity, s);


        }
    }
}
