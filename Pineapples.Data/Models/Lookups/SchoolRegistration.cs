using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpSchoolRegistration")]
    public partial class SchoolRegistration : SimpleCodeTable
    {
        [Key]

        [Column("rxCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "rx Code is required")]
        [Display(Name = "rx Code")]
        public override string Code { get; set; }

        [Column("rxDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "rx Description is required")]
        [Display(Name = "rx Description")]
        public override string Description { get; set; }

        [Column("rxDEscriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "rx D Escription L1")]
        public override string DescriptionL1 { get; set; }

        [Column("rxDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "rx Description L2")]
        public override string DescriptionL2 { get; set; }
    }
}
