using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpShifts")]
    public partial class Shift : SequencedCodeTable
    {
        [Key]

        [Column("shiftCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "shift Code is required")]
        [Display(Name = "shift Code")]
        public override string Code { get; set; }

        [Column("shiftName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "shift Name")]
        public override string Description { get; set; }

        [Column("shiftNameL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "shift Name L1")]
        public override string DescriptionL1 { get; set; }

        [Column("shiftNameL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "shift Name L2")]
        public override string DescriptionL2 { get; set; }

        [Column("shiftSeq", TypeName = "int")]
        [Display(Name = "shift Seq")]
        public override int? Seq { get; set; }
    }
}
