using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpTeacherStatus")]
    [Description(@"User configurable code field for Teacher Status.")]
    public partial class TeacherStatusValue : SimpleCodeTable
    {
        [Key]

        [Column("statusCode", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Required(ErrorMessage = "status Code is required")]
        [Display(Name = "status Code")]
        public override string Code { get; set; }

        [Column("statusDesc", TypeName = "nvarchar")]
        [MaxLength(200)]
        [StringLength(200)]
        [Display(Name = "status Desc")]
        public override string Description { get; set; }

        [Column("statusDescL1", TypeName = "nvarchar")]
        [MaxLength(200)]
        [StringLength(200)]
        [Display(Name = "status Desc L1")]
        public override string DescriptionL1 { get; set; }

        [Column("statusDescL2", TypeName = "nvarchar")]
        [MaxLength(200)]
        [StringLength(200)]
        [Display(Name = "status Desc L2")]
        public override string DescriptionL2 { get; set; }
    }
}
