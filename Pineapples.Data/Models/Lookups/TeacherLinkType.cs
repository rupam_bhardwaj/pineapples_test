using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpTeacherLinkTypes")]
    [Description(@"Functions of files that may be linked to a Teacher.")]
    public partial class TeacherLinkType : SimpleCodeTable
    {
        // override on length
        [Key]
        [MaxLength(20)]
        [StringLength(20)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "codeCode")]
        [ForceUpperCase]
        public override string Code { get; set; }
    }
}
