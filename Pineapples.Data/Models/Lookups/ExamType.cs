using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpExamTypes")]
    [Description(@"Type of Exams. A foreign key on Exams.")]
    public partial class ExamType : ChangeTracked
    {
        [Key]

        [Column("exCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "ex Code is required")]
        [Display(Name = "ex Code")]
        public string Code { get; set; }

        [Column("exName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "ex Name")]
        public string ExamName { get; set; }

        [Column("exLevel", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "ex Level")]
        [ClientLookup("levels")]
        public string ExamLevel { get; set; }

        [Column("exFirstOffered", TypeName = "int")]
        [Display(Name = "First Offered")]
        public int? YearFirstOffered { get; set; }

        [Column("exRegion", TypeName = "bit")]
        [Display(Name = "ex Region")]
        public bool? RegionalExam { get; set; }
    }
}
