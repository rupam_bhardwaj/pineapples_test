using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpElectorateN")]
    [Description(@"National Electorate. A foreign key on schools. Also on SchoolSurvey, since boundaries may not be stable over time.")]
    public partial class NationalElectorate : SequencedCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "Code")]
        public override string Code { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Description")]
        public override string Description { get; set; }

        [Column("codeSort", TypeName = "int")]
        [Display(Name = "Seq")]
        public override int? Seq { get; set; }

        [Column("engisID", TypeName = "int")]
        [Display(Name = "Gis ID")]
        public int? engisID { get; set; }
    }
}
