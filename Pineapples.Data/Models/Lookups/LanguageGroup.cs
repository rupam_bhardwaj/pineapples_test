using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpLanguageGroup")]
    [Description(@"Classification of languages. Principally used to group indigeneous or regional languages.")]
    public partial class LanguageGroup : SimpleCodeTable
    {
        [Key]

        [Column("lngGrp", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Required(ErrorMessage = "lng Grp is required")]
        [Display(Name = "lng Grp")]
        public override string Code { get; set; }

        [Column("lngGrpName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "lng Grp Name")]
        public override string Description { get; set; }

        [Column("lngGrpNameL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "lng Grp Name L1")]
        public override string DescriptionL1 { get; set; }

        [Column("lngGrpNameL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "lng Grp Name L2")]
        public override string DescriptionL2 { get; set; }
    }
}
