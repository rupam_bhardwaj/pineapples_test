using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpLandUse")]
    [Description(@"Type of use of land. Foreign key on schools, schoolsurvey. Used in Vanuatu.")]
    public partial class LandUse : SimpleCodeTable { }
}
