﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pineapples.Data.Models
{
    [Table("lkpEducationLevelsAlt")]
    [Description(@"Education Levels represent chronolgical devisions of the education system, based on Year of Edcuation. Each level defines a min and max year of education. A class Level belogns to an Edcuation Level if its Year of Edcuation is between this Min and Max. 
Year of Education 1 is first year of primary.Year 0 is ECE.
Enrolment Ratio reporting is done by Education Level, since the chornoligical dvisions allows the calcuation of the relevant ""official age"" population.")]
    public class EducationLevelAlt : SimpleCodeTable
    {
        [Key]
        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "Code is required")]
        [Display(Name = "Code")]
        public override string Code { get; set; }

        [Column("edlMinYear", TypeName = "int")]
        [Display(Name = "edl Min Year")]
        public int? MinYear { get; set; }

        [Column("edlMaxYear", TypeName = "int")]
        [Display(Name = "edl Max Year")]
        public int? MaxYear { get; set; }
    }


}
