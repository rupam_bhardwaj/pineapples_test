using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpSalaryLevel")]
    [Description(@"Salary level codes in the Civil Service. Salary points further divide these, and Pay rates can be linked to salary points.
A range of salary levels may be associated to any RoleGrade, hence to any establishm,ent position (which must define a RoleGrade).")]
    public partial class SalaryLevel : ChangeTracked
    {
        [Key]

        [Column("salLevel", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Required(ErrorMessage = "sal Level is required")]
        [Display(Name = "sal Level")]
        public string salLevel { get; set; }

        [Column("salLevelDesc", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "sal Level Desc")]
        public string salLevelDesc { get; set; }

        [Column("salDefaultCostPoint", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "sal Default Cost Point")]
        public string salDefaultCostPoint { get; set; }

        [Column("salExternal", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "sal External")]
        public string salExternal { get; set; }
    }
}
