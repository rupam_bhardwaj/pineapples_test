﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pineapples.Data
{
    public class LookupTable
    {
        //public string C { get; set; }
       // public int? ID { get; set; }
        //public string N { get; set; }
        public string codeCode { get; set; }
        public string codeDescription { get; set; }

    }
    public class RowColData
    {
        public string Row;
        public string Col;
        public int? M;
        public int? F;
    }
    public class PupilTable
    {
        public string SchoolNo { get; set; }
        public int Year { get; set; }
        public string TableName { get; set; }
        public LookupTable[] Rows;
        public LookupTable[] Cols;
        public RowColData[] Data;

    }
}
