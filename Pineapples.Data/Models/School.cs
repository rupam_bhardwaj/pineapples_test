﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
    [Table("Schools")]
    public partial class School:ChangeTracked
    {
        [Key]
        [MaxLength(50)]
        [StringLength(50)]
        [Required]
        [System.ComponentModel.Description("Unique identifier for school")]
        public string schNo { get; set; }
        [MaxLength(200)]
        public string schName { get; set; }
        [MaxLength(50)]
        public string schVillage { get; set; }
        [MaxLength(2)]
       
        public string iCode { get; set; }
        [MaxLength(10)]
        public string schType { get; set; }
        [MaxLength(50)]
        [Display(Prompt ="Address Line 1", Description ="address line 1")]
        public string schAddr1 { get; set; }
        [MaxLength(50)]
        [Display(Name="Address Line 2", Prompt = "Address Line 2", Description = "address line 2")]
        public string schAddr2 { get; set; }
        [MaxLength(50)]
        [Display(Prompt = "Address Line 3")]
        public string schAddr3 { get; set; }
        [MaxLength(50)]
        [Display(Description = "Address Line 4")]
        public string schAddr4 { get; set; }
        [MaxLength(50)]
        public string schPh1 { get; set; }
        [MaxLength(50)]
        public string schPh2 { get; set; }
        [MaxLength(50)]
        public string schFax { get; set; }
        [MaxLength(50)]
        public string schEmail { get; set; }
        [MaxLength(150)]
        public string schWWW { get; set; }
        [MaxLength(10)]
        public string schElectN { get; set; }
        [MaxLength(10)]
        public string schElectL { get; set; }
        [MaxLength(10)]
        public string schAuth { get; set; }
        [MaxLength(10)]
        public string schLang { get; set; }
        [MaxLength(10)]
        public string schLandOwner { get; set; }
        [MaxLength(10)]
        public string schLandUse { get; set; }
        public DateTime? schLandUseExpiry { get; set; }
        public short? schEst { get; set; }
        [MaxLength(50)]
        public string schEstBy { get; set; }
        public short? schClosed { get; set; }
        [MaxLength(50)]
        public string schCloseReason { get; set; }
        [MaxLength(10)]
        public string schRegStatus { get; set; }
        [MaxLength(20)]
        public string schReg { get; set; }
        public int? schRegYear { get; set; }
        public DateTime? schRegDate { get; set; }
        public DateTime? schRegStatusDate { get; set; }
        public int? schXtrnID { get; set; }
        [MaxLength(10)]
        public string schClass { get; set; }
        [MaxLength(8)]
        public string schXNo { get; set; }
        [MaxLength(10)]
        public string schpayptCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? schClosedLimit { get; set; }
        public int? schOrgUnitNumber { get; set; }
        [MaxLength(50)]
        public string schGLSalaries { get; set; }
        [MaxLength(50)]
        public string schParent { get; set; }
        [MaxLength(50)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string schEstablishmentPoint { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int schIsExtension { get; set; }
       
        public bool schDormant { get; set; }
        public decimal? schElev { get; set; }
        public decimal? schLat { get; set; }
        public decimal? schLong { get; set; }
    }

}
