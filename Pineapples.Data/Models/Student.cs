﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Pineapples.Data.Models
{
    [Table("Student_")]
    [Description("Each record in Student_ represents a student record present in the EMIS.")]
    public partial class Student
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public Guid? stuID { get; set; }

        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Card ID")]
        public string stuCardID { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Name Prefix")]
        public string stuNamePrefix { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Name")]
        public string stuGiven { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Middle Names")]
        public string stuMiddleNames { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Family Name")]
        public string stuFamilyName { get; set; }

        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Name Suffix")]
        public string stuNameSuffix { get; set; }

        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Given Name Soundex")]
        public string stuGivenSoundex  { get; set; }

        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Family Soundex")]
        public string stuFamilySoundex { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime? stuDoB { get; set; }

        [Display(Name = "Date of Birth Estimate")]
        public bool? stuDoBEst { get; set; }

        [MaxLength(1)]
        [Display(Name = "Gender")]
        public string stuGender { get; set; }

        [MaxLength(200)]
        [StringLength(200)]
        [Display(Name = "Ethnicity")]
        public string stuEthnicity { get; set; }  
        
        [Display(Name = "Create File Reference")]
        public Guid? stuCreateFileRef { get; set; }

        [Display(Name = "Edit File Reference")]
        public Guid? stuEditFileRef { get; set; }
    }
}