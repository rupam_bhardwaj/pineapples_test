﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
  public class PerfAssessFilter:Filter
  {
    public int? PerfAssessID { get; set; }
    public int? TeacherID { get; set; }
    public string ConductedBy { get; set; }
    public string SchoolNo { get; set; }
    public string District { get; set; }
    public string Island { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public string SubScoreCode { get; set; }
    public float? SubScoreMin { get; set; }
    public string SubScoreMax { get; set; }
    public string XmlFilter { get; set; }
  }

  public class PerfAssessTableFilter
  {
    public string row { get; set; }
    public string col { get; set; }
    public PerfAssessFilter filter { get; set;}
  }
}
