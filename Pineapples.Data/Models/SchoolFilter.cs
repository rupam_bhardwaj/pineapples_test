﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;


namespace Pineapples.Data
{   
    public class SchoolFilter : Filter
    {        
        public string SchoolNo { get; set; }
        public string SchoolName { get; set; }
        public int? SearchAlias { get; set; }
        public string AliasSource { get; set; }
        public int? AliasNot { get; set; }
        public string SchoolType { get; set; }
        public string SchoolClass { get; set; }
        public string District { get; set; }
        public string Island { get; set; }
        public string ElectorateN { get; set; }
        public string ElectorateL { get; set; }
        public string Authority { get; set; }
        public string Language { get; set; }
        public string InfrastructureClass { get; set; }
        public string InfrastructureSize { get; set; }
        public string NearestShipping { get; set; }
        public string NearestBank { get; set; }
        public string NearestPo { get; set; }
        public string NearestAirstrip { get; set; }
        public string NearestClinic { get; set; }
        public string Nearesthospital { get; set; }
        public int? SearchIsExtension { get; set; }
        public string PArentSchool { get; set; }
        public int? SEarchHasExtension { get; set; }
        public int? ExtensionReferenceYear { get; set; }
        public int? YearEstablished { get; set; }
        public string RegistrationStatus { get; set; }
        public int? SearchRegNo { get; set; }
        public DateTime? CreatedAfter { get; set; }
        public DateTime? EditedAfter { get; set; }
        public int? IncludeClosed { get; set; }
        public string ListName { get; set; }
        public string ListValue { get; set; }
        public XDocument XmlFilter { get; set; }

        public void ApplyUserFilter(ClaimsIdentity identity)
        {
            Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
            if (c != null)
            {
                District = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterAuthority");
            if (c != null)
            {
                Authority = c.Value;
            }  
        }

    }
}