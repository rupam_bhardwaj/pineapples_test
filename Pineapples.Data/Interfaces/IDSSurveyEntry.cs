﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;

namespace Pineapples.Data
{
    public interface IDSSurveyEntry
    {
        IDataResult PupilTable(string schoolNo, int year, string tableName);

        IDataResult SavePupilTable(PupilTable pupilTable, string user);


        object ResourceList(string schoolNo, int year, string category);

        IDataResult SaveResourceList(ResourceList resourceList, string user);

        /// <summary>
        /// Ndoe interfaces are for upload from the FSM excel based data collection tools
        /// </summary>
        /// <param name="listObject">an Xml object derived from the source Excel list</param>
        /// <param name="fileReference">guid representing the source file in the FileDB</param>
        /// <param name="user">user making the upgrade</param>
        /// <returns></returns>
        Task<IDataResult> NdoeSchoolsAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> NdoeStudentsAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> NdoeStaffAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> NdoeWashAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> NdoeValidateSchoolsAsync(XDocument schoolList);

    }
}
