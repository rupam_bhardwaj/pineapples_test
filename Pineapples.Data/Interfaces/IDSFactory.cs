﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pineapples.Data.DataLayer
{
    public interface IDSFactory
    {
        Pineapples.Data.DB.PineapplesEfContext Context { get; }
        IDSPerfAssess PerfAssess();
        IDSSchool School();
        IDSSchoolLink SchoolLink();
        IDSLookups Lookups();
        IDSPivoter Pivoter();
        IDSSchoolScatter SchoolScatter();
        IDSTeacher Teacher();
        IDSTeacherLink TeacherLink();
        IDSWorkforce Workforce();
        IDSExam Exam();
        IDSSelectors Selectors();
        IDSStudent Student();
        IDSSurvey Survey();
        IDSSurveyEntry SurveyEntry();
        IDSBooks Books();
        IDSQuarterlyReport QuarterlyReport();
        IDSSchoolAccreditation SchoolAccreditation();
        IDSIndicators Indicators();
        IDSDocument Document();
        IDSKobo Kobo();
    }
}
