﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Xml.Linq;

namespace Pineapples.Data
{
    public interface IDSKobo
    {
        Task<IDataResult> KoboValidateAsync(XDocument listObject, Guid fileReference, string user);
        Task<IDataResult> KoboAsync(XDocument listObject, Guid fileReference, string user);
    }
}
