﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
    public interface IDSSchool : IDSCrud<SchoolBinder, string>
    {
        IDataResult Filter(SchoolFilter fltr);
        IDataResult Table(string rowsplit, string colsplit, SchoolFilter fltr);
        IDataResult Geo(string geoType, SchoolFilter fltr);
    }
}
