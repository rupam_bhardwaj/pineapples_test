﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSQuarterlyReport : IDSCrud<QuarterlyReportBinder, int>
    {
        IDataResult Filter(QuarterlyReportFilter fltr);
    }
}
