// Type definitions for angular-permission 2.3.6
// Project: https://github.com/Narzerus/angular-permission
// Definitions by: Voislav Mishevski <https://github.com/vmishevski>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference path="../angularjs/angular.d.ts" />

declare namespace angularFileUpload {

  interface FileItem {
    /** Path on the server in which this file will be uploaded */
    url: string;
    /** Name of the field which will contain the file, default is file */
    alias: string;
    /** Headers to be sent along with this file.HTML5 browsers only. */
    headers: any;
    /** Data to be sent along with this file */
    formData: any[];
    /** request method. By default POST. HTML5 browsers only. */
    method: string;
    /** enable CORS.HTML5 browsers only. */
    withCredentials: boolean;
    /** Remove this file from the queue after uploading */
    removeAfterUpload: boolean;
    /** A sequence number upload.Read only. */
    index: number;
    /** File upload progress percentage.Read only. */
    progress: number;
    /** File is ready to upload.Read only. */
    isReady: boolean;
    /** true if the file was uploaded.Read only. */
    isUploaded: boolean;
    /** true if the file was uploaded successfully.Read only. */
    isSuccess: boolean;
    /** true if uploading was canceled.Read only. */
    isCancel: boolean;
    /** true if occurred error while file uploading.Read only. */
    isError: boolean;
    /** Reference to the parent Uploader object for this file.Read only. */
    uploader: any;

    /** Remove this file from the queue */
    remove();
    /** Upload this file */
    upload();
    /** Cancels uploading of this file */
    cancel();

  }

  interface UploaderContructor {
    new (): FileUploader;
  }
  /** the FileUploader service */
  interface FileUploader {
    /** Path on the server in which files will be uploaded */
    url: string;
    /** Name of the field which will contain the file, default is file */
    alias: string;
    /** Headers to be sent along with the files .HTML5 browsers only. */
    headers: any;
    /** Data to be sent along with the files */
    formData: any[];
    /** request method. By default POST. HTML5 browsers only. */
    method: string;
    /**  Items to be uploaded */
    queue: FileItem[];
    /** Upload progress percentage.Read only. */
    progress: number;
    /** Filters to be applied to the files before adding them to the queue.If the filter returns true the file will be added to the queue */
    filters: any[];
    /** Automatically upload files after adding them to the queue */
    autoUpload: boolean;
    /** Remove files from the queue after uploading */
    removeAfterUpload: boolean;
    /** true if uploader is html5- uploader.Read only. */
    isHTML5: boolean;
    /** true if a file is being uploaded.Read only. */
    isUploading: boolean;
    /** maximum count of files */
    queueLimit: number;
    /** enable CORS.HTML5 browsers only. */
    withCredentials: boolean;

    /**
     * Methods
     */

    uploadAll(): void;
    removeFromQueue(item: FileItem);

    /** 
     * Callbacks
     */

    /**  Fires after adding a single file to the queue. */
    onAfterAddingFile: (item: FileItem) => void;
    /** When adding a file failed. */
    onWhenAddingFileFailed: (item: FileItem, filter, options) => void;
    /** Fires after adding all the dragged or selected files to the queue. */
    onAfterAddingAll: (addedItems: FileItem[]) => void;
    /** Fires before uploading an item. */
    onBeforeUploadItem: (item: FileItem) => void;
    /** On file upload progress. */
    onProgressItem: (item: FileItem, progress) => void;
    /**On file successfully uploaded */
    onSuccessItem: (item: FileItem, response, status, headers) => void;
    /** On upload error  */
    onErrorItem: (item: FileItem, response, status, headers) => void;
    /**  On cancel uploading */
    onCancelItem: (item: FileItem, response, status, headers) => void;
    /**On file upload complete (independently of the sucess of the operation) */
    onCompleteItem: (item: FileItem, response, status, headers) => void;
    /**On upload queue progress */
    onProgressAll: (progress) => void;
    /**On all loaded when uploading an entire queue, or on file loaded when uploading a single independent file */
    onCompleteAll: () => void;
  }

}
