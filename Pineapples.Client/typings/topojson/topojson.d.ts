﻿declare module topojson {

    export interface topojsonObject {
 
    }
   export function feature(topology: any, object: topojsonObject): GeoJSON.Feature | GeoJSON.FeatureCollection;
   export function merge(topology: any, objects: any): any;
   export function mergeArcs(topology: any, objects: any): any;
   export function mesh(topology: any, object: any, filter?: any): any;
   export function meshArcs(topology: any, object?: any, filter?: any): any;
   export function neighbors(objects: any): any;
   export function presimplify(topojson: any, triangleArea?: any): any;
}