﻿// holdes some standard stuff applicable to any filter model
module Sw.Filter {
  export class Request {
    public static search = "search";
    public static table = "table";
    public static geo = "geo";
  }
  export interface IViewMode {
    key: string; // unique string id for viewmode, displayed in the dropdown list
    columnSet: number;  // columnSet parameter passed to the database
    gridClass: string;  // to apply to ui-grid
    gridOptions: any;
    media: string;
    editable: boolean | string[];   // simple case true or false ,otherwise an array of angular-permission roles
    defaultCellEditOnFocus: boolean;
    defaultAllowCellFocus: boolean;

    includeTrackingColumns?: boolean;

  }

  export interface IFilterParamManager {

    toFlashString(params: any): string;
    fromFlashString(flashstring: string): any;
  }

  export class FilterParamManager implements IFilterParamManager {
    constructor(protected lookups: Sw.Lookups.LookupService) { };

    public toFlashString(params: any) {
      return "";
    }

    public fromFlashString(flashstring: string) {
      // no-op - subclasses must provide
    }

    protected tokenise(str) {
      return Sw.Utils.splitargs(str);
    }

    private detokenise(arr: string[]): string {
      var out = arr.map(function (token) {
        if (token.indexOf("'") >= 0) {
          return '"' + token + '"';
        }
        if (token.indexOf('"') >= 0) {
          return "'" + token + "'";
        }
        if (token.indexOf(' ') >= 0) {
          return "'" + token + "'";
        }
        return token;

      });
      return out.join(" ");
    }
    // helper routine to buid the flash string from an array of param names
    // this array should correspond to those parsed in FlashFind,
    // and they should be in the same order
    protected flashStringBuilder(params: any, pp: any[]) {

      var parsed = [];

      pp.forEach((p) => {
        if (params[p]) {
          parsed.push(this.getParamString(p, params[p]));
        }
      });

      return this.detokenise(parsed);
    }
    // routine to get the flashfind string representing a parameter
    // normally this is just the identity
    // however for some cases (eg. when an Id is passed into the parameter)
    // it may need to be a string
    protected getParamString(name, value) {
      return value.toString();
    }
    protected cacheFind(s: string, params: any, cachename: string, paramname: string, identifier?: string) {
      let table: Sw.Lookups.ILookupList = this.lookups.cache[cachename];
      if (table) {
        let lkp = table.search(s);
        if (lkp) {
          if (!params[paramname]) {
            params[paramname] = lkp[identifier || "C"];
          }
          return true;
        }
      }
      return false;
    }
  }

  export class TableParams {
    constructor(public row?: string, public col?: string, public dataItem?: string) {
      if (dataItem === undefined) {
        this.dataItem = "total";
      }
    }
    public chartType: string;       // TO DO not really happy with this - one of these things is not like the others
    // one of these things just doesn't belong....
    // for now its just a convenient place to transmit this between views
    public flipped: boolean = false;

    public flip(): void {
      this.flipped = !(this.flipped);
    };

  }

  export class PagingParams {

    public pageNo: number = 1;
    public pageSize: number = 50;
    public sortColumn: string;
    public sortDirection: string;
    public columnSet: number = 1;
  }

  export class FindParams {
    constructor() {
      this.paging = new PagingParams();
      this.table = new TableParams();
      this.filter = {};
      this.geo = {};
    }

    public filter: any;
    public paging: PagingParams;
    public table: TableParams;
    public geo: any; // GeoParams
    public flashString: string;
    public viewMode: string;        // the current view mode;

    public filterPagedParams() {
      let params = angular.copy(this.filter);
      angular.extend(params, this.paging);
      return params;
    }
  }
  /**
   *
   * represents the configuration options for a finder  ecosystem
   * this includes the core filter co-ordinating activities between various
   * views (ie controllers) responsible for specifying the parameters of the
   * search and/or rendering the results
   * a findConfig can be returned by the filter, wrapped in a promise
   */
  export class FindConfig {
    // provides an identifier for a set of components - generally will be associated to all
    // components belonging to a route
    constructor() {
      this.current = new FindParams();
      this.defaults = new FindParams();
      this.identity = new FindParams();

    }
    public id: string;
    // lookup list for table and chart dropdowns
    public tableOptions: string;
    // lookup list for data values in table and maps
    public dataOptions: string = "appDataOptions";

    // array of view modes that are allowed
    public allowedViews: string[] = [];
    // defaults for all FindParam options
    public current: FindParams;

    // defaults for all FindParam options - suppplied by the view
    public defaults: FindParams;
    // locked values determined by the user identity - calculated by the filter
    public identity: FindParams;

    // returns true if the parameter specified by name is locked
    public isLocked(param: string) {
      if (this.identity.filter.hasOwnProperty(param)) {
        return true;
      }
      return false;
    }

    public reset() {
      // copy the default across the current
      angular.copy(this.defaults.filter, this.current.filter);
      angular.extend(this.current.filter, this.identity.filter);
    }

    // adds a set of feilds as defaults. These may be defaults used in a particular view
    public setDefaultFilter(defaults: any) {
      this.defaults.filter = defaults;
    }
    // indicates that a search has been done with this config, so on changin page, the new page
    // may want to immediately render without waiting for FindNow
    public prepared: boolean = true;
  }

  export class FindActions {
    public search: boolean;
    public table: boolean;
    public geo: boolean;
  }

  export interface IFindNowBroadcast {
    entity: string;
    findParams: FindParams;
    actions: string[];
    filter: IFilter;
  }

  export interface IFilter {

    createFindConfig(viewDefauts: any): ng.IPromise<FindConfig>;
    createTableCalculator(): Sw.ITableCalculator;
    FindNow(findParams: FindParams): void;
    FindNow(findParams: FindParams, request: Sw.Filter.Request): void;
    // flashfind support
    FlashFind(findParams: FindParams): void;

    TransformData(resultpack: any, method?: string): any;

    // requestSearch(): void;
    // requestTable(): void;
    // requestGeo(): void;
    // addRequest(request: string): void;
    // hasRequest(request: string): boolean;

    // params: any;
    // tableparams: TableParams;
    ParamManager: IFilterParamManager;

    ShowPage(findParams: FindParams, newPage?: number, newPageSize?: number): void;
    SortOn(findParams: FindParams, sortField?: string, sortDirection?: string): void;

    entity: string;
    // view mode
    // array of all ViewModes
    ViewModes: IViewMode[];
    // return array of all view mode names
    AllViewModes(): string[];
    // add more view modes
    PushViewModes(modes: IViewMode[]): void;
    GetViewMode(modeName: string): IViewMode;

    ViewDefaults: any;
    Action(actionName: string, columnField: string, rowData: any, baseState: string): void;
  }

  export interface IFilterCached extends IFilter {
    cacheResult: any;
    findNowLocal(localParams: any): void;
  }

  export class Filter implements IFilter {

    protected requests: string[] = [];
    // injectables
    protected $rootScope: ng.IRootScopeService;
    protected $state: ng.ui.IStateService;
    protected $q: ng.IQService;
    protected api: any;
    protected identity: Sw.Auth.IIdentity;

    protected searchApiMethod: string = "filterPaged";

    public ParamManager: FilterParamManager;
    public entity = "";

    public ViewModes: IViewMode[] = [];
    public ViewDefaults: any;

    public FindNow(findParams: FindParams, action?: Request): void {
      // return any identity based filter
      angular.extend(findParams.filter, this.identityFilter());

      findParams.flashString = this.ParamManager.toFlashString(findParams.filter);
      let actions = [];
      if (action) {
        actions.push(action);
      }
      this.$rootScope.$broadcast('FindNow', { entity: this.entity, findParams: findParams, actions: actions, filter: this });

      actions.forEach(request => {
        switch (request) {
          case Request.search:
            this.Search(findParams);
            break;
          case Request.table:
            this.Table(findParams);
            break;
          case Request.geo:
            this.Geo(findParams);
            break;
        }
      });
      this.requests = [];
    }

    public Search(findParams: FindParams): void {
      // determime the columnset from the view mode
      let vm = this.GetViewMode(findParams.viewMode);
      if (!vm) {
        vm = this.ViewModes[0];
        findParams.viewMode = vm.key;
      }
      if (vm && vm.columnSet) {
        findParams.paging.columnSet = vm.columnSet;
      } else {
        findParams.paging.columnSet = this.ViewDefaults.columnSet;
      }
      var promise = this.searchPromise(this.api, findParams.filterPagedParams());
      this.handlePromise(promise, Request.search);
    }
    public Geo(findParams: FindParams): void {
      console.log("geo");
      var promise = this.apiPromise(this.api, 'geo', findParams.filter);
      this.handlePromise(promise, Request.geo);
    }
    public Table(findParams: FindParams): void {
      console.log("table");

      var params = { row: findParams.table.row, col: findParams.table.col, dataItem: findParams.table.dataItem, filter: findParams.filter };
      var promise = this.apiPromise(this.api, 'table', params);
      this.handlePromise(promise, Request.table);
    }

    // FlashFind suport

    public FlashFind(findParams: FindParams) {
      // contstruct  he parameters from the flashstring
      // let params = this.ParamManager.fromFlashString(this.FlashString);
      let params = this.ParamManager.fromFlashString(findParams.flashString);
      angular.copy(params, findParams.filter);
      this.FindNow(findParams);
    }

    public ShowPage(params: FindParams, newPage?: number, newPageSize?: number): void {
      params.paging.pageNo = (newPage || 1);
      params.paging.pageSize = (newPageSize || params.paging.pageSize);
      this.FindNow(params);
    }

    public SortOn(params: FindParams, sortField?: string, sortDirection?: string): void {
      params.paging.sortColumn = (sortField || params.paging.sortColumn);      ///TO DO
      params.paging.sortDirection = (sortDirection || 'asc');
      this.ShowPage(params, 1);
    }

    public createFindConfig(viewDefaults: any) {
      let d = this.$q.defer();
      let config = new FindConfig();
      if (viewDefaults) {
        angular.extend(config.defaults.filter, viewDefaults.defaults);
        angular.extend(config.identity.filter, viewDefaults.locked);
      }
      config.reset();
      d.resolve(config);
      return d.promise;
    }

    // stub - only required if we want to use Chart and Table for this object
    public createTableCalculator() {
      return new TableCalculator();
    }

    protected identityFilter() {
      return {};
    }
    // Implementation
    // general method to construct a promise

    // Promise handling
    // -----------------------------------------------------------------
    private apiPromise(api, apimethod, params) {
      var promise: ng.IPromise<any> = api[apimethod](params);
      //     promise.method = apimethod;     // note this is a default, the caller may override
      return this.catchPromise(promise, this.entity);
    }

    // method to return the promise
    private searchPromise(api, params: any) {
      var apimethod = this.searchApiMethod;
      var promise = this.apiPromise(api, apimethod, params);
      return promise;
    }

    // add a standard error reporting catch to the promise
    // * no dependency on 'this'
    private catchPromise(promise, entity) {
      promise
        .catch(
        (e) => {
          this.$rootScope.$broadcast('SearchError', { entity: entity, error: e });
        });
      return promise;
    }

    protected broadcastData(data) {
      this.$rootScope.$broadcast('SearchComplete', data);
    }

    private handlePromise(promise: ng.IPromise<any>, method: string) {
      promise.then(
        // return pack is an object with
        // data, headers, config, status and statustext
        (resultPack) => {

          // processing of the recevied data before sending
          var data = this.TransformData(resultPack, method);

          this.broadcastData(data);
        }
      );
    }

    public TransformData(resultPack: any, method) {
      var summary = {
        numResults: resultPack.NumResults
        , firstRec: resultPack.FirstRec
        , lastRec: resultPack.LastRec
        , pageNo: resultPack.PageNo
        , pageSize: resultPack.PageSize
      };
      // renderOptions added for maps only
      // resultset: resultPack.ResultSet||resultPack is to evolve into restangular format
      return { entity: this.entity, method: method, resultset: resultPack.ResultSet || resultPack, summary: summary, renderOptions: { map: null } };
    }

    // ------------------------------------------------------------
    // View Mode management
    // ------------------------------------------------------------

    // pushViewModes
    // aceepts an array of viewModes and adds those to the current set,
    // overriding any that already exist

    public PushViewModes(modes: IViewMode[]) {
      var self = this;
      modes.forEach(function (mode) {
        _.remove(self.ViewModes, { key: mode.key, media: mode.media });
        self.ViewModes.push(mode);
      });
    }

    public GetViewMode(modeName: string): IViewMode {
      var vm: IViewMode = _.find(this.ViewModes, (viewMode) => (viewMode.key === modeName));
      return vm;
    };

    public AllViewModes(): string[] {
      return this.ViewModes.map(mode => mode.key);
    }

    // actions
    public Action(actionName, columnField, rowData, baseState) {
      var newstate = baseState + '.' + actionName;
      this.$state.go(newstate, { id: rowData[columnField], rowData: rowData, columnField: columnField });
    };
  }

  // a cached filter works with a single set of data,
  // and allows filters of that static data, rather than
  // requeries
  export class CachedFilter extends Filter implements IFilterCached {
    public cacheResult: any;     // a filter may cache a single result

    protected filterCache() {
      return this.cacheResult;
    }

    public findNowLocal(localParams: any) {
      let filteredData = _.filter(this.cacheResult.resultset, localParams);
      let result = angular.copy(this.cacheResult);
      result.resultSet = filteredData;
      this.broadcastData(result);
    }

    public TransformData(data, method) {
      this.cacheResult = super.TransformData(data, method);
      return this.cacheResult;
    }
  }
}
