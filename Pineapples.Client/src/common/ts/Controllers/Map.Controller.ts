﻿/*
--------------------------------------------------------------------
Application Map Controller

manages the display of a map of applications
--------------------------------------------------------------------
*/
module Sw.Maps {

  class MapController {

    private Ready = false;
    private colorBy = '';     // defaults to gender
    private spiderfy: boolean;
    private cluster: boolean;
    private enableKeyDragZoom: boolean;
    private selectedBox: any;
    private map: any;       // the google map

    public isWaiting = false;

    private currentMarkers: google.maps.Marker[] = [];
    private pointData: any[];

    private spiderfier: OverlappingMarkerSpiderfier;
    private markerClusterer: MarkerClusterer;

    public zoomSelector: any; // selected zoom to...

    private colorSelectors = {
      status: d3.scale.ordinal()
        .domain(['Unsucessful'])
    };

    static $inject = ["$scope", "$rootScope", "findConfig", "theFilter", "mapView"];
    constructor($scope, $rootScope, public findConfig, public theFilter, private helper) {

      this.selectedBox = {};
      this.spiderfy = helper.Spiderfy || true;
      this.cluster = helper.Cluster || true;
      this.enableKeyDragZoom = helper.EnableKeyDragZoom || true;

      this.map = null;

      // the FindNow button got pushed - we need to redraw the chart
      $scope.$on('FindNow', (event, data) => {
        if (data.filter.entity === this.theFilter.entity) {
          this.isWaiting = true;
          data.actions.push("geo");
        }

      });

      $scope.$on('SearchComplete', (event, resultpack) => {
        if (resultpack.entity === this.theFilter.entity && resultpack.method === "geo") {
          // the first recordset is the data - thgis is used to create the markers
          this.pointData = resultpack.resultset[0];
          // second recordset is the bounding box
          var markerBoundingBox = resultpack.resultset[1][0];
          markerBoundingBox.couName = "(all markers)";
          if (!this.selectedBox.couName) {
            this.selectedBox = markerBoundingBox;
          }
          this.Ready = true;      // not needed??
          this.redraw();          // this is where all the markers are drawn
          this.isWaiting = false;
        };
      });
      $scope.$on('SearchError', (event, resultpack) => {
        if (resultpack.entity === this.theFilter.entity) {
          // no point waiting around - this train aint comin'
          this.isWaiting = false;
        }
      });

      $scope.$on("MapChangeBox", (event, args) => {
        if (this.theFilter.entity === args.entity) {
          if (this.selectedBox !== args.box) {
            this.selectedBox = args.box;
            this.fitBox();
          };
        }
      });

      $scope.$on("MapChangeCluster", (event, args) => {
        if (this.theFilter.entity === args.entity) {
          if (this.cluster !== args.cluster) {
            this.cluster = args.cluster;
            this.redraw();
          };
        }
      });

      // using ng-map, this is how we get the goolge map object back to the controller
      $scope.$on('mapInitialized', (event, map) => {
        this.map = map;

        if (this.enableKeyDragZoom) {
         
          Sw.Maps.enableKeyDragZoom(map,{
            visualEnabled: true,
            visualPosition: google.maps.ControlPosition.LEFT_TOP,
            veilStyle: {
              webkitUserSelect: "none"
            }
          });
        }
        this.redraw();
      });

      theFilter.FindNow(findConfig.current);
    }

    private fitBox(box?: any) {
      if (!box) {
        box = this.selectedBox;
      }
      if (box && this.map) {
        if (box.lat !== undefined && box.lng !== undefined) {
          // its a point to zoom to check this before checking box
          var center = new google.maps.LatLng(box.lat, box.lng);
          // using global variable:
          this.map.panTo(center);
          if (box.zoom) {
            this.map.setZoom(box.zoom);
          };
        };
        if (box.n) {
          this.map.fitBounds(this.makeLatLngBounds(box));
          return;
        }
      };
    };

    private redraw() {
      if (this.map) {
        if (this.markerClusterer) {
          this.markerClusterer.clearMarkers();
        } else {
          this.currentMarkers.forEach(marker => {
            marker.setMap(null);
          });
        };
        this.currentMarkers = [];
        this.fitBox();
        // if clsutering, defer add marker to map when creating marker
        var maptarget = (this.cluster ? null : this.map);

        if (this.pointData) {
          var markers = [];

          this.pointData.forEach(row => {
            // <marker ng-repeat="row in mapc.pointData" position="{{row.sLat}},{{row.sLng}}" title="{{row.sFullName}}"></marker>

            // add the markers to the markers array - if there is no clustering, they are at the same time added to the map
            var marker = this.helper.MakeMarker(row, maptarget);
            markers.push(marker);
            if (!this.spiderfy) {
              // only set up the click events if spiderfy is not used
              // otherwise, let the spiderfier handle the clicks
              var f = this.helper.OnMarkerClick(row, marker, maptarget);
              if (f) {
                google.maps.event.addListener(marker, 'click', f);
              }
            }
          });

          // spiderfy, no cluster
          if (this.spiderfy) {

            if (this.spiderfier) {
              this.spiderfier.clearMarkers();
            } else {
              this.spiderfier = this.setSpiderfier(this.map);
            }
            // if not clustering, add these to the map straight away
            markers.forEach(mark => {
              this.spiderfier.addMarker(mark);
            });
            // add the click listener
            this.spiderfier.addListener('click', this.helper.OnSpiderClick(this.spiderfier, this.map));
          };
          if (this.cluster) {
            this.markerClusterer = this.setClusterer(this.map, markers);
          } else {
            this.markerClusterer = null;
          }

          this.currentMarkers = markers;
        };
      };
    };

    private setClusterer(map: google.maps.Map, markers: google.maps.Marker[]) {
      var zoom = 8;
      var mcOptions = { gridSize: 10, maxZoom: zoom, zoomOnClick: false };

      return new MarkerClusterer(map, markers, mcOptions);
    };

    private setSpiderfier(map: google.maps.Map) {

      // as written oms requires google maps when compiled - this "lazy loads"
      var oms = attachSpiderfier(this.map,
        {
          markersWontMove: true,
          markersWontHide: true,
          keepSpiderfied: true
        });
      oms.addListener('click', mark => {
        // iw.setContent(marker.desc);
        // iw.open(map, marker);
        // var data = markers[mark.idx];
        // document.title = "<sID>" + data.id + "</sID>";
      });
      oms.addListener('spiderfy', marks => {
        marks.forEach(mark => {
          let row = mark.row;
          mark.setIcon(this.helper.RowIcon(row, true));
          mark.title = row.sFullName;
        });
      });

      oms.addListener('unspiderfy', marks => {
        marks.forEach(mark => {
          var row = mark.row;
          mark.setIcon(this.helper.RowIcon(row, false));
          mark.title = (row.coloc > 1 ? row.coloc + ' people here...' : row.sFullName);
        });
      });
      return oms;
    };

    private makeLatLngBounds(nsew: google.maps.ext.NSEW) {
      if (nsew.n === undefined) {
        // TO DO
      }
      return this.makeLatLngBoundsNSEW(nsew.n, nsew.s, nsew.e, nsew.w);
    };

    private makeLatLngBoundsNSEW(n, s, e, w) {
      var bnds = new google.maps.LatLngBounds();
      var SW = new google.maps.LatLng(s, w);
      var NE = new google.maps.LatLng(n, e);
      bnds.extend(SW);
      bnds.extend(NE);
      return bnds;
    };

    private applyBoundsCountry(couName: string) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': couName }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          this.map.fitBounds(results[0].geometry.viewport);
        }
      });
    };
  };

  angular
    .module('sw.common')
    .controller('MapController', MapController);
}
