﻿module Sw.Auth {
  class LoginController {
    public ModelState: any;
    static $inject = ["$scope", "$state", "authenticationService", "GlobalSettings"];
    constructor(private _scope, private _state: ng.ui.IStateService, private _auth: Sw.Auth.IAuthenticationService, global) {
      global.set('fullscreen', true);
      _scope.$on('$destroy', function () {
        global.set('fullscreen', false);
      });
      _scope.messageText = '';
      _scope.rememberMe = false;
      _scope.loginState = "Log In";
      _scope.loginInProcess = false;
    }
    // control fullscreen on / off

    public logIn = function (user, password, persist) {
      this._scope.messageText = '';
      this._scope.loginState = 'Logging in...';
      this._scope.loginInProcess = true;
      this._auth.login(user, password, persist)
        .then(() => {
          this._scope.loginState = "Successful...please wait";
          this._scope.loginInProcess = false;
          if (this._scope.returnToState) {
            this._state.go(this._scope.returnToState.name, this._scope.returnToStateParams);
          } else {
            this._state.go('site.home');
          }
        },
        (response) => {
          this._scope.messageText = 'Login failed';
          this._scope.loginState = "Log In";
          this._scope.loginInProcess = false;
        });
    };

    public logOut = function () {
      this._auth.logOut();
      this._state.go("signin");
    };

    public changePassword(oldPassword: string, newPassword: string, confirmPassword: string) {
      this._scope.messageText = '';
      this._scope.loginState = 'processing...';
      this._auth.changePassword(oldPassword, newPassword, confirmPassword)
        .then(() => {
          this._scope.loginState = "PasswordChanged";
          if (this._scope.returnToState) {
            this._state.go(this._scope.returnToState.name, this._scope.returnToStateParams);
          } else {
            this._state.go('site.home');
          }
        },
        (response) => {
          this._scope.messageText = 'Change Password failed';
          this.ModelState = response.data.ModelState;
          this._scope.loginState = "Log In";
        });
    }

    public register(userName: string, email: string, password: string, confirmPassword: string, menuKey: string, country: string, permission: string) {
      this._scope.messageText = '';
      this._scope.loginState = 'processing...';
      this._auth.register(userName, email, password, confirmPassword, menuKey, country, permission)
        .then(() => {
          this._scope.loginState = "User Registered";
        },
        (response) => {
          this._scope.messageText = 'Register failed';
          this.ModelState = response.data.ModelState;
          this._scope.loginState = "Log In";
        });
    }
  }

  /**
   * This variation of the controller manages the popup login invoked from the http interceptor
   * on 401 errors. it does not attempt to redirect, it simply allows the dialog to be closed on successful authentication.
   *
   */
  class LoginPopupController {
    static $inject = ["$scope", "$state", "authenticationService", "$mdDialog"];
    constructor(private _scope, private _state: ng.ui.IStateService, private _auth: Sw.Auth.IAuthenticationService, private _mdDialog: ng.material.IDialogService) {
      _scope.messageText = '';
      _scope.rememberMe = false;
      _scope.loginState = "Log In";
      _scope.loginInProcess = false;
    }
    // control fullscreen on / off

    public logIn = (user, password, persist) => {
      this._scope.messageText = '';
      this._scope.loginState = 'Logging in...';
      this._scope.loginInProcess = true;
      this._auth.login(user, password, persist)
        .then(() => {
          this._scope.loginState = "Successful...please wait";
          this._scope.loginInProcess = false;
          this._mdDialog.hide();
        },
        (response) => {
          this._scope.messageText = 'Login failed';
          this._scope.loginState = "Log In";
          this._scope.loginInProcess = false;
        });
    };
  }

  angular
    .module("sw.common")
    .controller("LoginController", LoginController)
    .controller("LoginPopupController", LoginPopupController);
}
