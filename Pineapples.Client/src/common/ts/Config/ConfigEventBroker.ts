﻿// http://simplyaprogrammer.com/2015/05/angular-message-bus-and-smart-watch.html
module Sw {

  let decorator = delegate => {
    var rootScope: ng.IRootScopeService = delegate;

    Object.defineProperty(rootScope.constructor.prototype, '$bus', {
      get: function () {
        return {
          publish: (msg, data, sender) => {
            data = data || {};
            // emit goes to parents, broadcast goes down to children
            // since rootScope has no parents, this is the least noisy approach
            // however, with the caveat mentioned below
            rootScope.$emit(msg, data, sender);
          },
          subscribe: (msg, func) => {
            // ignore the event.  Just want the data
            var unbind = rootScope.$on(msg, (event, data, sender) => func(data, sender));
            // being able to enforce unbinding here is why decorating rootscope
            // is preferred over DI of an explicit bus service
            this.$on('$destroy', unbind);
          }
        };
      }
    });
    return delegate;
  };
  function eventBroker($provide: any) {
    $provide.decorator("$rootScope", ["$delegate", decorator]);
  };

  angular
    .module("sw.common")
    .config(["$provide", eventBroker]);
}
