﻿namespace Sw.Config {

  class Controller  {
    public states: Array<ng.ui.IState>;

    static $inject = ["$state"]
    constructor(public state: ng.ui.IStateService) {
      this.states = state.get();
    }
    public $onChanges(changes) {
    }
  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        log: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "common/stateaudit";
    }
  }

  function routes(stateProvider: ng.ui.IStateProvider) {
    let state = {
        url: "^/stateaudit",
        views: <{ [name: string]: ng.ui.IState }>{
          "@": {
            template: `<component-state-audit></component-state-audit>`
          }
        }
    }
    stateProvider.state("site.stateaudit", state);
  }

  angular
    .module("sw.common")
    .component("componentStateAudit", new Component())
    .config(["$stateProvider", routes]);


}
