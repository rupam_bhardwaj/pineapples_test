﻿module Sw.Maps {
  // colorRangeDesigner displays a sorted bar chart of values that will be used
  // in a choropleth. The user may drag guidelines on this chart to specify
  // the thresholds used for determining the color bands in the choropleth.
  // args -  the threshol
  // data - a data array the data array to use as a sample. This will be the same data fed to the choropleth
  // palette - an IColorBrewerPalette specifies the number of bands of colors to use
  // mapColorScale - an IMapColorScale that is designed by this tool
  // hiliteValue - key value for the highlighted point
  // yField - property for the y value
  // yLabel - label for the y values

  // define the IsolateScope of the directive
  // this will include those members bound to the directive attributes
  // and any other values we may want to place on scope
  // we can use the generic name IIsolateScope becuase it is not exported outside this closure
  interface IIsolateScope extends ng.IScope {
    height: number;
    width: number;
    data: any[];
    // keyField - the property in the data that represents the key
    // hiliteValue - key value for the highlighted point
    // yField - property for the y value
    // yLabel - label for the y values
    // hiliteClass - class for the hilite bar
    palette: IColorBrewerPalette;
    mapColorScale: IMapColorScale;
    keyField: string;
    yField: string;
    yLabel: string;
    hiliteValue: any;
    hiliteClass: string;   // field to format the hilited point
  }

  class ChartController {       // private to this closure
    public scope: IIsolateScope;

    // hold some private variables relating to the chart
    private _dataset: Plottable.Dataset;
    private _chart: Plottable.Components.Table;

    private _yScale: Plottable.QuantitativeScale<number>;

    private _draglines: Plottable.Components.DragLineLayer<{}>[];
    public colorScale: d3.scale.Threshold<number, string>;

    constructor() {
      this._draglines = [];
      for (let i = 0; i < 12; i++) {
        let dl: any = new Plottable.Components.DragLineLayer("horizontal");
        dl.tag = i;
        dl.onDragEnd(this._dragEnd);
        this._draglines.push(dl);
      }
    }
    // note we need this to be the calling DragLineLayer - so
    private _dragEnd = (dl) => {
      let index = dl.tag;
      this.scope.mapColorScale.thresholds[index] = Math.round(dl.value());
      // would be nice to wrap this somehow in the mapColorScale class
      // but there is no getter/setter support for  indexed arrays
      this.scope.mapColorScale.calcBuckets(this.scope.data);
    };

    private _getColorBrewerClass = (d: number) => {
      let c = this.colorScale(d);
      return c;
    };

    public setScaleType = (newType: string) => {
      this.scope.mapColorScale.scaletype = newType;
      this.scope.mapColorScale.calcThresholds(this.scope.data, this.scope.palette.bands);
    };

    public makeChart = (scope: IIsolateScope, element) => {
      // use plottable to make the chart

      // ------ datasets -----------------
      this._dataset = new Plottable.Dataset()
        .keyFunction((d, i) => d[scope.keyField]);

      // ------ scales, axes, axislabels
      let xScale: Plottable.QuantitativeScale<number> = new Plottable.Scales.Linear();
      this._yScale = new Plottable.Scales.Linear();

      // ------ animators removed

      // the chart
      let cht = new Plottable.Plots.Bar()
        .addDataset(this._dataset)
        .x((d, i) => i, xScale)
        .y((d) => d[scope.yField], this._yScale)
        .attr("fill", "")
        .attr("key", (d) => d[scope.keyField])
        .attr("index", (d, i) => i)
        .attr("class", (d) => this._getColorBrewerClass(d.value));

      let yAxis = new Plottable.Axes.Numeric(this._yScale, "left");

      let dlgroup = new Plottable.Components.Group(this._draglines);
      let group = new Plottable.Components.Group([cht, dlgroup]);
      // make a table component for the chart
      this._chart = new Plottable.Components.Table([
        [yAxis, group]
      ]);

      //// Create
      // this is a bit of translation to turn a jQuery element to a d3 element
      let svg = $(element).find('svg.colorRangeDesigner');
      let d3svg = d3.selectAll(svg.toArray());
      this._chart.renderTo(d3svg);
    };

    public drawChart = (scope: IIsolateScope, element) => {
      if (!this._chart) {
        this.makeChart(scope, element);
      }
      // set up the data
      this.colorScale = scope.mapColorScale.getColorBrewerScale(scope.palette.bands, scope.palette.invert);
      this._dataset.data(_.sortBy(scope.data, scope.yField));
    };

    public setDragLines(thresholds: number[]) {
      for (let i = 0; i < this._draglines.length; i++) {
        if (i < thresholds.length) {
          this._draglines[i]
            .value(thresholds[i])
            .scale(this._yScale)
            .removeClass("hidden");
        } else {
          this._draglines[i]
            .value(0)
            .scale(this._yScale)
            .addClass("hidden");
        }
      }
    }
  }

  // declaration of the class that implements the directive
  // does not need to be exported
  class ColorRangeDesigner implements ng.IDirective {
    // properties of the directive are public properties of this class

    // scope defines the bindings from the directive attributes to the isolate scope
    // it is not the scope itself
    public scope = {
      height: '@',
      width: '@',
      data: "=dataset",
      // keyField - the property in the data that represents the key
      palette: '=',
      mapColorScale: '=',
      // yField - property for the y value
      keyField: '@',
      yField: '@',
    };
    public restrict = "EA";
    public templateUrl = "common/colorRangeDesigner";
    public link: ng.IDirectiveLinkFn;
    // link the controller function to the definition of the directive - it will get passed to link
    public controller = ChartController;
    public controllerAs = "vm";

    // the arguments to the constructor are the injected dependencies
    constructor() {

      // define the directive link function in the constructor
      // put it on the prototype
      ColorRangeDesigner.prototype.link = (scope: IIsolateScope, element, attrs, ctrlr: ChartController) => {

        // give the controller a reference to scope
        ctrlr.scope = scope;
        // set any other instance variables on the controller...

        // link function sets up any required watches on the scope
        // this allows the chart to respond to chnages in the controller

        scope.$watch("data", function (newValue, oldValue) {
          if (newValue) {
            ctrlr.drawChart(scope, element);
          }
        }, true);
        scope.$watch("mapColorScale", function (newValue, oldValue) {
          if (newValue) {
            ctrlr.setDragLines(scope.mapColorScale.thresholds);
            ctrlr.drawChart(scope, element);
          }
        }, true);
        scope.$watch("palette.invert", function (newValue, oldValue) {
            ctrlr.drawChart(scope, element);
        }, false);

      };
    }

    // function to establish the chart objects and render

    // directive always get the factory - some plumbing
    public static factory() {
      // the definition of directive here will take any dependencies
      let directive = () => {
        return new ColorRangeDesigner();
      };
      return directive;
    }
  }

  angular
    .module('sw.common')
    .directive('colorRangeDesigner', ColorRangeDesigner.factory());
}
