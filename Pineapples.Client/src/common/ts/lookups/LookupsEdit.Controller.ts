﻿module Sw.Lookups {

  /**
   * @name colwidths
   * @description calculate the width of a set of columns
   * assumes each columndef has a width attribute, in px
   * 
   * TODO - Does not seem like it is being used. Also relying on a
   * hard coded width is not a good thing since specific width should only
   * ever be used when the data is of known size and equal among all pineapples
   * deployment in the Pacific.
   *
   * Also duplicate of this function in PagedListController.ts
   */
  var colwidths = function (columndefs, start, end) {
    var tot = 0;
    for (var i = start; i <= end; i++) {
      tot += parseFloat(columndefs[i].width);
    }
    return tot;
  };

  class LookupsEditController {

    public isWaiting = false;
    public pagination: any = {};
    public runOnLoad = false;

    public resultSet: any;

    // track rendered view
    public renderedViewMode = "";
    public renderedPageNo = 1;
    public renderedPageSize = 50;

    public gridClass: any;

    public baseState: string;
    protected baseOptions: any;

    public gridOptions: any;
    public gridApi: any;

    public columnWidths: any;

    protected onRegisterApi(gridApi) {
      this.gridApi = gridApi;
      gridApi.rowEdit.on.saveRow(this.scope, this.saveRow);
    }

    public saveRow = (rowEntity, force) => {
      if (force) {
        // set the row dirty first
        this.setDirty([rowEntity]);
      }
      let promise = this.api.post(rowEntity);
      this.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

    public setDirty = (rowEntities) => {
      this.gridApi.rowEdit.setRowsDirty(rowEntities);
    };

    static $inject = ['$scope', '$state', 'lookupsAPI', 'Lookups'];
    constructor(public scope, $state, public api, public lookups: Sw.Lookups.LookupService) {

      this.baseState = $state.current.name;

      // utilities for grid management
      this.columnWidths = colwidths;

      this.baseOptions = {
        enablePagination: false,
        useExternalSorting: false,
        // v 3 introduces various options about sorting that are overly complex and have to be removed 7 2 2016
        enableColumnMenus: false,
        onRegisterApi: (gridApi) => {
          this.onRegisterApi.call(this, gridApi);
        }
      };

      this.gridOptions = angular.copy(this.baseOptions);
      lookups.getList("schoolTypes").then(lookuplist => {
        this.gridOptions.data = lookuplist;
        this.gridOptions.columnDefs = [
          { field: 'C' },
          { field: 'N' }
        ];

      });
    }
  }

  angular
    .module('sw.common')
    // theFilter will be injected by ui-router resolve
    .controller('LookupsEditController', LookupsEditController);
}
