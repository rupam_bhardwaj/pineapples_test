﻿/* ------------------------------------------
    lookups Service
    ----------------------------------------
*/
module Sw.Lookups {

  /**
   * Required structure for a LookupEntry. Only used has array item and some
   * retrieval operation of {@link ILookupList}
   */
  export interface ILookupEntry {
    C: string;
    N: string;
    ID?: number;
    g?: string;
  }

  export interface ILookupList extends Array<ILookupEntry> {
    byCode(code: string): ILookupEntry;
    byCode(code: string, prop: string): any;

    // return a match based n a numeric id
    byID(id: number): ILookupEntry;
    byID(id: number, prop: string): any;

    search(srch: string): ILookupEntry;
    search(srch: string, prop: string): any;
    match(srch: string): Array<ILookupEntry>;
  }

  class LookupListMixin extends Array<ILookupEntry> {

    public byCode(code: string): ILookupEntry;
    public byCode(code: string, prop: string): any;
    public byCode(code: string, prop?: string) {
      let row = _.find(this, { C: code });
      if (row) {
        if (prop) {
          return row[prop];    // name is the default return
        }
        return row;
      }
      return null;

    }

    // return a match based n a numeric id
    public byID(id: number): ILookupEntry;
    public byID(id: number, prop: string): any;
    public byID(id: number, prop?: string) {
      let row = _.find(this, { ID: id });
      if (row) {
        if (prop) {
          return row[prop];    // name is the default return
        }
        return row;
      }
      return null;
    }

    public search(srch: string): ILookupEntry;
    public search(srch: string, prop: string): any;
    public search(srch: string, prop?: string) {
      srch = srch.toUpperCase();
      let row = _.find(this,
        (d) => (d.C.toUpperCase() === srch || (d.N && d.N.toUpperCase() === srch) ? true : false)
      );
      if (row) {
        if (prop) {
          return row[prop];    // name is the default return
        }
        return row;
      }
      return null;

    }

    public match(srch: string) {
      srch = srch.toUpperCase();
      let matches = _.filter(this,
        (d) => (d.C.toUpperCase().indexOf(srch) >= 0 || d.N.toUpperCase().indexOf(srch) >= 0 ? true : false)
      );
      return matches;
    }
  }
  /**
   *
   */
  class PickerController {

    public codeset: any;
    public selected: any;
    public title: string;
    public prompt: string;

    private _instance: angular.ui.bootstrap.IModalServiceInstance;

    static $inject = ["$uibModalInstance", "codeset", "code", "title", "prompt"];
    constructor(instance, codeset, code, title, prompt) {
      this._instance = instance;
      this.codeset = codeset;
      this.selected = code;
      this.title = title;
      this.prompt = prompt;
    }
    public ok() {
      // validation
      this._instance.close(this.selected);
    };

    public cancel () {
      this._instance.dismiss('cancel');
    };

    public select(selection) {
      this.selected = selection;
    }
  }

  export class LookupService {
    static $inject = ["$q", "lookupsAPI", "$uibModal"];
    constructor(protected _q: ng.IQService, protected _api, protected _uibModal) {
    }
    // this is a perfact example of where an index signature can enable the use of a "dictionary" across randomly created property names
    public cache: {
      [lookupname: string]: ILookupList
    };

    /**
     * ensure that a given set of lookups are available
     * when the promise is resolved all
     * @param names - array of lookup names
     *
     */
    public prepare(names: string[]) {
      // http://www.martin-brennan.com/using-q-all-to-resolve-multiple-promises/
      let p = names.map((name) => this.getList(name));
      return this._q.all<ILookupList>(p);
    }
    public getListFcn(name: string) {
      return () => this.getList(name);
    }

    /**
     * `getList` makes a trip to the backend and retrieves some lookup data from the database
     * (e.g. types of schools, a list of islands). Instead of directly using
     * this one it is better to get it from the cache since it would not require
     * another async trip to backend
     *
     * ### Example of direct use
     *
     * ```typescript
     * // first inject Lookups service
     * let years = this.$q.defer();
     * this.lookups.getList('surveyYears').then((list: Sw.Lookups.ILookupList) => {
     *   years = list
     * }, (reason: any) => {
     *   console.log("Unable to get surveyYears" + reason);
     * });
     * ```
     *
     * ### Example of getting from cache (recommended)
     * ```typescript
     * // first inject Lookups service and make public
     * static $inject = ['Lookups'];
     * constructor(public lookups: Sw.Lookups.LookupService) { }
     * ```
     * and then directly in the view
     * ```html
     * <select style="width:100%; color:#4b4b4b; font-size:14pt"
     *         ng-options="year.C as year.N for year in vm.lookups.cache['surveyYears']"
     *         ng-model="vm.baseYear"></select>
     * ```
     * @param {string} name - a valid lookup string (e.g.'schoolTypes','districts','authorities',
     * 'islands','electoraten','electoratel','listNames','schoolNames',
     * 'vocab','sysParams','paFields','paFrameworks','schoolFieldOptions',
     * 'schoolDataOptions','surveyYears')
     * @returns {ng.IPromise<ILookupList>}
     */
    public getList(name: string): ng.IPromise<ILookupList> {
      var d = this._q.defer();
      if (this.cache && this.cache[name]) {
        d.resolve(this.cache[name]);
      } else {
        // go read it and wait
        this._api.getList(name).then((result) => {
          Object.keys(result.plain()).forEach((codeType: string) => {
            angular.extend(result[codeType], LookupListMixin.prototype);
          });
          if (this.cache) {

            angular.extend(this.cache, result);
          } else {
            this.cache = result;
          }
          d.resolve(this.cache[name]);
        },
          error => {
            d.reject(error);
          }
        );
      };
      return d.promise;
    }

    public init = () => {
      var d = this._q.defer();
      if (this.cache) {
        d.resolve(this.cache);
      } else {
        this._api.core().then((result) => {
          this.addToCache(result);
            d.resolve(this.cache);
          },
          error => {
            d.reject(error);
          }
        );
      };
      return d.promise;
    };
    public findByID: (codeset: string, id: any) => ILookupEntry = (codeset, id) => {
      return <ILookupEntry>_.find(this.cache[codeset], { ID: id }) || { C: null, N: null };
    };
    public byCode(codeset: string, code: string): ILookupEntry;
    public byCode(codeset: string, code: string, prop: string): any;
    public byCode(codeset: string, code: string, prop?: string) {
      let codeTable: ILookupList = this.cache[codeset];
      if (codeTable) {
        return codeTable.byCode(code, prop);
      } else {
        return null;
      }
    }
    /**
     *
     * @returns {}
     */
    public pickCode = (codeset: string, code: string, title: string, prompt: string) => {
      let instance = this._uibModal.open({
        animation: true,
        templateUrl: 'dialog/codepicker',
        controller: 'pickerController',
        controllerAs: 'vm',
        size: 'md',
        resolve: {
          codeset: () => this.cache[codeset],
          code: () => code,
          title: () => title,
          prompt: () => prompt
        }
      });
      return instance.result;
    };


    protected addToCache(result) {
      // plain() removes all the restangular properties - maybe this should happen upstream
      Object.keys(result.plain()).forEach((codeType: string) => {
        angular.extend(result[codeType], LookupListMixin.prototype);
      });
      if (this.cache) {
        angular.extend(this.cache, result);
      } else {
        this.cache = result;
      }
    }
  };
  angular
    .module('sw.common')
    .controller("pickerController", PickerController)
    .service('Lookups', LookupService);
}
