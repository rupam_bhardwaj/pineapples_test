﻿namespace Sw.Charts {

  type rowcolDatum = {
    R: string,
    C: string,
    RC: string,
    Num: number
  };

  interface IBindings {
    height: any;
    width: any;
    data: any[];
    info: any;
    charttype: string;
    flip: any;
  }


  interface IIsolateScope extends ng.IScope {
    height: number;
    width: number;
    data: any[];
    info: string;
    charttype: string;
    flip: boolean;

    chartV: Plottable.Component;
    chartVC: Plottable.Component;
    chartH: Plottable.Component;
    chartHC: Plottable.Component;
  }

  class rcController extends Sw.Charts.SizeableChartController  implements IBindings {


    // bindings
    public height: any;
    public width: any;
    public data: any[];
    public info: any;
    public charttype: string;
    public flip: any;

    private _percentage: boolean = false;

    private stackedBarV: Plottable.Plot;
    private stackedBarH: Plottable.Plot;

    private clusteredBarV: Plottable.Plot;
    private clusteredBarH: Plottable.Plot;

    private categoryAxisLabel: Plottable.Components.AxisLabel;
    private valueAxisLabel: Plottable.Components.AxisLabel;

    private chartV: Plottable.Component;
    private chartVC: Plottable.Component;
    private chartH: Plottable.Component;
    private chartHC: Plottable.Component;

    private set: boolean; // chart is set up

    private currentBar: any;
    //private currentBar: any;

    public handler = (event) => {
      let d = event.target.__data__;
      console.log(d.R);
    };

    static $inject = ["$compile", "$timeout", "$scope", "$element"];
    constructor( public compile: ng.ICompileService, timeout: ng.ITimeoutService, public scope: ng.IScope,private element: any) {
      super(compile, timeout, scope, element);
    }

    public $onChanges(changes: any) {

      if (changes.charttype) {
        this.changeChartType();
      }
      // anything else causes drawChart
      this.drawChart(this.element );
    }

    // overridable
    public onResize(element) {
      if (this.size.height === 0) {
        return;
      }
      this.drawChart(element);
    }

    ////public onElementResize = (scope: IIsolateScope, element: ng.IAugmentedJQuery) => {
    ////  let svg = element.find("svg.v")[0];
    ////  if (svg.offsetHeight > 0 && svg.offsetWidth > 0) {
    ////    scope.chartV.redraw();
    ////  }
    ////  svg = element.find("svg.vc")[0];
    ////  if (svg.offsetHeight > 0 && svg.offsetWidth > 0) {
    ////    scope.chartVC.redraw();
    ////  }
    ////  svg = element.find("svg.h")[0];
    ////  if (svg.offsetHeight > 0 && svg.offsetWidth > 0) {
    ////    scope.chartH.redraw();
    ////  }

    ////  svg = element.find("svg.hc")[0];
    ////  if (svg.offsetHeight > 0 && svg.offsetWidth > 0) {
    ////    scope.chartHC.redraw();
    ////  }
    ////};

    public makeChart = (element) => {
      /**
       * scales, axes, axislabels
       */
      let colorScaleV = new Plottable.Scales.Color();
      let colorScaleVC = new Plottable.Scales.Color();
      let colorScaleH = new Plottable.Scales.Color();
      let colorScaleHC = new Plottable.Scales.Color();

      let categoryScaleV = new Plottable.Scales.Category();
      let valueScaleV = new Plottable.Scales.Linear();

      let categoryAxisV = new Plottable.Axes.Category(categoryScaleV, "bottom");
      categoryAxisV.tickLabelAngle(-90);
      let valueAxisV = new Plottable.Axes.Numeric(valueScaleV, "left");

      let categoryScaleVC = new Plottable.Scales.Category();
      let valueScaleVC = new Plottable.Scales.Linear();

      let categoryAxisVC = new Plottable.Axes.Category(categoryScaleVC, "bottom");
      categoryAxisVC.tickLabelAngle(-90);
      let valueAxisVC = new Plottable.Axes.Numeric(valueScaleVC, "left");

      let categoryScaleH = new Plottable.Scales.Category();
      let valueScaleH = new Plottable.Scales.Linear();

      let categoryAxisH = new Plottable.Axes.Category(categoryScaleH, "left");
      let valueAxisH = new Plottable.Axes.Numeric(valueScaleH, "top");

      let categoryScaleHC = new Plottable.Scales.Category();
      let valueScaleHC = new Plottable.Scales.Linear();

      let categoryAxisHC = new Plottable.Axes.Category(categoryScaleHC, "left");
      let valueAxisHC = new Plottable.Axes.Numeric(valueScaleHC, "top");

      // colorLegend legend
      let legendV = new Plottable.Components.Legend(colorScaleV)
        .maxEntriesPerRow(1)
        .xAlignment('left');

      let legendVC = new Plottable.Components.Legend(colorScaleVC)
        .maxEntriesPerRow(1)
        .xAlignment('left');

      let legendH = new Plottable.Components.Legend(colorScaleH)
        .maxEntriesPerRow(1)
        .xAlignment('left');

      let legendHC = new Plottable.Components.Legend(colorScaleHC)
        .maxEntriesPerRow(1)
        .xAlignment('left');

      // persist these on scope so we can get to the text property
      this.categoryAxisLabel = new Plottable.Components.AxisLabel("Enrolment 2007")
        .padding(1);
      this.valueAxisLabel = new Plottable.Components.AxisLabel("Enrolment 2008", -90)
        .padding(5);

      // first set up the animator
      // set up the animator

      let compile = this.compile;

      // tooltip , interaction, animation


      let tooltipper = (d: rowcolDatum) => {
        switch (this.charttype) {
          case "v":
          case "vc":
          case "h":
          case "hc":
            return (d.R + " / " + d.C + ": " + d.Num);
          case "vp":
          case "hp":
            return (d.R + " / " + d.C + ": " + d.Num.toFixed(2) + "%");
        }
      };

      this.tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(tooltipper);

      let self = this;
      let callbackAnimate = function (jr: Plottable.Drawers.JoinResult, attr, drawer) {
        (<d3.selection.Update<any>>jr.merge)
          .on("click", function (d) {
            console.log(d.R);
          })
          .on("mouseover", self.tip.show)
          .on("mouseout", self.tip.hide)
          .attr("ng-click", "vm.handler($event)")
          .call(function () {
            compile(this[0].parentNode)(self.scope);
          });

        this.innerAnimate(jr, attr, drawer);
      };
      let innerAnimator = new Plottable.Animators.Easing()
        // .exitEasingMode(Plottable.Animators.EasingFunctions.squEase("linear", 0, .5))
        // .easingMode(<any>Plottable.Animators.EasingFunctions.squEase("linear", .5, 1))
        .stepDuration(50)
        .stepDelay(0);
      let animator = new Plottable.Animators.Callback()
        .callback(callbackAnimate)
        .innerAnimator(innerAnimator);
      let svg;
      let d3svg;

      // ---------------- StackedBar ---------------------
      // this is the main s plot

      this.stackedBarV = new Plottable.Plots.StackedBar(Plottable.Plots.Bar.ORIENTATION_VERTICAL)
        .x((d) => d.R, categoryScaleV)
        .y((d) => d.Num, valueScaleV)
        .attr("fill", (d) => d.C, colorScaleV)
        .attr("opacity", .8)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      this.chartV = new Plottable.Components.Table([
        [valueAxisV, this.stackedBarV, legendV],
        [null, categoryAxisV, null]
      ]);
      svg = $(element).find("svg.v");
      d3svg = d3.selectAll(svg.toArray());
      d3svg.call(<any>this.tip);
      this.chartV.renderTo(d3svg);

      //// Vertical Clustered Bar
      this.clusteredBarV = new Plottable.Plots.ClusteredBar(Plottable.Plots.Bar.ORIENTATION_VERTICAL)
        .x((d) => d.R, categoryScaleVC)
        .y((d) => d.Num, valueScaleVC)
        .attr("fill", (d) => d.C, colorScaleVC)
        .attr("opacity", .8)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      this.chartVC = new Plottable.Components.Table([
        [valueAxisVC, this.clusteredBarV, legendVC],
        [null, categoryAxisVC, null]
      ]);
      svg = $(element).find("svg.vc");
      d3svg = d3.selectAll(svg.toArray());
      this.chartVC.renderTo(d3svg);

      /// Horizontal Stacked Bar
      this.stackedBarH = new Plottable.Plots.StackedBar(Plottable.Plots.Bar.ORIENTATION_HORIZONTAL)
        .y((d) => d.R, categoryScaleH)
        .x((d) => d.Num, valueScaleH)
        .attr("fill", (d) => d.C, colorScaleH)
        .attr("opacity", .8)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      this.chartH = new Plottable.Components.Table([
        [null, valueAxisH, null],
        [categoryAxisH, this.stackedBarH, legendH]
      ]);
      svg = $(element).find("svg.h");
      d3svg = d3.selectAll(svg.toArray());
      this.chartH.renderTo(d3svg);

      // Horizontal Clustered Bar
      this.clusteredBarH = new Plottable.Plots.ClusteredBar(Plottable.Plots.Bar.ORIENTATION_HORIZONTAL)
        .y((d) => d.R, categoryScaleHC)
        .x((d) => d.Num, valueScaleHC)
        .attr("fill", (d) => d.C, colorScaleHC)
        .attr("opacity", .8)
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, animator);

      this.chartHC = new Plottable.Components.Table([
        [null, valueAxisHC, null],
        [categoryAxisHC, this.clusteredBarH, legendHC]
      ]);
      svg = $(element).find("svg.hc");
      d3svg = d3.selectAll(svg.toArray());
      this.chartHC.renderTo(d3svg);

      this.currentBar = this.stackedBarV;

      this.changeChartType();
      this.set = true;
    };

    public drawChart = (element) => {
      // check there is something to do
      if (!this.data) {
        return;
      }
      if (!this.set) {
        this.makeChart(element);
      };

      /**
       * Set up required datasets
       */
      var rawData = this.data;
      // ths data needs to be transformed into a colection of datasets
      // first transform the data into a nest
      // each entry will become a Dataset and therefore a series
      // key will be the series identified, values is the series data
      // support flipping the R C
      let R = this.flip === "true" ? "C" : "R";
      let C = this.flip === "true" ? "R" : "C";

      var nestedData: any[] = d3.nest()
        .key((d: rowcolDatum) => d[C])
        .entries(rawData);
      // now nest along the R axis, to calculate the totals over all values for each R

      var nestedR: any[] = d3.nest()
        .key((d: rowcolDatum) => d[R])
        .rollup((leaves: rowcolDatum[]) => d3.sum(leaves, (d) => d.Num))
        .entries(rawData);

      let currentds = this.currentBar.datasets();
      // first step through all the exisitng datasets and find the ones we can kill
      currentds.forEach((cds) => {
        // is there an incoming series matching this dataset?
        if (!_.find(nestedData, { key: cds.metadata().seriesID })) {
          // no, there isn't so if this dataset is empty, kill it, otherwise set it to empty data so it
          // can exit gracefully
          if (cds.data().length) {
            this.currentBar
              .removeDataset(cds);
            // cds.data([]);
          } else {
            this.currentBar
              .removeDataset(cds);
          }
        }
      });
      // refresh this
      currentds = this.currentBar.datasets();
      nestedData.forEach((nd) => {
        let newdata = nd.values.map((x) => {
          let tot: number = this._percentage ? _.find(nestedR, { key: x[R] }).values : 100;
          return { R: x[R], C: x[C], Num: (x.Num * 100 / tot) };
        });
        let ds: Plottable.Dataset = _.find(currentds, (cds: Plottable.Dataset) => { return (cds.metadata().seriesID === nd.key); });
        if (ds) {
          // a dataset exists for this series
          ds.data(newdata);
        } else {
          ds = new Plottable.Dataset(newdata, { seriesID: nd.key })
            .keyFunction((x) => x.C + "|" + x.R);
          this.currentBar
            .addDataset(ds);
        };
      });
      // axis labels
      this.categoryAxisLabel.text('Foo bar');
      this.valueAxisLabel.text('bar foo 2008');
    };

    public changeChartType() {
      switch (this.charttype) {
        case "v":
        case "vp":
          this.currentBar = this.stackedBarV;
          break;
        case "vc":
          this.currentBar = this.clusteredBarV;
          break;
        case "h":
        case "hp":
          this.currentBar = this.stackedBarH;
          break;
        case "hc":
          this.currentBar = this.clusteredBarH;
          break;

      }
      switch (this.charttype) {
        case "hp":
        case "vp":
          this._percentage = true;
          break;
        default:
          this._percentage = false;
          break;
      }
    }

  }
  /**
   *  RowColChart is a custom directive to display data returned from
   *  Pineapples row col data as a stacked bar chart
   */
  class RowColChart implements ng.IComponentOptions {
    public bindings: any = {
      height: '@',
      width: '@',
      data: "<dataset",
      info: "<info",
      charttype: "@",
      flip: "@"

    };
    public template: any =
    `<div style="height:100%; width:100%" >
    <svg class="v"   ng-show="vm.charttype===\'v\' || vm.charttype===\'vp\'"></svg>
    <svg class="vc"  ng-show="vm.charttype===\'vc\'"  ></svg>
    <svg class="h"   ng-show="vm.charttype===\'h\' || vm.charttype===\'hp\'"   height={{vm.height}} width={{vm.width}}></svg>
    <svg class="hc"  ng-show="vm.charttype===\'hc\'"  height={{vm.height}} width={{vm.width}}></svg>
    </div>`;
    public restrict = "EA";

    public controller = rcController;
    public controllerAs = "vm";


    ////  scope.$watchGroup([
    ////    () => element[0].offsetHeight,
    ////    () => element[0].offsetWidth,
    ////    () => element.find("svg.v")[0].offsetHeight,
    ////    () => element.find("svg.v")[0].offsetWidth,
    ////    () => element.find("svg.vc")[0].offsetHeight,
    ////    () => element.find("svg.vc")[0].offsetWidth,
    ////    () => element.find("svg.h")[0].offsetHeight,
    ////    () => element.find("svg.h")[0].offsetWidth,
    ////    () => element.find("svg.hc")[0].offsetHeight,
    ////    () => element.find("svg.hc")[0].offsetWidth
    ////  ],
    ////    (newValue) => {
    ////      ctrlr.onElementResize(scope, element);
    ////    });
    ////};
    ////}
  }

  angular
    .module("sw.common")
    .component("rowColChart", new RowColChart());
}
