﻿namespace Sw.Charts {

  /**
   * @class Sw.Charts.Size
   * @classdesc a class that sets the size (i.e. of a plottable chart
   * @public
   * 
   * @example return new Size(div.height(), div.width()); returns the new
   * size of a chart based on the parent div element
   */
  export class Size {

    public height = 0;
    public width = 0;

    constructor(height, width) {
      this.height = height;
      this.width = width;
    }
  }

  /**
   * @class Sw.Charts.SizeableComponentController
   * @classdesc A controller class whoe purpose is to make chart responsive. It watches
   * the width of the HTML div element parent of the chart and when it changes sets the
   * new size of the chart
   * @public
   * @see Sw.Charts.Size
   *
   * @example TODO
   */
  export class SizeableComponentController {

    public size: Size;
    public width = 10;
    public getSize: () => Size;

    /**
     * @method newSize
     * @description overridable
     */
    public setSize(newSize: Size) {
      this.size = newSize;
    }

    /**
     * @method onResize
     * @description overridable
     */
    public onResize(element) {
    }

    // injections
    static $inject = ["$compile", "$timeout", "$scope", "$element"];

    constructor(public $compile: ng.ICompileService, public $timeout: ng.ITimeoutService, public scope: ng.IScope, element) {
      this.size = new Size(0, 0);

      let getSize = () => {
        let div = $(element).find("div");
        return new Size(div.height(), div.width());
      };

      scope.$watch(getSize, (newSize: Size) => {
        // makje this OR, not AND... often we get the width back ( from bootstrap col-xxx styles)
        // and we'll set the height to preserve aspect ratio.
        if (newSize.height !== 0 || newSize.width !== 0) {
          this.$timeout(() => {
            this.setSize(newSize);
          });
        };
      }, true);

      scope.$watch(() => this.size, () => {
        this.$timeout(() => {
          this.onResize(element);
        });
      }, true);

    }
  }

  export class SizeableChartController extends SizeableComponentController {

    public tip: d3tip.tip
    constructor(compile: ng.ICompileService, public timeout: ng.ITimeoutService, public scope: ng.IScope, element) {
      super(compile, timeout, scope, element);
    }

    public $onDestroy() {
      if (this.tip) {
        this.tip.destroy();
      }
    }
  }

}
