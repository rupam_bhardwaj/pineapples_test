﻿module Sw.Utils {
  export class RouteHelper {
    public static listState(entity, allowedViews?, defaultView?) {
      var state = {
        // all these are the same becuase the variation is in the Filter object
        url: '/list',
        data: {
        },
        views: <{ [name: string]: ng.ui.IState }>{
          "navbar@": {
            templateUrl: "common/PagedListParams",
            controller: "FilterController",
            controllerAs: "vm"
          },
          "@": {
            templateUrl: entity + "/PagedList", // begin move to server-side mvc routes
            controller: 'PagedListController',
            controllerAs: "listvm"
          }
        }
      };
      if (allowedViews) {
        state.data["allowedViews"] = allowedViews;
      }
      if (defaultView) {
        state.data["defaultView"] = defaultView;
      }
      return state;
    }

    public static editableListState(entity, allowedViews?, defaultView?) {
      var state = RouteHelper.listState(entity, allowedViews, defaultView);
      state.views["@"].templateUrl = entity + "/PagedListEditable";
      state.views["@"].controller = 'PagedListEditableController';
      return state;
    }

    public static chartState(featurename): ng.ui.IState {
      var state = {
        url: '/chart',

        views: <{ [name: string]: ng.ui.IState }>{
          "navbar@": {
            templateUrl: "common/chartparams", // local becuase we have to get acess to the data layer
            controller: "ChartParamsController",
            controllerAs: "vm"
          },
          "@": {
            templateUrl: "common/chart",
            controller: "ChartController",
            controllerAs: "vm"
          }
        },
        resolve: {
          palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
          mapColorScale: function () { return new Sw.Maps.MapColorScale(); }
        }
      };
      return state;
    };
    public static tableState(featurename): ng.ui.IState {
      var state = {
        url: '/table',

        views: <{ [name: string]: ng.ui.IState }>{
          "navbar@": {
            templateUrl: "common/chartParams",
            controller: "TableParamsController",
            controllerAs: "vm"
          },
          "@": {
            templateUrl: "common/table",
            controller: "TableController",
            controllerAs: "vm"
          }

        }
      };
      return state;
    };
    public static mapState(mapview?): ng.ui.IState {
      if (!mapview) {
        mapview = 'baseMapView';
      };
      var state = {
        url: '/map',
        views: <{ [name: string]: ng.ui.IState }>{
          "navbar@": {
            templateUrl: "common/MapParams",
            controller: "MapParamsController",
            controllerAs: "mappc"
          },
          "@": {
            templateUrl: "common/map",
            controller: "MapController",
            controllerAs: "mapc"
          }
        },
        resolve: {
          mapView: mapview
        }
      };
      return state;
    };

    // featurename matches the Web Api controller
    // filtername is the filtername js class
    // templatepath matches the mvc controller that dispenses the templates

    // tableOptions is a member of Lookups.cahce, which is the dropdown on table and charts pages
    public static tb(tableOptions) {
      return () => tableOptions;
    }

    public static featureState(featurename, filtername?, templatepath?, url?, usersettings?, tableoptions?, mapview?): ng.ui.IState {
      if (!mapview) {
        mapview = 'baseMapView';
      };
      var defUserSettings = { defaults: {}, locked: {} };
      usersettings = usersettings || defUserSettings;
      filtername = filtername || featurename + 'Filter';
      featurename = featurename.toLowerCase();
      templatepath = templatepath || featurename;
      url = (url || featurename);
      var state = {
        url: '/' + url,
        data: {
          roles: ['authenticated']
        },
        views: <{ [n: string]: ng.ui.IState }>{
          "headerbar@": {
            templateUrl: templatepath + "/searcher", // mvc formulation allows server-side logic to choose searher version, function encapsulation allows future client override too
            controller: "FilterController",
            controllerAs: "vm",

          },
          "headerbardropdownIcon@": { template: '<i class="fa fa-filter" uib-tooltip="Show Filter" tooltip-placement="right"></i>' },
          "flashFind@": {
            templateUrl: "common/FlashFindSearch",
            controller: "FilterController",
            controllerAs: "vm"
          },
          "flashFindCollapsed@": { template: '<i class="fa fa-bolt opacity-control"></i>' }
        },
        resolve: {
          theFilter: filtername,
          findConfig: [
            filtername, function (fltr) {
              if (fltr.createFindConfig) {
                return fltr.createFindConfig(usersettings);
              }
              return null;
            }
          ],
          palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
          mapColorScale: function () { return new Sw.Maps.MapColorScale(); },
          mapView: mapview
        }
      };

      if (tableoptions) {
        state.resolve["tableOptions"] = this.tb(tableoptions);
      }

      // TODO - Any way to not repeat same views below?!
      // see when moving to components

      // Views specific to .list states
      let viewFlashFindSearchList = "flashfindsearch@site." + featurename + ".list";
      let viewSearcherList = "searcher@site." + featurename + ".list";
      let viewPagedListParamsList = "pagedlistparams@site." + featurename + ".list";

      state.views[viewFlashFindSearchList] = {
        templateUrl: "generic/FlashFindSearchComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewSearcherList] = {
        templateUrl: templatepath + "/SearcherComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewPagedListParamsList] = {
        templateUrl: "generic/PagedListParamsComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };

      // Views specific to .chart states
      let viewFlashFindSearchChart = "flashfindsearch@site." + featurename + ".chart";
      let viewSearcherChart = "searcher@site." + featurename + ".chart";
      let viewManipulateChart = "manipulatechart@site." + featurename + ".chart";

      state.views[viewFlashFindSearchChart] = {
        templateUrl: "generic/FlashFindSearchComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewSearcherChart] = {
        templateUrl: templatepath + "/SearcherComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewManipulateChart] = {
        templateUrl: "generic/ChartManipulationComponent",
        controller: "ChartParamsController",
        controllerAs: "vm",
      };

      // Views specific to .table states
      let viewFlashFindSearchTable = "flashfindsearch@site." + featurename + ".table";
      let viewSearcherTable = "searcher@site." + featurename + ".table";
      let viewManipulateTable = "manipulatetable@site." + featurename + ".table";

      state.views[viewFlashFindSearchTable] = {
        templateUrl: "generic/FlashFindSearchComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewSearcherTable] = {
        templateUrl: templatepath + "/SearcherComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewManipulateTable] = {
        templateUrl: "generic/ChartManipulationComponent",
        controller: "TableParamsController",
        controllerAs: "vm",
      };

      // Views specific to .map states
      let viewFlashFindSearchMap = "flashfindsearch@site." + featurename + ".map";
      let viewSearcherMap = "searcher@site." + featurename + ".map";
      let viewManipulateMap = "manipulatemap@site." + featurename + ".map";

      state.views[viewFlashFindSearchMap] = {
        templateUrl: "generic/FlashFindSearchComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewSearcherMap] = {
        templateUrl: templatepath + "/SearcherComponent",
        controller: "FilterController",
        controllerAs: "vm",
      };
      state.views[viewManipulateMap] = {
        templateUrl: "generic/MapParamsComponent",
        controller: "MapParamsController",
        controllerAs: "mappc",
      };

      return state;
    };

    public static addFeatureState($stateProvider: ng.ui.IStateProvider, featurename, filtername, templatepath, url, usersettings, tableoptions, mapview) {
      var state = this.featureState(featurename, filtername, templatepath, url, usersettings, tableoptions, mapview);
      var statename = 'site.' + featurename.toLowerCase();
      $stateProvider
        .state(statename, state);
      return this;
    }

    public static addListState($stateProvider: ng.ui.IStateProvider, featurename, allowedViews?, defaultView?) {
      var state = this.listState(allowedViews, defaultView);
      var statename = 'site.' + featurename.toLowerCase() + '.list';
      $stateProvider
        .state(statename, state);
      return this;
    }

    public static addEditableListState($stateProvider: ng.ui.IStateProvider, featurename, allowedViews, defaultView) {
      var state = this.editableListState(allowedViews, defaultView);
      var statename = 'site.' + featurename.toLowerCase() + '.list';
      $stateProvider
        .state(statename, state);
      return this;
    }

    public static addChartState($stateProvider: ng.ui.IStateProvider, featurename) {
      var state = this.chartState(featurename);
      var statename = 'site.' + featurename.toLowerCase() + '.chart';
      $stateProvider
        .state(statename, state);
      return this;
    }

    public static addTableState($stateProvider: ng.ui.IStateProvider, featurename) {
      var state = this.tableState(featurename);
      var statename = 'site.' + featurename.toLowerCase() + '.table';
      $stateProvider
        .state(statename, state);
      return this;
    }

    public static addMapState($stateProvider: ng.ui.IStateProvider, featurename, mapview?: any) {
      var state = this.mapState(mapview);
      var statename = 'site.' + featurename.toLowerCase() + '.map';
      $stateProvider
        .state(statename, state);
      return this;
    }
  }
}