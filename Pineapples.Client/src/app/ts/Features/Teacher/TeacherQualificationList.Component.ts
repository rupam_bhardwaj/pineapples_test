﻿namespace Pineapples.Teachers {

  interface IBindings {
    qualifications: any;
  }

  class Controller implements IBindings {
    public qualifications: any;
 
    static $inject = ["$mdDialog", "$location", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, private $location: ng.ILocationService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.qualifications) {
//        console.log(this.surveys);
      }
    }

    public showQualification(qualification) {
      let options: any = {
        clickOutsideToClose: true,
        templateUrl: "teacher/qualificationdialog",
        controller: DialogController,
        controllerAs: "dvm",
        bindToController: true,
        locals: {
          qualification: qualification
          //payslip: ["teacherApi", (api) => {
          //  return api.payslip(id);
          //}]
        }

      }
      this.mdDialog.show(options);
    }
 
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        qualifications: '<',
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "teacher/qualificationlist";
    }
  }

  // popup dialog for audit log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog", "qualification"];
    constructor(mdDialog: ng.material.IDialogService, public payslip) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("componentTeacherQualificationList", new Component());
}
