﻿namespace Pineapples.Lookups {
  class Controller {
    public tableList: any[];

    static $inject = ["$state"];
    public constructor(public state: ng.ui.IStateService) { }
    private _table: string;
    public get table() {
      return this._table;
    }
    public set table(value) {
      this._table = value;
      this.state.go("site.lookups.lookup", { lkp: this.table });
    };
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        tableList: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "lookup/tableselector";
    }
  }
  angular
    .module("pineapples")
    .component("componentTableSelector", new Component());
}