﻿namespace Pineapples.Students {
  let modes = [
    {
      key: "Demographics",
      gridOptions: {
        columnDefs: [
          {
            field: 'stuDoB',
            name: 'Date of Birth',
            displayName: 'Date of Birth',
            cellFilter: "date:'d-MMM-yyyy'",
          },
          {
            field: 'stuGender',
            name: 'Gender',
            displayName: 'Gender',
            cellClass: 'gdAlignRight',
            lookup: 'gender',
          },
          {
            field: 'stuEthnicity',
            name: 'Ethnicity',
            displayName: 'Ethnicity',
            cellClass: 'gdAlignRight',
            lookup: 'ethnicities',
            enableSorting: true,
            sortDirectionCycle: ["asc", "desc"],
          },
          
        ]
      }
    },    
  ];

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['StudentFilter', pushModes]);
}
