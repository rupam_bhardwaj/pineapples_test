﻿namespace Pineapples.Students {

  interface IBindings {
    enrollments: any;
  }

  class Controller implements IBindings {
    public enrollments: any;
 
    static $inject = ["$mdDialog", "$location", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, private $location: ng.ILocationService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.enrollments) {
//        console.log(this.enrollments);
      }
    }

    //public showEnrollment(enrollment) {
    //  let options: any = {
    //    clickOutsideToClose: true,
    //    templateUrl: "student/enrollmentdialog",
    //    controller: DialogController,
    //    controllerAs: "dvm",
    //    bindToController: true,
    //    locals: {
    //      enrollment: enrollment
    //    }

    //  }
    //  this.mdDialog.show(options);
    //}

 
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        enrollments: '<',
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "student/enrollmentlist";
    }
  }

  //// popup dialog for audit log
  //class DialogController extends Sw.Component.MdDialogController {
  //  static $inject = ["$mdDialog", "enrollment"];
  //  constructor(mdDialog: ng.material.IDialogService, public payslip) {
  //    super(mdDialog);
  //  }
  //}

  angular
    .module("pineapples")
    .component("componentStudentEnrollmentList", new Component());
}
