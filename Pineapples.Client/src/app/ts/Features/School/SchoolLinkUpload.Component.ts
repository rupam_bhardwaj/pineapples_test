﻿namespace Pineapples.Schools {

  export class SchoolLinkUploadController extends Pineapples.Documents.DocumentUploader {

    public doc: any;      // the document record representing the photo

    

    public imageHeight: number;
    public school: School;

    public docFunction: string = "Photo";
    public document: any;     // this is the document object
    public allowUpload: boolean

    public docPath: string;

    static $inject = ["identity", "documentsAPI", "FileUploader", "$mdDialog"];
    constructor(public identity: Sw.Auth.IIdentity, docApi: any, FileUploader,  mdDialog: ng.material.IDialogService) {
      super(identity, docApi, FileUploader, mdDialog);
      this.uploader.url = "api/schoollinks/upload";

      this.imageHeight = this.imageHeight || 1200;
      this.model = {};      // TO DO new SchoolLink?
    }

    protected onSuccessItem(fileItem, response, status, headers) {
      this.doc = response.ResultSet;    // note slightly different format - not an array item
      this.school.Documents.push(this.doc);
      // update the tPhoho in the school object - we may have changed it
      this.school.schPhoto = this.doc.schPhoto
      // call the default implementation for housekeeping
      super.onSuccessItem(fileItem, response, status, headers);

    }

    protected onErrorItem(fileItem, response, status, headers) {
    }
    protected onCompleteItem(fileItem, response, status, headers) {
      if (status === 200) {
        this.doc = response.ResultSet;    // note slightly different format - not an array item
        // update the schPhoho in the school object - we may have changed it
        this.school.schPhoto = this.doc.schPhoto
        // call the default implementation for housekeeping
      }
      super.onCompleteItem(fileItem, response, status, headers);
    };

    protected onBeforeUploadItem(item) {

      // note that bindings that are not initialised in the component tag are stillpushed to the controller as undefined
      this.model.docTitle = item.file.name;
      this.model.docDate = item.file.lastModifiedDate;
      this.model.schNo = this.school._id();
      if (this.model.lnkFunction !== "PHOTO") {
        this.model.isCurrentPhoto = 0;
      }
      // call the default implementation
      super.onBeforeUploadItem(item);
    }

    public upload() {

      if (this.identity.isAuthenticated) {
        this.uploader.headers.Authorization = 'Bearer ' + this.identity.token;
      }
      this.uploader.uploadAll();
    }


    public get photoPath() {
      if (this.doc) {
        return this.documentPath(this.doc);
      }
      return this.missingImage;
    }
    public get photoThumbPath() {
      return this.thumbPath(this.doc, this.imageHeight);
    }

    // life cycyle hooks
    public $onChanges(changes) {
      if (changes.docFunction) {
        if (this.docFunction === undefined) {
          this.docFunction = "photo";
        }
      }
      if (this.document) {
        this.doc = this.document;
      }

    }

    public $onInit() {
      this.imageHeight = this.imageHeight || 1200;
      this.model = {};      // TO DO new SchoolLink?
    }
  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        imageHeight: "<",
        school: "<",
        docFunction: "<",
        document: "<",
        allowUpload: "<"
      };
      this.controller = SchoolLinkUploadController;
      this.controllerAs = "vm";
      this.templateUrl = "schoollink/Upload";
    }
  }
  angular
    .module("pineapples")
    .component("schoolLinkUploadComponent", new ComponentOptions());
}