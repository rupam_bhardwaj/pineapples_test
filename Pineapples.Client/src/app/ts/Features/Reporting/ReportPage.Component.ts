﻿namespace Pineapples.Reporting {

  class Controller {

    public folder: string;
    public promptForParams: string;
    public findConfig: Sw.Filter.FindConfig;

    constructor() { }

    // handler function to set up the parameters of the child report list
    // this is done by using the findConfig shared with the list, chart, map etc
    // and matching any params
    public setParams(report, context, instanceInfo) {
      if (this.findConfig.current && this.findConfig.current.filter) {
        instanceInfo.parameters = angular.copy(this.findConfig.current.filter);
      }
    }

  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        folder: "@",
        promptForParams: "@",
        findConfig: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "Reports/ReportsPage";
    }
  }

  angular
    .module("pineapples")
    .component("reportPage", new ComponentOptions());
}