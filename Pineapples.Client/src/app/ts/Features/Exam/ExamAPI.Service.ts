﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "exams");

    //svc.addRestangularMethod("read", "get");
    svc.read = (id) => svc.get(id).catch(errorService.catch);
    svc.new = () => {

      let e = new Pineapples.Exams.Exam({
        exID: null
      });
      restAngular.restangularizeElement(null, e, "exams");
      var d = $q.defer();
      d.resolve(e);
      return d.promise;
    }

    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("examsAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
