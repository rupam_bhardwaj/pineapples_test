﻿module Pineapples {

  let viewDefaults = {
    columnSet: 0,
    columnDefs: [
      {
        field: 'exID',
        name: 'Exam ID',
        displayName: 'Exam ID',
        editable: false,
        pinnedLeft: true,
        width: 70,
        cellTemplate: '<div class="ngCellText ui-grid-cell-contents" ng-class="col.colIndex()"><a ng-click="grid.appScope.listvm.action(\'item\',col.field, row.entity);">{{row.entity[col.field]}}</a></div>',
      },
      {
        field: 'exCode',
        name: 'Exam Code',
        displayName: 'Exam Name',
        pinnedLeft: true,
        width: 160,
        lookup: 'examTypes',
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"]
      },
      {
        field: 'exYear',
        name: 'Exam Year',
        displayName: 'Exam Year',
        pinnedLeft: true,
        width: 100,
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"]
      },
      //{
      //  field: 'exUser',
      //  name: 'Exam User',
      //  displayName: 'Exam User',
      //  pinnedLeft: true,
      //  width: 100,
      //  enableSorting: true,
      //  sortDirectionCycle: ["asc", "desc"]
      //},
      //{
      //  field: 'exDate',
      //  name: 'Exam Date',
      //  displayName: 'Exam Date',
      //  pinnedLeft: true,
      //  width: 100,
      //  enableSorting: true,
      //  sortDirectionCycle: ["asc", "desc"]
      //},
    ]
  };

  class ExamParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };
    
  }
  class ExamFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "examsAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.ViewDefaults = viewDefaults;
      this.entity = "exam";
      this.ParamManager = new ExamParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // cycle through any filters in the identity, use them
      // to construct the identity filter
      for (var propertyName in this.identity.filters) {
        switch (propertyName) {
          case "Authority":
          case "District":
          case "ElectorateN":
          case "ElectorateL":
            fltr[propertyName] = this.identity.filters[propertyName];
            break;
        }
      }
      return fltr;
    }
    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "exCode";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "District";
      config.defaults.table.col = "School Type";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      config.tableOptions = "examFieldOptions";
      config.dataOptions = "examDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      this.lookups.getList(config.tableOptions).then((list: Sw.Lookups.ILookupList) => {
        // resolve it when it the lookup is available
        d.resolve(config);
      }, (reason: any) => {
        // swallow the error
        console.log("Unable to get tableOptions for ExamFilter: " + reason);
        d.resolve(config);
      });
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("ExamFilter", ExamFilter);
}
