﻿module Pineapples {

  // TODO: This whole file will eventually be obsolete by the pupilTableGrid.ts, PupilTable.ts and pupilTablePanel.ts in feature/Survey

  export interface ILookupData {
    codeCode: string;
    codeDescription: string;
  }

  export interface IRowColData {
    row: any;
    col: any;
    M: number;
    F: number;
  }

  export class PupilTable {
    public schoolNo: string;
    public schoolName: string;
    public tableName: string;
    public year: number;
    public rows: ILookupData[];
    public cols: ILookupData[];
    public data: IRowColData[];

    constructor(base: any, rows: ILookupData[], cols: ILookupData[], data: IRowColData[]) {
      this.schoolNo = base.schoolNo;
      this.schoolName = base.schoolName;
      this.year = base.year;
      this.tableName = base.tableName;
      this.rows = rows;
      this.cols = cols;
      this.data = this._makeFat(data);
    }

    private _makeFat = (data: IRowColData[]) => {
      var f = new Array();
      for (var i = 0; i < this.rows.length; i++) {
        f[i] = [];
        for (var j = 0; j < this.cols.length; j++) {
          f[i][j] = {};
        }
      }

      data.forEach((e) => {
        var icol = _.findIndex(this.cols, { "codeCode": e.col });
        var irow = _.findIndex(this.rows, { "codeCode": e.row });
        if (icol >= 0 && irow >= 0) {
          f[irow][icol].M = e.M;
          f[irow][icol].F = e.F;
        }

      });
      return f;
    };

    public reduce = () => {
      this.data = this._makeThin(this.data);
    };

    private _makeThin = (fat) => {
      var thin = [];
      var r = 0;
      fat.forEach((row) => {
        var c = 0;
        row.forEach((col) => {
          if (col.F || col.M) {
            thin.push({ row: this.rows[r].codeCode, col: this.cols[c].codeCode, M: col.M, F: col.F });
          }
          c++;
        });
        r++;
      });
      return thin;
    };
  }

  class GridController {

    static $inject = ["schoolsAPI"];

    private _api;
    private _pt: PupilTable;
    private _year: number;
    private _tableName: string;

    constructor(api) {
      this.school = null;
      this._api = api;
      this._year = 2013;
      this._tableName = "ENROL";
    }
    public get pupilTable(): PupilTable {
      return this._pt;
    };

    public school: any;
    public get schoolNo(): string {
      return this.school.C;
    }

    public schoolSelected = (school) => {
      this.school = school;
      this.getPupilTable();
    };

    public getPupilTable() {
      this._api.pupilTable(this.schoolNo, this._year, this._tableName)
        .then((data: PupilTable) => {
          this._pt = data;
          this.school.C = data.schoolNo;
          this.school.N = data.schoolName;
        });
    }

    public savePupilTable() {
      this._api.savePupilTable(this.pupilTable)
        .then((data: PupilTable) => {
          this._pt = data;
          this.school.C = data.schoolNo;
          this.school.N = data.schoolName;
        });
    }

    // Use TS' accessors in lieu of $scope.$watch

    public get year(): number {
      return this._year;
    }

    public set year(newYear: number) {
      this._year = newYear;
      this.getPupilTable();
    }

    public get tableName(): string {
      return this._tableName;
    }

    public set tableName(newTableName: string) {
      this._tableName = newTableName;
      this.getPupilTable();
    }

  }
  angular
    .module("pineapples")
    .controller("gridController", GridController);
}
