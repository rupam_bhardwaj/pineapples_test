namespace Pineapples.SchoolAccreditations {

  let modes = [
    {
      key: "Leadership",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'L1',
            name: 'L1',
            displayName: 'L1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'L2',
            name: 'L2',
            displayName: 'L2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'L3',
            name: 'L3',
            displayName: 'L3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'L4',
            name: 'L4',
            displayName: 'L4',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Teacher Performance",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'T1',
            name: 'T1',
            displayName: 'T1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'T2',
            name: 'T2',
            displayName: 'T2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'T3',
            name: 'T3',
            displayName: 'T3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'T4',
            name: 'T4',
            displayName: 'T4',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Data Management",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'D1',
            name: 'D1',
            displayName: 'D1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'D2',
            name: 'D2',
            displayName: 'D2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'D3',
            name: 'D3',
            displayName: 'D3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'D4',
            name: 'D4',
            displayName: 'D4',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "NCSB",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'N1',
            name: 'N1',
            displayName: 'N1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'N2',
            name: 'N2',
            displayName: 'N2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'N3',
            name: 'N3',
            displayName: 'N3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'N4',
            name: 'N4',
            displayName: 'N4',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Facility",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'F1',
            name: 'F1',
            displayName: 'F1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'F2',
            name: 'F2',
            displayName: 'F2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'F3',
            name: 'F3',
            displayName: 'F3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'F4',
            name: 'F4',
            displayName: 'F4',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "SIP",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'S1',
            name: 'S1',
            displayName: 'S1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'S2',
            name: 'S2',
            displayName: 'S2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'S3',
            name: 'S3',
            displayName: 'S3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'S4',
            name: 'S4',
            displayName: 'S4',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "CO",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'CO1',
            name: 'CO1',
            displayName: 'CO1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'CO2',
            name: 'CO2',
            displayName: 'CO2',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Level Tally",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'LT1',
            name: 'LT1',
            displayName: 'LT1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'LT2',
            name: 'LT2',
            displayName: 'LT2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'LT3',
            name: 'LT3',
            displayName: 'LT3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'LT4',
            name: 'LT4',
            displayName: 'LT4',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'T',
            name: 'T',
            displayName: 'Total',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'SchLevel',
            name: 'SchLevel',
            displayName: 'School Level',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    }
  ]; // modes

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['SchoolAccreditationFilter', pushModes]);
}
