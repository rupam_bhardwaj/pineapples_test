﻿// Teachers Routes
namespace Pineappples.Ndoe {

  let routes = function ($stateProvider) {
    //var mapview = 'TeacherMapView';

    // root state for 'teachers' feature
    let state: ng.ui.IState;
    let statename: string;

    // base ndoe state
    state = {
      url: "^/ndoe",
      abstract: true
    };
    $stateProvider.state("site.ndoe", state);

    // upload state
    state = {
      url: "^/ndoe/upload",

      views: {
        "@": {
          component: "ndoeUploadComponent"
        }
      },
    };
    $stateProvider.state("site.ndoe.upload", state);

    // reload state for testing
    state = {
      url: '^/ndoe/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("ndoe/upload");
        $state.go("site.ndoe.upload");
      }]
    };
    statename = "site.ndoe.reload";
    $stateProvider.state(statename, state);

  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}