﻿module Pineapples.Survey {

  let routes = ($stateProvider: angular.ui.IStateProvider) => {

    // root state for 'survey' feature
    let state: ng.ui.IState;
    let statename: string;

    // Should this be abstract, I think so.
    statename = "site.surveys";
    state = {
      url: "^/surveys",
      abstract: true
    };
    $stateProvider.state(statename, state);

    // old standalone state accessed from menu "Enrolment Entry" before
    statename = "site.surveys.item";
    state = {
      url: "^/surveys/{schoolNo}/{year}",
      views: {
        "@": {
          templateUrl: "survey/Survey",
          controller: "ModelController",
          controllerAs: "vm"
        }
      },
      resolve: {
        theModel: ["$stateParams", (s) => {
          return s;
        }]
      }
    }
    $stateProvider.state(statename, state);

    // state to edit surveys within site.schools.list.item parent state
    //statename = "site.schools.list.item.survey";
    //state = {
    //  url: "^/schools/{schoolNo}/surveys/{year}",
    //  views: {
    //    "@": { //componentsurvey@site.schools.list.item ?
    //      component: "survey"
    //    }
    //  },
    //  resolve: {
    //    schoolSurvey: ["$stateParams", (s) => {
    //      return s;
    //    }]
    //  }
    //};
    //$stateProvider.state(statename, state);

    statename = "site.surveys.reload";
    state = {
      url: '^/surveys/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("survey/Survey");        
        $state.go("site.surveys.item");
      }]
    };
    $stateProvider.state(statename, state);

  };

  angular
    .module("pineapples")
    .config(["$stateProvider", routes]);
}
