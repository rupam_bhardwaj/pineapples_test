﻿namespace Pineapples.Survey {

  export interface ILookupData {
    code: string;
    name: string;
  }

  export interface IRowData {
    resourceCode: string;
    available: number;
    adequate: number;
    num: number;
    qty: number;
    condition: number;
    functioning: any; // what type?
    classLevel: any; // what type?
    location: any; // what type?
  }

  //export class ResourceCategory {
  //  public categoryCode: string;
  //  public categoryName: string;
  //  public showAdequate: number;
  //  public showAvailable: number;
  //  public showNum: number;
  //  public showQty: number;
  //  public showCondition: number;
  //  public showFunctioning: number;
  //}

  export class ResourceList {

    public schoolNo: string;
    public schoolType: string;
    public schoolName: string;
    public category: string;
    public resourceCategory: any;
    public year: number;
    public surveyID: number;
    public rows: ILookupData[];
    public data: IRowData[];

    constructor(data: any) {
      angular.extend(this, data);
      this.data = this._makeFat(this.data);
    }

    private _makeFat = (data: IRowData[]) => {
      var f = new Array();
      for (var i = 0; i < this.rows.length; i++) {
        var r = this.rows[i];
        f[i] = { resourceCode: r.code };
      }

      data.forEach((e) => {
        var irow = _.findIndex(this.rows, { "code": e.resourceCode });
        if (irow >= 0) {
          f[irow] = e;
          f[irow].available = (e.available === -1 ? true : false);
          f[irow].functioning = (e.functioning === -1 ? true : false);
          f[irow].adequate = (e.adequate === -1 ? true : false);
        }
      });
      return f;
    };

    // TODO: adapt below
    //public reduce = () => {
    //  this.data = this._makeThin(this.data);
    //};

    //private _makeThin = (fat) => {
    //  var thin = [];
    //  var r = 0;
    //  fat.forEach((row) => {
    //    var c = 0;
    //    row.forEach((col) => {
    //      if (col.F || col.M) {
    //        thin.push({ row: this.rows[r].codeCode, col: this.cols[c].codeCode, M: col.M, F: col.F });
    //      }
    //      c++;
    //    });
    //    r++;
    //  });
    //  return thin;
    //};

  }

}