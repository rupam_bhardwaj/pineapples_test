﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "survey");
    svc.read = (id) => svc.get(id).catch(errorService.catch);

    svc.audit = (schoolNo: string, year: number) => {
      let path: string = "audit" + "/" + schoolNo + "/" + year ;
      return svc.customGET(path);
    };
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("surveyAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
