﻿module Pineapples {

  let routes = ($stateProvider: angular.ui.IStateProvider) => {
    var featurename = 'PerfAssess';
    var filtername = 'PerfAssessFilter';
    var templatepath = "perfassess";
    var url = "perfassess";
    var tableoptions = 'paFields';

    var usersettings = null;

    // root state for 'school' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath);

    // default 'api' in this feature is schoolsAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "perfAssessAPI";

    let statename = "site.perfassess";
    $stateProvider.state("site.perfassess", state);

    // takes an editable list state
    state = Sw.Utils.RouteHelper.editableListState("PerfAssess");
    statename = "site.perfassess.list";
    state.url = "^/perfassess/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '/chart',

      views: {
        "navbar@": {
          templateUrl: "common/chartgroupparam",        // local becuase we have to get acess to the data layer
          controller: "TableParamsController",
          controllerAs: "vm"
        },
        "@": {
          templateUrl: "common/chart",
          controller: "ChartController",
          controllerAs: "vm"
        }

      },
      resolve: {
        palette: function () { return new Sw.Maps.ColorBrewerPalette(); },
        mapColorScale: function () { return new Sw.Maps.MapColorScale(); }
      }
    };
    $stateProvider.state("site.perfassess.chart", state);

    $stateProvider
      .state("site.perfassess.entry", {
        url: "/entry",
        views: {
          "@": {
            templateUrl: "perfassess/entry",
            controller: "perfAssessController",
            controllerAs: "vm"
          }
        },
      });
  };

  angular
    .module("pineapples")
    .config(["$stateProvider", routes]);
}
