﻿namespace Pineapples.PerfAssess {

  export class PerfAssess extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(paData) {
      super();
      this._transform(paData);
      angular.extend(this, paData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let qr = new PerfAssess(resultSet);
      return qr;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).Framework + '-' + (<any>this)._id();
    }

    public _type() {
      return "perfassess";
    }

    public _id() {
      return (<any>this).Id;
    }
    public _transform(newData) {
      // convert these incoming data values
      this._transformDates(newData, ["Date"]);
      return super._transform(newData);
    }

    // default beforeSave drops any array properties (ie related records) 
    // however, in this case we need the indicators array to Save, so just drop IndicatorLevels
    public _beforeSave() {
      this.IndicatorLevels = null;
      return this;
    }

    // collections
    public Indicators: any[];
    public IndicatorLevels: any[];
  }
}
