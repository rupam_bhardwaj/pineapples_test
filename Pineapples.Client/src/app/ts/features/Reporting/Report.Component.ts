﻿namespace Pineapples.Reporting {
  class Controller {

    static $inject = ["$mdDialog", "reportManager"];
    constructor(
      private mdDialog: ng.material.IDialogService,
      private rmgr: Pineapples.Reporting.IReportManagerService) { }
    public report: Report;
    public tooltip: string;
    public reportContext: any;
    public setParams: any;

    public job: IReportJob;
    public run() {
      let cancel: boolean = false;
      let instanceInfo = { parameters: null, filename: null, cancel: false };
      this.setParams({ report: this.report, context: this.reportContext, instanceInfo: instanceInfo });
      if (instanceInfo.cancel) {
        return;
      }

      if (instanceInfo.parameters === null && this.reportContext) {
        // this allows a caller to directly specify the report arguments by passing that as context
        instanceInfo.parameters = this.reportContext;
      }
      this.job = this.rmgr.createJob(this.report, instanceInfo);
    }

    public showError(event) {
      if (this.job.error) {
        this.mdDialog.show(
          this.mdDialog.alert()
            .clickOutsideToClose(true)
            .htmlContent(this.job.error)
            .ok('Close')
        )
      }
      event.stopPropagation();
    }
  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor(layout) {
      this.bindings = {
        report: '<',
        tooltip: "@",
        reportContext: '<context',
        setParams: "&"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "reports/reportDefComponent/" + layout;
    }
  }
  angular
    .module("pineapples")
    .component("componentReportDef", new ComponentOptions("card"))
    .component("reportDefCard", new ComponentOptions("card"))
    .component("reportDefButton", new ComponentOptions("button"))
    .component("reportDefIcon", new ComponentOptions("icon"))
    .component("reportDefRow", new ComponentOptions("row"));
}