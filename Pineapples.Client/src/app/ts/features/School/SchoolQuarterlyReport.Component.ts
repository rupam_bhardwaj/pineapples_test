﻿namespace Pineapples.Schools {

  interface IBindings {
    quarterlyreports: any;
  }

  class Controller implements IBindings {
    public quarterlyreports: any;

    constructor() { }

    public $onChanges(changes) {
      if (changes.quarterlyreports) {
        console.log('$onChanges quarterlyreports: ', this.quarterlyreports);
      }
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        quarterlyreports: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "school/quarterlyreports";
    }
  }

  angular
    .module("pineapples")
    .component("schoolQuarterlyReports", new Component());
}