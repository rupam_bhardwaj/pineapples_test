﻿module Pineapples {

  let viewDefaults = {
    columnSet: 0,
    columnDefs: [
      {
        field: 'schNo',
        name: 'School ID',
        displayName: 'School ID',
        headerCellFilter: 'vocab',
        editable: false,
        width: 80,
        pinnedLeft: true,
        cellTemplate: '<div class="ngCellText ui-grid-cell-contents" ng-class="col.colIndex()"><a  ng-click="grid.appScope.listvm.action(\'item\',col.field, row.entity);">{{row.entity[col.field]}}</a></div>'
      },
      {
        field: 'schName',
        name: 'Name',
        editable: false,
        width: 180,
        pinnedLeft: true
      },
      {
        field: 'schType',
        name: 'schType',
        displayName: 'School Type',
        headerCellFilter: 'vocab',
        editable: false,
        width: 105,
        pinnedLeft: true,
        lookup: 'schoolTypes',
      },
      {
        field: 'schClass',
        name: 'Class',
        displayName: 'School Class',
        headerCellFilter: 'vocab',
        editable: false,
        width: 100,
        pinnedLeft: true,
        lookup: 'schoolClasses',
      },
      {
        field: 'schAuth',
        name: 'auth',
        displayName: 'Authority',
        editable: false,
        pinnedLeft: true,
        lookup: 'authorities',
        width: 80
      },
    ]
  };

  class SchoolParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };

    public toFlashString(params: any) {
      var tt = ['SchoolType', 'District',
        'Authority', 'SchoolName'
      ];
      return this.flashStringBuilder(params, tt);
    }

    public fromFlashString(flashstring: string) {
      if (flashstring.trim().length === 0) {
        return;
      }
      let params: any = {};
      var parts = this.tokenise(flashstring);         // 8 3 2015 smarter Tokenise
      var parsed = [];
      for (var i = 0; i < parts.length; i++) {
        var s = parts[i].trim();
        var S = s.toUpperCase();

        if (this.cacheFind(S, params, "schoolTypes", "SchoolType")) {
          continue;
        }
        if (this.cacheFind(S + ' SCHOOL', params, "schoolTypes", "SchoolType")) {
          continue;
        }
        if (this.cacheFind(S, params, "districts", "District")) {
          continue;
        }
        if (this.cacheFind(S, params, "authorities", "Authority")) {
          continue;
        }
        if (s.indexOf('*') >= 0 || s.indexOf('?') >= 0 || s.indexOf('%') >= 0) {
          if (!params.SchoolName) {
            params.SchoolName = s;
            parsed.push(s);
            continue;
          }
        }
        if (parts.length === 1) {
          params.schNo = s;
          parsed.push(s);
        }
      }

      return params;
    }

    protected getParamString(name, value) {
      switch (name) {
        case 'AwardType':
          return this.lookups.findByID('awardTypes', value).C;
        default:
          return value.toString();
      }

    }
  }
  class SchoolFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "schoolsAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.ViewDefaults = viewDefaults;
      this.entity = "school";
      this.ParamManager = new SchoolParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // cycle through any filters in the identity, use them
      // to construct the identity filter
      for (var propertyName in this.identity.filters) {
        switch (propertyName) {
          case "Authority":
          case "District":
          case "ElectorateN":
          case "ElectorateL":
            fltr[propertyName] = this.identity.filters[propertyName];
            break;
        }
      }
      return fltr;
    }

    public createTableCalculator() {
      return new SchoolTableCalculator();
    }
    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "SchNo";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "District";
      config.defaults.table.col = "School Type";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      config.tableOptions = "schoolFieldOptions";
      config.dataOptions = "schoolDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      this.lookups.getList(config.tableOptions).then((list: Sw.Lookups.ILookupList) => {
        d.resolve(config);
      }, (reason: any) => {
        console.log("Unable to get tableOptions for SchoolFilter: " + reason);
        d.resolve(config);
      });
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("SchoolFilter", SchoolFilter);
}
