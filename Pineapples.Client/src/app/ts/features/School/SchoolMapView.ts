namespace Pineapples.Schools {
  /*
  School Map View helper

  this service provides handlkers for creating markers for schools
  These handlers are called by the MapController when required

  */
  // return the icon to represent the row data - ie the school

 

  class SchoolMapView extends sw.common.maps.mapViewHelper {


    public LatField: string = 'Lat';
    public LngField: string = 'Lng';
    public TitleField: string = 'Name';
    public EntityPlural: string = "schools";

    private st: {
      [name: string]: any
    } = {};
        
    public RowIcon(row, ignoreColoc) {
      if (!ignoreColoc && (row.coloc > 1)) {

        return this.Icons.circle.default();

        //    };
      };
      var icon = this.Icons.circle;
      switch (row.SchoolType) {
        case 'PS':
            return this.getIcon('green');

        case 'CHS':
          return this.getIcon('red');

        case 'ECE':
          return this.getIcon('blue');

        case 'PSS':
          return this.getIcon('orange');

        case 'NSS':
          return this.getIcon('yellow');
        case "RTC":
          return this.getIcon("purple");
      }
      return icon.default();

    };

    private getIcon(color) {
      if (!this.st[color]) {
        var icon = angular.copy(this.Icons.circle.default());
        icon.fillColor = color;
        this.st[color] = icon;
      }
      return this.st[color];
    }
  }
  angular
    .module('pineapples')
    .service('SchoolMapView', [SchoolMapView]);
}