﻿namespace Pineapples.Indicators {
  function indicatorsRoute(sp: ng.ui.IStateProvider, context: string, edLevel: string, schooltype: string, sector: string) {
    let stateName: string = "";
    if (context === "") {
      stateName = "site.indicators." + edLevel.toLowerCase();
    }
    else {
      stateName = "site.indicators." + context + "." + edLevel.toLowerCase();
    };
    let url = "/" + edLevel.toLowerCase();
    let templateUrl = "indicators/" + edLevel.toLowerCase();

    let data: any = {
      selectedEdLevel: edLevel.toUpperCase(),
      selectedSchoolType: schooltype,
      selectedSector: sector
    };
    let state: ng.ui.IState = {
      url: url,
      data: data,
      views: {
        "@": {
          component: "indicatorsComponent"
        }

      }
    };
    sp.state(stateName, state);
  }

  /**
   * Generate a route to create an Indicators page for a district
   * This only needs to modify the call to get the vermdata
   * @param sp stateProviderservice - needed to register the new state
   * @param context
   * @param edLevel
   * @param schooltype
   * @param sector
   * @param districtCode
   */
  function indicatorsDistrictRoute(sp: ng.ui.IStateProvider, context: string, edLevel: string, schooltype: string, sector: string, districtCode: string) {
    // construct the route name including the district
    let stateName: string = "";
    if (context === "") {
      stateName = "site.indicators." + edLevel.toLowerCase() + "." + districtCode.toLowerCase(); // e.g. site.indicators.pri.pni
    }
    else {
      stateName = "site.indicators." + context + "." + edLevel.toLowerCase() + "." + districtCode.toLowerCase();        // e.g. site.indicators.fedemis.pri.ksa
    };

    let url = "/" + districtCode.toLowerCase();                                // #/indicators/fedemis/pri/yap
    let templateUrl = "indicators/" + edLevel.toLowerCase();                     // doesn't change - same template in each district

    let data: any = {
      selectedEdLevel: edLevel.toUpperCase(),
      selectedSchoolType: schooltype,
      selectedSector: sector,
      selectedDistrict: districtCode
    };
    let state: ng.ui.IState = {
      url: url,
      data: data,
      views: {
        "@": {
          component: "indicatorsComponent"
        }

      },
      // resolves are now specific to the route for districts, allowing the customised vermpaf
      resolve: {
        // resolve the calculator used in the component - this will get a cached one
        // or else read the required vermdata and make one
        indicatorCalc: ['IndicatorsMgr', function (mgr: IndicatorsMgr) {
          return mgr.getCalculator(districtCode);
        }]
      }
    };
    sp.state(stateName, state);
  }
  var routes = function ($stateProvider) {
    $stateProvider
      .state('site.indicators', {
        url: '/indicators',
        abstract: true,

        resolve: {
          // resolve the calculator
          indicatorCalc: ['IndicatorsMgr', function (mgr: IndicatorsMgr) {
            return mgr.getCalculator("");
          }]
        }
      })

    // The following are particular to each country where this EMIS is deployed
    // The code should reflect what has been setup in the database tables lkpEducationLevels, SchoolTypes, EducationSectors
    // This should also be configured in the IdentitiesP's Navigation table

    // KEMIS Indicators pages
    $stateProvider
      .state('site.indicators.kemis', {
        url: '/kemis',
        abstract: true,
      });
    indicatorsRoute($stateProvider, "kemis", "PRI", "P", "PRI");
    indicatorsRoute($stateProvider, "kemis", "JS", "JS", "JSS");
    indicatorsRoute($stateProvider, "kemis", "SS", "SS", "SEC");

    // SIEMIS Routes
    $stateProvider
      .state('site.indicators.siemis', {
        url: '/siemis',
        abstract: true,
      });
    indicatorsRoute($stateProvider, "siemis", "PRI", "PS", "PRI");
    indicatorsRoute($stateProvider, "siemis", "JS", "CHS", "JS");
    indicatorsRoute($stateProvider, "siemis", "SS", "PSS", "SEC");
    indicatorsRoute($stateProvider, "siemis", "SEC", "PSS", "SEC");

    // MIEMIS Routes
    $stateProvider
      .state('site.indicators.miemis', {
        url: '/miemis',
        abstract: true,
      });
    indicatorsRoute($stateProvider, "miemis", "PRI", "PRI", "PS");
    indicatorsRoute($stateProvider, "miemis", "SEC", "SEC", "SS");
    indicatorsRoute($stateProvider, "miemis", "ECE", "ECE", "ECE");

    // FedEMIS Routes
    $stateProvider
      .state('site.indicators.fedemis', {
        url: '/fedemis',
        abstract: true,
      });
    indicatorsRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE");
    indicatorsRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI");
    indicatorsRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC");
    // state specific routes
    indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE","PNI");
    indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "PNI");
    indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "PNI");

    indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE", "YAP");
    indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "YAP");
    indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "YAP");

    indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE", "KSA");
    indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "KSA");
    indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "KSA");

    indicatorsDistrictRoute($stateProvider, "fedemis", "ECE", "STECE", "ECE", "CHK");
    indicatorsDistrictRoute($stateProvider, "fedemis", "PRI", "STPS", "PRI", "CHK");
    indicatorsDistrictRoute($stateProvider, "fedemis", "SEC", "STSS", "SEC", "CHK");

    // PalEMIS Routes
    $stateProvider
      .state('site.indicators.palemis', {
        url: '/palemis',
        abstract: true,
      });
    indicatorsRoute($stateProvider, "palemis", "ECE", "STECE", "ECE");
    indicatorsRoute($stateProvider, "palemis", "PRI", "STPS", "PRI");
    indicatorsRoute($stateProvider, "palemis", "SEC", "STSS", "SEC");

    $stateProvider
      .state('site.indicators.drill', {
        url: '/drill',
        params: { theModel: [], drill: "enrol" },
        views: {
          "actionpane@site.indicators": {
            //https://github.com/angular-ui/ui-router/wiki#alternative-ways-to-set-the-template
            templateUrl: function (stateParams) {
              return "indicators/drill/" + stateParams.drill;
            },
            controller: 'ModelController',
            controllerAs: "vm"
          }
        },
        resolve: {
          theModel: ['$stateParams', function ($stateParams) {
            return $stateParams.theModel;
          }]
        }
      });
    $stateProvider
      .state('site.admin', {
        url: '/admin',
        abstract: true,
      });

    $stateProvider
      .state('site.admin.warehouse', {
        url: '/warehouse',
        views: {
          "@": {
            template: `<component-warehouse-admin></component-warehouse-admin>`
          }
        }
      });

  };

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])
}
