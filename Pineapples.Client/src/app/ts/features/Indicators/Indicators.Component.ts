﻿namespace Pineapples.Indicators {

  interface IBindings {
    indicatorCalc: IndicatorCalc, 
    pageLayout: string;
  }
  class Controller implements IBindings {

    public selectedEdLevel: string;
    public selectedSchoolType: string;
    public selectedSector: string;
    public selectedDistrict: string;

    public title: string = '';
    public source: string = '';

    // Change to Indicators type when converting Indicators function object from
    // IndicatorsController.js
    public selectedYearData: any = {};
    public baseYearData: any = {};

    private statedata: any;
    private baseState: string;

    // manipulated through public accessors selectedYear/baseYear
    private _selYear: number;
    private _bYear: number;

    public indicatorCalc: IndicatorCalc;
    public pageLayout: string;

    static $inject = ["$mdDialog", "IndicatorsMgr", "$state", "Lookups"];

    constructor(public mdDialog: ng.material.IDialogService, protected indicatorsMgr: IndicatorsMgr, protected $state: ng.ui.IStateService, public lookups: Sw.Lookups.LookupService) {

      this.statedata = $state.current.data;
      this.baseState = $state.current.name;

      this.selectedEdLevel = this.statedata.selectedEdLevel; // 'PRI';
      this.selectedSchoolType = this.statedata.selectedSchoolType;
      this.selectedSector = this.statedata.selectedSector;
      this.selectedDistrict = this.statedata.selectedDistrict;

    }

    public $onChanges(changes) {
      // just force the load of the selectedYear and baseYear
      this.setDisplay();
    }

    public showIndicatorInfo(indicator: string) {
      let options: any = {
        clickOutsideToClose: true,
        templateUrl: "indicators/info/Indicator" + indicator,
        controller: DialogController,
        controllerAs: "dvm",
        bindToController: true,
      }
      this.mdDialog.show(options);
    }

    public action(action: any, yr: any): void {
      let startyoe: number = this.indicatorCalc.edLevelFirstYoE(yr, this.selectedEdLevel);
      let endyoe: number = this.indicatorCalc.edLevelLastYoE(yr, this.selectedEdLevel);
      let startage: number = this.indicatorCalc.edLevelStartAge(yr, this.selectedEdLevel);
      let model: any = {
        year: yr,
        edLevel: this.selectedEdLevel,
        startAge: startage,
        startYoE: startyoe,
        atag: this.indicatorsMgr.get(this.indicatorCalc, yr, this.selectedEdLevel, this.selectedSector, this.selectedDistrict)
      };

      switch (action) {
        case "enrol":
        case "pop":
          // get the start year of ed and end year of ed
          // make an array of these
          let e: Array<any> = [];
          let p: Array<any> = [];
          for (let i = startyoe; i <= endyoe; i++) {
            let enrol: any = {};
            enrol.yoe = i;
            enrol.T = this.indicatorCalc.yoeEnrol(yr, i, "");
            enrol.M = this.indicatorCalc.yoeEnrol(yr, i, "M");
            enrol.F = this.indicatorCalc.yoeEnrol(yr, i, "F");
            e.push(enrol);

            let pop: any = {};
            pop.yoe = i;
            pop.age = i - startyoe + startage;
            pop.T = this.indicatorCalc.yoePop(yr, i, "");
            pop.M = this.indicatorCalc.yoePop(yr, i, "M");
            pop.F = this.indicatorCalc.yoePop(yr, i, "F");
            p.push(pop);
          };
          model.enrol = e;
          model.pop = p;
          break;
        case "oage":
          break;
      };
      // pass the whole IndicatorsMgr cache object
      switch (action) {
        case "enrol":
          model.title = "Enrolment by Year Level";
          break;
        case "pop":
          model.title = "Population by Year Level";
          break;
        case "oage":
          model.title = "Enrolment by Age";
      }

      //$state.go(this.baseState + ".drill", { theModel: model, drill: action });
      this.$state.go("site.Indicators.drill", { theModel: model, drill: action });
    };
    // Use TS' accessors in lieu of $scope.$watch

    private setDisplay() {
      this.selectedYear = this.selectedYear;
      this.baseYear = this.baseYear;
    }

    public get selectedYear(): number {
      return this.indicatorsMgr.selectedYear;   // put this in the service so it is remembered from page to page
    }

    public set selectedYear(newValue: number) {
      this.indicatorsMgr.selectedYear = newValue;
      this.selectedYearData = this.indicatorsMgr.get(this.indicatorCalc, this.selectedYear, this.selectedEdLevel, this.selectedSector, this.selectedDistrict);
      if (this.selectedYear <= this.baseYear) {
        this.baseYear = this.selectedYear + 1;
      }
    }

    public get baseYear(): number {
      return this.indicatorsMgr.baseYear;
    }

    public set baseYear(newValue: number) {
      this.indicatorsMgr.baseYear = newValue;
      this.baseYearData = this.indicatorsMgr.get(this.indicatorCalc, this.baseYear, this.selectedEdLevel, this.selectedSector, this.selectedDistrict);
      if (this.selectedYear <= this.baseYear) {
        this.selectedYear = this.baseYear + 1;
      }
    }

    // allow for variable template using the ng-include method

    public get selectedTemplate() {
      return "indicators/" + (this.pageLayout ? this.pageLayout : this.selectedEdLevel); 
    }
    
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      this.bindings = {
        indicatorCalc: "<",
        pageLayout: "@"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.template = `<div ng-include='vm.selectedTemplate'></div>`;
    }
  }

  // popup dialog for info log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog"];
    constructor(mdDialog: ng.material.IDialogService) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("indicatorsComponent", new Component());
}
