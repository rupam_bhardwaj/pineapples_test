﻿namespace Pineapples.Indicators {

  /**
   * @class IndicatorSet
   * @description A plain object to hold precalculated indicators values.
   * The purpose is so that we do not do the expensive vermdata loookups on each digest cycle. i.e. a 'facade'
   * @see IndicatorsMgr, IndicatorsData, IndicatorsController, IndicatorsFactory
   */
  export class IndicatorSet {

    public Population: any;
    public Enrolment: any;
    public NetEnrolment: any;
    public Repeaters: any;
    public GER: any;
    public NER: any;
    public Intake: any;
    public NetIntake: any;
    public GIR: any;
    public NIR: any;
    public YearsOfSchooling: any;
    public OfficialStartAge: any;
    public AgeRange: any;

    // The teacher related indicators below are in two groups (Teachers/Teachers2, etc.). They are the same data but from
    // a different location in the XML file and come from a different source in the DB
    // TeacherTraining vs SurveyYearTeacherQual
    // the source of this data is set with the TEACHER_QUALCERT_SOURCE flag in sysParam table and care must be taken to use
    // the correct indicator depending on your teacher qualifications data source.
    public Teachers: any;
    public TeachersCert: any;
    public TeachersCertP: any;
    public TeachersQual: any;
    public TeachersQualP: any;
    public PTR: any;
    public CertPTR: any;
    public QualPTR: any;   
    public Teachers2: any;
    public TeachersCert2: any;
    public TeachersCertP2: any;
    public TeachersQual2: any;
    public TeachersQualP2: any;
    public PTR2: any;
    public CertPTR2: any;
    public QualPTR2: any;

    public RepRate: any;
    public Survival4: any;
    public Survival6: any;
    public Survival8: any;
    public Survival12: any;
    public GIRLast: any;

    public constructor(protected p: IndicatorCalc, protected year: number, protected edLevel: string, protected edSector: string) {

      let edLevelNode = p.getEdLevelNode(year, edLevel);

      this.Population = p.getMFT(edLevelNode, 'pop');
      this.Enrolment = p.getMFT(edLevelNode, 'enrol');
      this.NetEnrolment = p.getMFT(edLevelNode, 'nenrol');
      this.Repeaters = p.getMFT(edLevelNode, 'rep');
      this.GER = p.getMFT(edLevelNode, 'ger');
      this.NER = p.getMFT(edLevelNode, 'ner');
      this.Intake = p.getMFT(edLevelNode, 'intake');
      this.NetIntake = p.getMFT(edLevelNode, 'nIntake');
      this.GIR = p.getMFT(edLevelNode, 'gir');
      this.NIR = p.getMFT(edLevelNode, 'nir');
      this.YearsOfSchooling = p.edLevelNumYears(year, edLevel);
      this.OfficialStartAge = p.edLevelStartAge(year, edLevel);
      this.AgeRange = (this.OfficialStartAge).toString() + "-" + (this.OfficialStartAge + this.YearsOfSchooling - 1).toString() + " yo";

      let teacherNode = p.getSectorTeacherNode(year, edSector);

      this.Teachers = p.getMFT(teacherNode, "teachers");
      this.TeachersCert = p.getMFT(teacherNode, "cert");
      this.TeachersCertP = p.getMFT(teacherNode, "certperc");
      this.TeachersQual = p.getMFT(teacherNode, "cert");
      this.TeachersQualP = p.getMFT(teacherNode, "certperc");
      this.PTR = p.sectorPTR(year, edSector);
      this.CertPTR = p.sectorCertPTR(year, edSector);
      this.QualPTR = p.sectorQualPTR(year, edSector);

      let teacherNode2 = p.getSectorTeacherNode2(year, edSector);

      this.Teachers2 = p.getMFT(teacherNode2, "Teachers");
      this.TeachersCert2 = p.getMFT(teacherNode2, "Cert");
      this.TeachersCertP2 = p.getMFT(teacherNode2, "CertPerc");
      this.TeachersQual2 = p.getMFT(teacherNode2, "Qual");
      this.TeachersQualP2 = p.getMFT(teacherNode2, "QualPerc");
      this.PTR2 = p.get(teacherNode2, "PTR");
      this.CertPTR2 = p.get(teacherNode2, "CertPTR");
      this.QualPTR2 = p.get(teacherNode2, "QualPTR");

      // get last year's ed level node
      edLevelNode = p.getEdLevelNode(year - 1, edLevel);
      let LYE = p.getMFT(edLevelNode, 'enrol')       // last year's enrol

      this.RepRate = {
        T: this.Repeaters.T / LYE.T,
        M: this.Repeaters.M / LYE.M,
        F: this.Repeaters.F / LYE.F,
        I: (this.Repeaters.F / LYE.F) / (this.Repeaters.M / LYE.M),
        P: (this.Repeaters.F / LYE.F) / (this.Repeaters.T / LYE.T)
      };

      this.Survival4 = {
        T: p.yoeSurvivalTo(year - 1, 4, ''),
        M: p.yoeSurvivalTo(year - 1, 4, 'M'),
        F: p.yoeSurvivalTo(year - 1, 4, 'F'),
      };

      this.Survival4.I = this.Survival4.F / this.Survival4.M;
      this.Survival4.P = this.Survival4.F / this.Survival4.T;

      this.Survival6 = {
        T: p.yoeSurvivalTo(year - 1, 6, ''),
        M: p.yoeSurvivalTo(year - 1, 6, 'M'),
        F: p.yoeSurvivalTo(year - 1, 6, 'F'),
      };
      this.Survival6.I = this.Survival6.F / this.Survival6.M;
      this.Survival6.P = this.Survival6.F / this.Survival6.T;

      this.Survival8 = {
        T: p.yoeSurvivalTo(year - 1, 8, ''),
        M: p.yoeSurvivalTo(year - 1, 8, 'M'),
        F: p.yoeSurvivalTo(year - 1, 8, 'F'),
      };
      this.Survival8.I = this.Survival8.F / this.Survival8.M;
      this.Survival8.P = this.Survival8.F / this.Survival8.T;

      this.Survival12 = {
        T: p.yoeSurvivalTo(year - 1, 12, ''),
        M: p.yoeSurvivalTo(year - 1, 12, 'M'),
        F: p.yoeSurvivalTo(year - 1, 12, 'F'),
      };
      this.Survival12.I = this.Survival12.F / this.Survival12.M;
      this.Survival12.P = this.Survival12.F / this.Survival12.T;

      // gir into last year
      let lyoe = p.edLevelLastYoE(year, edLevel);
      let yoeNode = p.getYoENode(year, lyoe);

      this.GIRLast = p.getMFT(yoeNode, 'gir');
    }

  }

  /**
   * @class IndicatorsMgr
   * @description A service to construct the Indicators object required by the particular page.
   * @see Indicators, IndicatorsData, IndicatorsController, IndicatorsFactory
   */
  export class IndicatorsMgr {

    public setcache: any;
    public calccache: any
    //public p: any;

    static $inject = ["IndicatorsAPI"];
    constructor(public api: Pineapples.Api.IIndicatorsApi) {
      this.setcache = {};
      this.calccache = {};
      this.selectedYear = 2015;       // to do 
      this.baseYear = 2014;
    }

    /**
     * return a calculator from the cache, or else a promise to a calculator
     * @param districtCode
     * @param xml
     */
    public getCalculator(districtCode: string) {
      let cachekey = districtCode;
      if (!this.calccache[cachekey]) {
        // read the required data ( may already be cached at the http level )
        // then use it to construct the IndicatorSet
        let promise;
        if (districtCode == "") {
          promise = this.api.vermdata();
        } else {
           // new method in api class to get district version
          promise = this.api.vermdataDistrict(districtCode);
        }
        return promise.then((xml) => {
          this.calccache[cachekey] = new IndicatorCalc(xml);
          return this.calccache[cachekey];
        });
      }
      return this.calccache[cachekey];
    }

    public get(indicatorCalc: IndicatorCalc, surveyYear: number, edLevelCode: string, edSector: string, districtCode: string) {
      let cachekey = [surveyYear, edLevelCode, edSector, districtCode].join("_");
      if (!this.setcache[cachekey]) {
        // read the required data ( may already be cached at the http level )
        // then use it to construct the IndicatorSet
        let indicatorSet = new IndicatorSet(indicatorCalc, surveyYear, edLevelCode, edSector);
        this.setcache[cachekey] = indicatorSet;
      }
      return this.setcache[cachekey];
    }

    public selectedYear: number;      // place to hold the comparison year in a two year array
    public baseYear: number;
  }

  angular
    .module('pineapples')
    .service('IndicatorsMgr', IndicatorsMgr);

}