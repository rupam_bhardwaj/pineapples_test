﻿namespace Pineapples.Filters {

  class pineapplesFilters {

    /**
     * surveyYearFilter is an angular filter to present the survey year
     * in a context-specific way. ie usually survey year corresponds to a calandar year
     * in which case it may be presented simple as a numeric : e.g. 2015
     * However in some contexts the survey (or "school")  year may cross into 2 calandar years
     * in which case it is customary to present it in a form like 2015-16 or 2015-2016, or even SY2015-16
     * This filter handles this based on the sysParam entry SURVEY_YEAR_FORMAT
     * The text of this is a template for the presentation. It may include these tokens:
     * -- yyyy current year 4 digits
     * -- yy current year 2 digits
     * -- nnnn next year (ie survey year + 1) 4 digits
     * -- nn next year 2 digits
     * Other text in the template is output unchanged.
     * Exmaples (year = 2015)
     * SYyyyy-nn    =>SY2015-16
     * SY yyyy nnnn   => SY 2015 2016
     * Survey yy/nn   => survey 15/16
     * CENSUSyy       => CENSUS15
     * (no sysparam)  => 2015
     * If the sysParam entry is not defined, function returns survey year unmodified
    **/
    static surveyYearFilter(lookups: Sw.Lookups.LookupService) {
      let template: string = null;
      let fcn = (value) => {
        if (template == null) {
          // cache the template one time only
          template = lookups.byCode("sysParams", "SURVEY_YEAR_FORMAT","N");
          if (template === null) {
            template = "";
          }

        }
        if (template === "") {
          return value;
        }
        // force the return of name
        let ret = template;
        
        let y = String(value);
        let y1 = String(value + 1);
        ret = ret.replace("nnnn", y1);
        ret = ret.replace("nn", y1.substr(2));
        ret = ret.replace("yyyy", y);
        ret = ret.replace("yy", y.substr(2));

        return ret;
      };
      return fcn;
    }

  }
  angular
    .module("pineapples")
    .filter("surveyYear", ['Lookups', pineapplesFilters.surveyYearFilter])
}
