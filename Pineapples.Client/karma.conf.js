/// <reference path="scripts/vendor/angular-bootstrap/ui-bootstrap-tpls.js" />
/// <reference path="scripts/vendor/angular-bootstrap/ui-bootstrap-tpls.js" />
/// <reference path="scripts/vendor/lodash/dist/lodash.compat.js" />
/// <reference path="scripts/vendor/lodash/dist/lodash.compat.js" />
// Karma configuration

module.exports = function(config) {
  config.set({

    // list of files / patterns to load in the browser
    
    // base path, that will be used to resolve files and exclude
    basePath: '',

  frameworks: ["jasmine"],

  // list of files / patterns to load in the browser
  files: [
    'http://maps.google.com/maps/api/js',
    'scripts/vendor/lodash/dist/lodash.compat.js',
    'scripts/vendor/angular/angular.js',
    'scripts/vendor/angular-bootstrap/ui-bootstrap-tpls.js',
    'scripts/vendor/angular-ui-grid/ui-grid.js',
    'scripts/vendor/angular-ui-router/release/angular-ui-router.js',
    'scripts/vendor/ngmap/build/scripts/ng-map.js',
    'scripts/vendor/restangular/dist/restangular.js',
    'scripts/vendor/angular-dimple/dist/angular-dimple.js',
    'scripts/vendor/angular-dc/dist/angular-dc.js',
    'scripts/vendor/ng-token-auth/customised/ng-token-auth.js',
    'bower_components/angular-mocks/angular-mocks.js',
    'scripts/app/theme.js',
    'scripts/app/pineapples.js',

    'test/tests.js'
  ],


  // list of files to exclude
  exclude: [

  ],




  // web server port
  port: 9876,


  // cli runner port
  runnerPort: 9100,


  // enable / disable colors in the output (reporters and logs)
  colors: true,


  // level of logging
  // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
  logLevel: config.LOG_INFO,


  // enable / disable watching file and executing tests whenever any file changes
  autoWatch: true,


  // Start these browsers, currently available:
  // - Chrome
  // - ChromeCanary
  // - Firefox
  // - Opera
  // - Safari (only Mac)
  // - PhantomJS
  // - IE (only Windows)
  browsers: ['Chrome'],


  // If browser does not capture in given timeout [ms], kill it
  captureTimeout: 60000,

  browserNoActivityTimeout: 60000,


  // Continuous Integration mode
  // if true, it capture browsers, run tests and exit
  singleRun: false
  });
};
